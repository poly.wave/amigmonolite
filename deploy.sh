#!/usr/bin/env bash

sudo apt update
sudo apt upgrade

sudo apt install docker.io
sudo apt install docker-compose
sudo apt install git
sudo apt install make
sudo apt install nodejs
sudo apt install npm

sudo make build
sudo make start
