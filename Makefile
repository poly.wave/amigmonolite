# Makefile

build:
	sudo docker-compose build
	sudo chmod -R 0777 ./src/storage

start:
	sudo docker-compose up -d

stop:
	sudo docker-compose down -v

watch:
	cd src && npm run watch

dev:
	cd src && npm run dev

prod:
	cd src && npm run production

logs:
	sudo docker-compose logs -f

console:
	sudo docker exec -it application /bin/sh

list:
	sudo docker ps

restart:
	make stop
	make start
	make logs

rebuilt:
	make stop
	make build
	make start
