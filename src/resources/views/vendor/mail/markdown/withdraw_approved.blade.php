@component('mail::message')
{{-- Intro Lines --}}
<div>
   Your Withdrawal request {{$amount}} {{$currency}} to {{$type}} was approved.
</div>
@endcomponent
