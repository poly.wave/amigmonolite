@component('mail::message')
{{-- Intro Lines --}}
<div>
    Your one time code: {{$code}}
</div>
@endcomponent
