@component('mail::message')
{{-- Intro Lines --}}
<div>
    Wellcome to AMIG Forex & Crypto Markets.<br>
    Your login: {{$email}}<br>
    Your password: {{$password}}
</div>
@endcomponent
