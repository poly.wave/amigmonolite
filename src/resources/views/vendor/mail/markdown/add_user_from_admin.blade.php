@component('mail::message')
{{-- Intro Lines --}}
<div>
    Hello {{$name}} <br>
    Wellcome to AMIG Forex & Crypto Markets.<br>
    Its random password resetted by admin. Please change it at first login. If you are not request this urgently inform Support team and change password immediately.<br>
    Your login: {{$email}}<br>
    Your password: {{$password}}
    Please click below to activate your account.<br>
   <a href="{!!$activate!!}" style="display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;
       text-align: center;
       white-space: nowrap;
       vertical-align: middle;
       -ms-touch-action: manipulation;
       touch-action: manipulation;
       cursor: pointer;
       -webkit-user-select: none;
       -moz-user-select: none;
       -ms-user-select: none;
       user-select: none;
       background-image: none;
       border: 1px solid transparent;
       border-radius: 4px;
       background: #3F51B5;
       color: #fff;">Activate account</a>
</div>
@endcomponent
