@component('mail::message')
{{-- Intro Lines --}}
<div>
  Registered new user with this email {{$email}}
</div>
@endcomponent
