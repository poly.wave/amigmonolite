@component('mail::message')
{{-- Intro Lines --}}
<div>
 <p>Deposit from {{$user_name}} by {{$type}} {{$amount}} {{$currency_name}} received</p>
</div>
@endcomponent
