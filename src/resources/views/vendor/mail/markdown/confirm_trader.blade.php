@component('mail::message')
{{-- Intro Lines --}}
<div>
  <p>Hello,</p>
 <p>Thanks for registration at the our MetaTrader 5 Server.</p>
 <p>Please note that you must confirm your registration.</p>
 <p>Use the following link to activate your account:</p>
 <p><a href="{{$text}}">Activate</a></p>
</div>
@endcomponent
