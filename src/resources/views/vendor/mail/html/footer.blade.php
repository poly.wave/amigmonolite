<tr>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width:100%;">
        <tr>
            <td align="center" style="padding-left:9px;padding-right:9px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnFollowContent">
                    <tr>
                        <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" valign="top">
                                        <!--[if mso]>
                                        <table align="center" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                            <tr>
                                                <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                        <tr>
                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                    <tr>

                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                            <a href="https://twitter.com/AmigLlc" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" alt="Twitter" style="display:block;" height="24" width="24" class=""></a>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                            <tr>
                                                <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                        <tr>
                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                    <tr>

                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                            <a href="https://www.facebook.com/amigfx/" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" alt="Facebook" style="display:block;" height="24" width="24" class=""></a>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                            <tr>
                                                <td valign="top" style="padding-right:10px; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                        <tr>
                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                    <tr>

                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                            <a href="https://www.amigfx.com" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-link-48.png" alt="Forex and Crypto Markets" style="display:block;" height="24" width="24" class=""></a>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


                                        <table align="left" border="0" cellpadding="0" cellspacing="0" style="display:inline;">
                                            <tr>
                                                <td valign="top" style="padding-right:0; padding-bottom:9px;" class="mcnFollowContentItemContainer">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem">
                                                        <tr>
                                                            <td align="left" valign="middle" style="padding-top:5px; padding-right:10px; padding-bottom:5px; padding-left:9px;">
                                                                <table align="left" border="0" cellpadding="0" cellspacing="0" width="">
                                                                    <tr>

                                                                        <td align="center" valign="middle" width="24" class="mcnFollowIconContent">
                                                                            <a href="http://instagram.com/amigcapital" target="_blank"><img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-instagram-48.png" alt="Instagram" style="display:block;" height="24" width="24" class=""></a>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->

                                        <!--[if mso]>
                                        </tr>
                                        </table>
                                        <![endif]-->
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</tr>

<tr>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width:100%;">
        <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="min-width: 100%; padding: 10px 18px 25px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EEEEEE;">
                    <tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                    </table>
                <!--
                                <td class="mcnDividerBlockInner" style="padding: 18px;">
                                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
                -->
            </td>
        </tr>

    </table>
</tr>

<tr>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
        <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner" style="padding-top:9px;">
                <!--[if mso]>
                <table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
                    <tr>
                <![endif]-->

                <!--[if mso]>
                <td valign="top" width="600" style="width:100%;">
                <![endif]-->
                <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width:100%; min-width:100%;" width="100%" class="mcnTextContentContainer">
                    <tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:0; padding-right:18px; padding-bottom:9px; padding-left:18px;">

                            For Support<br>
                            <a href="mailto:support@amigfx.com">support@amigfx.com</a><br>
                            <br>
                            <em>Copyright © {{ date('Y') }} Amig Capital LLC, All rights reserved.</em><br>
                            <br>
                            RISK WARNING!<br>
                            All financial products traded on margin carry a high degree of risk to your capital. They may not be suited to all investors and you can lose more than your initial deposit. Please ensure that you fully understand the risks.<br>
                            We do not accept residents of the United States, Canada &amp; Turkey.<br>
                            <br>
                            <br>
                        </td>
                    </tr>
                    </table>
                <!--[if mso]>
                </td>
                <![endif]-->

                <!--[if mso]>
                </tr>
                </table>
                <![endif]-->
            </td>
        </tr>

    </table>
</tr>
