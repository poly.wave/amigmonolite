@extends('layouts.fx')

@section('content')
    <!-- text start -->
    <!--<div class="container text clearfix">
        <div class="contacts pull-left left">
            <h2>Contact Us</h2>
            <p>
                <strong>Phone:</strong> <a href="tel:+1 777 777 77 77">+1 777 777 77 77</a>
            </p>
            <p>
                <strong>Fax:</strong> <a href="tel:+1 777 777 77 77">+1 777 777 77 77</a>
            </p>
            <p>
                <strong>E-mail:</strong> <a href="mailto:support@amigfx.com">support@amigfx.com</a>
            </p>
            <p>
                <strong>Address:</strong> 26 NOVEMBAR BB, ULCINJ, Montenegro
            </p>
        </div>

        <div class="map pull-right right">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d484.2444930189533!2d19.242844765000996!3d42.440485625911705!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5d7394fc13e86668!2sThe+Capital+Plaza!5e0!3m2!1str!2str!4v1500403077313" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen=""></iframe>
        </div>
    </div>-->
    <!-- text finish -->

    <div class="container clearfix" style="padding-top: 20px;">
        <div class="contact-form pull-left left">
            <div class="title">{{ __('contact_us.content.contact') }}

            </div>

            <div class="article">{{ __('contact_us.content.contact1') }}

            </div>

            <form action="/mail.php" method="post">
                <fieldset>
                    <input type="text" placeholder="{{ __('contact_us.content.fname') }}" class="past first" name="first_name" required="">
                    <input type="text" placeholder="{{ __('contact_us.content.lname') }}" class="past last" name="last_name" required="">
                </fieldset>

                <fieldset>
                    <input type="email" placeholder="{{ __('contact_us.content.email') }}" class="past first" name="email" required="">
                    <input type="tel" placeholder="{{ __('contact_us.content.mobile') }}" class="past last" name="phone" required="">
                </fieldset>

                <fieldset class="last">
                    <textarea name="text" placeholder="{{ __('contact_us.content.message') }}" required></textarea>
                </fieldset>

                <div class="captcha_wrapper" style="margin-bottom: 30px; padding-left: 0; padding-right: 0;">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha" data-sitekey="6LejXp8UAAAAAIp8P2UcG6QM52dN2l0wkXHcJbdS"></div>
                </div>

                <button class="btn btn-gold">{{ __('contact_us.content.send') }}</button>
            </form>
        </div>
    </div>
@endsection
