@extends('layouts.fx')

@section('content')
    <!-- text start -->
    <div class="container text clearfix">
        <h2>{{ __('account_types.content.steps') }}</h2>

        <div class="introducion">
            <div class="four clearfix">
                <div class="column">
                    <img src="images/source/RegAccTypeStep1.png">
                    <div class="title"> {{ __('account_types.content.step1') }}</div>
                    <div class="article">{{ __('account_types.content.step1desc') }}</div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep2.png">
                    <div class="title">{{ __('account_types.content.step2') }}</div>
                    <div class="article">{{ __('account_types.content.step2desc') }}</div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep3.png">
                    <div class="title">{{ __('account_types.content.step3') }}</div>
                    <div class="article-Cells-SubTitles">{{ __('account_types.content.step3desc') }}</div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep4.png">
                    <div class="title">{{ __('account_types.content.step4') }}</div>
                    <div class="article">{{ __('account_types.content.step4desc') }}!</div>
                </div>
            </div>

            <a href="/register" class="btn btn-gold">{{ __('account_types.content.register') }}</a>
        </div>

        <div class="divider"></div>

        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">{{ __('account_types.content.minacc') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #616161;">{{ __('account_types.content.stnacc') }}</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">{{ __('account_types.content.mindep') }}</div>
                    <div class="td">100 USD
                    </div>
                    <div class="td">10,000 USD
                    </div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.maxbal') }}</div>
                    <div class="td">None</div>
                    <div class="td">None</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.sprdfrom') }}</div>
                    <div class="td">1.5 pips
                    </div>
                    <div class="td">1.5 pips
                    </div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.minlot') }}</div>
                    <div class="td">0.01</div>
                    <div class="td">0.01</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.leverage') }}</div>
                    <div class="td">1:100, 1:200</div>
                    <div class="td">1:100, 1:200</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.stnlot') }}</div>
                    <div class="td">100,000</div>
                    <div class="td">100,000</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.acctype') }}</div>
                    <div class="td">{{ __('account_types.content.minacctype') }}</div>
                    <div class="td">{{ __('account_types.content.stnacctype') }}</div>
                </div>
            </div>



            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: green;">{{ __('account_types.content.vipacc') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #ebcf6d;">{{ __('account_types.content.proacc') }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.mindep') }}</div>
                    <div class="td">$50,000
                        USD
                    </div>
                    <div class="td">$50,000
                        USD
                    </div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.maxbal') }}</div>
                    <div class="td">None</div>
                    <div class="td">None</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.sprdfrom') }}</div>
                    <div class="td">0.7
                        pips
                    </div>
                    <div class="td">0.7
                        pips
                    </div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.minlot') }}</div>
                    <div class="td">0.01</div>
                    <div class="td">0.01</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.leverage') }}</div>
                    <div class="td">1:100, 1:200</div>
                    <div class="td">1:100</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.stnlot') }}</div>
                    <div class="td">100,000</div>
                    <div class="td">100,000</div>
                </div>
                <div class="tr">
                    <div class="td">{{ __('account_types.content.acctype') }}</div>
                    <div class="td">{{ __('account_types.content.vipacctype') }}</div>
                    <div class="td">{{ __('account_types.content.proacctype') }}</div>
                </div>
            </div>
        </div>


        <div class="divider"></div>

        <div class="form-group">
            <h2>{{ __('account_types.content.requiredoc') }} </h2>

            <div class="col-xs-12">
                <div class="title">{{ __('account_types.content.indvacc') }}</div>
                <ol class="list">
                    <li>{{ __('account_types.content.indvacc1') }}</li>
                    <li>{{ __('account_types.content.indvacc2') }}</li>
                </ol>
            </div>
            <div class="col-xs-12">
                <div class="title">{{ __('account_types.content.jointacc') }}</div>
                <ol class="list">
                    <li>{{ __('account_types.content.jointacc1') }}</li>
                    <li>{{ __('account_types.content.jointacc2') }}</li>
                    <li>{{ __('account_types.content.jointacc3') }}</li>
                    <li>{{ __('account_types.content.jointacc4') }}</li>
                </ol>
            </div>
            <div class="col-xs-12">
                <div class="title">{{ __('account_types.content.coacc') }}</div>
                <ol class="list">
                    <li>{{ __('account_types.content.coacc1') }}</li>
                    <li>{{ __('account_types.content.coacc2') }}</li>
                    <li>{{ __('account_types.content.coacc3') }}</li>
                    <li>{{ __('account_types.content.coacc4') }}</li>
                    <li>{{ __('account_types.content.coacc5') }}</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- text finish -->
@endsection
