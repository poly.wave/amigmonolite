@extends('layouts.fx')

@section('content')
    <div>
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div id="tradingview_572c8"></div>
            <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL chart</span></a> by TradingView</div>
            <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
            <script type="text/javascript">
                new TradingView.widget(
                    {
                        "width": '100%',
                        "height": 800,
                        "symbol": "NASDAQ:AAPL",
                        "interval": "D",
                        "timezone": "Etc/UTC",
                        "theme": "Dark",
                        "style": "1",
                        "locale": "en",
                        "toolbar_bg": "rgba(0, 0, 0, 1)",
                        "enable_publishing": false,
                        "allow_symbol_change": true,
                        "container_id": "tradingview_572c8"
                    }
                );
            </script>
        </div>
        <!-- TradingView Widget END -->
    </div>
@endsection
