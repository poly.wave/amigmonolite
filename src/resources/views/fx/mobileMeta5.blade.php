@extends('layouts.fx')

@section('content')
    <!-- text start -->
    <div class="container text clearfix">
        <h2>{{ __('mobile_meta_5.content.mmt') }}</h2>

        <p class="SectionContent">{{ __('mobile_meta_5.content.mmtintro') }}</p>
        <p class="SectionContent">{{ __('mobile_meta_5.content.mmtintro1') }}</p>
        <p class="SectionContent">{{ __('mobile_meta_5.content.mmtintro2') }}</p>

        <img class="img-responsive" src="images/source/MetaTrader5.PNG" style="margin-bottom: 20px;">

        <p class="SectionContent">{{ __('mobile_meta_5.content.mmtintro3') }}</p>

        <div class="divider"></div>

        <div class="two clearfix">
            <div class="column">
                <img class="img-responsive" src="images/source/MT5_logo_1.PNG">
                <p class="title">iOS</p>
                <ol class="list" type="a">
                    <li>{{ __('mobile_meta_5.content.ios') }}</li>
                    <li>{{ __('mobile_meta_5.content.ios1') }}</li>
                </ol>

                <p class="SectionContent"><a class="DownloadBtn hvr-float" href="https://itunes.apple.com/us/app/metatrader-5-forex-stocks/id413251709" target="_blank"><img src="images/source/AppleStore.png"></a></p>
            </div>

            <div class="column">

                <img class="img-responsive" src="images/source/MT5_logo_1.PNG">
                <p class="title">Android</p>
                <ol class="list" type="a">
                    <li>{{ __('mobile_meta_5.content.android') }}</li>
                    <li>{{ __('mobile_meta_5.content.android1') }}</li>
                </ol>
                <p class="SectionSpace"></p>
                <p class="SectionContent"><a class="DownloadBtn hvr-float" href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader5&amp;hl=en-us" target="_blank"><img src="images/source/GooglePlay.png"></a></p>
            </div>
        </div>
    </div>
    <!-- text finish -->
@endsection
