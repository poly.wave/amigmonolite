@extends('layouts.fx')

@section('content')
    <div class="start-trading">
        <div class="container clearfix">
            <div class="pull-left">
                <span class="title">{{ __('index.content.title') }}</span>
            </div>

            <div class="pull-right">
                <a href="/register?demo" class="btn open-demo-account">{{ __('index.content.opendemo') }}</a>
                <a href="/register?live" class="btn btn-gold open-live-account">{{ __('index.content.openlive') }}</a>
            </div>
        </div>
    </div>

    <div class="stats-trading">
        <div class="container">
            <ul class="list">
                <li>
                    <span class="big">{{ __('index.content.1200') }}</span>
                    <span class="normal">{{ __('index.content.leverage') }}</span>
                </li>

                <li>
                    <span class="big">
                        <i class="icon icon-black icon-arrow-down"></i>
                    </span>
                    <span class="normal">{{ __('index.content.spreads') }} <br>& {{ __('index.content.slippage') }}</span>
                </li>

                <li>
                    <span class="big">100</span>
                    <span class="normal">{{ __('index.content.currencies') }}<br>{{ __('index.content.indices') }} &<br>{{ __('index.content.commodities') }}</span>
                </li>

                <li>
                    <span class="big">$100</span>
                    <span class="normal">{{ __('index.content.minimum') }}<br>{{ __('index.content.depositl') }}</span>
                </li>

                <li>
                    <span class="big">$0</span>
                    <span class="normal">{{ __('index.content.fee') }}</span>
                </li>

                <li>
                    <span class="big">24</span>
                    <span class="normal">{{ __('index.content.support') }}</span>
                </li>
            </ul>
        </div>
    </div>

    <div class="course-trading">
        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>

            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
                {
                    "symbols": [
                    {
                        "proName": "FOREXCOM:SPXUSD",
                        "title": "S&P 500"
                    },
                    {
                        "proName": "FX_IDC:EURUSD",
                        "title": "EUR/USD"
                    },
                    {
                        "proName": "BITSTAMP:BTCUSD",
                        "title": "BTC/USD"
                    },
                    {
                        "proName": "BITSTAMP:ETHUSD",
                        "title": "ETH/USD"
                    },
                    {
                        "description": "Dow 30",
                        "proName": "CURRENCYCOM:US30"
                    },
                    {
                        "description": "Brent OIL",
                        "proName": "TVC:UKOIL"
                    },
                    {
                        "description": "Silver",
                        "proName": "OANDA:XAGUSD"
                    },
                    {
                        "description": "Gold",
                        "proName": "OANDA:XAUUSD"
                    },
                    {
                        "description": "GBPUSD",
                        "proName": "OANDA:GBPUSD"
                    }
                ],
                    "colorTheme": "dark",
                    "isTransparent": false,
                    "locale": "en"
                }
            </script>
        </div>
        <!-- TradingView Widget END -->
    </div>

    <div class="choose">
        <div class="container">
            <h2 class="title">{{ __('index.content.whyamig') }}?</h2>
            <span class="article">{{ __('index.content.asfx') }}</span>

            <ul class="three-columns clearfix">
                <li data-id="1">
                    <i class="icon icon-large icon-security"></i>

                    <span class="name">{{ __('index.content.trusted') }}</span>
                    <span class="description">{{ __('index.content.trusted1') }}</span>
                </li>

                <li data-id="2">
                    <i class="icon icon-large icon-study"></i>

                    <span class="name">{{ __('index.content.segregated') }}</span>
                    <span class="description">{{ __('index.content.segregated1') }}</span>
                </li>

                <li data-id="3">
                    <i class="icon icon-large icon-clock"></i>

                    <span class="name">{{ __('index.content.quick') }}</span>
                    <span class="description">{{ __('index.content.quick1') }}</span>
                </li>

                <li data-id="4">
                    <i class="icon icon-large icon-level"></i>

                    <span class="name">{{ __('index.content.ultra') }}</span>
                    <span class="description">{{ __('index.content.ultra1') }}</span>
                </li>

                <li data-id="5">
                    <i class="icon icon-large icon-money"></i>

                    <span class="name">{{ __('index.content.negative') }}</span>
                    <span class="description">{{ __('index.content.negative1') }}</span>
                </li>

                <li data-id="6">
                    <i class="icon icon-large icon-customer"></i>

                    <span class="name">{{ __('index.content.dedicated') }}</span>
                    <span class="description">{{ __('index.content.dedicated1') }}</span>
                </li>
            </ul>
        </div>
    </div>



@endsection
