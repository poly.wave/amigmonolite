@extends('layouts.fx')

@section('content')
    <!-- text start -->
    <div class="container text">
        <p>
            {!! __('aboutus.content.1') !!}
        </p>
        <div class="divider divider-1"></div>
        <p>{!! __('aboutus.content.2') !!}</p>
        <p>{!! __('aboutus.content.3') !!} </p>
        <p>
            {!! __('aboutus.content.4') !!} </p>
        <div class="divider divider-3"></div>

        <h2>{!! __('aboutus.content.5') !!}</h2>
        <p>
            {!! __('aboutus.content.6')!!}</p>
        <p>
            {!! __('aboutus.content.7') !!}
        </p>
        <p>{!! __('aboutus.content.8') !!}</p>
        <p>
            {!! __('aboutus.content.9') !!}
        </p>
        <p>{!! __('aboutus.content.10') !!}</p>

        <div class="divider divider-2"></div>
        <h2>{!! __('aboutus.content.11') !!}</h2>
        <p>
            {!! __('aboutus.content.12') !!}
        </p>
    </div>
    <!-- text finish -->
@endsection
