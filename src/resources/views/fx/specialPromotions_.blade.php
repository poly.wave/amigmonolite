@extends('layouts.fx')

@section('content')
    <div class="parallax-container" data-image-src="images/promotions/iphonex.jpg">
        <div class="container">
            <h4>WIN IPHONE X!</h4>

            <p>
                Win iPhoneX prize!
            </p>

            <p>
                Register to the promotion through online online form. Trade enough lots within 30 days from the day of registration. This campaign is only for new clients.
            </p>

            <p>
                Fill the form and get in touch with your customer representative, now!
            </p>
        </div>
    </div>

    <div class="parallax-container" data-image-src="images/promotions/bonus.jpg">
        <div class="container">
            <h4>GET 20% BONUS UP TO 5000 USD</h4>

            <p>
                If you are new in the market or you decided to move your existing trading account to our unique trading environment and you have some floating positions with you;
            </p>

            <p>
                Don’t worry!
            </p>

            <p>
                We are here to provide you the best service!
            </p>
        </div>
    </div>

@endsection
