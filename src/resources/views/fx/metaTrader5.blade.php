@extends('layouts.fx')

@section('content')
    <!-- text start -->
    <div class="container text clearfix">



        <h2>{{ __('meta_trader_5.content.mt') }}</h2>

        <img class="img-responsive" src="images/source/MT5_logo_1.PNG">

        <p><b>{{ __('meta_trader_5.content.mtintro') }}</b></p>
        <p><b></b>{{ __('meta_trader_5.content.mtintro1') }}</p>
        <p><b>{{ __('meta_trader_5.content.mtintro2') }}!</b></p>

        <p class="SectionContent">{{ __('meta_trader_5.content.mtintro3') }} </p>
        <p class="SectionContent">{{ __('meta_trader_5.content.mtintro4') }}</p>

        <p class="buttons">
            <a href="/register" class="btn btn-gold">{{ __('meta_trader_5.content.register') }}</a>
            <a href="https://download.mql5.com/cdn/mobile/mt5/ios?server=AMIGCapital-MT5Demo,AMIGCapital-MT5Live" class="btn btn-gold">{{ __('meta_trader_5.content.opendemo') }} </a>
            <a href="https://download.mql5.com/cdn/web/15233/mt5/amigcapital5setup.exe" class="btn btn-gold" target="_blank">{{ __('meta_trader_5.content.downmtwin') }}</a>
            <a href="https://download.mql5.com/cdn/web/15233/mt5/amigcapital5setup.exe" class="btn btn-gold" target="_blank">{{ __('meta_trader_5.content.downmtmac') }}</a>
        </p>

        <div class="divider"></div>

        <h2>{{ __('meta_trader_5.content.why') }}</h2>

        <ul class="list">
            <li><img src="images/source/tick.png"><span class="AdvItems">{{ __('meta_trader_5.content.why1') }}</span></li>
            <li><img src="images/source/tick.png"><span class="AdvItems">{{ __('meta_trader_5.content.why2') }}</span></li>
            <li><img src="images/source/tick.png"><span class="AdvItems">{{ __('meta_trader_5.content.why3') }}</span></li>
            <li><img src="images/source/tick.png"><span class="AdvItems">{{ __('meta_trader_5.content.why4') }}</span></li>
            <li><img src="images/source/tick.png"><span class="AdvItems">{{ __('meta_trader_5.content.why5') }}</span></li>
        </ul>

        <div class="divider"></div>

        <h2>{{ __('meta_trader_5.content.features') }}</h2>

        <img src="images/source/screen_us.jpg" class="fill-img">

        <div class="divider"></div>

        <h2>{{ __('meta_trader_5.content.benefits') }}</h2>

        <div class="three clearfix">
            <div class="column">
                <img src="images/source/MT5_06.png">
                <p><b>{{ __('meta_trader_5.content.benefits1') }}<br></b></p>
            </div>
            <div class="column">
                <img src="images/source/MT5_08.png">
                <p><b>{{ __('meta_trader_5.content.benefits2') }}<br></b></p>
            </div>
            <div class="column">
                <img src="images/source/MT5_03.png">
                <p><b>{{ __('meta_trader_5.content.benefits3') }}<br></b></p>
            </div>
        </div>
    </div>
    <!-- text finish -->
@endsection
