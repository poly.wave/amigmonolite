@extends('layouts.fx')

@section('content')
    <div class="container">
        <p class="SectionSubTitle">Prepaid Card Introduction</p>
        <ul class="SectionContent">
            <li>AMIG Fx provides clients and partners with safe, convenient, global deposit/withdrawal methods.</li>
            <li>The AMIG Fx Prepaid Card is an account aggregation service, allowing you to choose how to manage
                your funds as efficiently as possible.</li>
            <li>AMIG Fx works with a trusted global payment provider. AMIG Fx provides you with an aggregate
                solution - use just one account to manage your AMIGFXPrepaid Card.</li>

        </ul>

        <p class="SectionSubTitle">Prepaid Cards</p>
        <ol class="SectionContent" type="1">
            <li>You get the lowest transaction fees when depositing or withdrawing funds from your AMIG Fx trading accounts.</li>
            <li>Your funds are held confidentially.</li>
            <li>Rebate bonuses will be paid directly into your Prepaid Card.</li>
            <li>Online Enquiries: you can check your account and trade online 24/7.</li>
            <li>You can withdraw funds from multiple accounts registered in your name.</li>
            <li>You can choose both to deposit and withdraw funds via AMIGFXPrepaid Card</li>
            <li>There is no need for a bank account and you won't have to undergo a credit review.</li>
        </ol>

        <p class="SectionSubTitle">Why Apply for a Prepaid Card?</p>
        <ol class="SectionContent" type="1">
            <li>You can use the AMIG Fx Prepaid Card in online stores and shops worldwide.</li>
            <li>You can use the AMIG Fx Prepaid Card to withdraw local currency from ATMs around the world.
                It only takes 2 hours to gain access to your funds when you load up your AMIG Fx Prepaid Card
                from your AMIG Fx account.</li>
            <li>You can check your balance and load funds to your account directly through the AMIG Fx Prepaid Card.</li>
            <li>If you lose your card, AMIG Fx will provide a replacement card and transfer the balance from your
                original card to the new one. (Please report a lost card immediately.)</li>
            <li>The AMIG Fx Prepaid Card is available in USD.</li>
        </ol>
    </div>
@endsection
