<div class="sidebar">
    <div class="sidebar-custom ">
        <a href="/user/settings" style="text-decoration: none;">
            <img class="img-avatar" src="@if(Auth::user()->avatar){{ Auth::user()->avatar }} @else /images/icons/icon-customer.png @endif" alt="{{ Auth::user()->email }}" alt="Avatar">

            <div><text style="margin-top: 10px; font-size: 20px; color: #c1b1a0;">
                {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</text>
            </div>
        </a>

        <div class="chart-header-text">
            {{ $userGroup->name }}
        </div>

        <div>
            {{ __('member.sidebar.balance') }} <text style="color: #bc8a54;">{{ $userCurrency->symbol . Auth::user()->balance }}</text>
        </div>


    </div>

    <nav class="sidebar-nav sidebar-nav-custom">
        <ul class="nav">
            @foreach($menu as $key => $nav)
                <li class="nav-item @if(isset($nav->dropdown)) nav-dropdown @endif">
                    <a class="nav-link @if(isset($nav->dropdown)) nav-dropdown-toggle @endif" href="{{ url(implode('/', ['member', $key])) }}">
                        <i class="nav-icon m fa fa-{{ $nav->icon }} "></i> {{ $nav->name }}
                    </a>

                    @if(isset($nav->dropdown))
                        <ul class="nav-dropdown-items">
                            @foreach($nav->dropdown as $_key => $item)
                                <li class="nav-item @if(isset($item->dropdown)) nav-dropdown @endif">
                                    <a class="nav-link @if(isset($item->dropdown)) nav-dropdown-toggle @endif" href="{{ url(implode('/', ['member', $key, $_key])) }}">
                                        <i class="nav-icon fa fa-{{ $item->icon }}"></i> {{ $item->name }}
                                    </a>

                                    @if(isset($item->dropdown))
                                        <ul class="nav-dropdown-items">
                                            @foreach($item->dropdown as $__key => $_item)
                                                <li class="nav-item @if(isset($_item->dropdown)) nav-dropdown @endif">
                                                    <a class="nav-link @if(isset($_item->dropdown)) nav-dropdown-toggle @endif" href="{{ url(implode('/', ['member', $key, $_key, $__key])) }}">
                                                        <i class="nav-icon fa fa-{{ $_item->icon }}"></i> {{ $_item->name }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
        </ul>
    </nav>
</div>
