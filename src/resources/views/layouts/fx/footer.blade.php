<!-- Footer -->
<footer>
    <section class="bottom-menu">
        <div class="container">
            <ul class="list">
                <li>
                    <a href="/about-us" class="item">{{ __('fx.layouts.footer.menu.about') }}</a>
                </li>

                <li>
                    <a class="item">{{ __('fx.layouts.footer.menu.account') }}</a>

                    <ul class="sublist">
                        <li><a href="/account-types">{{ __('fx.layouts.footer.menu.account') }}</a></li>
                        <li><a href="/register">{{ __('fx.layouts.footer.menu.register') }}</a></li>
                        <li><a href="/special-promotions">{{ __('fx.layouts.footer.menu.special') }}</a></li>
                        <li><a href="/deposit-option">{{ __('fx.layouts.footer.menu.deposit') }}</a></li>
                        <li><a href="/withdraw-option">{{ __('fx.layouts.footer.menu.withdraw') }}</a></li>
                    </ul>
                </li>
                <li>
                    <a class="item">{{ __('fx.layouts.footer.menu.market') }}</a>

                    <ul class="sublist">
                        <li><a href="/economic-calendar">{{ __('fx.layouts.footer.menu.economic') }}</a></li>
                        <li><a href="/daily-fx-bulletin">{{ __('fx.layouts.footer.menu.daily') }}</a></li>
                    </ul>
                </li>
                <li>
                    <a class="item">{{ __('fx.layouts.footer.menu.education') }}</a>

                    <ul class="sublist">
                        <li><a href="/whats-forex">{{ __('fx.layouts.footer.menu.whatis') }}</a></li>
                        <li><a href="/fx-trading">{{ __('fx.layouts.footer.menu.fxtrading') }}</a></li>
                        <li><a href="/fundamental-analysis">{{ __('fx.layouts.footer.menu.fundamental') }}</a></li>
                        <li><a href="/technical-analysis">{{ __('fx.layouts.footer.menu.technical') }}</a></li>
                        <li><a href="/major-players">{{ __('fx.layouts.footer.menu.major') }}</a></li>
                    </ul>
                </li>
                <li>
                    <a class="item">{{ __('fx.layouts.footer.menu.partnership') }}</a>

                    <ul class="sublist">
                        <li><a href="/advantages">{{ __('fx.layouts.footer.menu.partnering') }}</a></li>
                        <li><a href="/introducing-brokers">{{ __('fx.layouts.footer.menu.ib') }}</a></li>
                        <li><a href="/white-label">{{ __('fx.layouts.footer.menu.white') }}</a></li>
                    </ul>
                </li>
                <li>
                    <a class="item">{{ __('fx.layouts.footer.menu.platforms') }}</a>

                    <ul class="sublist">
                        <li><a href="/meta-trader-5">{{ __('fx.layouts.footer.menu.mt5') }}</a></li>
                        <li><a href="/mobile-meta-5">{{ __('fx.layouts.footer.menu.mmt5') }}</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/contact-us" class="item">{{ __('fx.layouts.footer.menu.contactus') }}</a>
                </li>
            </ul>
        </div>
    </section>

    <section class="navigation">
        <div class="container">
            <div class="desc">
                {{ __('fx.layouts.footer.riskwarning') }}
            </div>
        </div>
    </section>

    <section class="copyright">
        <div class="container">
            <span><?= date('Y') ?> {{ __('fx.layouts.footer.co') }}</span> <a href="/terms-of-use">Terms of use</a>
            <span class="socials">
                <a href="https://www.instagram.com/amigcapital/">
                  <img src="{{asset('/images/icons/insta.png')}}" alt="intsagram">
                </a>
                <a href="https://twitter.com/AmigLlc">
                  <img src="{{asset('/images/icons/twitter.png')}}" alt="twitter">
                </a>
                <a href="https://www.facebook.com/amigfx/">
                  <img src="{{asset('/images/icons/fb.png')}}" alt="facebook">
                </a>
                <a href="https://api.whatsapp.com/send?phone=+905517009108">
                  <img src="{{asset('/images/icons/watsapp.png')}}" alt="watsapp">
                </a>
            </span>
        </div>
    </section>
</footer>
