<!-- Header -->
<header>
    <div class="open-menu">
        <div id="menu-icon-wrapper" class="menu-icon-wrapper" style="visibility: hidden;">
            <svg width="1000px" height="1000px">
                <path id="pathA" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
                <path id="pathB" d="M 300 500 L 700 500"></path>
                <path id="pathC" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
            </svg>
            <button id="menu-icon-trigger" class="menu-icon-trigger"></button>
        </div><!-- menu-icon-wrapper -->
    </div>
    @if (\Session::has('success'))
        <div class="row">
          <div class="alert alert-success alert-block" style="background: #4caf50;padding: 20px;text-align: center;">
            <strong>{{\Session::get('success')}}</strong>
          </div>
        </div>
    @endif
    @if (\Session::has('danger'))
        <div class="row">
          <div class="alert alert-danger alert-block" style="background: #f44336;padding: 20px;text-align: center;">
            <strong>{{\Session::get('danger')}}</strong>
          </div>
        </div>
    @endif
    <section class="top-bar">
        <div class="container clearfix">
            <div class="logo pull-left">
                <a href="/">{{ __('fx.layouts.header.logotext') }}</a>
            </div>

            <div class="socials pull-left">
                <a href="https://www.instagram.com/amigcapital/">
                  <img src="{{asset('/images/icons/insta.png')}}" alt="intsagram">
                </a>
                <a href="https://twitter.com/AmigLlc">
                  <img src="{{asset('/images/icons/twitter.png')}}" alt="twitter">
                </a>
                <a href="https://www.facebook.com/amigfx/">
                  <img src="{{asset('/images/icons/fb.png')}}" alt="facebook">
                </a>
                <a href="https://api.whatsapp.com/send?phone=+905517009108">
                  <img src="{{asset('/images/icons/watsapp.png')}}" alt="watsapp">
                </a>
            </div>

            <div class="auth pull-right">
                @if (Auth::user())
                  <a href="/member/main" class="btn btn-gold open-account">{{ __('member.header.account') }}</a>
                  <a href="/logout" class="btn client-login">{{ __('member.header.logout') }}</a>
                @else
                  <a href="/register" class="btn btn-gold open-account">{{ __('fx.layouts.header.open') }}</a>
                  <a href="/login" class="btn client-login">{{ __('fx.layouts.header.login') }}</a>
                @endif
                @include('layouts.fx.changeLanguage')
            </div>


        </div>
    </section>

    @include('layouts.fx.menu')

    @if(isset($isHome))
        @include('layouts.fx.slider')
    @else
        <section class="sub-title">
            <div class="container">
                <h1>{{ $title }}</h1>
            </div>
        </section>
    @endif
</header>
