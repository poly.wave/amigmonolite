<section class="dummy" id="dummy">
    <div class="container clearfix">
        <ul class="list pull-right">
            <li>
                <a href="/about-us" class="item">{{ __('fx.layouts.menu_main.about') }}</a>
            </li>

            <li>
                <a class="item">{{ __('fx.layouts.menu_main.account') }}</a>

                <ul class="sublist">
                    <li><a href="/account-types">{{ __('fx.layouts.menu_main.account') }}</a></li>
                    <li><a href="/register">{{ __('fx.layouts.menu_main.register') }}</a></li>
                    <li><a href="/special-promotions">{{ __('fx.layouts.menu_main.special') }}</a></li>
                    <li><a href="/deposit-option">{{ __('fx.layouts.menu_main.deposit') }}</a></li>
                    <li><a href="/withdraw-option">{{ __('fx.layouts.menu_main.withdraw') }}</a></li>
                </ul>
            </li>
            <li>
                <a class="item">{{ __('fx.layouts.menu_main.market') }}</a>

                <ul class="sublist">
                    <li><a href="/economic-calendar">{{ __('fx.layouts.menu_main.economic') }}</a></li>
                    <li><a href="/daily-fx-bulletin">{{ __('fx.layouts.menu_main.daily') }}</a></li>
                </ul>
            </li>
            <li>
                <a class="item">{{ __('fx.layouts.menu_main.education') }}</a>

                <ul class="sublist">
                    <li><a href="/whats-forex">{{ __('fx.layouts.menu_main.whatis') }}</a></li>
                    <li><a href="/fx-trading">{{ __('fx.layouts.menu_main.fxtrading') }}</a></li>
                    <li><a href="/fundamental-analysis">{{ __('fx.layouts.menu_main.fundamental') }}</a></li>
                    <li><a href="/technical-analysis">{{ __('fx.layouts.menu_main.technical') }}</a></li>
                    <li><a href="/major-players">{{ __('fx.layouts.menu_main.major') }}</a></li>
                </ul>
            </li>
            <li>
                <a class="item">{{ __('fx.layouts.menu_main.partnership') }}</a>

                <ul class="sublist">
                    <li><a href="/advantages">{{ __('fx.layouts.menu_main.partnering') }}</a></li>
                    <li><a href="/introducing-brokers">{{ __('fx.layouts.menu_main.ib') }}</a></li>
                    <li><a href="/white-label">{{ __('fx.layouts.menu_main.white') }}</a></li>
                </ul>
            </li>
            <li>
                <a class="item">{{ __('fx.layouts.menu_main.platforms') }}</a>

                <ul class="sublist">
                    <li><a href="/meta-trader-5">{{ __('fx.layouts.menu_main.mt5') }}</a></li>
                    <li><a href="/mobile-meta-5">{{ __('fx.layouts.menu_main.mmt5') }}</a></li>
                </ul>
            </li>
            <li>
                <a href="/contact-us" class="item">{{ __('fx.layouts.menu_main.contactus') }}</a>
            </li>


        </ul>
    </div>
</section>
