<!-- Header -->
<header>
    <section class="top-bar">
        <div class="container clearfix">
            <div class="logo1 pull-left">
                <a href="/">AMIG Capital LLC</a>
            </div>

            <div class="links pull-right">
                <a href="/">{{ __('capital.main') }}</a>
                <a href="/about-us">{{ __('capital.about') }}</a>
                <a href="/">{{ __('capital.projects') }}</a>
                <a href="/contact-us">{{ __('capital.contacts') }}</a>
                @include('layouts.fx.changeLanguage')

            </div>
        </div>
    </section>

    @include('layouts.capital.menu')

    @if(isset($isHome))
        @include('layouts.capital.slider')
    @else
        <section class="sub-title">
            <div class="container">
                <h1>{{ $title }}</h1>
            </div>
        </section>
    @endif
</header>
