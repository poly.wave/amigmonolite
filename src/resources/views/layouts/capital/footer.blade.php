<!-- Footer -->
<footer>
    <section class="copyright">
        <div class="container">
            <span> <a href="/privacy">2017 - {{ date('Y') }} {{ __('capital.co') }} Privacy Policy</a></span>
        </div>
    </section>
</footer>

<!-- CONTENTS
<div style="display: none;" id="finance">
    <h2>Finance</h2>

    <p>You are awesome.</p>
</div>

<div style="display: none;" id="investment">
    <h2>Investment</h2>

    <p>You are awesome.</p>
</div>

<div style="display: none;" id="innovation_and_technology">
    <h2>Innovation & Technology</h2>

    <p>You are awesome.</p>
</div>

<div style="display: none;" id="forex_and_crypto_markets">
    <h2>Forex & Crypto Markets</h2>

    <p>You are awesome.</p>
</div>
-->
