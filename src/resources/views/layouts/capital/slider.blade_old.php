<section class="parallax">
    <div class="parallax-window">
        <div class="slides">
            <div class="slide slide--current" data-id="0">
                <div class="slide__img simple" style="background-image: url(images/slider/s-logo.jpg);"></div>
            </div>

            <div class="slide" data-id="1">
                <div class="container">
                    <h4 class="black">{{ __('capital.finance') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/finance.jpg);"></div>
            </div>

            <div class="slide" data-id="2">
                <div class="container">
                    <h4>{{ __('capital.fxcrypto') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/forex-&-crypto-markets.jpg);"></div>
            </div>

            <div class="slide" data-id="3">
                <div class="container">
                    <h4>{{ __('capital.innovation') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/innovation-&-technology--.jpg);"></div>
            </div>

            <div class="slide" data-id="4">
                <div class="container">
                    <h4>{{ __('capital.invest') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/investment.jpg);"></div>
            </div>

            <div class="slide interval" data-id="5">
                <div class="slide__img simple" style="background-image: url(images/slider/new/1.jpg);"></div>
            </div>

            <div class="slide interval" data-id="6">
                <div class="slide__img simple" style="background-image: url(images/slider/new/2.jpg);"></div>
            </div>

            <div class="slide interval" data-id="7">
                <div class="slide__img simple" style="background-image: url(images/slider/new/3.png);"></div>
            </div>
        </div>

        <div class="container">
            <div class="buttons">
                <a href="#" class="btn btn-gold btn-large" data-id="1" ><span>{{ __('capital.finance') }}</span></a>
                <a href="#" class="btn btn-gold btn-large" data-id="4" ><span>{{ __('capital.invest') }}</span></a>
                <a href="#" class="btn btn-gold btn-large" data-id="3"><span>{{ __('capital.innovation') }}</span></a>
                <?php if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip= $_SERVER['HTTP_CLIENT_IP'];
                } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip= $_SERVER['REMOTE_ADDR'];
                }
                $location="";
                if (@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip)) {
                    $dataArray = json_decode(@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
                    $location = $dataArray->geoplugin_countryCode;
                  } ?>
                  <?php if ($location=="TR"): ?>
                          <a href="http://amigfx1.com" class="btn btn-gold btn-large" data-id="2" ><span>{{ __('capital.fxcrypto') }}</span></a>
                      <?php else: ?>
                          <a href="http://amigfx.com" class="btn btn-gold btn-large" data-id="2" ><span>{{ __('capital.fxcrypto') }}</span></a>
                  <?php endif; ?>
                <!-- CONTENTS
                 data-fancybox data-src="#finance"
                 data-fancybox data-src="#investment"
                 data-fancybox data-src="#innovation_and_technology"
                 data-fancybox data-src="#forex_and_crypto_markets"
                 -->
                </div>

            </div>
        </div>
    </section>
