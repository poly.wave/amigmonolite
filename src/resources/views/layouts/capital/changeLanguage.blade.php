<div class="change-language">
    <a href="/" class="btn-arrow select-language">
        @switch(App::getLocale())
            @case('en')
            <span>English</span>
            @break

            @case('mne')
            <span>Crnogorski</span>
            @break

            @case('ru')
            <span>Русский</span>
            @break

            @case('tr')
            <span>Türkçe</span>
            @break
        @endswitch
    </a>

    <ul class="dropdown">
        <li>
            <a href="?lang=en" class="btn-arrow choose-language">
                <span>English</span>
            </a>
        </li>
        <li>
            <a href="?lang=mne" class="btn-arrow choose-language">
                <span>Crnogorski</span>
            </a>
        </li>

        <li>
            <a href="?lang=ru" class="btn-arrow choose-language">
                <span>Русский</span>
            </a>
        </li>
        <li>
            <a href="?lang=tr" class="btn-arrow choose-language">
                <span>Türkçe</span>
            </a>
        </li>
    </ul>
</div>
