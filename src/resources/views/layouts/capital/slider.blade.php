<section class="parallax">
    <div class="parallax-window">
        <div class="slides">
            <div class="slide slide--current" data-id="0">
                <div class="slide__img simple" style="background-image: url(images/slider/s-logo.jpg);"></div>
            </div>

            <div class="slide" data-id="1">
                <div class="container">
                    <h4 class="black">{{ __('capital.finance') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/finance.jpg);"></div>
            </div>

            <div class="slide" data-id="2">
                <div class="container">
                    <h4>{{ __('capital.fxcrypto') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/forex-&-crypto-markets.jpg);"></div>
            </div>

            <div class="slide" data-id="3">
                <div class="container">
                    <h4>{{ __('capital.innovation') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/innovation-&-technology--.jpg);"></div>
            </div>

            <div class="slide" data-id="4">
                <div class="container">
                    <h4>{{ __('capital.invest') }}</h4>
                </div>
                <div class="slide__img simple" style="background-image: url(images/slider/investment.jpg);"></div>
            </div>

            <div class="slide interval" data-id="5">
                <div class="slide__img simple" style="background-image: url(images/slider/new/1.jpg);"></div>
            </div>

            <div class="slide interval" data-id="6">
                <div class="slide__img simple" style="background-image: url(images/slider/new/2.jpg);"></div>
            </div>

            <div class="slide interval" data-id="7">
                <div class="slide__img simple" style="background-image: url(images/slider/new/3.png);"></div>
            </div>
        </div>

        <div class="container">
            <div class="buttons">
                <a href="/finance" class="btn btn-gold btn-large" data-id="1" ><span>{{ __('capital.finance') }}</span></a>
                <a href="/invest" class="btn btn-gold btn-large" data-id="4" ><span>{{ __('capital.invest') }}</span></a>
                <a href="/innovation" class="btn btn-gold btn-large" data-id="3"><span>{{ __('capital.innovation') }}</span></a>
                <a href="/etrade" class="btn btn-gold btn-large" data-id="2" ><span>{{ __('capital.fxcrypto') }}</span></a>

            </div>

        </div>
    </div>
</section>
