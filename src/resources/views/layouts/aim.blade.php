<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="#" type="image/x-icon">

    <title>AiCan {{ $title }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/ssite.css') }}" rel="stylesheet">
    <meta name="google-site-verification" content="wXc_r9WzaxcIOv0z6LrQhk2lDRIZ6ebngoBnm__E-Yw" />
</head>
<body>
<!-- Prolouder
<div id="preloader">
    <object>
        <embed src="images/dclogo.svg">
    </object>
</div> -->

<div id="main">
@include('layouts.aim.header')
<!--Content -->
    <section class="content
            @if(empty($isHome))
    @if(isset($contentClass))
    {{ $contentClass }}
    @else
        light
@endif
    @endif
        ">
        @yield('content')
    </section>
    @include('layouts.aim.footer')
</div>

<!-- Scripts -->
<script src="{{ asset('js/site.js') }}" defer></script>
</body>
</html>
