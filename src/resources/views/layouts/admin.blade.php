<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="api-token" content="{{ Auth::user()->api_token }}">

      <title>Administration - {{ $title }}</title>

      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body class="app header-fixed footer-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

      @include('layouts.admin.header')

      <div class="app-body">
          @include('layouts.admin.sidebar')

          <main class="main main-custom ">
              <div class="page-header clearfix">
                  <i class="nav-icon fa fa-{{ $icon }} float-left"></i>
                  <h3 class="title float-left">{{ $title }}</h3>
              </div>

              <div class="container-fluid">
                  <div class="ui-view">
                      @yield('content')
                  </div>
              </div>
          </main>
      </div>

      @include('layouts.admin.footer')

      <!-- Scripts -->
      <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
