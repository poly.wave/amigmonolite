<div class="change-language">
    <a href="/" class="btn-arrow select-language">
        @switch(App::getLocale())
            @case('en')
            <span><img src="/images/flags/en.png">English</span>
            @break

            @case('ru')
            <span><img src="/images/flags/ru.png">Русский</span>
            @break

            @case('tr')
            <span><img src="/images/flags/tr.png">Türkçe</span>
            @break
        @endswitch
    </a>

    <ul class="dropdown">
        <li>
            <a href="?lang=en" class="btn-arrow choose-language">
                <span><img src="/images/flags/en.png"></span>
            </a>
        </li>

        <li>
            <a href="?lang=ru" class="btn-arrow choose-language">
                <span><img src="/images/flags/ru.png"></span>
            </a>
        </li>
        <li>
            <a href="?lang=tr" class="btn-arrow choose-language">
                <span><img src="/images/flags/tr.png"></span>
            </a>
        </li>
    </ul>
</div>
