<!-- Header -->
<header>


    <section class="top-bar">
        <div class="container clearfix">
            <a href="#" >Doctors</a>
            <a href="#" >Doctors1</a>
            <a href="#" >Doctors2</a>
            <a href="#" >Doctors3</a>
            <a href="#" >Doctors4</a>
        </div>

        <div class="container clearfix">
            <div class="logo pull-left"> <!-- logo pull-left -->
                <a href="/">AiCan</a>
            </div>

            <div class="auth pull-right">
                <a href="/login" class="btn client-login">Login</a>


            </div>


        </div>
    </section>

    @include('layouts.aim.menu')

    @if(isset($isHome))
        @include('layouts.aim.slider')

    @endif
</header>
