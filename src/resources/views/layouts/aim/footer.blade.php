<footer>
    <section class="copyright">
        <div class="container">
            <span> 2017 - {{ date('Y') }} AiCan </span>
        </div>
    </section>
</footer>
