@foreach ($fields as $field)
    @if (is_array($field))
        <div class="row">
            @php
                $amount = count($field);
            @endphp

            @foreach($field as $field)
                <div class="col-sm-{{ isset($field->col) ? $field->col : ceil(12 / $amount) }}">
                    @include('layouts.form.field')
                </div>
            @endforeach
        </div>
    @else
        @include('layouts.form.field')
    @endif
@endforeach
