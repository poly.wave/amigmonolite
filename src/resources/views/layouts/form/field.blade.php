@if ($field->label)
    @if ($field->type != 'checkbox')
        <div class="input-group">
            <label for="{{ $field->name }}" @if (isset($field->help))
            data-toggle="tooltip"
                   title="{{ $field->help }}"
                @endif>
                {{ $field->label }}

                @if ($field->required)
                    <text style="color: red;">*</text>
                @endif

                @if (isset($field->help))
                    <i class="fa fa-question-circle"></i>
                @endif
            </label>
        </div>
    @endif
@endif

<div class="input-group mb-3">
    @if ($field->type == 'file' && isset($field->mime_type) && isset($field->image))
        @if ($field->image && $field->mime_type == 'image')
            <a href="{{ $field->image }}" class="fancybox border-{{ $field->border_color }}" style="width: 20%; border: 2px solid #000; position: relative;">
                <img src="{{ $field->image }}" style="width: 100%; height: auto;">

                @if (isset($field->image_overlay_text))
                    <div class="text-overlay text-{{ $field->border_color }}">{{ $field->image_overlay_text }}</div>
                @endif
            </a>
        @endif
    @endif

    @if ($field->type == 'select')
        <select data-selected="{{ old($field->name) ? old($field->name) : (isset($field->value) ? $field->value : '') }}"
                @if(isset($field->data))
                @foreach ($field->data as $key => $value)
                data-{{ $key }}="{{ $value }}"
                @endforeach
                @endif id="{{ $field->name }}" name="{{ $field->name }}" class="selectpicker form-control{{ $errors->has($field->name) ? ' is-invalid' : '' }}" @if ($field->autofocus) autofocus @endif autocomplete="off">
            @if (isset($field->options))
                @foreach ($field->options as $option)
                    <option value="{{ $option->value }}" @if ($option->selected) selected @endif>
                             {{$option->name}}
                     </option>
                @endforeach
            @endif
        </select>
    @elseif ($field->type == 'select-bank')
        <select data-selected=""
                @if(isset($field->data))
                @foreach ($field->data as $key => $value)
                data-{{ $key }}="{{ $value }}"
                @endforeach
                @endif id="{{ $field->name }}" name="{{ $field->name }}" class="selectpicker selectpickerbank form-control{{ $errors->has($field->name) ? ' is-invalid' : '' }}" @if ($field->autofocus) autofocus @endif autocomplete="off">
                @if (isset($field->options))
                    @foreach ($field->options as $option)
                        <option value="{{ $option->value }}" @if ($option->selected) selected @endif></option>
                    @endforeach
                @endif
        </select>

    @elseif ($field->type == 'checkbox')
        <div class="form-check{{ $errors->has($field->name) ? ' is-invalid' : '' }}">
            <input class="form-check-input" type="checkbox" name="{{ $field->name }}" id="{{ $field->name }}" {{ old($field->name) || $field->value ? 'checked' : '' }}>
            <label class="form-check-label" for="{{ $field->name }}">
                {{ $field->label }}
                @if(isset($field->link))
                    <a href="{{ $field->link->url }}">{{ $field->link->name }}</a>
                @endif
            </label>
        </div>
    @elseif ($field->type == 'file')
        <input style="padding: 0.2rem 0.2rem;" @if(isset($field->data))
        @foreach ($field->data as $key => $value)
        data-{{ $key }}="{{ $value }}"
               @endforeach
               @endif id="{{ $field->name }}" name="{{ $field->name }}" type="{{ $field->type }}" class="jfilestyle form-control{{ $errors->has($field->name) ? ' is-invalid' : '' }}">
    @else
        <input @if(isset($field->data))
               @foreach ($field->data as $key => $value)
               data-{{ $key }}="{{ $value }}"
               @endforeach
               @endif id="{{ $field->name }}" name="{{ $field->name }}" type="{{ $field->type }}" class="form-control{{ $errors->has($field->name) ? ' is-invalid' : '' }}" value="{{ old($field->name) ? old($field->name) : $field->value }}" placeholder="{{ $field->placeholder }}" @if ($field->autofocus) autofocus @endif autocomplete="off">
    @endif

    @if ($field->type != 'checkbox' && isset($field->icon))
        <div class="input-group-prepend">
            <span class="input-group-text text-muted">
                @if (substr_count($field->icon, ','))
                    @foreach(explode(',', $field->icon) as $icon)
                        <i class="fab fa-{{ trim($icon) }} mx-1"></i>
                    @endforeach
                @else
                    <i class="fa fa-{{ $field->icon }} mx-1"></i>
                @endif
            </span>
        </div>
    @endif

    @if ($errors->has($field->name))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($field->name) }}</strong>
        </span>
    @endif
</div>
