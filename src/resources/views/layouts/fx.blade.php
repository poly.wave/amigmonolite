<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">

    <title>{{ __('fx.layouts.title') }} {{ $title }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
</head>
<body>
    <div id="preloader">
        <object>
            <embed src="/images/dclogo.svg">
        </object>
    </div>

    <div id="main">
        @include('layouts.fx.header')
        <!--Content -->
        <section class="content
            @if(empty($isHome))
                @if(isset($contentClass))
                    {{ $contentClass }}
                @else
                    light
                @endif
            @endif
            ">
            @yield('content')
        </section>
        @include('layouts.fx.footer')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/site.js') }}" defer></script>

    <!-- Scripts Finteza -->
    <script type="text/javascript">
        (function(a,e,f,g,b,c,d){a[b]||(a.FintezaCoreObject=b,a[b]=a[b]||function(){(a[b].q=a[b].q||[]).push(arguments)},a[b].l=1*new Date,c=e.createElement(f),d=e.getElementsByTagName(f)[0],c.async=!0,c.defer=!0,c.src=g,d&&d.parentNode&&d.parentNode.insertBefore(c,d))})
        (window,document,"script","https://content.mql5.com/core.js","fz");
        fz("register","website",{id:"ykpstejjgbjmejpcjuljoxwpdgppjfxatm",trackHash:true,trackLinks:true,timeOnPage:true});
    </script>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script>
        window.__lc = window.__lc || {};
        window.__lc.license = 11904858;
        ;(function(n,t,c){function i(n){return e._h?e._h.apply(null,n):e._q.push(n)}var e={_q:[],_h:null,_v:"2.0",on:function(){i(["on",c.call(arguments)])},once:function(){i(["once",c.call(arguments)])},off:function(){i(["off",c.call(arguments)])},get:function(){if(!e._h)throw new Error("[LiveChatWidget] You can't use getters before load.");return i(["get",c.call(arguments)])},call:function(){i(["call",c.call(arguments)])},init:function(){var n=t.createElement("script");n.async=!0,n.type="text/javascript",n.src="https://cdn.livechatinc.com/tracking.js",t.head.appendChild(n)}};!n.__lc.asyncInit&&e.init(),n.LiveChatWidget=n.LiveChatWidget||e}(window,document,[].slice))
    </script>
    <noscript><a href="https://www.livechatinc.com/chat-with/11904858/" rel="nofollow">Chat with us</a>, powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a></noscript>
    <!-- End of LiveChat code -->

</body>
</html>
