<header class="app-header navbar header-custom">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <a class="navbar-brand" href="/admin/main">
        <img class="navbar-brand-full" src="/images/logod.png" width="70">
        <img class="navbar-brand-minimized" src="/images/logod.png" width="35">
    </a>

    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="change-block">
      <input type="number" name="amount" value="1" id="convert-amount" class="form-control">
      <select class="form-control" name="from" id="convert-from">
        <option value="USD" selected>USD</option>
        <option value="RUB">RUB</option>
        <option value="TRY">TRY</option>
      </select>
      <span>to </span>
      <select class="form-control" name="to" id="convert-to">
        <option value="USD">USD</option>
        <option value="RUB">RUB</option>
        <option value="TRY" selected>TRY</option>
      </select>
      <?php $rate = new \App\Models\ConvertCurrency; ?>
      <span>= <span id="amount-charge">{{$rate->fetch_exchange_rates("USD","TRY",1)}}</span></span>
    </div>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="@if(Auth::user()->avatar){{ Auth::user()->avatar }} @else /images/icons/icon-customer.png @endif" alt="{{ Auth::user()->email }}">
            </a>

            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header text-center"><strong>{{ __('member.header.account') }}</strong></div>
                <!-- <a class="dropdown-item" href="/user/settings">
                    <i class="fa fa-user"></i> {{ __('member.header.settings') }}
                </a> -->
                <div class="divider"></div>
                <a class="dropdown-item" href="/logout">
                    <i class="fa fa-lock"></i> {{ __('member.header.logout') }}
                </a>
            </div>
        </li>
    </ul>

    <div>
        <ul class="nav navbar-nav ml-auto mr-3">
            <li class="nav-item dropdown">
                <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="dropdown-header text-center">
                        @switch(App::getLocale())
                            @case('en')
                            <span>{{ __('member.header.lang.en') }}</span>
                            @break

                            @case('ru')
                            <span>{{ __('member.header.lang.ru') }}</span>
                            @break

                            @case('tr')
                            <span>{{ __('member.header.lang.tr') }}</span>
                            @break
                        @endswitch
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="?lang=en">
                        <i class="fa fa-flag"></i> English
                    </a>
                    <a class="dropdown-item" href="?lang=ru">
                        <i class="fa fa-flag"></i> Русский
                    </a>
                    <a class="dropdown-item" href="?lang=tr">
                        <i class="fa fa-flag"></i> Türkçe
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>
