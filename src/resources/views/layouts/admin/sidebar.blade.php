<div class="sidebar">
    <div class="sidebar-custom ">
        <a href="#" style="text-decoration: none;">
            <img class="img-avatar" src="@if(Auth::user()->avatar){{ Auth::user()->avatar }} @else /images/icons/icon-customer.png @endif" alt="{{ Auth::user()->email }}" alt="Avatar">

            <div><text style="margin-top: 10px; font-size: 20px; color: #c1b1a0;">
                {{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</text>
            </div>
        </a>

        <div class="chart-header-text">

        </div>
    </div>

    <nav class="sidebar-nav sidebar-nav-custom">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/admin/users">
                    <i class="nav-icon m fa fa-users"></i> {{ __('admin.sidebar.Users') }}
                </a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle"href="#">
                    <i class="nav-icon m fa fa-receipt"></i> {{ __('admin.sidebar.Transactions') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/transactions/deposits">
                            <i class="nav-icon fa fa-money-bill"></i> {{ __('admin.sidebar.Deposits') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/transactions/withdrawals">
                            <i class="nav-icon fa fa-money-bill"></i> {{ __('admin.sidebar.Withdrawals') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/transactions/earnings">
                            <i class="nav-icon fa fa-money-bill"></i> {{ __('admin.sidebar.Earnings') }}
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/logs">
                    <i class="nav-icon m fa fa-history"></i> {{ __('admin.sidebar.Logs') }}
                </a>
            </li>
        </ul>
    </nav>
</div>
