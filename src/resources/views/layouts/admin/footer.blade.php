<footer class="app-footer footer-custom">
    <div>
        <a href="https://amigfx.com">{{ __('member.footer.company_fx') }}</a>
        <span>&copy;{{ date('Y') }} </span>
    </div>
    <div class="socials">
      <a href="http://instagram.com/amigcapital">
        <img src="{{asset('/images/icons/insta.png')}}" alt="intsagram">
      </a>
      <a href="https://twitter.com/AmigLlc">
        <img src="{{asset('/images/icons/twitter.png')}}" alt="twitter">
      </a>
      <a href="https://www.facebook.com/amigfx">
        <img src="{{asset('/images/icons/fb.png')}}" alt="facebook">
      </a>
      <a href="https://wa.me/+905517009108">
        <img src="{{asset('/images/icons/watsapp.png')}}" alt="watsapp">
      </a>
    </div>
    <div class="ml-auto">
        <span>{{ __('member.footer.powered_by') }}</span>
        <a href="https://amigcapital.com">{{ __('member.footer.company') }}</a>
    </div>
</footer>
