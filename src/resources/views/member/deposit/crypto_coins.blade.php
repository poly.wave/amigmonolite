@extends('layouts.member')

@section('content')
<div class="row">
    <form class="" action="/member/deposit/charge-crypto" method="post" style="width:100%">
      @csrf
      <div class="col-sm-3">
        @include('layouts.form.fields')
        <div class="input-group">
          <button type="submit" name="button" class="btn btn-success">Confirm</button>
        </div>
      </div>
    </form>
</div>
@endsection
