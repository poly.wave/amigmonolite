@extends('layouts.member')

@section('content')
    @if (\Session::has('success'))
        <div class="row">
          <div class="alert alert-success alert-block" style="background: #4caf50;padding: 20px;text-align: center;">
            <strong>{{\Session::get('success')}}</strong>
          </div>
        </div>
    @endif
    <form class="" action="/member/deposit/bank-transfer" method="post" style="width:100%">
      @csrf
      <div class="col-sm-6">
        @include('layouts.form.fields')
        <div class="input-group">
          <button type="submit" name="button" class="btn btn-success">Confirm</button>
        </div>
      </div>
    </form>
    <br>
    <?php if (Auth::user()->hasBankDeposit()): ?>
        <div class="card">
            <div class="card-header chart-header">
                <i class="fa fa-list"></i>
                <strong class="chart-header-text">{{ __('member.deposit.bank_transfer.title') }}</strong>
            </div>

            <div class="card-body card-body-custom">
                <table class="table table-bordered table-striped dataTable" id="dataTableCompanyCrypto" data-url="/api/company/banks/table" data-hide-nav="true">
                    <thead>
                        <tr>
                            <th data-data="icon" data-name="icon">Icon</th>
                            <th data-data="name" data-name="name">{{ __('member.deposit.bank_transfer.bank_name') }}</th>
                            <th data-data="account_name" data-name="account_name">{{ __('member.deposit.bank_transfer.accunt_name') }}</th>
                            <th data-data="iban" data-name="iban">{{ __('member.deposit.bank_transfer.iban') }}</th>
                            <th data-data="swift" data-name="swift">{{ __('member.deposit.bank_transfer.swift') }}</th>
                            <th data-data="address" data-name="address">{{ __('member.deposit.bank_transfer.bank_address') }}</th>
                            <th data-data="currency_name" data-name="currency_name">{{ __('member.deposit.bank_transfer.currency') }}</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    <?php endif; ?>

@endsection
