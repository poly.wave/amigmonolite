@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" id="hidden-content" role="alert" style="display:none;">
            <h4 class="alert-heading">{{ __('member.deposit.credit_card.well') }}</h4>
            <p>{{ __('member.well1') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" id="hidden-content" role="alert" style="display:none;">
            <h4 class="alert-heading">{{ __('member.deposit.credit_card.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-3">
            <form method="POST" action="/member/deposit/credit_card" id="deposit_credit_card">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.deposit.credit_card.confirm') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
