@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" id="hidden-content" role="alert" style="display:none;">
            <h4 class="alert-heading">{{ __('member.withdrawal.well') }}</h4>
            <p>{{ __('member.withdrawal.reqsucc') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" id="hidden-content" role="alert" style="display:none;">
            <h4 class="alert-heading">{{ __('member.withdrawal.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/member/withdrawal_request" id="withdrawal_request">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.withdrawal.create') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
