@extends('layouts.member')

@section('content')
    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.trading.trdacc') }}</strong>
        </div>
        <div class="card-body card-body-custom">
          <?php if (count(\App\Models\UserTradingAccount::where('user_id',\Auth::user()->id)->get())==0 && (\App\Models\UserFiles::where([['user_id',\Auth::user()->id],['document_type_id',1]])->first() || \App\Models\UserPassport::where([['user_id',\Auth::user()->id],['approve_id',1]])->first())): ?>
            @if (\Session::has('success'))
                <div class="row">
                  <div class="alert alert-success alert-block" style="background: #4caf50;padding: 20px;text-align: center;">
                    <strong>{{\Session::get('success')}}</strong>
                  </div>
                </div>
              @endif
              @if (\Session::has('error'))
                <div class="row">
                  <div class="alert alert-danger alert-block" style="background: #f44336;padding: 20px;text-align: center;">
                    <strong>{!!\Session::get('error')!!}</strong>
                  </div>
                </div>
              @endif
              <form method="POST" action="/trading/register">
              @csrf
               <input type="hidden" name="user[group]" class="form-control" value="real\AMG\MVariable-USD-Mini"/>
              <div class="input-group">
                <label for="leverage">Leverage</label>
              </div>
              <div class="input-group mb-3">
                <select name="user[leverage]" class="form-control">
                  <option value="1:100">100</option>
                  <option value="1:200">200</option>
                </select>
              </div>
              <div class="input-group">
                <label for="password">Password</label>
              </div>
              <div class="input-group mb-3">
                <input type="password" name="user[password]" class="form-control"/>
              </div>
              <div class="input-group">
                <label for="confirm_password">Confirm password</label>
              </div>
              <div class="input-group mb-3">
                <input type="password" name="user[confirm_password]" class="form-control"/>
              </div>
              <input type="hidden" name="user[name]" class="form-control" value="{{\Auth::user()->first_name." ".\Auth::user()->last_name}}"/>
              <input type="hidden" name="user[email]" class="form-control" value="{{\Auth::user()->email}}"/>
              <input type="hidden" name="user[invest_password]" class="form-control" value="Qwerty123"/>
              <input type="hidden" name="user[city]" class="form-control" value="Web site"/>
              <input type="hidden" name="user[zipcode]" class="form-control" value="34000"/>
              <input type="hidden" name="user[state]" class="form-control" value="Dashboard"/>
              <input type="hidden" name="user[country]" class="form-control" value="{{\App\Models\Countries::find(\Auth::user()->country_id)->first()?\App\Models\Countries::find(Auth::user()->country_id)->first()->name:'Turkey'}}"/>
              <input type="hidden" name="user[address]" class="form-control" value="Web Dashboard"/>
              <input type="hidden" name="user[phone]" class="form-control" value="+900000001"/>
              <input type="hidden" name="user[phone_password]" class="form-control" value="Qwerty123"/>
              <div class="form-group">
              <label for="verify_code">Verification code</label>
              <img src="/image" width="60" height="22" align="absbottom" style="border: 1px dashed buttonshadow;"/>&nbsp;&nbsp;confirm: <input type="text" name="verify_code" size="6" style="width: 55px;"/>
              </div>
              <div class="form-group">
              <input type="submit" name="a[register]" value="Register" size="30" style="width: 175px;" class="btn btn-success"/>
              </div>
              </form>
          <?php endif; ?>
            <table class="table table-bordered table-striped dataTable" id="dataTableUserTradingAccount" data-url="/api/user/trading_account/table" data-hide-nav="true">
                <thead>
                    <tr>
                        <th data-data="platform" data-name="platform">{{ __('member.trading.platform') }}</th>
                        <th data-data="user_trading_leverage_name" data-name="user_trading_leverage_name">{{ __('member.trading.leverage') }}</th>
                        <th data-data="login" data-name="login">{{ __('member.trading.account') }}</th>
                        <th data-data="password" data-name="password">{{ __('member.trading.pwd') }}</th>
                        <th data-data="currency_name" data-name="currency_name">{{ __('member.trading.currency') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div>

    </div>
<div>


    <div>
        <p>For download Meta Trader 5 trading platform you can use icons below.</p>

    </div>
    <div class="osdownload">
        <a href="https://download.mql5.com/cdn/web/15233/mt5/amigcapital5setup.exe">
            <img src="{{asset('/images/source/WinOS.png')}}" alt="Windows">
        </a>
        <a href="https://download.mql5.com/cdn/web/15233/mt5/amigcapital5setup.exe">
            <img src="{{asset('/images/source/AppleOS.png')}}" alt="MacOS">
        </a>
        <a href="https://itunes.apple.com/us/app/metatrader-5-forex-stocks/id413251709">
            <img src="{{asset('/images/source/AppleStore.png')}}" alt="AppStore">
        </a>
        <a href="https://play.google.com/store/apps/details?id=net.metaquotes.metatrader5&amp;hl=en-us">
            <img src="{{asset('/images/source/GooglePlay.png')}}" alt="PlayMarket">
        </a>
    </div>
</div>
    <div>
        <p>Select the AMIG Capital server within the Meta Tader 5  Mobile application.</p>
    </div>

@endsection
