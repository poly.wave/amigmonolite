@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.personal_information.well') }}</h4>
            <p>{{ __('member.profile.personal_information.updated') }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/member/profile/personal_information" id="personal_information" enctype="multipart/form-data">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.profile.personal_information.button_update') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
