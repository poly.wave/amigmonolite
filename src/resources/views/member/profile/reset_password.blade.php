@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.reset_password.well') }}</h4>
            <p>{{ __('member.profile.reset_password.updated') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.reset_password.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/member/profile/reset_password" id="reset_password">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.profile.reset_password.button_update') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
