@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.cards.well') }}</h4>
            <p>{{ __('member.profile.wallet.cards.card_created') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.cards.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row mb-4">
        <div class="col-lg-5">
            <form method="POST" action="/member/profile/wallet/cards" id="prodile_wallet_user_card">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.profile.wallet.cards.button_add') }}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.profile.wallet.cards.creditcard') }}</strong>
        </div>

        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableUserCards" data-url="/api/user/cards/table" data-hide-nav="true">
                <thead>
                    <tr>
                        <th data-data="number" data-name="number">{{ __('member.profile.wallet.cards.cardno') }}</th>
                        <th data-data="name" data-name="name">{{ __('member.profile.wallet.cards.fullname') }}</th>
                        <th data-data="month" data-name="month">{{ __('member.profile.wallet.cards.mm') }}</th>
                        <th data-data="year" data-name="year">{{ __('member.profile.wallet.cards.yy') }}</th>
                        <th data-data="cvv" data-name="cvv">{{ __('member.profile.wallet.cards.cvv') }}</th>
                        <th data-data="currency_name" data-name="currency_name">{{ __('member.profile.wallet.cards.currency') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
