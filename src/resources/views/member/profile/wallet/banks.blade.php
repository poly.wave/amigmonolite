@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.banks.well_done') }}</h4>
            <p>{{ __('member.profile.wallet.banks.created') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.banks.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row mb-4">
        <div class="col-sm-6">
            <form method="POST" action="/member/profile/wallet/banks" id="prodile_wallet_user_bank">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.profile.wallet.banks.button_add') }}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.profile.wallet.banks.bank_details') }}</strong>
        </div>

        <div class="card-body card-body-custom">


            <table class="table table-bordered table-striped dataTable" id="dataTableUserBanks" data-url="/api/user/banks/table" data-hide-nav="true">
                <thead>
                    <tr>
                        <th data-data="name" data-name="name">{{ __('member.profile.wallet.banks.bankname') }}</th>
                        <th data-data="address" data-name="address">{{ __('member.profile.wallet.banks.address') }}</th>
                        <th data-data="iban" data-name="iban">{{ __('member.profile.wallet.banks.iban') }}</th>
                        <th data-data="swift" data-name="swift">{{ __('member.profile.wallet.banks.swift') }}</th>
                        <th data-data="currency_name" data-name="currency_name">{{ __('member.profile.wallet.banks.currency') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
