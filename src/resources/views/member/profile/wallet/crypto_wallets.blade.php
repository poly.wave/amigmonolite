@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.crypto_wallets.well') }}</h4>
            <p>{{ __('member.profile.wallet.crypto_wallets.wallet_created') }}</p>
        </div>
    @endif

    @if (isset($error))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.wallet.crypto_wallets.wrong') }}</h4>
            <p>{{ $error }}</p>
        </div>
    @endif

    <div class="row mb-4">
        <div class="col-sm-6">
            <form method="POST" action="/member/profile/wallet/crypto_wallets" id="prodile_wallet_user_crypto_wallet">
                @csrf
                @include('layouts.form.fields')

                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('member.profile.wallet.crypto_wallets.button_add') }}</button>
                </div>
            </form>
        </div>
    </div>

    <div class="card chart-header" style="display: none;">
        <div class="card-header bg-dark">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.profile.wallet.crypto_wallets.crypto_wallet') }}</strong>
        </div>

        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableUserCryptoWallets" data-url="/api/user/crypto_wallets/table" data-hide-nav="true">
                <thead>
                    <tr>
                        <th data-data="name" data-name="name">{{ __('member.profile.wallet.crypto_wallets.crname') }}</th>
                        <th data-data="account" data-name="account">{{ __('member.profile.wallet.crypto_wallets.crwallet') }}</th>
                        <th data-data="crypto_coin_id" data-name="crypto_coin_id">{{ __('member.profile.wallet.crypto_wallets.crpref') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
