@extends('layouts.member')

@section('content')
    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.profile.passport.title') }}</strong>
        </div>
        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableUserPassport" data-url="/member/profile/user_passport_table">
                <thead>
                <tr>
                    <th data-data="passport_id" data-name="passport_id">{{ __('member.profile.passport.passport_id') }}</th>
                    <th data-data="date_of_issue" data-name="date_of_issue">{{ __('member.profile.passport.date_of_issue') }}</th>
                    <th data-data="date_of_expiry" data-name="date_of_expiry">{{ __('member.profile.passport.date_of_expiry') }}</th>
                    <th data-data="issuring_authority" data-name="issuring_authority">{{ __('member.profile.passport.issuring_authority') }}</th>
                    <th data-data="approve" data-name="approve">{{ __('member.profile.passport.approve') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
