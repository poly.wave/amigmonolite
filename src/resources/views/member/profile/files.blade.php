@extends('layouts.member')

@section('content')
    @if (isset($success))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{ __('member.profile.files.well') }}</h4>
            <p>{{ __('member.profile.files.updated') }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/member/profile/files" id="files" enctype="multipart/form-data" class="row">
                @csrf
                <div class="col-sm-4">
                    <div class="input-group">
                        <label for="type">Select Type</label>
                    </div>
                    <div class="input-group mb-3">
                      <select class="form-control" name="type">
                         <option value="bill">Bill</option>
                         <option value="contract">Contract</option>
                         <option value="driving_license">Driving License</option>
                         <option value="government_id">Government ID</option>
                         <option value="passport">Passport</option>
                      </select>
                    </div>
                </div>
                <div class="col-sm-4">
                  <div class="input-group">
                      <label for="type">Choose file</label>
                  </div>
                  <div class="input-group mb-3">
                    <input type="file" name="file" class="form-control jfilestyle">
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="input-group mb-3">
                      <button type="submit" class="btn btn-success">{{ __('member.profile.files.button_update') }}</button>
                  </div>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="col-sm-12">
      <div class="row">
        @foreach ($files as $file)
        <div class="col-sm-2 fancybox border-{{$file->approve == 'approved'?'success':($file->approve == 'pending'?'primary':'danger')}}" style="margin-right: 15px; border: 2px solid #000; position: relative;">
          <a href="/storage/{{ $file->file }}">
              <img src="/storage/{{ $file->file }}" style="width: 100%; height: 150px;object-fit: contain;">
          </a>
          <div class="text-overlay text-{{$file->approve == 'approved'?'success':($file->approve == 'pending'?'primary':'danger')}}">{{ $file->approve }}</div>
        </div>
        @endforeach
      </div>
    </div>
@endsection
