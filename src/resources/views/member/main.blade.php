@extends('layouts.member')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <div class="card text-white card-custom-balance">
                <div class="card-body ">

                    <div class="text-value text-custom-lg ">{{ $userCurrency->symbol . Auth::user()->balance }}</div>
                    <div class="card-custom-text" >{{ __('member.main.balance') }}</div>
                </div>

            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white card-custom-deposits">
                <div class="card-body">

                    <div class="text-value text-custom-lg">{{ $userCurrency->symbol . Auth::user()->deposits }}</div>
                    <div class="card-custom-text">{{ __('member.main.deposits') }}</div>
                </div>


            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white card-custom-earnings">
                <div class="card-body">

                    <div class="text-value text-custom-lg ">{{ $userCurrency->symbol . Auth::user()->earnings }}</div>
                    <div class="card-custom-text">{{ __('member.main.earnings') }}</div>
                </div>

            </div>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card text-white card-custom-withdrawals">
                <div class="card-body">
                    <div class="text-value text-custom-lg">{{ $userCurrency->symbol . Auth::user()->withdrawals }}</div>
                    <div class="card-custom-text">{{ __('member.main.withdrawals') }}</div>


                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header chart-header">
            <i class="fa fa-chart-area icon-custom"></i>
            <span class="chart-header-text ">{{ __('member.main.stat') }} </span>
        </div>

        <div class="card-body chart-body">
            <div class="chart-wrapper">
                <canvas class="chart" id="commonStatistic"></canvas>
            </div>
        </div>
    </div>
@endsection
