@extends('layouts.member')

@section('content')
    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.report.deposits.title') }}</strong>
            <button type="button" data-table="dataTableUserDeposits" class="btn btn-danger btn-excel">Export to Excel</button>
            <button type="button" data-table="dataTableUserDeposits" class="btn btn-warning btn-pdf">Export to Pdf</button>
        </div>
        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableUserDeposits" data-url="/api/user/deposits/table">
                <thead>
                <tr>

                    <th data-data="name" data-name="name">{{ __('member.report.deposits.name') }}</th>
                    <th data-data="amount" data-name="amount">{{ __('member.report.deposits.amount') }}</th>
                    <th data-data="currency" data-name="currency">{{ __('member.report.deposits.currency') }}</th>
                    <th data-data="approve" data-name="approve" >{{ __('member.report.deposits.approve') }}</th>
                    <th data-data="time" data-name="time">{{ __('member.report.deposits.date_time') }} </th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
