@extends('layouts.member')

@section('content')
    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.report.earnings.title') }}</strong>
            <button type="button" data-table="dataTableEarnings" class="btn btn-danger btn-excel">Export to Excel</button>
            <button type="button" data-table="dataTableEarnings" class="btn btn-warning btn-pdf">Export to Pdf</button>
        </div>

        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableEarnings" data-url="/api/user/earnings/table">
                <thead>
                <tr>
                    <th data-data="name" data-name="name">{{ __('member.report.earnings.name') }}</th>
                    <th data-data="amount" data-name="amount">{{ __('member.report.earnings.amount') }}</th>
                    <th data-data="currency" data-name="currency">{{ __('member.report.earnings.currency') }}</th>
                    <th data-data="time" data-name="time">{{ __('member.report.earnings.date_time') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
