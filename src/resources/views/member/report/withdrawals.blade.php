@extends('layouts.member')

@section('content')
    <div class="card" style="display: none;">
        <div class="card-header chart-header">
            <i class="fa fa-list"></i>
            <strong class="chart-header-text">{{ __('member.report.withdrawals.title') }}</strong>
            <button type="button" data-table="dataTableUserWithdrawals" class="btn btn-danger btn-excel">Export to Excel</button>
            <button type="button" data-table="dataTableUserWithdrawals" class="btn btn-warning btn-pdf">Export to Pdf</button>
        </div>

        <div class="card-body card-body-custom">
            <table class="table table-bordered table-striped dataTable" id="dataTableUserWithdrawals" data-url="/api/user/withdrawals/table">
                <thead>
                <tr>
                    <th data-data="name" data-name="name">{{ __('member.report.withdrawals.name') }} </th>
                    <th data-data="amount" data-name="amount">{{ __('member.report.withdrawals.amount') }}</th>
                    <th data-data="currency" data-name="currency">{{ __('member.report.withdrawals.currency') }}</th>
                    <th data-data="approve" data-name="approve" >{{ __('member.report.withdrawals.approve') }}</th>
                    <th data-data="withdrawal_option_name" data-name="withdrawal_option_name">{{ __('member.report.withdrawals.type') }}</th>
                    <th data-data="time" data-name="time">{{ __('member.report.withdrawals.date_time') }}</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
