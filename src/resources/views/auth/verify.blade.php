@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('passwords.verifyemail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('passwords.linksent') }}
                        </div>
                    @endif

                        {{ __('passwords.checkemail') }}
                    {{ __('passwords.didnotreceive') }}, <a href="{{ route('verification.resend') }}">{{ __('passwords.requestanother') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
