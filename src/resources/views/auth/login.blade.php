@extends('layouts.auth')

@section('auth')
    <div class="col-md-8">
        <div class="card-group">
            <div class="card">
                <div class="card-body p-5">
                    <div>

                    </div>
                    @if (\Session::has('code_error'))
                        <div class="row">
                          <div class="alert alert-danger alert-block" style="background: #f44336;padding: 20px;text-align: center;">
                            <strong>{{\Session::get('code_error')}}</strong>
                          </div>
                        </div>
                    @endif
                    <h1><a href="/">{{ __('auth.login') }}</a></h1>
                    <p class="text-muted">{{ __('auth.logindesc') }}</p>

                    <form method="POST" action="{{ route('login') }}" id="login">
                        @csrf

                        @include('layouts.form.fields')
                        @if (\Session::has('code_error'))
                        <div class="input-group">
                          <label for="code">One time code</label>
                        </div>
                        <div class="input-group mb-3">
                          <input type="text" name="code" class="form-control"/>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary px4">{{ __('auth.loginbttn') }}</button>
                            </div>
                            <div class="col-8 text-right">
                                <a class="btn btn-link px-0 text-sm-right" href="{{ route('password.request') }}">{{ __('auth.forgotpwd') }}</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer p-4 d-lg-none">
                    <div class="col-12 text-right">
                        <a class="btn btn-outline-primary btn-block mt-3" href="{{ route('register') }}">{{ __('auth.regbttn') }}</a>
                    </div>
                </div>
            </div>

            <div class="card text-white bg-primary py-5 d-md-down-none">
                <div class="card-body text-center">
                    <div>
                        <h2>{{ __('auth.signup') }}</h2>
                        <p>{{ __('auth.singupdesc') }}</p>
                        <a class="btn btn-primary active mt-2" href="{{ route('register') }}">{{ __('auth.regnowbttn') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
