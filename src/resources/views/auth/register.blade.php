@extends('layouts.auth')

@section('auth')
    <div class="col-md-8">
        <div class="card-group">
            <div class="card">
                <div class="card-body p-5">
                    <h1>{{ __('auth.regform') }}</h1>
                    <p class="text-muted">{{ __('auth.newacc') }}</p>

                    <form method="POST" action="{{ route('register') }}" id="register">
                        @csrf
                        @include('layouts.form.fields')

                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-block btn-success btn-primary">{{ __('auth.createacc') }}</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card-footer p-4 d-lg-none">
                    <div class="col-12 text-right">
                        <a class="btn btn-outline-primary btn-block" href="{{ route('login') }}">{{ __('auth.loginp') }}</a>
                    </div>
                </div>
            </div>

            <div class="card text-white bg-primary py-5 d-md-down-none">
                <div class="card-body text-center">
                    <div>
                        <h2>{{ __('auth.login') }}</h2>
                        <p>{{ __('auth.loginpdesc') }}</p>
                        <a class="btn btn-primary active mt-2" href="{{ route('login') }}">{{ __('auth.loginp') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
