@extends('layouts.admin')

@section('content')
<div class="card" style="display: none;">
    <div class="card-body card-body-custom">
        <table class="table table-bordered table-striped dataTable" id="dataTableLogss" data-url="/admin/logs/table">
            <thead>
            <tr>
                <th data-data="id" data-name="id">{{ __('admin.id') }}</th>
                <th data-data="username" data-name="username">{{ __('admin.users.first_name') }}</th>
                <th data-data="type" data-name="type">{{ __('admin.users.title') }}</th>
                <th data-data="ip" data-name="ip">IP</th>
                <th data-data="device" data-name="device" >{{ __('admin.users.device') }}</th>
                <th data-data="os" data-name="os">{{ __('admin.users.os') }}</th>
                <th data-data="location" data-name="location">{{ __('admin.users.location') }}</th>
                <th data-data="created_at" data-name="created_at">{{ __('admin.users.created_at') }}</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
