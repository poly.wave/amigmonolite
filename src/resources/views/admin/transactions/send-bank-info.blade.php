@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/admin/transactions/send-bank-info/{{$id}}" id="send-bank-info" enctype="multipart/form-data">
                @csrf
                @include('layouts.form.fields')
                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('admin.save') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
