@extends('layouts.admin')

@section('content')
@if (\Session::has('warning'))
    <div class="row">
      <div class="alert alert-warning alert-block" style="padding: 20px;text-align: center;">
        <strong>{{\Session::get('warning')}}</strong>
      </div>
    </div>
@endif
<div class="card" style="display: none;">
    <div class="card-header chart-header">
        <a class="btn btn-primary" href="/admin/transactions/create-withdrawal"><i class="fa fa-cart-plus"></i></a>
    </div>
    <div class="card-body card-body-custom">
        <table class="table table-bordered table-striped dataTable" id="dataTableWithdrawal" data-url="/admin/transactions/withdrawals-table">
            <thead>
            <tr>
                <th data-data="id" data-name="id">{{ __('admin.transactions.id') }}</th>
                <th data-data="user" data-name="user">{{ __('admin.transactions.user') }}</th>
                <th data-data="name" data-name="name">{{ __('admin.transactions.name') }}</th>
                <th data-data="amount" data-name="amount">{{ __('admin.transactions.amount') }}</th>
                <th data-data="currency" data-name="currency" >{{ __('admin.transactions.currency') }}</th>
                <th data-data="time" data-name="time">{{ __('admin.transactions.time') }}</th>
                <th data-data="approve" data-name="approve">{{ __('admin.transactions.approve') }}</th>
                <th data-data="action" data-name="action"></th>
            </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
