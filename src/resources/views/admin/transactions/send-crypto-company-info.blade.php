@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/admin/transactions/send-crypto-company-info/{{$id}}" id="send-crypto-company-info" enctype="multipart/form-data">
                @csrf
                @include('layouts.form.fields')
                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('admin.save') }}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
