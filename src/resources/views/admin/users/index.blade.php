@extends('layouts.admin')

@section('content')
<div class="card" style="display: none;">
    <div class="card-header chart-header">
        <a title="{{__('admin.add')}}" href="/admin/users/create" class="btn btn-primary"><i class="fa fa-user-plus"></i></a>
    </div>
    <div class="card-body card-body-custom">
        <table class="table table-bordered table-striped dataTable" id="dataTableUsers" data-url="/admin/users/table">
            <thead>
            <tr>
                <th data-data="id" data-name="id">{{ __('admin.id') }}</th>
                <th data-data="mt_login" data-name="mt_login">{{ __('admin.users.mt_account') }}</th>
                <th data-data="first_name" data-name="first_name">{{ __('admin.users.first_name') }}</th>
                <th data-data="last_name" data-name="last_name">{{ __('admin.users.last_name') }}</th>
                <th data-data="email" data-name="email" >{{ __('admin.users.email') }}</th>
                <th data-data="phone" data-name="phone">{{ __('admin.users.phone') }}</th>
                <th data-data="group" data-name="group">{{ __('admin.users.group') }}</th>
                <th data-data="created_by" data-name="created_by">{{ __('admin.users.created_by') }}</th>
                <th data-data="action" data-name="action"></th>
            </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
