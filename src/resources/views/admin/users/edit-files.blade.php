@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <form method="POST" action="/admin/users/edit-files/{{$id}}" id="edit_deposit" enctype="multipart/form-data">
                @csrf
                @include('layouts.form.fields')
                <div class="input-group">
                    <button type="submit" class="btn btn-success">{{ __('admin.save') }}</button>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <div class="col-sm-12">
      <div class="row">
        @foreach ($files as $file)
        <?php if (strpos($file->file, 'storage/') !== false): ?>
            <?php
                $f = $file->file;
             ?>
        <?php else: ?>
            <?php
                $f = '/storage/'.$file->file;
             ?>
        <?php endif; ?>

        <div class="col-sm-3 fancybox border-{{$file->approve == 'approved'?'success':($file->approve == 'pending'?'primary':'danger')}}" style="margin-right: 15px; border: 2px solid #000; position: relative;">
          <a href="{{ $f }}">
              <img src="{{ $f }}" style="width: 100%; height: 250px;object-fit: contain;">
          </a>
          <div class="text-overlay text-{{$file->approve == 'approved'?'success':($file->approve == 'pending'?'primary':'danger')}}">{{ $file->approve }}</div>
        </div>
        @endforeach
      </div>
    </div>
@endsection
