<html>
    <head></head>

    <body>
        <h1>New user withdrawal request</h1>

        <p>
            <h3>User data:</h3>
            <strong>Email:</strong> {{ $user->email }}<br>
            <strong>Id:</strong> {{ $user->id }}
        </p>

        <p>
            <h3>Request data:</h3>
            <strong>Amount:</strong> {{ $data->amount }} {{ $data->currency_name }}<br>
            <strong>Name:</strong> {{ $data->name }}<br>
            <strong>Withdrawal option:</strong> {{ $data->withdrawal_option_name }}
        </p>
    </body>
</html>
