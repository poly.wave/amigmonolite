@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>About Us</h2>
        <p>
            We are <strong>Amazing Money Investment Group</strong>
        </p>
        <div class="divider divider-1"></div>
        <p>
            <strong>AMIG Capital LLC</strong> has quickly grown into one of the largest <strong>Finance</strong>
            and <strong>Investment</strong> sector. We provide all investors to benefit from our new and different
            applications in order to enlarge and popularize the investment and finance choice.
        </p>
        <p>We give our customers a special investment service at <strong>AMIG Capital LLC</strong> to contribute more
            to the analysis and evaluation process, to resolve a potential problem on the spot, and for
            a seamless support.</p>
        <p>
            We provide more professional and faster service to each customer.
        </p>
        <div class="divider divider-3"></div>

        <h2>Our Services</h2>
        <p><strong>AMIG Capital LLC</strong> reduces the client costs with the most competitive
            prices in the market, creating the best trading conditions for investors.</p>
        <div class="divider divider-1"></div>
        </p>
    </div>
    <!-- text finish -->
@endsection
