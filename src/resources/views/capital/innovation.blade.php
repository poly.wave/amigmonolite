@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>Innovation and Technology Consulting Services</h2>
        <h3>Bring digital transformation down to earth.</h3><br>
        <h3>But how do you do that?</h3><br>
        <p>
            Digital transformation often feels like an incredibly complex initiative with huge risks
            and uncertainties. And it actually is.<br>

            When the scope of change is overwhelming, we believe in starting smart and simple. We break
            complexity into an easily digestible roadmap for change. It’s all about smaller pieces, faster
            iterations, and providing maximum value wherever possible. That way, every iteration adds up
            to a global business transformation.<br>
            We help you pivot to new technology step-by-step while getting the most out of your legacy systems.<br>
        </p>
        <div class="divider divider-3"></div>

        <p>
            ✔ IT strategy & planning<br>
            ✔ IT infrastructure audit & upgrade<br>
            ✔ Software architecture & design<br>
            ✔ Business process analysis<br>
            ✔ Choice of technology and framework<br>
            ✔ DevOps consulting<br>
        </p>


        <div class="divider divider-1"></div>
        <p>
            <strong>Innovation and Technology Consulting Services</strong> fee is $6800.00 including VAT.
            Click to buy then send to us your receipt by email.
        </p>
        <div>

            <?php
            $m_shop = '1288148030';
            $m_orderid = '1';
            $m_amount = number_format(6800, 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode('Innovation and Technology Consulting Services');
            $m_key = 'e355tFxFELFeJxyG';

            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc
            );

            $arHash[] = $m_key;

            $sign = strtoupper(hash('sha256', implode(':', $arHash)));
            ?>
            <form method="post" action="https://payeer.com/merchant/">
                <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                <input type="hidden" name="m_sign" value="<?=$sign?>">
                <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
            </form>

        </div>

    </div>
    <!-- text finish -->
@endsection
