@extends('layouts.capital')

@section('content')
    <div class="start-trading keep">
        <div class="container clearfix">
            <div class="pull-left">
                <span class="title">{{ __('capital.keep') }}</span>
            </div>

            <div class="pull-right">
                <form action="/mail.php" method="post">
                    <input type="text" placeholder="{{ __('capital.name') }}" name="name" required>
                    <input type="email" placeholder="{{ __('capital.email') }}" name="email" required>

                    <div class="captcha_wrapper">
                        <script src='https://www.google.com/recaptcha/api.js'></script>
                        <div class="g-recaptcha" data-sitekey="6LcxX58UAAAAAMYDmiNWdOFowAIDCCctHgvBDcYx"></div>
                    </div>

                    <button class="btn btn-gold">{{ __('capital.gobttn') }}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
