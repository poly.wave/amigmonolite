@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text clearfix">
        <div class="contacts pull-left left">
            <h2>{{ __('capital.office') }}</h2>
            <p>
                <strong>{{ __('capital.phone') }}:</strong> <a href="tel:+382 68 328 736">+382 68 328 736</a>
            </p>
            <p>
                <strong>{{ __('capital.email') }}:</strong> <a href="mailto:support@amigcapital.com">support@amigcapital.com</a>
            </p>
            <p>
                <strong>{{ __('capital.address') }}:</strong> 26 Novembar BB, 85360, Ulcinj, Montenegro
            </p>
        </div>
        <div class="map pull-right right">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1247.9925618367167!2d19.208708620815504!3d41.93062372295263!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x134e135ec5b2df75%3A0x7ea15ff12811aef6!2s26.+Novembra%2C+Ulcinj+85360%2C+Montenegro!5e0!3m2!1sen!2str!4v1555311434472!5m2!1sen!2str" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

    <!-- text finish -->

    <div class="container clearfix">
        <div class="contact-form pull-left left">
            <div class="title">
                {{ __('capital.cfhead') }}
            </div>

            <div class="article">
                {{ __('capital.cfdesc') }}
            </div>

            <form action="/mail.php" method="post">
                <fieldset>
                    <input type="text" placeholder="{{ __('capital.fname') }}" class="past first" name="first_name" required="">
                    <input type="text" placeholder="{{ __('capital.lname') }}" class="past last" name="last_name" required="">
                </fieldset>

                <fieldset>
                    <input type="email" placeholder="{{ __('capital.femail') }}" class="past first" name="email" required="">
                    <input type="tel" placeholder="{{ __('capital.mobile') }}" class="past last" name="phone" required="">
                </fieldset>

                <fieldset class="last">
                    <textarea name="text" placeholder="{{ __('capital.message') }}" required></textarea>
                </fieldset>

                <div class="captcha_wrapper" style="margin-bottom: 30px;">
                    <script src='https://www.google.com/recaptcha/api.js'></script>
                    <div class="g-recaptcha" data-sitekey="6LcxX58UAAAAAMYDmiNWdOFowAIDCCctHgvBDcYx"></div>
                </div>

                <button class="btn btn-gold">{{ __('capital.sendbtn') }}</button>
            </form>
        </div>
    </div>
@endsection
