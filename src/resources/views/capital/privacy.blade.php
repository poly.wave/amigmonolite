@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>Privacy Policy</h2>
        <p>
            <strong>AMIG Capital</strong>tracks visitors to this Web site in the aggregate only.
            Our Web site does not automatically collect personal information, but we do collect such information if:
        </p>
        <div class="divider divider-1"></div>
        <p>

            You apply for a service or to be included in our consultant database. Information you submit through email
            or any online forms will be retained but not shared outside of AMIG without your consent.
        </p>
        <p>
            You send an email directly to AMIG Capital from the Contacts page.
            We will use your name and address in responding to your email. Names and email addresses are retained
            with your consent.
        </p>
        <div class="divider divider-3"></div>
        <p>
            We will be happy to answer your questions about our privacy policy and how we
            collect, use, disclose, transfer, and store your information..
        </p>






    </div>
    <!-- text finish -->
@endsection
