@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>Finance Consulting Services</h2>
        <h3>Everyone strives to achieve financial independence.</h3><br>
        <h3>But how do you do that?</h3><br>
        <p>
            Financial literacy is not included in the curriculum of secondary and higher educational
            institutions, unless you receive a corresponding profession. It is difficult for an unprepared
            person to understand the flows of economic information, which, moreover, has the property of
            changing quickly. You will need an independent financial advisor who can help not only maintain
            a stable level of income, but also increase it.
        </p>
        <div class="divider divider-3"></div>
        <h3>Why is it profitable to get the services of a financial adviser in our company?</h3><br>
        <p>
            Our main advantage lies in the professional approach of experts.<br>
            The Investment Adviser has important functions:<br><br>

            ✔ Tracks market trends<br>
            ✔ Informed on new investment instruments and other opportunities<br>
            ✔ Continuous monitoring of legislation, so always up to date<br>
        </p>

        <h3>WHO NEEDS FINANCIAL ADVISOR SERVICES</h3> <br>
        <p>
            Most often, a financial adviser is employed by start-up investors or individuals interested in
        optimizing costs and achieving certain goals. <br>
            The specialist will help:<br><br>

            ✔Creating Personal Financial Plan<br>
            ✔Set goals (accumulate for education, increase pension savings, receive income from
        investments and many others)<br>
            ✔Teach Competently manage capital<br><br>

           <strong>AS A RESULT, YOU WILL:</strong> <br><br>

            ✔ Finance Consulting<br>
            ✔ Assistance in addressing emerging investment challenges<br>
            ✔ Indibidual approach<br>



        <div class="divider divider-1"></div>
        <p>
            <strong>General Finance Consulting Service</strong> fee is $3000.00 including VAT.
            Click to buy then send to us your receipt by email.
        </p>
        <div>

            <?php
            $m_shop = '1288148030';
            $m_orderid = '1';
            $m_amount = number_format(3000, 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode('General Finance Consulting Service');
            $m_key = 'e355tFxFELFeJxyG';

            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc
            );

            $arHash[] = $m_key;

            $sign = strtoupper(hash('sha256', implode(':', $arHash)));
            ?>
            <form method="post" action="https://payeer.com/merchant/">
                <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                <input type="hidden" name="m_sign" value="<?=$sign?>">
                <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
            </form>

        </div>

    </div>
    <!-- text finish -->
@endsection
