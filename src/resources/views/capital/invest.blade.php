@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>Investment Consulting Services</h2>
        <h3>We are here to help you to make the best investment decisions.</h3><br>
        <p>
            With our extensive experience in the sector, we provide expert guidance in setting strategies
            for successful investment endeavors in such competitive market conditions.
        </p>
        <div class="divider divider-3"></div>
        <h3>Some services we provide in the scope of investment consultancy are as follows:</h3><br>
        <p>
            ✔ "Highest and Best Use" studies,<br>
            ✔ Feasibility and market studies,<br>
            ✔ Management Company and brand search,<br>
            ✔ Management, franchise, lease, technical service agreements,<br>
            ✔ Appraisals and valuations,<br>
            ✔ Interpretation of third party reports.<br>

        </p>

        <div class="divider divider-1"></div>
        <p>
            <strong>General Investment Consulting Service</strong> fee is $7300.00 including VAT.
            Click to buy then send to us your receipt by email.
        </p>
        <div>

            <?php
            $m_shop = '1288148030';
            $m_orderid = '1';
            $m_amount = number_format(7300, 2, '.', '');
            $m_curr = 'USD';
            $m_desc = base64_encode('General Investment Consulting Service');
            $m_key = 'e355tFxFELFeJxyG';

            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc
            );

            $arHash[] = $m_key;

            $sign = strtoupper(hash('sha256', implode(':', $arHash)));
            ?>
            <form method="post" action="https://payeer.com/merchant/">
                <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                <input type="hidden" name="m_sign" value="<?=$sign?>">
                <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
            </form>

        </div>

    </div>
    <!-- text finish -->
@endsection
