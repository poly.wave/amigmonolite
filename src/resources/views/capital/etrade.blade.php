@extends('layouts.capital')

@section('content')
    <!-- text start -->
    <div class="container text">
        <h2>eTrade Solutions</h2>


        <div class="introducion">
            <div class="four clearfix">
                <div class="column">
                    <img src="images/source/RegAccTypeStep1.png">
                    <div class="title">STEP 01</div>
                    <div class="article">Sign Contract which we send you! </div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep2.png">
                    <div class="title">STEP 02</div>
                    <div class="article">Chooce you needed and click topay.</div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep3.png">
                    <div class="title">STEP 03</div>
                    <div class="article-Cells-SubTitles">Send Scanned contract and receipt to us.</div>
                </div>
                <div class="column">
                    <img src="images/source/RegAccTypeStep4.png">
                    <div class="title">STEP 04</div>
                    <div class="article">Go on!</div>
                </div>
            </div>
        </div>

        <div class="divider"></div>

        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Website</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #616161;">Website + Mobile app</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">Design for choose</div>
                    <div class="td">2
                    </div>
                    <div class="td">3
                    </div>
                </div>
                <div class="tr">
                    <div class="td">Updates and changes</div>
                    <div class="td">2</div>
                    <div class="td">5</div>
                </div>
                <div class="tr">
                    <div class="td">Support</div>
                    <div class="td">6 months
                    </div>
                    <div class="td">1 Year
                    </div>
                </div>
                <div class="tr">
                    <div class="td">Price include all VAT</div>
                    <div class="td">$ 5,000.00
                    </div>
                    <div class="td">$ 7,000.00
                    </div>
                </div>

                <div class="tr">
                    <div class="td"></div>
                    <div class="td">
                        <?php
                        $m_shop = '1288148030';
                        $m_orderid = '1';
                        $m_amount = number_format(5000, 2, '.', '');
                        $m_curr = 'USD';
                        $m_desc = base64_encode('Website development');
                        $m_key = 'e355tFxFELFeJxyG';

                        $arHash = array(
                            $m_shop,
                            $m_orderid,
                            $m_amount,
                            $m_curr,
                            $m_desc
                        );

                        $arHash[] = $m_key;

                        $sign = strtoupper(hash('sha256', implode(':', $arHash)));
                        ?>
                        <form method="post" action="https://payeer.com/merchant/">
                            <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                            <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                            <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                            <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                            <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                            <input type="hidden" name="m_sign" value="<?=$sign?>">
                            <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                            <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                            <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
                        </form>
                    </div>
                    <div class="td">
                        <?php
                        $m_shop = '1288148030';
                        $m_orderid = '1';
                        $m_amount = number_format(7000, 2, '.', '');
                        $m_curr = 'USD';
                        $m_desc = base64_encode('Website and Mobile app development');
                        $m_key = 'e355tFxFELFeJxyG';

                        $arHash = array(
                            $m_shop,
                            $m_orderid,
                            $m_amount,
                            $m_curr,
                            $m_desc
                        );

                        $arHash[] = $m_key;

                        $sign = strtoupper(hash('sha256', implode(':', $arHash)));
                        ?>
                        <form method="post" action="https://payeer.com/merchant/">
                            <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                            <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                            <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                            <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                            <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                            <input type="hidden" name="m_sign" value="<?=$sign?>">
                            <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                            <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                            <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
                        </form>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: green;">CRM System</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #ebcf6d;">Trading System integration</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tr">
                    <div class="td">Platforms</div>
                    <div class="td">Php, Laravel, MySql

                    </div>
                    <div class="td">Meta Trader 4, Meta Trader 5

                    </div>
                </div>
                <div class="tr">
                    <div class="td">Integrated systems</div>
                    <div class="td">All platforms and languages</div>
                    <div class="td">All platforms and languages</div>
                </div>
                <div class="tr">
                    <div class="td">Support</div>
                    <div class="td">1 Year
                    </div>
                    <div class="td">1 Year
                    </div>
                </div>


                <div class="tr">
                    <div class="td">Price include all VAT</div>
                    <div class="td">
                        $ 8,500.00
                    </div>
                    <div class="td">
                        $ 15,000.00
                    </div>
                </div>
                <div class="tr">
                    <div class="td"></div>
                    <div class="td">

                        <?php
                        $m_shop = '1288148030';
                        $m_orderid = '1';
                        $m_amount = number_format(8500, 2, '.', '');
                        $m_curr = 'USD';
                        $m_desc = base64_encode('CRM System');
                        $m_key = 'e355tFxFELFeJxyG';

                        $arHash = array(
                            $m_shop,
                            $m_orderid,
                            $m_amount,
                            $m_curr,
                            $m_desc
                        );

                        $arHash[] = $m_key;

                        $sign = strtoupper(hash('sha256', implode(':', $arHash)));
                        ?>
                        <form method="post" action="https://payeer.com/merchant/">
                            <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                            <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                            <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                            <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                            <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                            <input type="hidden" name="m_sign" value="<?=$sign?>">
                            <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                            <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                            <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
                        </form>

                    </div>
                    <div class="td">

                        <?php
                        $m_shop = '1288148030';
                        $m_orderid = '1';
                        $m_amount = number_format(15000, 2, '.', '');
                        $m_curr = 'USD';
                        $m_desc = base64_encode('Trading System integration');
                        $m_key = 'e355tFxFELFeJxyG';

                        $arHash = array(
                            $m_shop,
                            $m_orderid,
                            $m_amount,
                            $m_curr,
                            $m_desc
                        );

                        $arHash[] = $m_key;

                        $sign = strtoupper(hash('sha256', implode(':', $arHash)));
                        ?>
                        <form method="post" action="https://payeer.com/merchant/">
                            <input type="hidden" name="m_shop" value="<?=$m_shop?>">
                            <input type="hidden" name="m_orderid" value="<?=$m_orderid?>">
                            <input type="hidden" name="m_amount" value="<?=$m_amount?>">
                            <input type="hidden" name="m_curr" value="<?=$m_curr?>">
                            <input type="hidden" name="m_desc" value="<?=$m_desc?>">
                            <input type="hidden" name="m_sign" value="<?=$sign?>">
                            <?php /*
<input type="hidden" name="form[ps]" value="2609">
<input type="hidden" name="form[curr[2609]]" value="USD">
*/ ?>
                            <?php /*
<input type="hidden" name="m_params" value="<?=$m_params?>">
<input type="hidden" name="m_cipher_method" value="AES-256-CBC">
*/ ?>
                            <input type="submit" class="btn btn-gold" name="m_process" value="Click to pay" />
                        </form>

                    </div>
                </div>

            </div>
        </div>


    <!-- text finish -->
@endsection
