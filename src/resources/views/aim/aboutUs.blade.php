@extends('layouts.aim')

@section('content')
    <!-- Header Top Wrap Start -->
    <div class="header-top-wrap border-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <p class="text-center top-message"><a href="#">Now Hiring:</a> Are you a driven and motivated 1st Line IT Support Engineer?</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Header Top Wrap End -->


@endsection
