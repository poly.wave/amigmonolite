<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '
                <h2 class="SectionSubTitle">Advantages of FX trading</h2>
        <p>
            Although the Forex market is by far the largest and most liquid in
            the world, day traders have, up to now, focused on seeking profits
            mainly in stocks and futures markets. This is mainly due to the restrictive
            nature of bank-offered Forex trading services.
        </p>
        <p>
            There are many advantages to trading spot Foreign Exchange as opposed to
            trading stocks and futures. The main advantages are listed below.
        </p>

        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">24-hour Market</h2>
        <p>
            FX is a global market that never sleeps. It is active 24 hours a day for almost 7
            days a week. Most activity takes place between the times that the New Zealand market
            opens on Monday, which is Sunday evening in Europe, and the US market closes on Friday evening.
        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">Liquidity</h2>
        <p>
            The FX market is huge and is still expanding. Daily average volume now exceeds USD 3.2 trillion.
            Technology has made this market accessible to almost anyone, and retail traders have flocked to FX.
        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">Leverage</h2>
        <p>
            FX margin ratios tend to be higher than those available in equity because it is more liquid - there is
            nearly always a price in FX - and it tends to be less volatile..
        </p>


        ',

    ],


];
