<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'sidebar'  => [
        'Users'  => 'Users',
        'Transactions'  => 'Transactions',
        'Deposits'  => 'Deposits',
        'Withdrawals'  => 'Withdrawals',
        'Earnings'  => 'Earnings',
        'Manually'  => 'Manually',
        'Logs'  => 'Logs',

    ],
    'users'  => [
        'title'  => 'Description',
        'device'  => 'Device',
        'os'  => 'OS',
        'location'  => 'Location',
        'created_at'  => 'Created Time',
        'mt_account'  => 'MT account',
        'first_name'  => 'First Name',
        'last_name'  => 'Last Name',
        'email'  => 'E-mail',
        'password'  => 'Password',
        'phone'  => 'Phone',
        'group'  => 'Group',
        'add user' => 'Add new user',
        'update user'=>'Update User',
        'avatar' => 'User Avatar',
        'day_of_birth'=>'Day of Birthday',
        'gender'=>'Gender',
        'language'=>'Language',
        'currency'=>'Currency',
        'country'=>'Country',
        'region'=>'Region',
        'city'=>'City',
        'resident_address' => 'Resident Address',
        'created_by'=>'Created By',
        'change files status'=>'Change files status',
        'change passport status'=>'Change passport status'

    ],
    'transactions'  => [
        'add'  => 'Add',
        'id'  => 'Id',
        'user'  => 'User',
        'name'  => 'Title',
        'amount'  => 'Amount',
        'currency'  => 'Currency',
        'time'  => 'Time',
        'approve'  => 'Approved',
        'bank_name' => 'Bank Name',
        'add deposit' => 'Add Deposit',
        'edit deposit' => 'Edit Deposit',
        'add withdrawal' => 'Add Withdrawal',
        'edit withdrawal' => 'Edit Withdrawal',
        'add earning' => 'Add Earning',
        'edit earning' => 'Edit Earning',
        'action' => 'Action',
        'deal' => 'Deal',
        'order' => 'Order',
        'entry' => 'Entry',
        'symbol' => 'Symbol',
        'comment' => 'Comment',
        'manually' => 'Add to metatrader?',
        'document_type_id'=>'Document Type'

    ],
    'Are you sure you want to delete this user?'=>'Are you sure you want to delete this user?',
    'Are you sure you want to delete this item?'=>'Are you sure you want to delete this item?',
    'id'  => 'Id',
    'save'  => 'Save',
    'add'  => 'Add',
    'edit'=>'Edit',
    'delete'=>'Delete',
    'title' => 'AMIG Admin',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'  => 'LOGIN',
    'loginbttn'  => 'Login',
    'logindesc'  => 'Sign In to your account',
    'forgotpwd'  => 'Forgot your password?',
    'regbttn'  => 'Register',
    'regnowbttn'  => 'Register Now!',
    'signup'  => 'Sign Up',
    'singupdesc'  => 'If you dont have an account Just Register.',

    'regform'  => 'Register form',
    'loginp'  => 'LOGIN PAGE',
    'loginpdesc'  => 'If you have account just login.',
    'newacc'  => 'Create your account',
    'createacc'  => 'Create Account',

    'emailaddress'  => 'E-mail Address',
    'password'  => 'Password',
    'remember'  => 'Remember me',


];
