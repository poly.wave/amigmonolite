<?php

return [
    /*
    |--------------------------------------------------------------------------
    | USER Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*  */



    /*AuthenticatedController*/

    'main'  => 'MAIN',
    'trading'  => 'TRADING',
    'depositby'  => 'DEPOSIT BY',
    'banktransfer'  => 'Bank Transfer',
    'creditcard'  => 'Credit Card',
    'cryptocoins'  => 'Crypto Coins',
    'withdrawal'  => 'WITHDRAWAL',
    'report'  => 'REPORT',
    'deposits'  => 'Deposits',
    'earnings'  => 'Earnings',
    'withdrawals'  => 'Withdrawals',
    'profile'  => 'PROFILE',
    'personalinfo'  => 'Personal Information',
    'user_passport'  => 'Passport / ID',
    'wallet'  => 'Wallet',
    'reset'  => 'Reset Password',
    'banks'  => 'Bank Accounts',
    'creditcards'  => 'Credit Cards',
    'cryptowallets'  => 'Crypto Wallets',
    'files'  => 'Files',
    'usersettings'  => 'User Settings',







];
