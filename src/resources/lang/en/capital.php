<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Layouts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title'  => 'AMIG Capital LLC - Investment & Finance Consulting ',
    /*
    Menu
    */
     'main'  => 'Home',
     'about'  => 'About US',
     'projects'  => 'Projects',
      'contacts'  => 'Contacts',
    /*
    Main
    */
    'keep'  => 'KEEP IN TOUCH',
    'name'  => 'Name',
    'gobttn'  => 'GO',
    /*
    Contacts
    */
    'office'  => 'Main Office',
    'phone'  => 'Phone',
    'email'  => 'e-mail',
    'address'  => 'Address',
    /*
    Contact Form
    */
    'cfhead'  => 'Contact Form',
    'cfdesc'  => 'We are here ready to answer you question.',
    'fname'  => 'First Name',
    'lname'  => 'Last Name',
    'femail'  => 'Email Address',
    'mobile'  => 'Mobile',
    'message'  => 'Message',
    'sendbtn'  => 'Send',

    /*
     Slider
    */
        'finance'  => 'FINANCE',
        'fxcrypto'  => 'eTRADING SOLUTIONS',
        'innovation'  => 'INNOVATION & TECHNOLOGY',
        'invest'  => 'INVESTMENT',
    /*
     Footer
    */
            'co'  => 'AMIG Capital LLC',




];
