<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'fname'  => 'First Name',
        'lname'  => 'Last Name',
        'email'  => 'Email Address',
        'mobile'  => 'Mobile',
        'message'  => 'Message',
        'send'  => 'Send',
        'contact'  => 'Contact Form',
        'contact1'  => 'We are here ready to answer you question.',


    ],



];
