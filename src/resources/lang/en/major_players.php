<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '

        <h2 class<h2 class="SectionSubTitle">WHO ARE THE MAJOR PLAYERS</h2>
        <p>
            Over the last few years, the Foreign Exchange market has expanded from one where
            banks would execute transactions between themselves to one in which many other kinds
            of financial institution participate. These include brokers and market-makers, non-financial
            corporations, investment firms, pension funds and hedge funds.
        </p>
        <p>
            Its focus has broadened from servicing importers and exporters to handling the vast amounts
            of overseas investment and other capital flows that currently take place. Lately, Foreign Exchange
            day trading has become increasingly popular, with various firms offering trading facilities to the small investor.
        </p>
        <p>
            Foreign Exchange is an \'over-the-counter\' (OTC) market, which means that there is no central exchange and clearing
            house where orders are matched. Geographical trading \'centres\' exist around the world, however, and are (in order
            of importance): London, New York, Tokyo, Singapore, Frankfurt, Geneva & Zurich, Paris and Hong Kong. Essentially,
            Foreign Exchange deals are made between participants on the basis of trust and reputation to deliver on an agreement.
            In the case of banks trading with one another, they do so solely on that basis. In the retail market, customers demand
            a written, legally accepted contract between themselves and their broker in exchange for a deposit of funds on which
            basis the customer may trade. <br>
            Some market participants may be involved in the \'goods\' market, conducting international transactions for the purchase
            or sale of merchandise. Some may be engaged in \'direct investment\' in plant and equipment or may be in the \'money market\',
            trading short-term debt instruments internationally. The various investors, hedgers and speculators may be focused on any
            time period from a few minutes to several years. But whether official or private, and whether their motive be investment,
            hedging, speculation, arbitrage, paying for imports or seeking to influence the rate, they are all part of the aggregate demand
            for, and supply of the currencies involved - they all play a role in determining the exchange rate at that moment.
        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">THE WORLD BANKS OF FOREX TRADING</h2>
        <p>
            <strong>Bank of Canada (Canada)</strong>  <br>
            The Bank of Canada is the nation\'s Central Bank. It is not a commercial bank and does not offer banking services to the public.
            Rather, it is responsible for Canada\'s monetary policy, bank notes, financial system and fund management. Its principal role,
            as defined in the Bank of Canada Act, is \'to promote the economic and financial welfare of Canada\'.
        </p>
        <p>
            <strong>Bank of England (United Kingdom)</strong>  <br>
            The Bank\'s roles and functions have evolved and changed over its three-hundred-year history. Since its foundation, it has been the Government\'s banker and, since the late 18th century, it has generally been a banker to the banking system - the bankers\' bank. As well as providing banking services to its customers, the Bank of England manages the UK\'s Foreign Exchange and gold reserves. <br>
            The Bank has two core purposes - monetary and financial stability. The Bank is perhaps most visible to the general public through its bank notes and, more recently, its interest rate decisions. The Bank has had a monopoly on the issue of bank notes in England and Wales since the early 20th century. But it is only since 1997 that the Bank has had statutory responsibility for setting the UK\'s official interest rate.
        </p>
        <p>
            <strong>Bank of Japan (Japan)</strong>  <br>
            The Bank of Japan is the Central Bank of Japan. It is an established juridical person based on the Bank of Japan Law (hereafter the Law) and is not a government agency or a private corporation. The Law sets the Bank\'s objectives as \'to issue bank notes and to carry out currency and monetary control\' and \'to ensure smooth settlement of funds among banks and other financial institutions, thereby contributing to the maintenance of a well-ordered financial system\'. <b>
                The Law also stipulates the Bank\'s principle of currency and monetary control, as follows: \'Its aim shall be currency and monetary control through the pursuit of price stability, contributing to the sound development of the national economy\'.
        </p>
        <p>
            <strong>Bank of Mexico (Mexico)</strong>  <br>
            The Bank of Mexico is the country\'s Central Bank, constitutionally independent as to function and administration. Its purpose is to provide the economy of Mexico with its national currency. In so doing, its main objective is to try to stabilise the spending power of the currency. Additionally, it is up to the Bank to promote the healthy development of the financial system and to ensure the efficient functioning of the country\'s payment systems.
        </p>
        <p>
            <strong>European Central Bank (European Union)<strong>  <br>
                    The ECB is the Central Bank for Europe\'s single currency, the Euro. The ECB’s main task is to maintain the Euro\'s purchasing power and thus price stability in the Euro Area. The Euro Area comprises the 18 European Union countries that have introduced the Euro since 1999.
        </p>
        <p>
            <strong>Federal Reserve (United States)</strong>  <br>
            The Federal Open Market Committee (FOMC) sets monetary policy to help promote national economic goals. The New York Fed\'s President is the only regional Bank President with a permanent vote and is traditionally selected as its Vice-Chairman. Other presidents serve one-year terms on a rotating basis.
        </p>
        <p>
            <strong>Reserve Bank of Australia</strong>  <br>
            The Reserve Bank of Australia\'s (RBA) main responsibility is monetary policy. Policy decisions are made by the Reserve Bank Board, with the objective of achieving low and stable inflation over the medium term. Its other major roles are maintaining the stability of the financial system and promoting the safety and efficiency of the payment system. The Bank is an active participant in financial markets, manages Australia\'s foreign reserves, issues Australian currency notes and serves as banker to the Australian Government. The information provided by the Reserve Bank includes statistics - for example, on interest rates, exchange rates and money and credit growth - and a range of publications about its operations and research.
        </p>
        <p>
            <strong>Reserve Bank of New Zealand (New Zealand)</strong>  <br>
            The Reserve Bank of New Zealand is the country\'s Central Bank. Its overall purpose is to maintain the stability and efficiency of the financial system. It does so in five ways:<br>
            Operating monetary policy so as to achieve and maintain price stability. <br>
            Promoting the functioning of a sound and efficient financial system. <br>
            Meeting the currency needs of the public.<br>
            Overseeing and operating efficient payment systems.<br>
            Providing effective support services to the Bank.<br>
        </p>
        <p>
            <strong>Swiss National Bank (Switzerland)</strong>  <br>
            The Swiss National Bank conducts the country’s monetary policy as an independent Central Bank. It is obliged by the Constitution and by statute to act in accordance with the interests of the country as a whole. Its primary goal is to ensure price stability, while taking due account of economic developments. In so doing, it creates an appropriate environment for economic growth. The Law also stipulates the Bank\'s principle of currency and monetary control as follows: \'Its aim shall be currency and monetary control through the pursuit of price stability, contributing to the sound development of the national economy\'.
        </p>="SectionSubTitle">INTRODUCING BROKERS & AFFILIATES</h2>

        <p>
            Do you already have a business team and are you searching for more potential products? Are you a broker
            offering clients online trading services on your MetaTrader-based trading platform?
        </p>

        <p>
            <a href="/contact-us" class="item">Contact Us.</a> <strong>We will connect with you as soon as possible</strong>.
        </p>




        ',

    ],

];
