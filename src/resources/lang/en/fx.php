<?php

return [




    'layouts'  => [

        'title' => 'AMIG Forex & Crypto Markets - ',

        'header'  => [
            'open'  => 'Open Account',
            'login'  => 'Client Login',
            'logotext'  => 'AMIG Fx',
        ],

        'footer'  => [
            'menu'  => [
                'about'  => 'About',
                'account'  => 'Account Types',
                'register'  => 'Open Account',
                'special'  => 'Special Promotions',
                'deposit'  => 'Deposit Options',
                'withdraw'  => 'Withdraw Options',
                'market'  => 'Market Tools',
                'economic'  => 'Economic Calendar',
                'daily'  => 'Daily FX Bulletin',
                'education'  => 'Education',
                'whatis'  => 'What is Forex',
                'fxtrading'  => 'FX trading',
                'fundamental'  => 'Fundamental',
                'technical'  => 'Technical',
                'major'  => 'Major players',
                'partnership'  => 'Partnership',
                'partnering'  => 'Partnering',
                'ib'  => 'IB',
                'white'  => 'White Label',
                'platforms'  => 'Platforms',
                'mt5'  => 'Meta Trader 5',
                'mmt5'  => 'Mobile MT5',
                'contactus'  => 'Contact Us',
            ],
            'co'  => 'AMIG Fx',
            'riskwarning'  => 'RISK WARNING! All financial products traded on margin carry a high degree of risk to your capital.
                       They may not be suited to all investors and you can lose more than your initial deposit. Please
                       ensure that you fully understand the risks. We do not accept residents of the United States, Canada & Turkey.',
        ],

        'menu_main'  => [
            'about'  => 'About',
            'account'  => 'Account Types',
            'register'  => 'Open Account',
            'special'  => 'Special Promotions',
            'deposit'  => 'Deposit Options',
            'withdraw'  => 'Withdraw Options',
            'market'  => 'Market Tools',
            'economic'  => 'Economic Calendar',
            'daily'  => 'Daily FX Bulletin',
            'education'  => 'Education',
            'whatis'  => 'What is Forex',
            'fxtrading'  => 'FX trading',
            'fundamental'  => 'Fundamental',
            'technical'  => 'Technical',
            'major'  => 'Major players',
            'partnership'  => 'Partnership',
            'partnering'  => 'Partnering',
            'ib'  => 'IB',
            'white'  => 'White Label',
            'platforms'  => 'Platforms',
            'mt5'  => 'Meta Trader 5',
            'mmt5'  => 'Mobile MT5',
            'contactus'  => 'Contact Us',
            ],

        'slider'  => [
            'want'=> 'Want',
            'more'=> 'MORE',
            'info'=> 'INFO',
            'previous'  => 'Previous',
            'next'  => 'Next',
            'getstartedbttn'  => 'Get Started',
            'agree'  => 'I would like to receive upcoming offers',
            'fname'  => 'First Name',
            'lname'  => 'Last Name',
            'email'  => 'Email Address',
            'mobile'  => 'Mobile',
            'message'  => 'Message',
            'send'  => 'Send',
            'contact'  => 'Contact Form',
            'contact1'  => 'We are here ready to answer you question.',
        ],
    ],

    'controller'  => [
        'title'  => [
            'home'  => 'Home',
            'whitelabel'  => 'White Label',
            'withdrawoptions'  => 'Withdraw Option',
            'termofuse'  => 'Terms of Use',
            'whatisforex'  => 'What is Forex',
            'specialpro'  => 'Special Promotions',
            'technicalan'  => 'Technical Analysis',
            'mobilemt5'  => 'Mobile Meta Trader 5',
            'metatrader5'  => 'Meta Trader 5',
            'aboutus'  => 'About Us',
            'accounttypes'  => 'Account Types',
            'advantages'  => 'Advantages',
            'Campaigns'  => 'Campaigns',
            'dailyfx'  => 'Daily Fx Bulletin',
            'connect'  => 'Contact Us',
            'companyprofile'  => 'Company Profile',
            'depositoption'  => 'Deposit Option',
            'calendar'  => 'Economic Calendar',
            'edu'  => 'Education',
            'funal'  => 'Fundamental Analysis',
            'adfxtrading'  => 'Advantages of FX trading',
            'ibroker'  => 'Introducing Brokers',
            'majorplayer'  => 'Major Players',
        ],
    ],
];
