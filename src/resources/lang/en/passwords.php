<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'reseth' => "Reset Password.",
    'resetdesc' => "Reset you password.",
    'resetbttn' => "Reset.",
    'email' => "Email Address.",
    'pwd' => "Password.",
    'pwdconf' => "Confirm Password.",
    'verifyemail' => "Verify Your Email Address.",
    'linksent' => "A fresh verification link has been sent to your email address.",
    'checkemail' => "Before proceeding, please check your email for a verification link.",
    'didnotreceive' => "If you did not receive the email",
    'requestanother' => "click here to request another",
];
