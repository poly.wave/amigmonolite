<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title' => 'AMIG Member Authorization',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login'  => 'LOGIN',
    'loginbttn'  => 'Login',
    'logindesc'  => 'Sign In to your account',
    'forgotpwd'  => 'Forgot your password?',
    'regbttn'  => 'Register',
    'regnowbttn'  => 'Register Now!',
    'signup'  => 'Sign Up',
    'singupdesc'  => 'If you dont have an account Just Register.',

    'regform'  => 'Register form',
    'loginp'  => 'LOGIN PAGE',
    'loginpdesc'  => 'If you have account just login.',
    'newacc'  => 'Create your account',
    'createacc'  => 'Create Account',

    'emailaddress'  => 'E-mail Address',
    'password'  => 'Password',
    'remember'  => 'Remember me',

];
