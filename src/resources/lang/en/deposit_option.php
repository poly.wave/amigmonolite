<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '

        <h2 class="SectionSubTitle">Choose your deposit method</h2>
        <p>
            For your ease and convenience we offer multiple deposit methods for you to fund your account.
        </p>
        <p>
            Choose from <strong>Bank Wire transfer</strong>, <strong>Credit Card</strong> or an array of other options:
        </p>
        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Deposit<br>Fees</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #616161;">Estimated<br>Time</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">Bank Wire Transfer</div>
                    <div class="td">None</div>
                    <div class="td">1-5 working days</div>
                </div>
                <div class="tr">
                    <div class="td">VISA - MASTER CARDS</div>
                    <div class="td">None</div>
                    <div class="td">Instantly to 2 hrs</div>
                </div>
                <div class="tr">
                    <div class="td">BITCOIN</div>
                    <div class="td">3%</div>
                    <div class="td">Instantly to 2 hrs
                        <a href="https://cex.io/r/1/up120172948/1"> Click to buy</a>

                </div>
            </div>

        </div>'
    ],

];
