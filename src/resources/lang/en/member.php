<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Member Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header'  => [
        'title'  => 'AMIG Members Area',
        'account'  => 'ACCOUNT',
        'settings'  => 'SETTINGS',
        'logout'  => 'LOGOUT',
        'lang'  => [
            'en'  => 'English',
            'ru'  => 'Русский',
            'tr'  => 'Türkçe',
        ],
    ],

    'footer'  => [
        'company_fx'  => 'AMIG Forex',
        'company'  => 'AMIG',
        'powered_by'  => 'Powered by',
    ],

    'sidebar'  => [
        'balance'  => 'Balance:',
    ],

    'main'  => [
        'title'  => 'MAIN',
        'balance'  => 'BALANCE',
        'stat'  => 'Common Statistic:',
        'withdrawals'  => 'WITHDRAWALS',
        'earnings'  => 'EARNINGS',
        'deposits'  => 'DEPOSITS',
    ],
    'trading'  => [
        'title'  => 'Trading',
        'trdacc'  => 'Trading account list',
        'account'  => 'MT ACCOUNT',
        'platform'  => 'PLATFORM',
        'leverage'  => 'LEVERAGE',
        'mtaccount'  => 'MT ACCOUNT',
        'pwd'  => 'PASSWORD',
        'currency'  => 'CURRENCY',
    ],
    'deposit'  => [
        'title'  => 'Deposits',
        'banktransfer'  => 'Company Bank Accounts',
        'bycreditcard'  => 'By Credit Card',
        'cryptocoins'  => 'Company Cryptocurrecy Wallets',
        'bank_transfer' => [
            'title'  => 'Bank Accounts',
            'bank_name'  => 'BANK NAME',
            'accunt_name'  => 'ACCOUNT NAME',
            'iban'  => 'IBAN',
            'swift'  => 'SWIFT Code',
            'bank_address'  => 'BANK ADDRESS',
            'currency'  => 'CURRENCY',
            ],
        'credit_card' => [
            'well'  => 'Well done!',
            'wrong'  => 'Something went wrong!',
            'complete_error' => 'All fields are require to complete',
            'ctitle'  => 'Full name (on the card)',
            'fullname'  => 'NAME SURNAME',
            'cardno'  => 'Card number',
            'cardnumber'  => 'YOUR CARD NUMBER',
            'mm'  => 'MM',
            'yy'  => 'YY',
            'exp'  => 'Expiration',
            'cvv'  => 'CVV',
            'confirm'  => 'Confirm',
            ],
        'crypto_coins' => [
            'title'  => 'Company Crypto Coin Addresses',
            'coin_name'  => 'Cryptocurrency',
            'coin_wallet'  => 'Wallet address',
        ],

    ],
    'withdrawal'  => [
        'title'  => 'WITHDRAWAL',
        'purpose'  => 'Comment',
        'purposelable'  => 'Comment',
        'withamount'  => 'The amount you want to withdraw',
        'withamountlable'  => 'Amount',
        'withcurr'  => 'Preferred currency for withdrawal',
        'withtype'  => 'Withdrawal type',
        'well'  => 'Well done!',
        'wrong'  => 'Something went wrong!',
        'reqsucc'  => 'Your request has send successfully',
        'create'  => 'CREATE',

    ],
    'report'  => [
        'title'  => 'REPORT',
        'deposit'  => 'REPORT',
        'earning'  => 'Earnings',
        'withdrawal'  => 'Withdrawals',
        'deposits'  => [
            'title'  => 'DEPOSIT HISTORY',
            'name'  => 'NAME',
            'amount'  => 'AMOUNT',
            'date_time'  => 'Date and Time',
            'currency'  => 'Currency',
            'approve'  => 'STATUS',
        ],
        'earnings'  => [
            'title'  => 'EARNING HISTORY',
            'name'  => 'NAME',
            'amount'  => 'AMOUNT',
            'currency'  => 'Currency',
            'date_time'  => 'Date and Time',
        ],
        'withdrawals'  => [
            'title'  => 'WITHDRAWAL HISTORY',
            'name'  => 'NAME',
            'amount'  => 'AMOUNT',
            'currency'  => 'Currency',
            'date_time'  => 'Date and Time',
            'type'  => 'WITHDRAWAL TYPE',
            'approve'  => 'STATUS',
        ],

    ],
    'profile'  => [
        'title'  => 'PROFILE',
        'personal_information'  => [
            'avatar'  => 'Change Avatar',
            'title'  => 'Personal Information',
            'email'  => 'E-mail Address',
            'emailpl'  => 'e.g. example@example.com',
            'name'  => 'Your Name',
            'namepl'  => 'First Name',
            'sname'  => 'Your Surname',
            'snamepl'  => 'Last Name',
            'dofb'  => 'Day Of Birth',
            'dofbpl'  => 'dd.mm.yyyy',
            'phone'  => 'Phone Number',
            'phonepl'  => 'e.g. +1 (111) 111 11 11',
            'gender'  => 'Gender',
            'preflang'  => 'Preferred Language',
            'prefcurr'  => 'Preferred Currency',
            'country'  => 'Country',
            'region'  => 'Region',
            'city'  => 'City',
            'resident'  => 'Resident Address',
            'well'  => 'Well Done!',
            'updated'  => 'Your changes was successfully updated',
            'button_update'  => 'Update',
            'two_auth'=>'Two authentication',
        ],
        'passport'  => [
            'title'  => 'Passport',
            'passport_id'  => 'Passport Id',
            'date_of_issue'  => 'Date Of Issue',
            'date_of_expiry'  => 'Date Of Expiry',
            'issuring_authority'  => 'Issuring Authority',
            'approve'  => 'Status',
          ],
        'wallet'  => [
            'title'  => 'Wallet',
            'banks'  => [
                'title'  => 'Bank Accounts',
                'bankname'  => 'Bank name',
                'address'  => 'Bank address',
                'iban'  => 'IBAN',
                'swift'  => 'swift',
                'currency'  => 'Currency',
                'well_done'  => 'Well Done!',
                'created'  => 'New bank was created successfully',
                'bank_details'  => 'Your Bank Details',
                'button_add'  => 'Add',
            ],
            'cards'  => [
                'title'  => 'Credit Cards',
                'ctitle'  => 'Full name (on the card)',
                'fullname'  => 'NAME SURNAME',
                'cardno'  => 'Card number',
                'cardnopl'  => 'e.g. 1234-5678-9012-3456',
                'mm'  => 'MM',
                'mmm'  => 'Month',
                'yy'  => 'YY',
                'yyy'  => 'Year',
                'currency'  => 'Currency',
                'help'  => 'Three-digits code on the back of your card',
                'cvv'  => 'CVV',
                'well'  => 'Well done!',
                'well1'  => 'Your request has send successfully',
                'wrong'  => 'Something went wrong!',
                'card_created'  => 'New credit card was created successfully',
                'button_add'  => 'ADD',
                'creditcard'  => 'Your Credit Cards',
            ],
            'crypto_wallets'  => [
                'title'  => 'Crypto Wallets',
                'crtitle'  => 'Company Crypto Coin Addresses',
                'crwallet'  => 'Wallet address',
                'crpref'  => 'Preferred Coin',
                'crname'  => 'Coin Name',
                'well'  => 'Well done!',
                'crypto_wallet'  => 'Your Crypto Wallets',
                'wallet_created'  => 'New crypto coin wallet was created successfully',
                'wrong'  => 'Something went wrong!',
                'button_add'  => 'ADD',

                ],
        ],
        'reset_password'  => [
            'title'  => 'Reset Password',
            'pwdold'  => 'Write current password',
            'pwdold1'  => 'Old password',
            'pwdnew'  => 'Write new password',
            'pwdnew1'  => 'New password',
            'pwdnew2'  => 'Confirm new password',
            'incorrect'  => 'Incorrect old password',
            'well'  => 'Well Done!',
            'updated'  => 'Your password was successfully updated',
            'wrong'  => 'Something went wrong!',
            'button_update'  => 'Update',
        ],
        'files'  => [
            'title'  => 'Files',
            'well'  => 'Well Done!',
            'updated'  => 'Your files was successfully uploaded',
            'button_update'  => 'Upload',
        ],
    ],


      'usersettings'  => 'User Settings',


    ];
