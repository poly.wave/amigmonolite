<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [

        '1'=> '
         <h2 class="SectionSubTitle">INTRODUCTION TO THE FOREIGN EXCHANGE MARKETS</h2>
        <p>
            Although the foreign exchange market is the largest traded market in the world,
            its reach in the retail sector pales in comparison to the equity and fixed Income markets.
            This is in large part due to a general lack of awareness of <strong> FX </strong> in the investor community,
            along with a lack of understanding of how and why currencies move. Adding to the mystique
            of this market is the fact that there is no physical central exchange akin to the NYSE or the ASX.
            The FX markets operate on a 24-hour basis.
        </p>
        <p>
            Traditionally, access to the FX market was limited to the bank community, which traded large blocks
            of currencies for commercial, hedging or speculative purposes. The creation of firms like Amig Capital has
            opened the door of forex trading to such institutions as fund and money managers, as well as to the individual
            retail trader. This sector of the market has grown exponentially over the past several years.
        </p>

        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">WHAT IS FX TRADING?</h2>
        <p>
            In an FX transaction, one currency is sold in exchange for another one. The rate expresses the
            relative value between the two currencies. Currencies are normally identified by a three-digit ‘Swift’
            code. For instance, EUR = the Euro, USD = the US Dollar, CHF = the Swiss Franc and so on. A full list
            of codes can be found here. A EUR/USD rate of 1.5000 means that EUR 1 is worth USD 1.5.
        </p>
        <p>
            Sometimes, EUR/USD is referred to as a currency pair. The rate can be inverted. So a EUR/USD rate
            of 1.5000 is the same as a USD/EUR rate of 0.6666. In other words, USD 1 is worth EUR 0.6666. The
            market convention is that most currencies tend to be quoted against the dollar, but there are
            notable exceptions, such as with the EUR/USD already mentioned, GBP/USD (UK Pound Sterling) and
            AUD/USD (the Australian Dollar). This is not as confusing as it may sound.
        </p>


        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">FOREIGN CURRENCY SYMBOLS</h2>
        <p>

            Currencies, like equities, have their own symbols that distinguish one from another. Since currencies
            are quoted in terms of the value of one against the value of another, a currency pair includes the
            \'name\' for both currencies, separated by a forward slash (\'/\'). The \'name\' is a three-letter acronym.
            The first two letters are, in most cases, reserved for identification of the country. The last letter is the
            first letter of the unit of currency for that country.  <br>
            For example, <br>
            USD = United States Dollar <br>
            GBP = Great Britain Pound <br>
            JPY = Japanese Yen <br>
            CAD = Canadian Dollar <br>
            CHF = Confederatio Helvetica (Latin for Swiss Confederation) Franc <br>
            NZD = New Zealand Dollar <br>
            AUD = Australian Dollar <br>
            NOK = Norwegian Krona <br>
            SEK = Swedish Krona <br>
        </p>
        <p>
            Since the European Euro has no specific country attached to it, it goes simply by the acronym EUR.
            By combining one currency (EUR) with another (USD), you create a currency pair - EUR/USD.
        </p>
        <br>

        <h2 class="SectionSubTitle">BASE AND COUNTER-CURRENCIES</h2>
        <p>
            One currency in a currency pair is always dominant. This is called the base currency. The base
            currency is identified as the first currency in a currency pair. It is also the currency that remains
            constant when determining a currency pair\'s price.
        </p>
        <p>
            The Euro is the dominant base currency against all other global currencies. As a result, currency
            pairs against the EUR will be identified as EUR/USD, EUR/GBP, EUR/CHF, EUR/JPY, EUR/CAD, etc. All
            have the EUR acronym as the first in the sequence.
        </p>
        <p>
            The British Pound is next in the hierarchy of currency name domination. The major currency pairs
            versus the GBP would therefore be identified as GBP/USD, GBP/CHF, GBP/JPY, GBP/CAD and so on.
            Apart from EUR/GBP, expect to see GBP as the first currency in a currency pair.
        </p>
        <p>
            The USD is the next most dominant base currency. USD/CAD, USD/JPY, USD/CHF would be the
            normal currency pair convention for the major currencies. Since the EUR and the GBP are more
            dominant in terms of base currencies, the dollar is quoted as EUR/USD and GBP/USD. Knowing the
            base currency is important, as it determines the values of currencies (notional or real) exchanged
            when a Foreign Exchange deal is transacted. The counter-currency is the second currency in a currency pair notation.  <br>
        </p>
        <br>

        <h2 class="SectionSubTitle">FX MARKET PARTICIPANTS</h2>
        <p>
            There are many different types of participant in the FX market, and they are frequently looking for
            very different outcomes when they trade. This is why although FX is often described as a ‘zero-sum’
            game – what one investor makes is equal, in theory, to what another has lost – there are numerous
            opportunities to make money. FX can be thought of as a pie from which everyone can have a decent meal.
        </p>
        <p>
            Traditionally, banks have been the main participants in the FX market. They still remain the largest
            players in terms of market share, but transparency has made the FX market far more democratic.
            Now, virtually everyone has access to the same, extremely narrow prices that are quoted in the
            inter-bank market. So, banks remain the main players in the FX market, but a new breed of market
            maker, such as hedge fund and commodity trading advisors, has emerged over the past decade.
        </p>
        <p>
            Central Banks can also play an important role in the FX market, while international corporations
            have a natural interest in trading because of their exposure to FX risk.
        </p>
        <p>
            Retail FX has expanded rapidly over the past decade and while precise figures are hard to come by,
            this sector is believed to represent as much as 20% of the FX market.
        </p>

    </div>
        ',



        ],

];
