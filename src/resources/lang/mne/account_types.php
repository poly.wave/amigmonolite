<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'steps'  => '4 Step for open account',
        'step1'  => 'STEP 01',
        'step1desc'  => 'Submit your details',
        'step2'  => 'STEP 02',
        'step2desc'  => 'Upload your ID',
        'step3'  => 'STEP 03',
        'step3desc'  => 'Fund your account',
        'step4'  => 'STEP 04',
        'step4desc'  => 'Start trading',
        'register'  => 'Register',


        'minacc'  => 'Mini Account',
        'stnacc'  => 'Standard Account',
        'mindep'  => 'Minimum deposit',
        'maxbal'  => 'Maximum balance',
        'sprdfrom'  => 'Spreads from',
        'minlot'  => 'Minimum lot size',
        'stnlot'  => 'Standard lot size',
        'acctype'  => 'Account Type',
        'leverage'  => 'Leverage',
        'minacctype'  => 'Individual Account',
        'stnacctype'  => 'Individual Account, Joint Account, Corporate Account',
        'vipacc'  => 'VIP Account',
        'proacc'  => 'Pro-ECN Account',
        'vipacctype'  => 'Individual Account, Joint Account, Corporate Account',
        'proacctype'  => 'Individual Account, Joint Account, Corporate Account',


        'requiredoc'  => 'Required Documents for account',
        'indvacc'  => 'Individual Account',
        'indvacc1'  => '1. Passport or government-issued ID (copies of both front and back).',
        'indvacc2'  => '2. Proof of residential address (bills dated within the last 3 months, such as phone bills, credit card bills or insurance bills).',
        'jointacc'  => 'Joint Account',
        'jointacc1'  => '1. Passport or government-issued ID (copies of both front and back).',
        'jointacc2'  => '2. Proof of residential address (bills dated within the last 3 months, such as phone bills, credit card bills or insurance bills).',
        'jointacc3'  => '3. The Second Passport or government-issued ID (copies of both front and back).',
        'jointacc4'  => '4. The Second Proof of residential address (bills dated within the last 3 months, such as phone bills, credit card bills or insurance bills).',
        'coacc'  => 'Corporate Account',
        'coacc1'  => '1. Certificate of Registration (copy)',
        'coacc2'  => '2. Proof of company address (bills dated within the last 3 months, such as phone bills, bank statement or utility bills)',
        'coacc3'  => '3. List of directors and greater-than-10% shareholders.',
        'coacc4'  => '4. Certified passport or certified government-issued ID, and proof of residential address for each of the director and shareholder listed in section 3.',
        'coacc5'  => '5. In the case of Trust, please also upload a certified copy of the trust deed.',



    ],

];
