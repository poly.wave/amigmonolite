<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
         <p>
            Are you a regulated financial institution authorized to hold clients\' funds ? Are you thinking about expanding your product portfolio and offering your clients online FX and CFDs trading with the security
and quality ?
        </p>
        <p>
            We provide Limited and Full White Label solutions to regulated financial institutions around the world. Your clients would also benefit from our top-class security, liquidity pool, IT infrastructure, organization, and knowledge, as well as many other assets, all of which makes us the largest online FX and CFD trading broker. Our Limited White Label partnership program is suitable for institutions choosing not to handle any administrative work, while our Full White Label program caters to institutions happy to be responsible for back-office work. In either case, you will be able to offer your clients highly professional trading services with user-friendly platforms carrying the name of your institution. You can enjoy our great IT and marketing support, and grow your revenue by charging your clients a commission and/or higher spreads. Most importantly, your clients will get a highly competitive and professional online FX and CFD trading service that they can use with confidence.
        </p>

        <p>
            <a href="/contact-us" class="item">Contact Us.</a> <strong>We will connect with you as soon as possible</strong>.
        </p>


        ',

    ],

];
