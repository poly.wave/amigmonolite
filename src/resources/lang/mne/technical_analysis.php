<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
        <h2 class="SectionSubTitle">TECHNICAL ANALYSIS</h2>
        <p>
            The concept of trading FX is simple, once it is realised that a currency is a commodity whose
            value fluctuates against another currency. By buying (or selling) a currency, FX Traders aim to earn a
            profit from the movement in the FX rate. The beauty of FX is that the cost of trading is so low. This means
            that trades can be transacted for the extreme short term, literally seconds, as well as for a longer duration.
        </p>
        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">WHAT TO LOOK FOR IN TECHNICALS</h2>
        <p>
            <strong>Find the Trend</strong> <br>
            One of the first things you\'ll ever hear in technical analysis is the following
            motto: \'the trend is your friend\'. Finding the prevailing trend will help you become
            aware of the overall market direction and offer you better visibility - especially when
            shorter-term movements tend to clutter the picture. Weekly and monthly charts are more
            ideally suited for identifying longer-term trends. Once you have found the overall trend,
            you could select the trend of the time horizon in which you wish to trade. Thus, you could
            effectively buy on the dips during rising trends, and sell the rallies during downward trends.
        </p>
        <p>
            <strong>Support & Resistance</strong> <br>
            Support and resistance levels are points where a chart experiences recurring upward or downward pressure. A support
            level is usually the low point in any chart pattern (hourly, weekly or annually), whereas a resistance level is the high,
            or peak point of the pattern. These points are identified as support and resistance when they show a tendency to reappear.
            It is best to buy/sell near support/resistance levels that are unlikely to be broken.<br>
            Once these levels are broken, they tend to become the opposite obstacle. Thus, in a rising market, a resistance level that
            is broken could serve as a support for the upward trend; whereas in a falling market, once a support level is broken, it
            could turn into a resistance.
        </p>

        <p>
            <strong>Lines & Channels</strong> <br>
            Trend lines are simple, yet helpful tools to confirm the direction of market trends. An upward, straight line is drawn by connecting at least two successive lows. Naturally, the second point must be higher than the first. The continuation of the line helps determine the path along which the market will move. An upward trend is a concrete method of identifying support lines/levels. Conversely, downward lines are charted by connecting two points or more. The validity of a trading line is partly related to the number of connection points. Yet it\'s worth mentioning that points must not be too close together. A channel is defined as the price path drawn by two parallel trend lines. The lines serve as an upward, downward or straight corridor for the price. A familiar property of a channel for a connecting point of a trend line is to lie between the two connecting points of its opposite line.
        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">THE BASIC THEORIES</h2>
        <p>
            <strong>Charts</strong> <br>
            There are three main types of chart used in technical analysis: <br>
            Line Chart:
            The line chart is a graphical depiction of the exchange rate history of a currency pair over time. The line is constructed by connecting daily closing prices. <br>
            Bar Chart:
            The bar chart is a depiction of the price performance of a currency pair, made up of vertical bars at set intra-day time intervals (e.g. every 30 minutes). Each bar has 4 \'hooks\', representing the opening, closing, high and low (OCHL) exchange rates for the time interval. <br>
            Candlestick Chart:
            The candlestick chart is a variant of the bar chart, except that the candlestick chart depicts OCHL prices as \'candlesticks\' with a \'wick\' at each end. When the opening rate is higher than the closing rate the candlestick is \'solid\'. When the closing rate exceeds the opening rate, the candlestick is \'hollow\'. <br>
        </p>
        <p>
            <strong>Support & Resistance Levels</strong> <br>
            One use of technical analysis is to derive \'support\' and \'resistance\' levels. The underlying idea is that the market will tend to trade above its support levels and below its resistance levels. A support level indicates a specific price level that the currency will have difficulties crossing below. If the price repeatedly fails to move below this particular point, a straight-line pattern will appear. <br>
            Resistance levels, on the other hand, indicate a specific price level that the currency will have difficulties crossing above. Recurring failure of the price to move above this point will produce a straight-line pattern. <br>
            If a support or resistance level is broken, the market is expected to follow through in that direction. These levels are determined through analysis of the chart and by assessment of where the market has encountered unbroken support or resistance in the past. <br>
        </p>
        <p>
            <strong>Moving Averages</strong> <br>
            Moving averages provide another tool for tracking price trends. A moving average is, in its simplest form, an average of prices that rolls over time. A 10-day moving average is calculated by adding the last 10 days’ closing prices and then dividing them by 10. On the following day, the oldest price is dropped and the new day’s closing price is added instead; now these 10 prices are divided by 10. In this way, the average \'moves\' each day. <br>
            Moving averages provide a more mechanical approach to entering or exiting the market. To help identify entry and exit points, moving averages are frequently superimposed onto bar charts. When the market closes above the moving average, it is generally interpreted as a buy signal. It is, in the same way, considered a sell signal when the market closes below the moving average. Some traders prefer to see the moving average line actually change direction before accepting it as a buy or sell signal.<br>
            The sensitivity of a moving average line and the number of buy and sell signals it produces are directly correlated with the chosen time period for the moving average. A 5-day moving average will be more sensitive and will prompt more buy and sell signals than a 20-day moving average. If the average is too sensitive, traders may find themselves jumping in and out of the market too often. On the other hand, if the moving average is not sensitive enough, traders risk missing opportunities by identifying buy and sell signals too late.<br>
            Moving averages can be extremely useful tools for the technical trader.<br>

        </p>
        <p>
            <strong>Trend Line</strong> <br>
            A trend line helps identify the trend, as well as potential areas of support and resistance. A trend line is a straight line that connects at least two important peaks or troughs in the price action of an underlying tradable. No other price action must break the trend line between the two points. In this way, a trend line marks a support or a resistance area where the price has turned (peaks and troughs) and has not been violated. The longer a trend line, the more valid it is, especially if price has touched the line several times without penetration. <br>
            The penetration of a long-term trend line may be an indication that a reversal of the trend is about to occur. However, there is no guarantee that this will happen. As with all indicators of a price trend reversal, there is no proven method that predetermines where future prices will go.<br>

        </p>
        <p>
            <strong>Retracements</strong> <br>
            When a market is moving swiftly in a given direction, it may sometimes pull back as market participants take their profits. This phenomenon is known as a retracement. It often presents a good opportunity to re-enter the market at more attractive levels before the underlying trend resumes. <br>
            Using Fibonacci ratios is a common way of measuring retracements.<br>
        </p>',


    ],



];
