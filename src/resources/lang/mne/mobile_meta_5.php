<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mmt'  => ' Mobile Meta Trader 5',
        'mmtintro'  => 'Mobile Trading on MT5',
        'mmtintro1'  => 'Trade from anywhere in the world, anytime, from the convenience of your smartphone or tablet.',
        'mmtintro2'  => 'Super-fast one-click trading ─ all from the palm of your hand.',
        'mmtintro3'  => ' Whether your device of choice is an Android or iOS, with AMIGFX MetaTrader 5 you can trade on the go, anytime, anywhere! Simply download the MT5 app from either the Apple App Store or Google Play and you’re good to go!',
        'ios'  => 'Click the App Store button below to download the MT5 app.',
        'ios1'  => 'Select the AMIGFX server within the MT5 app.',
        'android'  => 'Click the Google Play button below to download the MT5 app',
        'android1'  => 'Select the AMIGFX server within the MT5 app.',


    ],




];
