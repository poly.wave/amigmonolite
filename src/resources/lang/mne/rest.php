<?php

return [

    /*
    |--------------------------------------------------------------------------
    | for rest api requests
    |--------------------------------------------------------------------------
    */

    'error_auth' => 'Invalid Auth key.',
    'success_registr'=>'Successfully created a new account. Please check your email and activate your account',
    'confirm_email'=>'Unable to login. Please verify using link of confirmation email.',
    'logged_success'=>'You have logged in successfully.',
    'incorrect_password'=>'Unable to login. Incorrect password.',
    'email_ntexist'=>"Unable to login. Email doesn't exist.",
    'ntf_email'=>"We can't find a user with that e-mail address.",
    'password_reset'=>"We have e-mailed your password reset link!",
    'not_regions'=>"There are not regions with such data.",
    'not_cities'=>"There are not cities with such data.",
    'not_user'=>"There is no user with such id.",
    'invalid_token'=>"This password reset token is invalid.",
    'rst_password'=>"Your password has been successfully restored.",
    'userinfo_update'=>"That user info has been updated․",
    'incorrect_oldpassword'=>"Incorrect old password.",
    'password_update'=>"Your password was successfully updated.",
     'not_deposits'=>"That user has no deposits.",
    'not_withdrawals'=>"That user has no withdrawals.",
    'not_earnings'=>"That user has no earnings.",
    'ntapproved_deposits'=>"That user has no approved deposits.",
    'ntapproved_withdrawals'=>"That User has no approved withdrawals.",
    'ntinfo_passport'=>"That User has no passport․",
    'nttradinfo_account'=>"That user has no trading account.",

     'create_passport'=>"The passport was successfully created.",
     'create_file'=>"User file created successfully.",
     'nt_file'=>"User has no files.",
     'withdrawal_committed'=>"User Withdrawal successfully was committed.",
     'bank_transfer'=>"Bank Transfer successfuly added.",
     'deposed_created'=>"Deposed created successfuly.",


    'wallet_created'=>"The wallet was successfully created․",
    'not_banktransfer'=>"That User has no bank transfers.",

     'not_creditcards'=>"That User has no credit cards.",

    'not_cryptocurrencies'=>"That User has no crypto currencies.",

     'nt_leverage'=>"There are not leverages.",
     'notoptions_withdrawals'=>"There are not withdrawal types.",
     'not_cryptocoins'=>"There are not crypto coins.",
     'error_service'=>"404 this service not available.",
     'success_trading_acc'=>"To complete registration go to your email box and click on the confirmation link.",
     'update_user_earnings'=>"Success",
     'not_crypto_company'=>"There are not crypto companies."

];
