<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => ' <p><strong>Terms of Use</strong></p>


        <p>
            Please read these terms and conditions carefully. By accessing this World Wide Website ("Web Site") and any pages hereof, you are indicating that you have read, acknowledge, and agree to be bound by these Terms of Use. If you do not agree to these Terms of Use, do not access this Web Site. AMIG Capital ("AMIG") reserves the right to change these Terms of Use which you are responsible for regularly reviewing and your continued use of this Web Site constitutes agreement to all such changes.

            </p>
        <div class="divider divider-1"></div>',



    ],


];
