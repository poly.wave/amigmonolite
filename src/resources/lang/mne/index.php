<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Index Language Lines
    |--------------------------------------------------------------------------
    */

    'content'  => [
        'title' => 'START TRADING WITH AMIG',
        'opendemo'  => 'Open Demo Account',
        'openlive'  => 'Open Live Account',
        'leverage'  => 'Leverage',
        '1200'  => '1:200',
        'spreads'  => 'Low Spreads',
        'slippage'  => 'Slippage',
        'currencies'  => 'Currencies',
        'indices'  => 'Indices',
        'commodities'  => 'Commodities',
        'minimum'  => 'Minimum',
        'depositl'  => 'Deposit',
        'fee'  => 'Deposit Fee',
        'support'  => 'Hours Support',
        'whyamig'  => 'WHY CHOOSE AMIG FOREX',
        'asfx'  => 'As your FX broker',
        'trusted'  => 'Trusted',
        'trusted1'  => 'AMIG FOREX being formed for traders by traders.',
        'segregated'  => 'Segregated funds',
        'segregated1'  => 'Clients funds are held in a segregated client account.',
        'quick'  => 'Quick Withdrawals',
        'quick1'  => 'All withdraws are processed within 24-48 hours.',
        'ultra'  => 'Ultra-Fast Execution',
        'ultra1'  => 'Up to 12 times faster execution speed and 10 times latency reduction with optical fibre connections to Interbank Servers in New York & London, allowing ultra-fast one-click trading',
        'negative'  => 'Negative Balance Protection',
        'negative1'  => 'Trade confidently and with the peace of mind that your losses cannot exceed your account balance.',
        'dedicated'  => 'Dedicated Account Manager',
        'dedicated1'  => 'Enjoy old fashioned customer service from an account manager dedicated to your success.',

    ],

];
