<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '
         <h2 class="SectionSubTitle">Withdrawals are quick and easy</h2>

        <p>
            At AMIG Fx, our withdrawal process is quick and easy and all withdrawals are processed within 24 hours.

            You may withdraw your funds at any time straight from your Client Login Area.

            Funds can be withdrawn straight back to your nominated bank account or back to your credit card if you
            used a credit card to deposit. It\'s simply that easy.
        </p>
        <p>
            Choose from <strong>Bank Wire transfer</strong>, <strong>Credit Card</strong> or an array of other options:
        </p>
        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Withdrawal<br>Fees</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Estimated<br>Time</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">Bank Wire Transfer</div>
                    <div class="td">25 USD</div>
                    <div class="td">24-48 hrs</div>
                </div>
                <div class="tr">
                    <div class="td">VISA - MASTER CARDS</div>
                    <div class="td">3.5%</div>
                    <div class="td">24-48 hrs</div>
                </div>
                <div class="tr">
                    <div class="td">BITCOIN</div>
                    <div class="td">3%</div>
                    <div class="td">2-5 hrs</div>
                </div>
            </div>

        </div>
        '
    ],

];
