<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /* Personal info*/
    'email'  => 'E-mail Address',
    'emailpl'  => 'e.g. example@example.com',
    'pwdnew'  => 'Create Your Password',
    'pwdnew1'  => 'Password',
    'pwdnew2'  => 'Confirm Password',
    'name'  => 'Your Name',
    'namepl'  => 'First Name',
    'sname'  => 'Your Last Name',
    'snamepl'  => 'Last Name',
    'dofb'  => 'Day Of Birth',
    'dofbpl'  => 'dd.mm.yyyy',
    'phone'  => 'Phone Number',
    'phonepl'  => 'e.g. +1 (111) 111 11 11',
    'gender'  => 'Gender',
    'preflang'  => 'Preferred Language',
    'prefcurr'  => 'Preferred Currency',
    'country'  => 'Country',
    'region'  => 'Region',
    'city'  => 'City',
    'resident'  => 'Resident Address',
    'agree'  => 'I have read and agree to the',
    'agreename'  => 'Declaration of Commitment and Responsibility',






];
