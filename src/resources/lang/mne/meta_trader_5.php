<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mt'  => 'Meta Trader 5',
        'mtintro'  => 'Introducing the AMIG Fx MetaTrader 5',
        'mtintro1'  => 'More power.  More precision.  More Features.',
        'mtintro2'  => 'Available now at AMIG Fx, download it free today',
        'mtintro3'  => 'The AMIG Fx MT5 delivers everything traders have grown to love about their AMIG Fx MT4 but with a few extra enhancements.  More power.  More precision.  More options.',
        'mtintro4'  => 'MiniWith new added features and technical upgrades, the AMIG Fx MT5 platform will provide you with all of the tools you will need to take on the markets.mum',
        'demoacc'  => 'Open Demo Account',
        'downmtwin'  => 'Download MetaTrader5 (WINDOWS)',
        'downmtmac'  => 'Download MetaTrader5 (MAC OS)',
        'why'  => 'Why choose AMIG Fx MetaTrader 5',
        'why1'  => 'Integrated all-in-one multi-functional FX trading platform ',
        'why2'  => 'Built-in MetaTrader Market – The largest online store of robots & technical indicators ',
        'why3'  => 'Enhanced web trading – trade from any browser, any device ',
        'why4'  => 'Superior copy trading (signals), freelance database and Virtual Hosting ',
        'why5'  => '24-hour expert support from AMIG Fx MT5 specialist ',
        'features'  => 'Features of AMIGFX MetaTrader 5 ',
        'benefits'  => 'Benefits of AMIGFX MetaTrader 5 ',
        'benefits1'  => 'Enhance Productivity ',
        'benefits2'  => 'Advanced Automated Trading ',
        'benefits3'  => 'Increased Accuracy ',


    ],




];
