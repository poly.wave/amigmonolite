<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '

        <h2 class="SectionSubTitle">INTRODUCING BROKERS & AFFILIATES</h2>

        <p>
            Do you already have a business team and are you searching for more potential products? Are you a broker
            offering clients online trading services on your MetaTrader-based trading platform?
        </p>

        <p>
            <a href="/contact-us" class="item">Contact Us.</a> <strong>We will connect with you as soon as possible</strong>.
        </p>




        ',

    ],

];
