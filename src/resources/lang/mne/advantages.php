<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
         <h2 class="SectionSubTitle">PARTNERING WITH AMIG Fx</h2>
        <p>
            <strong>Explore New Business Opportunities Through a Partnership with AMIG Fx</strong>
        </p>
        <p>
            Online Forex and CFD trading represents a tremendous business opportunity for all market participants. You can
            expand your product portfolio quickly and easily and add online Forex and CFD trading services for your
            clients with the help of our years of experience.
        </p>
        <p>
            The other key advantages to partnering with AMIG Fx include：
        </p>
        <p>
            - Highest Rebate & Benefits<br>
            - Convenient & Quick Deposit/Withdrawal<br>
            - Individual Registration for & Commission Paid to Each Rank<br>
            - Customised Commission System & Trading Conditions for Partners<br>
            - Deposit Bonus Promotion<br>
            - Weekly Commission Payment<br>
            - Education & Training Support<br>
            - Powerful Back Office<br>
            - Website Resources<br>
            <br>
        </p>
<p>
            <a href="/contact-us" class="item"><strong>Join Us</strong>.</a> <strong>We will connect with you as soon as possible</strong>.
        </p>



        ',

    ],
];
