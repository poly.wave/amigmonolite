<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
        <h2 class="SectionSubTitle">FUNDAMENTAL ANALYSIS</h2>
        <p>
            <strong>Understanding Fundamental Analysis</strong> <br>
            The two primary approaches to analysing currency markets are fundamental analysis and technical analysis. Fundamentals focus on financial and economic theories, as well as political developments, to determine forces of supply and demand. One clear point of distinction between fundamentals and technicals is that fundamental analysis studies the causes of market movements, while technical analysis studies the effects of market movements.<br>
            Fundamental analysis comprises the examination of macro-economic indicators, asset markets and political considerations when evaluating one nation\'s currency relative to another. Macro-economic indicators include figures such as growth rates, as measured by Gross Domestic Product, interest rates, inflation, unemployment, money supply, Foreign Exchange reserves and productivity. Asset markets comprise stocks, bonds and property. Political considerations affect the level of confidence in a nation\'s government, the climate of stability and level of certainty.<br>
            Sometimes governments stand in the way of market forces affecting their currencies, intervening to keep currencies from deviating markedly from undesired levels. Currency interventions are conducted by Central Banks and usually have a notable, albeit a temporary impact on FX markets. A Central Bank could undertake unilateral purchases/sales of its currency against another currency or engage in a concerted intervention in which it collaborates with other Central Banks for a much more pronounced effect. Alternatively, some countries can manage to move their currencies merely by hinting at or threatening intervention.The following is a list of key US economic indicators: <br>
        </p>
        <p>
            <strong>Current Account</strong> <br>
            The current account is an important part of international trade data, as it is the broadest measure of sales and purchased goods, services, interest payments and unilateral transfers. The trade balance is contained in the current account. In general, a current account deficit can weaken the currency.
        </p>
        <p>
            <strong>Balance of Trade</strong> <br>
            The trade balance reflects the difference between a nation\'s exports and imports of goods. A positive trade balance, or a surplus, occurs when a country\'s exports exceed imports. A negative trade balance, or a deficit, occurs when more goods are imported than exported.<br>
            Trade balances are closely followed by players in Forex because of the influence they can have. It is often used as an assessment of the overall economic activity in a country’s or region’s economy. Export activities not only reflect the competitive position of the country in question, but also the strength of economic activity abroad. Trends in the import activity reflect the strength of domestic economic activity.<br>
            A country that runs a significant trade balance deficit tends generally to have a weak currency. However, this can be offset by substantial financial investment flowing in.<br>
        </p>
        <p>
            <strong>Durable Goods Index</strong> <br>
            Durable goods orders are a measure of the new orders placed with domestic manufacturers for immediate and future delivery of factory hard goods. Monthly percentage changes reflect the rate of change of these orders. <br>
            The durable goods orders index is a major indicator of manufacturing sector trends. Rising durable goods orders are normally associated with stronger economic activity and can lead to higher short-term interest rates, which is usually supportive for a currency.<br>
        </p>
        <p>
            <strong>Gross Domestic Product</strong> <br>
            Gross domestic product (GDP) is the broadest measure of aggregate economic activity available. It is an indicator of the market value of all goods and services produced within a country. GDP is reported quarterly and is followed very closely, as it is the primary indicator of the strength of economic activity.<br>
            The GDP report has three releases: 1) advance release (first); 2) preliminary release (1st revision); and 3) final release (2nd, and last revision). These revisions usually have a substantial impact on the markets.<br>
            A high GDP figure is usually followed by expectations of higher interest rates, which is mostly positive for the currency concerned, at least in the short term, unless there are also inflationary pressures.<br>
            In addition to the GDP figures, there are the GDP deflators, which measure the change in prices in total GDP, as well as for each component. The GDP deflators are another key inflation measure along with the CPI. In contrast to the CPI, the GDP deflators have the advantage of not being a fixed basket of goods and services, which means that changes in consumption patterns or the introduction of new goods and services will be reflected in the deflators.<br>
        </p>
        <p>
            <strong>Housing Starts</strong> <br>
            Housing starts measure initial construction of residential units (houses and flats) each month. Housing starts are closely watched, as they give an indication of the general sentiment in the economy. High construction activity is usually associated with increased economic activity and confidence, and can be predictive of higher short-term interest rates.
        </p>
        <p>
            <strong>Consumer Price Index</strong> <br>
            The Consumer Price Index (CPI) is a measure of inflation. It takes the average level of prices of a fixed basket of goods and services purchased by consumers.<br>
            CPI is a primary inflation indicator because consumer spending accounts for nearly two-thirds of economic activity. A rising CPI is often followed by higher short-term interest rates, which can be supportive for a currency in the short term. However, if inflation becomes a long-term problem, confidence in the currency will eventually be undermined and it will weaken.
        </p>
        <p>
            <strong>Payroll Employment</strong> <br>
            The Payroll Employment report (also known as the Labour report) is currently regarded as the most important of all US economic indicators. It is usually released on the first Friday of the month. The report provides a comprehensive look at the economy and is a measure of the number of people being paid as employees by non-farm business establishments and units of government. Monthly changes in payroll employment reflect the net number of new jobs created or lost during the month. The Payroll Employment report is widely followed as an important indicator of economic activity. <br>
            Large increases in payroll employment are considered signs of strong economic activity that could eventually lead to higher interest rates, which is generally supportive of the currency, at least in the short term. If, however, it is estimated that inflationary pressure is building up, this may undermine the longer-term confidence in the currency.
        </p>
        <p>
            <strong>Producer Price Index</strong> <br>
            The Producer Price Index measures the monthly change in wholesale prices and is broken down by commodity, industry and stage of production. <br>
            The PPI gives an important inflation indication, as it measures price changes in the manufacturing sector - and inflation at the producer level often gets passed straight through to consumers.
        </p>



        ',

    ],





];
