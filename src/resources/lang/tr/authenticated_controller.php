<?php

return [
    /*
    |--------------------------------------------------------------------------
    | USER Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*  */



    /*AuthenticatedController*/


    'main'  => 'ANA',
    'trading'  => 'TİCARET',
    'depositby'  => 'FONLAMA',
    'banktransfer'  => 'Havale/EFT',
    'creditcard'  => 'Kredi Kartı',
    'cryptocoins'  => 'Kriptopara',
    'withdrawal'  => 'ÇEKİM TALİMATI',
    'report'  => 'RAPOR',
    'deposits'  => 'Deposit',
    'earnings'  => 'Kazançlar',
    'withdrawals'  => 'Para çekimleri',
    'profile'  => 'PROFİL',
    'personalinfo'  => 'Kişisel Bilgiler',
    'wallet'  => 'Cüzdan',
    'reset'  => 'Şifreyi yenile',
    'banks'  => 'Banka Hesapları',
    'creditcards'  => 'Kredi Kartları',
    'cryptowallets'  => 'KriptoCüzdan',
    'files'  => 'Dosyalar',
    'usersettings'  => 'Ayarlar ',







];
