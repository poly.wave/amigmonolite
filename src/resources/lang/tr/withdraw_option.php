<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '
        <h2 class="SectionSubTitle">Para çekme işlemleri hızlı ve kolaydır</h2>

        <p>
            AMIG Fx\'de para çekme işlemimiz hızlı ve kolaydır ve tüm para çekme işlemleri 24 saat içinde işleme koyulur. Paranızı istediğiniz zaman doğrudan Müşteri Giriş Alanınızdan çekebilirsiniz. Para yatırmak için kredi kartı kullandıysanız, paralar doğrudan belirtilen banka hesabınıza veya kredi kartınıza geri çekilebilir. Sadece bu kadar kolay.
        </p>

        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Para Çekme<br>Ücretleri</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">İşlem<br>Süresi</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">Banka havalesi</div>
                    <div class="td">25 - 80 USD</div>
                    <div class="td">24-48 hrs</div>
                </div>
                <div class="tr">
                    <div class="td">Banka KARTi</div>
                    <div class="td">3.5%</div>
                    <div class="td">24-48 hrs</div>
                </div>
                <div class="tr">
                    <div class="td">Kriptopara</div>
                    <div class="td">3%</div>
                    <div class="td">2-5 hrs</div>
                </div>
            </div>


        </div>






        ',
    ],

];
