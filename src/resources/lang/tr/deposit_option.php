<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
         <h2 class = "SectionSubTitle"> Para yatırma yönteminizi seçin </h2>
        <p> Kolaylığınız ve rahatlığınız için hesabınıza para yatırmanız için birden fazla para yatırma yöntemi sunuyoruz.
        </p>
        <p>
            <strong> Banka Havalesi </strong>, <strong> Kredi Kartı </strong> veya başka bir dizi seçenek arasından seçim yapın:
        </p>
        <div class = "special-table">
            <div class = "row">
                <div class = "tr">
                    <div class = "th"> </div>
                    <div class = "th">
                        <div class = "imgLabel outerShadow">
                            <Div>
                                <div class = "box" style = "color: # 000;"> Ücretleri Yatır </div>
                            </div>
                        </div>
                    </div>
                    <div class = "th">
                        <div class = "imgLabel outerShadow">
                            <div>
                                <div class = "box" style = "renk: # 616161;"> Tahmini <br> Zaman </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = "tr">
                    <div class = "td"> Banka Havalesi </div>
                    <div class = "td"> Yok </div>
                    <div class = "td"> 1-5 iş günü </div>
                </div>
                <div class = "tr">
                    <div class = "td"> VİZE - MASTER KARTLARI </div>
                    <div class = "td"> Yok </div>
                    <div class = "td"> Anında 2 saate </div>
                </div>
                <div class = "tr">
                    <div class = "td"> BITCOIN </div>
                    <div class = "td">% 3 </div>
                    <div class = "td"> Anında 2 saate kadar <a href="https://cex.io/r/1/up120172948/1"> Satın almak için tıklayın </a>

                </div>
            </div>

        </div>'
    ],

];
