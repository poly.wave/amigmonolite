<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'content'  => [
        '1' => ' <h2>About Us</h2>
        <p>
            Established in 2018, <strong>AMIG Capital LTD</strong> has quickly grown
            into one of the largest <strong>forex</strong> and <strong>CFD</strong> brokers. We provide
            all investors to benefit from our new and different applications in order to enlarge and
            popularize the investment brokerage market.
        </p>
        <p>Our customers can access and process all instant financial data, market analyzes and other
            information needed quickly and safely thanks to our advanced online
            <strong>forex trading</strong> platforms that use the latest technologies.</p>
        <p>We give our customers a special investment service at <strong>AMIG Capital LTD</strong> to contribute more to the analysis and evaluation process, to resolve a potential problem on the spot, and for a seamless support.</p>
        <p>
            We provide more professional and faster service to each customer with our advanced CRM system.
        </p>
        <div class="divider divider-3"></div>

        <h2>Diverse Product Offering</h2>
        <p>
            Offers competitive trading conditions with an easy to use proprietary trading platform as
            well as <strong>MetaTrader 5</strong>. Clients can trade with MetaTrader 5, one of the most
            popular and reliable trading platform available in the industry.
        </p>
        <p>
            We provide trading services for more than 60 currency pairs as well as indices, CFDs, gold,
            silver, oil and other commodities.
        </p>
        <p><strong>AMIG Capital LTD</strong> reduces the client costs with the most competitive
            prices in the market, creating the best trading conditions for investors.</p>
        <p>
            At the same time as AMIG Capital LTD we offer <strong>ECN</strong> account type.
        </p>
        <p>With the high level of technological investment infrastructures and the prices received from
            the leading liquidity providers of the world, investors are able to carry out their transactions
            transparently, day and night without interruption, with wide product range minimized price
            spread deviation and rejected order statistics.</p>
        <div class="divider divider-1"></div>
        <p>
            <img src="images/about.jpg" class="full-img">
        </p>
        <div class="divider divider-2"></div>
        <h2>Competitive Spreads</h2>
        <p>
            AMIG Capital LTD offers some of the tightest spreads in the industry with our
            5-decimal-digit pricing. Clients enjoy leverage
            as high as 1:200, as well as minilots and microlots. Our deal-execution systems are fully automated; there is no dealing desk, and therefore no human intervention.
        </p>',
    ],


];
