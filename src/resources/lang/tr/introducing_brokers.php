<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
<div class="container text">
        <h2 class="SectionSubTitle">Tanıtım Brokerleri</h2>

        <p>
            Zaten bir iş ekibiniz var ve daha fazla potansiyel ürün mü arıyorsunuz? MetaTrader tabanlı işlem platformunuzda müşterilere çevrimiçi işlem hizmetleri sunan bir broker mısınız?</p>

        <p>
            <a href="/contact-us" class="item">Bize Ulaşın.</a> <strong>En kısa sürede sizinle bağlantı kuracağız </strong>.
        </p>
    </div>


        ',

    ],

];
