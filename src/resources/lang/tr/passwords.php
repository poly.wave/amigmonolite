<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Parolalar en az 8 karakter uzunluğunda ve onayla eşleşmelidir.',
    'reset' => 'Parolanız sıfırlandı!',
    'sent' => 'Size e-posta ile bir şifre sıfırlama linki gönderdik!',
    'token' => 'Bu şifre sıfırlama linki geçerli değil',
    'user' => "Bu e-posta adresine sahip bir kullanıcı bulamıyoruz",
    'reseth' => "Parolayı sıfırla.",
    'resetdesc' => "Parolayı sıfırla.",
    'resetbttn' => "Sıfırla.",
    'email' => "E-posta adresi.",
    'pwd' => "Şifre.",
    'pwdconf' => "Parolayı onaylayın.",
    'verifyemail' => "E-posta adresini doğrula..",
    'linksent' => "E-posta adresinize yeni bir doğrulama linki gönderildi.",
    'checkemail' => "Devam etmeden önce, onay linki için e-postanızı kontrol edin",
    'didnotreceive' => "Mektubu almadıysanız",
    'requestanother' => "bir başkasını istemek için burayı tıklayın",
];
