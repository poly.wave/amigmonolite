<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '

       <p><strong>Kullanım Şartları</strong></p>


        <p>
            lütfen bu koşulları ve kuralları dikkatli okuyun. Bu World Wide Web Sitesine ("Web Sitesi") ve buradaki sayfalara erişerek, bu Kullanım Koşullarını okuduğunuzu, onayladığınızı ve kabul ettiğinizi belirtmiş olursunuz. Bu Kullanım Koşullarını kabul etmiyorsanız, bu Web Sitesine erişmeyin. AMIG Capital ("AMIG"), düzenli olarak gözden geçirmekten sorumlu olduğunuz bu Kullanım Koşullarını değiştirme hakkını saklı tutar ve bu Web Sitesini kullanmaya devam etmeniz, bu tür değişikliklerin tümünü kabul eder.
        </p>

        <div class="divider divider-1"></div>




 ',



    ],


];
