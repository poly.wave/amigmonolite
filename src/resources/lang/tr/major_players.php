<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
 <h2 class="SectionSubTitle">BÜYÜK OYUNCULAR KİMDİR?</h2>
        <p>
            Son birkaç yılda, Döviz piyasası bankaların kendi aralarında işlem yapacakları piyasadan, diğer birçok finansal kurumun katıldığı piyasaya doğru genişledi. Bunlar arasında aracı kurumlar ve piyasa yapıcılar, finansal olmayan şirketler, yatırım firmaları, emeklilik fonları ve riskten korunma fonları bulunmaktadır.  </p>
        <p>
            Odak noktası, ithalatçılara ve ihracatçılara hizmet vermekten, şu anda gerçekleşen büyük miktardaki denizaşırı yatırım ve diğer sermaye akışlarını ele almaya kadar genişledi. Son zamanlarda, Döviz günü ticareti giderek daha popüler hale gelmiştir ve çeşitli firmalar küçük yatırımcıya ticaret olanakları sunmaktadır. </p>
        <p>
            Döviz, \'tezgah üstü\' (OTC) bir pazardır, yani emirlerin eşleştiği merkezi bir takas ve takas odası yoktur. Bununla birlikte, coğrafi ticaret \'merkezleri\' dünya çapında mevcuttur ve (önem sırasına göre): Londra, New York, Tokyo, Singapur, Frankfurt, Cenevre & Zürih, Paris ve Hong Kong. Esasen, anlaşmalar yapmak için güven ve itibar temelinde katılımcılar arasında Döviz anlaşmaları yapılır. Birbirleri ile ticaret yapan bankalarda, bunu sadece bu temelde yaparlar. Perakende pazarında müşteriler, kendileri ve aracıları arasında, müşterinin işlem yapabileceği bir fon depozitosu karşılığında yazılı, yasal olarak kabul edilmiş bir sözleşme talep ederler. <br>
            Bazı piyasa katılımcıları, mal alım satımı için uluslararası işlemler yürüten \'mal\' pazarına katılabilirler. Bazıları tesis ve ekipmanlara “doğrudan yatırım” yapabilir ya da kısa vadeli borç enstrümanları ticareti yaparak “para piyasasında” olabilir. Çeşitli yatırımcılar, riskten korunucular ve spekülatörler birkaç dakikadan birkaç yıla kadar herhangi bir zaman dilimine odaklanabilir. Ancak ister resmi ister özel, isterse yatırımları, finansal riskten korunma, spekülasyon, arbitraj, ithalat için ödeme yapma veya oranı etkileme arayışı içinde olsunlar, bunların tümü ilgili para birimlerinin toplam talebinin ve arzının bir parçasıdır. o anda döviz kurunun belirlenmesindeki rolü. </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">DÜNYA FOREX TİCARET BANKALARI</h2>
        <p>
            <strong>Kanada Bankası (Kanada) Kanada</strong>  <br>
            Bankası, ülkenin Merkez Bankası\'dır. Ticari bir banka değildir ve halka bankacılık hizmeti sunmaz. Aksine, Kanada\'nın para politikası, banknotlar, finansal sistem ve fon yönetiminden sorumludur. Kanada Bankası Yasası\'nda tanımla </p>
        <p>
            <strong>Bank of England (İngiltere)</strong>  <br>
            Bankanın rolleri ve işlevleri üç yüz yıllık tarihi boyunca değişti ve değişti. Kuruluşundan bu yana Hükümetin bankacısıdır ve 18. yüzyılın sonlarından beri genellikle bankacılık sistemine - bankacılar bankasına - banker olmuştur. Bank of England, müşterilerine bankacılık hizmetlerinin yanı sıra İngiltere\'nin Döviz ve altın rezervlerini de yönetmektedir.<br>
            Banka\'nın iki temel amacı vardır: parasal ve finansal istikrar. Banka, banka notları ve yakın zamanda faiz oranı kararları aracılığıyla belki de en çok kamuoyu tarafından görülebilir. Banka, 20. yüzyılın başlarından beri İngiltere ve Galler\'de banknotlar konusunda tekel sahibidir. Ancak sadece 1997\'den bu yana Banka\'nın İngiltere\'nin resmi faiz oranını belirlemede yasal sorumluluğu bulunmaktadır.
        </p>
        <p>
            <strong>Japonya Merkez Bankası (Japonya)</strong>  <br>
            Japonya Merkez Bankası\'dır.Japonya Merkez Bankası Yasası\'na (bundan sonra Kanun olarak anılacaktır) dayanan yerleşik bir tüzel kişidir ve bir devlet kurumu veya özel bir şirket değildir. Kanun, Banka\'nın hedeflerini \'banknotlar vermek ve para ve parasal kontrol yapmak\' ve \'fonların bankalar ve diğer finansal kurumlar arasında düzgün bir şekilde çözümlenmesini sağlamak, böylece düzenli bir finansal sistemin korunmasına katkıda bulunmak\' olarak belirlemektedir. Kanun ayrıca, Banka\'nın para ve parasal kontrol ilkesini de şu şekilde öngörmektedir: “Amacı, fiyat istikrarını takip ederek parasal ve parasal kontrol olmak ve ülke ekonomisinin sağlıklı gelişmesine katkıda bulunmaktır”. </p>
        <p>
            <strong>Meksika Merkez Bankası (Meksika) </strong>  <br>
            Meksika Merkez Bankası, ülkenin Merkez Bankası olup, anayasal olarak işlev ve idare bakımından bağımsızdır. Amacı, Meksika ekonomisine ulusal para birimini sağlamaktır. Bunu yaparken, temel amacı paranın harcama gücünü dengelemeye çalışmaktır. Ayrıca, finansal sistemin sağlıklı gelişimini teşvik etmek ve ülkenin ödeme sistemlerinin etkin işleyişini sağlamak Banka\'ya bağlıdır.</p>
        <p>
            <strong>Avrupa Merkez Bankası (Avrupa Birliği)<strong>  <br>
            ECB, Avrupa\'nın tek para birimi olan Euro için Merkez Bankası\'dır. ECB\'nin temel görevi Euro\'nun satın alma gücünü ve dolayısıyla Euro Bölgesi\'nde fiyat istikrarını sağlamaktır. Euro Bölgesi, 1999\'dan beri Euro\'yu piyasaya süren 18 Avrupa Birliği ülkesinden oluşmaktadır.  </p>
        <p>
            <strong>Federal Rezerv (ABD)</strong>  <br>
            Federal Açık Piyasa Komitesi (FOMC), ulusal ekonomik hedeflerin geliştirilmesine yardımcı olmak için para politikası belirler. New York Fed Başkanı, kalıcı oy kullanan tek bölgesel Banka Başkanıdır ve geleneksel olarak Başkan Yardımcısı olarak seçilir. Diğer başkanlar dönüşümlü olarak bir yıllık dönemlerde görev yapıyorlar.  </p>
        <p>
            <strong>Avustralya Merkez Bankası (Avustralya)</strong>  <br>
            Merkez Bankası\'nın (RBA) ana sorumluluğu para politikasıdır. Politika kararları, orta vadede düşük ve istikrarlı bir enflasyon elde etmek amacıyla Rezerv Bankası Kurulu tarafından alınır. Diğer önemli rolleri finansal sistemin istikrarını korumak ve ödeme sisteminin güvenliğini ve verimliliğini teşvik etmektir. Banka finansal piyasalarda aktif bir katılımcıdır, Avustralya\'nın dış rezervlerini yönetir, Avustralya para birimi cinsinden banknotları düzenler ve Avustralya Hükümetine bankerlik yapar. Rezerv Bankası tarafından sağlanan bilgiler (örneğin, faiz oranları, döviz kurları, para ve kredi büyümesi hakkında) istatistikleri ve faaliyetleri ve araştırmaları ile ilgili çeşitli yayınları içermektedir.
        </p>
        <p>
            <strong>Yeni Zelanda Merkez Bankası (Yeni Zelanda)</strong>  <br>
            Yeni Zelanda Merkez Bankası ülkenin Merkez Bankası\'dır. Genel amacı finansal sistemin istikrarını ve verimliliğini korumaktır. Bunu beş şekilde yapar:
            Fiyat istikrarını sağlamak ve sürdürmek için para politikasının işletilmesi.Sağlam ve verimli bir finansal sistemin işleyişini teşvik etmek. Halkın para birimi ihtiyaçlarını karşılamak. Verimli ödeme sistemlerinin denetlenmesi ve işletilmesi. Bankaya etkin destek hizmetleri sunmak. </p>
        <p>
            <strong>İsviçre Ulusal Bankası (İsviçre)</strong>  <br>
            İsviçre Ulusal Bankası, ülkenin para politikasını bağımsız bir Merkez Bankası olarak yürütmektedir. Anayasa ve tüzük ile bir bütün olarak ülkenin çıkarlarına uygun davranmakla yükümlüdür. Birincil hedefi, ekonomik gelişmeleri dikkate alarak fiyat istikrarını sağlamaktır. Böylece ekonomik büyüme için uygun bir ortam yaratır. Kanun ayrıca, Banka\'nın para ve parasal kontrol ilkesini şu şekilde öngörmektedir: “Amacı, fiyat istikrarını takip ederek parasal ve parasal kontrol olmak, ülke ekonomisinin sağlıklı gelişmesine katkıda bulunmaktır”.</p>



        ',

    ],

];
