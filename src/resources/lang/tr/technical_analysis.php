<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
       <h2 class="SectionSubTitle">TEKNİK ANALİZ</h2>
        <p>
            Döviz ticareti kavramı, bir para biriminin değeri başka bir para birimine göre dalgalanan bir meta olduğu anlaşıldığında basittir. FX Traders, bir para birimi satın alarak (veya satarak), döviz kurundan hareketten kar elde etmeyi hedefler. FX\'in güzelliği, ticaret maliyetinin çok düşük olmasıdır. Bu, işlemlerin aşırı kısa vadede, kelimenin tam anlamıyla saniyeler içinde ve daha uzun bir süre için gerçekleştirilebileceği anlamına gelir. </p>
        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">TEKNİKLERDE NEYE BAKMALI</h2>
        <p>
            <strong>Trendi Bulun</strong> <br>
            Teknik analizde ilk duyacağınız şeylerden biri şu slogan: \'trend arkadaşınızdır\'. Hakim eğilimi bulmak, genel pazar yönünün farkına varmanıza ve daha iyi görünürlük sunmanıza yardımcı olacaktır. Haftalık ve aylık grafikler daha uzun vadeli eğilimleri belirlemek için daha uygundur. Genel eğilimi bulduktan sonra, işlem yapmak istediğiniz zaman ufkunun eğilimini seçebilirsiniz. Böylece, yükselen trendler sırasında düşüşleri etkili bir şekilde satın alabilir ve düşüş trendleri sırasında mitingleri satabilirsiniz.</p>
        <p>
            <strong>Destek ve Direnç</strong> <br>
            Destek ve direnç seviyeleri, bir grafiğin yukarı veya aşağı doğru tekrarlayan basınçlarla karşılaştığı noktalardır. Bir destek seviyesi genellikle herhangi bir grafik desenindeki (saat, haftalık veya yıllık) düşük noktadır, oysa bir direnç seviyesi desenin yüksek veya tepe noktasıdır. Bu noktalar, yeniden ortaya çıkma eğilimi gösterdiklerinde destek ve direnç olarak tanımlanır. Kırılması muhtemel olmayan destek / direnç seviyelerinin yakınında satın almak / satmak en iyisidir.
            Bu seviyeler kırıldığında, karşıt engel olma eğilimindedirler. Bu nedenle, yükselen bir pazarda, kırılan bir direnç seviyesi, artış eğilimini destekleyebilir; Oysa düşen pazarda, destek seviyesi bozulduğunda direnişe dönüşebilir. </p>

        <p>
            <strong>Hatlar ve Kanallar</strong> <br>
            Trend çizgileri, piyasa trendlerinin yönünü doğrulamak için basit ancak yardımcı araçlardır. En az iki ardışık düşük değer bağlanarak yukarı doğru düz bir çizgi çizilir. Doğal olarak, ikinci nokta birinciden daha yüksek olmalıdır. Hattın devamı, piyasanın hareket edeceği yolu belirlemeye yardımcı olur. Artış eğilimi destek hatlarını / seviyelerini tanımlamak için somut bir yöntemdir. Tersine, aşağı doğru çizgiler iki veya daha fazla nokta birleştirilerek çizilir. Bir işlem hattının geçerliliği kısmen bağlantı noktalarının sayısıyla ilişkilidir. Yine de noktaların birbirine çok yakın olmaması gerektiğini belirtmek gerekir. Kanal, iki paralel eğilim çizgisi tarafından çizilen fiyat yolu olarak tanımlanır. Hatlar fiyat için yukarı, aşağı veya düz bir koridor görevi görür.</p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">TEMEL TEORİ</h2>
        <p>
            <strong>Grafikler</strong> <br>
            Teknik analizde kullanılan üç ana grafik türü vardır: <br>
            Çizgi Grafik: Çizgi grafik, bir döviz çiftinin zaman içindeki döviz kuru geçmişinin grafiksel bir tasviridir. Hat, günlük kapanış fiyatları birleştirilerek inşa edilmiştir.
            Çubuk Grafik: Çubuk grafik, belirli gün içi zaman aralıklarında (örneğin her 30 dakikada bir) dikey çubuklardan oluşan bir döviz çiftinin fiyat performansının bir tasviridir. Her çubukta, zaman aralığı için açılış, kapanış, yüksek ve düşük (OCHL) döviz kurlarını temsil eden 4 \'kanca\' bulunur.
            Şamdan Grafiği: Şamdan grafiği çubuk grafiğin bir çeşididir, ancak şamdan grafiği OCHL fiyatlarını her iki uçta bir \'fitil\' olan \'şamdanlar\' olarak tasvir eder. Açılma oranı kapanma oranından daha yüksek olduğunda, mum çubuğu \'katı\' olur. Kapanma oranı açılış hızını aştığında, şamdan \'içi boş\' olur.  </p>
        <p>
            <strong>Destek ve Direnç Seviyeleri</strong> <br>
            Teknik analizlerden biri \'destek\' ve \'direnç\' seviyelerini elde etmektir. Temel fikir, piyasanın destek seviyelerinin üzerinde ve direnç seviyelerinin altında ticaret yapma eğiliminde olmasıdır. Destek seviyesi, para biriminin aşağıdan geçmekte zorlanacağı belirli bir fiyat seviyesini gösterir. Fiyat art arda bu noktanın altına inmezse, düz çizgi deseni görünecektir.
            Direnç seviyeleri ise, para biriminin yukarıdan geçmekte zorlanacağı belirli bir fiyat seviyesini gösterir. Fiyatın bu noktanın üzerine çıkmaması tekrarlanan bir düz çizgi modeline yol açacaktır.
            Eğer bir destek veya direnç seviyesi bozulursa, piyasanın bu yönde ilerlemesi beklenir. Bu seviyeler, grafiğin analizi ve piyasanın geçmişte kesintisiz destek veya dirençle karşılaştığı yerlerin değerlendirilmesi ile belirlenir.   </p>
        <p>
            <strong>Hareketli Ortalamalar</strong> <br>
            Hareketli ortalamalar fiyat eğilimlerini izlemek için başka bir araç sağlar. Hareketli bir ortalama, en basit haliyle, zaman içinde değişen ortalama fiyatlardır. 10 günlük hareketli ortalama, son 10 günlük kapanış fiyatları eklenerek ve sonra 10\'a bölünerek hesaplanır. Ertesi gün, en eski fiyat düşürülür ve bunun yerine yeni günün kapanış fiyatı eklenir; Şimdi bu 10 fiyat 10\'a bölünür. Bu şekilde, her gün ortalama \'hamle\' olur.
            Hareketli ortalamalar pazara girmek veya piyasadan çıkmak için daha mekanik bir yaklaşım sağlar. Giriş ve çıkış noktalarını belirlemeye yardımcı olmak için hareketli ortalamalar genellikle çubuk grafiklerin üzerine yerleştirilir. Piyasa hareketli ortalamanın üzerinde kapandığında, genellikle bir alım sinyali olarak yorumlanır. Aynı şekilde, piyasa hareketli ortalamanın altına düştüğünde bir satış sinyali olarak kabul edilir. Bazı trader\'lar hareketli ortalama çizgisinin alım veya satım sinyali olarak kabul etmeden önce yön değiştirdiğini görmeyi tercih eder.
            Hareketli ortalama çizgisinin duyarlılığı ve ürettiği alış ve satış sinyallerinin sayısı, hareketli ortalama için seçilen zaman periyodu ile doğrudan ilişkilidir. 5 günlük hareketli ortalama daha hassas olacak ve 20 günlük hareketli ortalamadan daha fazla alım ve satım sinyali verecektir. Ortalama çok hassassa, trader\'lar kendilerini piyasaya çok sık girip çıkıyorlar. Öte yandan, hareketli ortalama yeterince hassas değilse, trader\'lar alım ve satım sinyallerini çok geç tespit ederek eksik fırsatlar riskiyle karşı karşıya kalırlar.
            Hareketli ortalamalar teknik tüccar için son derece yararlı araçlar olabilir.
        </p>
        <p>
            <strong>Trend Çizgisi</strong> <br>
            Trend çizgisi, trendin yanı sıra potansiyel destek ve direnç alanlarının belirlenmesine yardımcı olur. Eğilim çizgisi, altta yatan bir işlemin fiyat hareketinde en az iki önemli tepe veya oluğu birbirine bağlayan düz bir çizgidir. Başka hiçbir fiyat hareketi iki nokta arasındaki eğilim çizgisini kırmamalıdır. Bu şekilde, bir trend çizgisi, fiyatın döndüğü (zirve ve dip noktaları) ve ihlal edilmediği bir destek veya direnç alanını işaret eder. Trend çizgisi ne kadar uzun olursa, özellikle fiyat çizgiye nüfuz etmeden birkaç kez dokunmuşsa, o kadar geçerli olur.
            Uzun vadeli bir eğilim çizgisinin penetrasyonu, trendin tersine çevrilmesinin bir göstergesi olabilir. Ancak bunun olacağının garantisi yoktur. Fiyat eğiliminin tersine çevrilmesine ilişkin tüm göstergelerde olduğu gibi, gelecekteki fiyatların nereye gideceğini önceden belirleyen kanıtlanmış bir yöntem yoktur.
        </p>




        ',


    ],



];
