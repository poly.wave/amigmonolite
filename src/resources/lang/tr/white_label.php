<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '

         <p>
            Müşterilerin fonlarını tutma yetkisi olan düzenlenmiş bir finansal kuruluş musunuz? Ürün portföyünüzü genişletmeyi ve müşterilerinize güvenlik ve kalite ile çevrimiçi FX ve CFDS ticareti sunmayı düşünüyor musunuz? </p>
        <p>
            Dünyanın dört bir yanındaki yasal kurumlara Sınırlı ve Tam Beyaz Etiket çözümleri sunuyoruz. Müşterileriniz aynı zamanda birinci sınıf güvenlik, likidite havuzumuz, BT altyapımız, organizasyonumuz ve bilgimizin yanı sıra bizi en büyük çevrimiçi FX ve CFD işlem brokeri yapan diğer birçok varlıktan da faydalanacaklar. Sınırlı Beyaz Etiket ortaklık programımız herhangi bir idari işi yapmamayı seçen kurumlar için uygundur, Tam Beyaz Etiket programımız ise arka ofis çalışmasından sorumlu olmaktan mutluluk duyan kurumlara hitap etmektedir. Her iki durumda da, kurumunuzun adını taşıyan kullanıcı dostu platformlarla müşterilerinize son derece profesyonel ticaret hizmetleri sunabileceksiniz. Harika BT ve pazarlama desteğimizin tadını çıkarabilirsiniz, ve müşterilerinize komisyon ve / veya daha yüksek spreadler yükleyerek gelirinizi artırabilirsiniz. En önemlisi, müşterileriniz güvenle kullanabildikleri son derece rekabetçi ve profesyonel bir çevrimiçi FX ve CFD ticaret hizmeti alacaklar.</p>

        <p>
            <a href="/contact-us" class="item">Bize Ulaşın.</a> <strong>En kısa sürede sizinle bağlantı kuracağız .</strong>.
        </p>


        ',

    ],

];
