<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /* Personal info*/
    'email'  => 'E-posta Adresi',
    'emailpl'  => ' example@example.com',
    'pwdnew'  => 'Şifrenizi girin',
    'pwdnew1'  => 'Şifre',
    'pwdnew2'  => 'Şifreyi tekrar girin',
    'name'  => 'İsminiz',
    'namepl'  => 'İsminiz',
    'sname'  => 'Soyadınız',
    'snamepl'  => 'Soyadınız',
    'dofb'  => 'Doğum tarihi',
    'dofbpl'  => 'gg.aa.yyyy',
    'phone'  => 'Telefon numarası',
    'phonepl'  => ' +1 (111) 111 11 11',
    'gender'  => 'Cinsiyet',
    'preflang'  => 'Tercih Edilen Dil',
    'prefcurr'  => 'Tercih Edilen Para Birimi',
    'country'  => 'Ülke',
    'region'  => 'Bölge',
    'city'  => 'Şehir',
    'resident'  => 'Yerleşik Adres',
    'agree'  => 'Ben okudum ve kabul ediyorum',
    'agreename'  => 'Taahhüt ve Sorumluluk Beyanı',






];
