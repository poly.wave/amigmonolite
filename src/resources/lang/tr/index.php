<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Index Language Lines
    |--------------------------------------------------------------------------
    */

    'content'  => [
        'title' => 'AMIG-le TİCARETE BAŞLAYIN',
        'opendemo'  => 'DEMO hesap aç',
        'openlive'  => 'GERÇEK hesap aç',
        'leverage'  => 'Kaldıraç',
        '1200'  => '1:200',
        'spreads'  => 'Düşük spred',
        'slippage'  => 'Slippage',
        'currencies'  => 'Parite',
        'indices'  => 'İndeks',
        'commodities'  => 'Commodities',
        'minimum'  => 'Mini',
        'depositl'  => 'yatırım',
        'fee'  => 'Komisyon',
        'support'  => 'saat Destek',
        'whyamig'  => 'Niye AMIG Forex',
        'asfx'  => 'Sizin broker şirketiz olmalı',
        'trusted'  => 'Güvenilir',
        'trusted1'  => 'AMIG FOREX Treyderler için Treyderler tarafından yaradılmıştır',
        'segregated'  => 'Ayrılmış fonlar',
        'segregated1'  => 'Müşteri fonları ayrı bir müşteri hesabında tutulur.',
        'quick'  => 'Hızlı Para Çekme',
        'quick1'  => 'Tüm para çekme işlemleri 12-36 saat içinde işleme koyulur.*',
        'ultra'  => 'Ultra Hızlı Uygulama',
        'ultra1'  => 'New York ve Londra\'daki Interbank Sunucularına fiber optik bağlantılarla 12 kata kadar daha hızlı yürütme hızı ve 10 kata kadar gecikme azaltma, ultra hızlı tek tıklamayla ticarete izin verme',
        'negative'  => 'Negatif Denge Koruması',
        'negative1'  => 'Kayıplarınızın hesap bakiyenizi aşamadığını güvenle ve gönül rahatlığıyla ticaret yapın.',
        'dedicated'  => 'Özel Hesap Yöneticisi',
        'dedicated1'  => 'Eski moda bir hesap yöneticisinden Başarınıza adanmış müşteri hizmetlerinin keyfini çıkarın.',

    ],

];
