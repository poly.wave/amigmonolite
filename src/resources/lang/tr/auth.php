<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title' => 'AMIG  - Müşteri girişi',
    'failed' => 'Bu kimlik bilgileri kayıtlarımızla eşleşmiyor.',
    'throttle' => 'Çok fazla giriş denemesi. Lütfen :seconds seconds saniye içinde tekrar deneyin.',
    'login'  => 'Giriş',
    'loginbttn'  => 'GİRİŞ',
    'logindesc'  => 'Hesabınıza giriş yapın',
    'forgotpwd'  => 'Şifrenizi mi unuttunuz?',
    'regbttn'  => 'Kaydol',
    'regnowbttn'  => 'Şimdi kaydolun!',
    'signup'  => 'Kaydol',
    'singupdesc'  => 'Bir hesabınız yoksa, kayıt olun',

    'regform'  => 'Kayıt formu',
    'loginp'  => 'GİRİŞ',
    'loginpdesc'  => 'Bir hesabınız varsa, sadece giriş yapın.',
    'newacc'  => 'Hesap oluştur',
    'createacc'  => 'Hesap oluştur',
    
];
