<?php

return [

    /*
    |--------------------------------------------------------------------------
    | for rest api requests
    |--------------------------------------------------------------------------

    |
    */
    'error_auth' => 'Geçersiz Kimlik Doğrulama anahtarı.',
    'success_registr'=>'Başarıyla yeni bir hesap oluşturuldu. Lütfen e-postanızı kontrol edin ve hesabınızı etkinleştirin',
    'confirm_email'=>'Giriş yapılamıyor. Lütfen onay e-postası bağlantısını kullanarak doğrulayın.',
    'logged_success'=>'Başarıyla giriş yaptınız.',
    'incorrect_password'=>'Giriş yapılamıyor. Yanlış parola.',
    'email_ntexist'=>"Giriş yapılamıyor. E-posta mevcut değil.",
    'password_reset'=>"Parola sıfırlama bağlantınızı e-posta ile gönderdik!",
    'ntf_email'=>"Bu e-posta adresine sahip bir kullanıcı bulamıyoruz.",
    'not_regions'=>"Bu tür verilere sahip bölgeler yoktur.",
    'not_cities'=>"Bu tür verilere sahip şehirler yok.",
    'not_user'=>"Böyle bir kimliğe sahip kullanıcı yok.",
    'invalid_token'=>"Bu şifre sıfırlama jetonu geçersiz.",
    'rst_password'=>"Şifreniz başarıyla geri yüklendi.",
    'userinfo_update'=>"Bu kullanıcı bilgisi güncellendi․",
    'incorrect_oldpassword'=>"Yanlış eski şifre.",
    'password_update'=>"Şifreniz başarıyla güncellendi.",
    'not_deposits'=>"Bu kullanıcının mevduatı yok.",
    'not_withdrawals'=>"Bu kullanıcının para çekme hakkı yoktur.",
    'not_earnings'=>"Bu kullanıcının hiçbir kazancı yok.",

    'ntapproved_deposits'=>"Bu kullanıcının onaylanmış mevduatı yok.",
    'ntapproved_withdrawals'=>"Bu Kullanıcının onaylanmış bir para çekme işlemi yoktur.",
    'ntinfo_passport'=>"Bu Kullanıcının pasaportu yok․",
    'nttradinfo_account'=>"Bu kullanıcının ticaret hesabı yok.",

    'create_passport'=>"Pasaport başarıyla oluşturuldu.",
    'create_file'=>"Kullanıcı dosyası başarıyla oluşturuldu.",
    'nt_file'=>"Kullanıcının dosyası yok.",

    'withdrawal_committed'=>"Kullanıcı Geri Çekme işlemi başarıyla gerçekleştirildi.",
    'bank_transfer'=>"Banka Havalesi başarıyla eklendi.",
    'deposed_created'=>"başarıyla oluşturuldu.",

    'wallet_created'=>"Cüzdan başarıyla oluşturuldu․",
    'not_banktransfer'=>"Bu Kullanıcının banka havalesi yoktur.",

    'not_creditcards'=>"Bu Kullanıcının kredi kartı yoktur.",

    'not_cryptocurrencies'=>"Bu Kullanıcının kripto para birimi yoktur.",

    'nt_leverage'=>"Kaldıraç yok.",
    'notoptions_withdrawals'=>"Para çekme türleri yok.",
    'not_cryptocoins'=>"Kripto para yok.",
    'error_service'=>"404 bu hizmet kullanılamıyor.",
    'success_trading_acc'=>"Kaydı tamamlamak için e-posta kutunuza gidin ve onay bağlantısına tıklayın.",
    'update_user_earnings'=>"Başarı",
    'not_crypto_company'=>"Kripto şirketleri yok."

];
