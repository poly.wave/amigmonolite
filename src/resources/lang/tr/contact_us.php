<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'fname' => 'Ad',
        'lname' => 'Soyadı',
        'email' => 'E-posta Adresi',
        'mobile' => 'Mobil',
        'message' => 'Mesaj',
        'send' => 'Gönder',
        'contact' => 'İletişim Formu',
        'contact1' => 'Sorunuzu cevaplamaya hazırız.',
    ],
];
