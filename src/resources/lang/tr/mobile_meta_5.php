<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mmt'  => ' Mobile Meta Trader 5',
        'mmtintro'  => 'MT5\'te Mobil İşlem',
        'mmtintro1'  => 'Akıllı telefonunuzun veya tabletinizin rahatlığında dünyanın herhangi bir yerinden, her zaman, ticaret yapın.',
        'mmtintro2'  => 'Süper hızlı tek tıklamayla ticaret ─ avucunuzun içinden.',
        'mmtintro3'  => 'Seçtiğiniz cihazınız ister Android ister iOS olsun, AMIGFX MetaTrader 5 ile hareket halindeyken, istediğiniz zaman, istediğiniz yerde ticaret yapabilirsiniz! MT5 uygulamasını Apple App Store veya Google Play\'den indirmeniz yeterlidir!',
        'ios'  => 'MT5 uygulamasını indirmek için aşağıdaki App Store düğmesini tıklayın.',
        'ios1'  => 'MT5 uygulaması içinden AMİG Capıtal sunucusunu seçin.',
        'android'  => 'MT5 uygulamasını indirmek için aşağıdaki Google Play düğmesini tıklayın.',
        'android1'  => 'MT5 uygulaması içinden AMİG Capıtal sunucusunu seçin.',


    ],




];
