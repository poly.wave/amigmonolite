<?php

return [
    /*
    |--------------------------------------------------------------------------
    | USER Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*  */

    'settings'  => 'Gösterge tablosu özelleştirme araçları (şablon, dil veya profil fotoğrafını değiştir) yakında eklenecek. Teşekkür.',
    /*Controller*/
    'jan'  => 'Jan',
    'feb'  => 'Feb',
    'mar'  => 'Mar',
    'apr'  => 'Apr',
    'may'  => 'May',
    'jun'  => 'Jun',
    'jul'  => 'Jul',
    'aug'  => 'Aug',
    'sep'  => 'Sep',
    'oct'  => 'Oct',
    'nov'  => 'Nov',
    'dec'  => 'Dec',



];
