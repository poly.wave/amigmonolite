<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
         <h2 class = "SectionSubTitle"> AMIG Forex İLE ORTAKLIK </h2>
        <p>
            <strong> AMIG Fx ile Ortaklık Yoluyla Yeni İş Fırsatlarını Keşfedin </strong>
        </p>
        <p>
            Çevrimiçi Forex ve CFD ticareti, tüm piyasa katılımcıları için muazzam bir iş fırsatını temsil eder. Yapabilirsin
            ürün portföyünüzü hızlı ve kolay bir şekilde genişletin ve çevrimiçi Forex ve CFD işlem hizmetlerinizi
            yılların tecrübesi ile müşteriler.
        </p>
        <p>
            AMIG Fx ile ortaklık kurmanın diğer önemli avantajları:
        </p>
        <p>
            - En Yüksek İndirim ve Avantajlar <br>
            - Kullanışlı ve Hızlı Para Yatırma / Çekme <br>
            - Her Bir Sıraya Ödenen Bireysel Kayıt ve Komisyon <br>
            - Ortaklar için Özelleştirilmiş Komisyon Sistemi ve Ticaret Koşulları <br>
            - Para Yatırma Bonusu Promosyonu <br>
            - Haftalık Komisyon Ödemesi <br>
            - Eğitim ve Öğretim Desteği <br>
            - Güçlü Arka Ofis <br>
            - Web Sitesi Kaynakları <br>
            <br>
        </p>
<p>
            <a href="/contact-us" class="item"> <strong> Bize Katılın </strong>. </a> <strong> En kısa sürede sizinle bağlantı kuracağız </strong>.
        </p>



        ',


    ],
];
