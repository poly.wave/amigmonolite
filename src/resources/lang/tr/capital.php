<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Layouts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title' => 'AMIG Capital LLC - Yatırım ve Finansman Danışmanlığı',
    /*
    Menu
    */
    'main' => 'Ana Sayfa',
    'about' => 'Hakkımızda',
    'projeler' => 'Projeler',
    'contacts' => 'Irtibat',
    /*
    Main
    */
    'keep'  => 'İrtibatta olalım',
    'name'  => 'Ad',
    'gobttn'  => 'GO',
    /*
    Contacts
    */
    'office' => 'Merkez Ofis',
    'phone' => 'Telefon',
    'email' => 'e-posta',
    'address' => 'Adres',
    /*
    Contact Form
    */
    'cfhead' => 'İletişim Formu',
    'cfdesc' => 'Sorunuzu cevaplamak için buradayız.',
    'fname' => 'Ad',
    'lname' => 'Soyadı',
    'femail' => 'E-posta Adresi',
    'mobile' => 'Mobil',
    'message' => 'Mesaj',
    'sendbtn' => 'Gönder',

    /*
     Slider
    */
    'finance' => 'FİNANS',
    'fxcrypto' => 'FOREX & CRYPTO PİYASALARI',
    'innovation' => 'YENİLİK VE TEKNOLOJİ',
    'invest' => 'YATIRIM',
    /*
     Footer
    */
            'co'  => 'AMIG Capital LLC',




];
