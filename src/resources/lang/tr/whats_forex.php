<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [

        '1'=> '

        <h2 class="SectionSubTitle">DÖVİZ PİYASALARINA GİRİŞ</h2>
        <p>
            Döviz piyasası dünyanın en büyük işlem gören piyasası olmasına rağmen, perakende sektöründeki erişimi özsermaye ve sabit Gelir piyasalarına kıyasla azalmaktadır. Bu, büyük ölçüde , yatırımcı topluluğunda YP\'lerin genel olarak bilinmemesinin yanı sıra para birimlerinin nasıl ve neden hareket ettiğini anlama eksikliğinden kaynaklanmaktadır. Bu pazarın gizemine ek olarak, NYSE veya ASX\'e benzer fiziksel bir merkezi değişim olmadığı gerçeğidir. FX piyasaları 24 saat çalışır.
        </p>
        <p>
            Geleneksel olarak, döviz piyasasına erişim, ticari, riskten korunma veya spekülatif amaçlar için büyük para birimleri ticareti yapan banka topluluğu ile sınırlıydı. Amig Capital gibi firmaların yaratılması, bireysel perakende tüccarın yanı sıra fon ve para yöneticileri gibi kurumlara forex ticaretinin kapısını açtı. Pazarın bu sektörü son birkaç yılda katlanarak büyümüştür.
        </p>

        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">FX TİCARET NEDİR?</h2>
        <p>
            Bir döviz işleminde, bir para birimi diğeri karşılığında satılır. Oran, iki para birimi arasındaki göreli değeri ifade eder. Para birimleri normal olarak üç basamaklı bir \'Swift\' kodu ile tanımlanır. Örneğin, EUR = Euro, USD = ABD Doları, CHF = İsviçre Frangı vb. Kodların tam listesini burada bulabilirsiniz. 1.5000 EUR / USD oranı, 1 EUR\'nun 1.5 USD değerinde olduğu anlamına gelir.</p>
        <p>
            Bazen EUR / USD döviz çifti olarak adlandırılır. Hız tersine çevrilebilir. Dolayısıyla, 1.5000 EUR / USD kuru 0.6666 USD / EUR kuru ile aynıdır. Başka bir deyişle, 1 USD değerinde 0.6666 EUR değerinde. Piyasa sözleşmesi, çoğu para biriminin dolara karşı teklif verme eğilimindedir, ancak daha önce bahsedilen EUR / USD, GBP / USD (UK Pound Sterling) ve AUD / USD (Avustralya Doları) gibi dikkate değer istisnalar vardır. Bu kulağa geldiği kadar kafa karıştırıcı değil. </p>


        <div class="divider divider-3"></div>

        <h2 class="SectionSubTitle">YABANCI PARA SEMBOLLERİ</h2>
        <p>

            Para birimlerinin, özkaynaklar gibi, birini diğerinden ayıran kendi sembolleri vardır. Para birimleri, birinin değeri ile diğerinin değerine göre belirtildiği için, bir para birimi çifti, her iki para birimi için bir eğik çizgi (\'/\') ile ayrılmış \'adı\' içerir. \'İsim\' üç harfli bir kısaltmadır. İlk iki harf, çoğu durumda, ülkenin tanımlanması için ayrılmıştır. Son harf, o ülkenin para biriminin ilk harfidir.<br>
         Örneğin, <br>
            USD = ABD Doları<br>
            GBP = İngiltere Poundu<br>
            JPY = Japon Yeni<br>
            CAD = Kanada Doları<br>
            CHF = Konfederasyon Helvetica (İsviçre Konfederasyonu için Latince) Frangı<br>
            NZD = Yeni Zelanda Doları<br>
            AUD = Avustralya Doları<br>
            NOK = Norveç Kronu<br>
            SEK = İsveç Kronu<br>
        </p>
        <p>
            Avrupa Euro\'sunun kendine özgü bir ülkesi olmadığından, EUR kısaltması ile geçer. Bir para birimini (EUR) başka bir para birimiyle (USD) birleştirerek, bir para birimi çifti yaratırsınız - EUR / USD. </p>
        <br>

        <h2 class="SectionSubTitle">TABAN VE SAYICI-PARA BİRİMLERİ</h2>
        <p>
            Bir döviz çiftindeki bir para birimi her zaman baskındır. Buna temel para birimi denir. Temel para birimi, bir döviz çiftindeki ilk para birimi olarak tanımlanır. Aynı zamanda bir döviz çiftinin fiyatını belirlerken sabit kalan para birimidir. </p>
        <p>
            Euro, diğer tüm küresel para birimlerine karşı baskın temel para birimidir. Sonuç olarak, EUR karşısındaki döviz çiftleri EUR / USD, EUR / GBP, EUR / CHF, EUR / JPY, EUR / CAD, vb. Olarak tanımlanacaktır. </p>
        <p>
            ngiliz Sterlini, para birimi adı hakimiyeti hiyerarşisinde. Bu nedenle GBP karşısında büyük döviz çiftleri GBP / USD, GBP / CHF, GBP / JPY, GBP / CAD vb. Olarak tanımlanacaktır. EUR / GBP\'nin dışında, GBP\'nin bir döviz çiftindeki ilk para birimi olarak görülmesini bekliyoruz.  </p>
        <p>
            USD bir sonraki en baskın temel para birimidir. USD / CAD, USD / JPY, USD / CHF başlıca para birimleri için normal döviz çifti sözleşmesi olacaktır. EUR ve GBP baz para birimleri açısından daha baskın olduğu için dolar EUR / USD ve GBP / USD cinsinden işlem görmektedir. Bir Döviz Alım Satım işlemi yapıldığında takas edilen para birimlerinin (kavramsal veya gerçek) değerlerini belirlediğinden, temel para birimini bilmek önemlidir. Karşı döviz, bir döviz çifti gösterimi içindeki ikinci para birimidir.
            <br>
        </p>
        <br>

        <h2 class="SectionSubTitle">FX PAZAR KATILIMCILARI</h2>
        <p>
            FX piyasasında birçok farklı katılımcı türü vardır ve ticaret yaparken sıklıkla çok farklı sonuçlar ararlar. Bu yüzden FX genellikle \'sıfır toplamlı\' bir oyun olarak tanımlanmasına rağmen - bir yatırımcının yaptığı şey teorik olarak bir başkasının kaybettiklerine eşittir - para kazanmak için çok sayıda fırsat vardır. FX, herkesin iyi bir yemek yiyebileceği bir pasta olarak düşünülebilir. </p>
        <p>
            Geleneksel olarak, bankalar döviz piyasasında ana katılımcılar olmuştur. Pazar payı açısından hala en büyük oyuncular olmaya devam ediyorlar, ancak şeffaflık FX pazarını daha demokratik hale getirdi. Şimdi, neredeyse herkes, bankalar arası piyasada kote edilen aynı, son derece dar fiyatlara erişebiliyor. Böylece, bankalar döviz piyasasında ana oyuncular olmaya devam etmektedir, ancak son on yılda riskten korunma fonu ve emtia ticareti danışmanları gibi yeni bir piyasa yapıcı türü ortaya çıkmıştır. </p>
        <p>
            Merkez Bankaları da YP piyasasında önemli bir rol oynayabilirken, uluslararası şirketler YP riskine maruz kalmaları nedeniyle alım satıma doğal bir ilgi duymaktadırlar.
        </p>
        <p>
            Perakende FX son on yılda hızlı bir şekilde genişledi ve kesin rakamlara ulaşmak zor olsa da, bu sektörün döviz piyasasının% 20\'sini temsil ettiğine inanılıyor.
        </p>

    </div>






        ',



        ],

];
