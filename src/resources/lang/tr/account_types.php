<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'steps'  => 'Hesap açmak için 4 adım ',
        'step1'  => 'ADIM 01',
        'step1desc' => 'Bilgilerinizi girin',
        'step2'  => 'ADIM 02',
        'step2desc' => 'Kimliğinizi yükleyin',
        'step3'  => 'ADIM 03',
        'step3desc' => 'Hesabınıza para yatırın',
        'step4'  => 'ADIM 04',
        'step4desc' => 'İşlem yapmaya başla',
        'register' => 'Kaydol',


        'minacc' => 'Mini Hesap',
        'stnacc' => 'Standart Hesap',
        'mindep' => 'Minimum depozito',
        'maxbal' => 'Maksimum bakiye',
        'sprdfrom' => 'Şpredler',
        'minlot' => 'Minimum lot büyüklüğü',
        'stnlot' => 'Standart lot büyüklüğü',
        'acctype' => 'Hesap Türü',
        'kaldıraç' => 'Kaldıraç',
        'minacctype' => 'Bireysel Hesap',
        'stnacctype' => 'Bireysel Hesap, Ortak Hesap, Kurumsal Hesap',
        'vipacc' => 'VIP Hesabı',
        'proacc' => 'Pro-ECN Hesabı',
        'vipacctype' => 'Bireysel Hesap, Ortak Hesap, Kurumsal Hesap',
        'proacctype' => 'Bireysel Hesap, Ortak Hesap, Kurumsal Hesap',


        'requiredoc' => 'Hesap için Gerekli Belgeler',
        'indvacc' => 'Bireysel Hesap',
        'indvacc1' => '1. Pasaport veya devlet tarafından verilmiş kimlik (hem ön hem de arka kopyaları). ',
        'indvacc2' => '2. İkamet adresi kanıtı (son 3 ay içinde telefon faturaları, kredi kartı faturaları veya sigorta faturaları gibi faturalar). ',
        'jointacc' => 'Ortak Hesap',
        'jointacc1' => '1. Pasaport veya devlet tarafından verilmiş kimlik (hem ön hem de arka kopyaları). ',
        'jointacc2' => '2. İkamet adresi kanıtı (son 3 ay içinde telefon faturaları, kredi kartı faturaları veya sigorta faturaları gibi faturalar). ',
        'jointacc3' => '3. İkinci Pasaport veya devlet tarafından verilmiş kimlik (hem ön hem de arka kopyaları). ',
        'jointacc4' => '4. İkinci ikamet adresi belgesi (telefon faturaları, kredi kartı faturaları veya sigorta faturaları gibi son 3 ay içindeki faturalar). ',
        'coacc' => 'Kurumsal Hesap',
        'coacc1' => '1. Tescil Belgesi (kopya) ',
        'coacc2' => '2. Şirket adresinin kanıtı (son 3 ay içinde telefon faturaları, banka ekstresi veya faturalar gibi faturalar)',
        'coacc3' => '3. Yönetim kurulu üyeleri ve% 10\'dan fazla hissedar. ',
        'coacc4' => '4. Onaylı pasaport veya onaylı resmi kimlik belgesi ve bölüm 3\'te listelenen her bir yönetici ve hissedar için ikamet adresi kanıtı. ',
        'coacc5' => '5. Güven durumunda, lütfen güven belgesinin onaylı bir kopyasını da yükleyin. ',

    ],

];
