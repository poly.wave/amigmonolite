<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'AMIG Yöneticisi',
    'failed' => 'Bu kimlik bilgileri kayıtlarımızla eşleşmiyor.',
    'throttle' => 'Çok fazla giriş denemesi. Lütfen birkaç saniye içinde tekrar deneyin. ',
    'login' => 'GİRİŞ',
    'loginbttn' => 'Giriş',
    'logindesc' => 'Hesabınızda oturum açın',
    'forgotpwd' => 'Şifrenizi mi unuttunuz?',
    'regbttn' => 'Kaydol',
    'regnowbttn' => 'Şimdi Kayıt Ol!',
    'signup' => 'Kaydol',
    'singupdesc' => 'Bir hesabınız yoksa Sadece Kaydolun.',

    'regform' => 'Formu kaydet',
    'loginp' => 'GİRİŞ SAYFASI',
    'loginpdesc' => 'Hesabınız varsa sadece giriş yapın.',
    'newacc' => 'Hesabınızı oluşturun',
    'createacc' => 'Hesap Oluştur',

    'emailaddress' => 'E-posta Adresi',
    'password' => 'Şifre',
    'remember' => 'Beni hatırla',

];
