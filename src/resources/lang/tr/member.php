<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Member Language Lines
    |--------------------------------------------------------------------------
    */
    'header'  => [
        'title'  => 'AMIG Üye alanı',
        'account'  => 'HESAP',
        'settings'  => 'AYARLAR',
        'logout'  => 'ÇIKIŞ',
        'lang'  => [
            'en'  => 'Eng',
            'ru'  => 'Рус',
            'tr'  => 'Türk',
        ],
    ],

    'footer'  => [
        'company_fx'  => 'AMIG Forex',
        'company'  => 'AMIG',
        'powered_by'  => 'Destek',
    ],

    'sidebar'  => [
        'balance'  => 'Bakiye:',
    ],

    'main'  => [
        'title'  => 'Ana ',
        'balance'  => 'BAKİYE',
        'withdrawals'  => 'PARA ÇEKİMİ',
        'earnings'  => 'KAZANÇ',
        'deposits'  => 'YATIRIM',
        'stat'  => 'Genel istatistik:',
    ],
    'trading'  => [
        'title'  => 'Ticaret',
        'trdacc'  => 'Hesap listesi',
        'account'  => 'HESAP',
        'platform'  => 'PLATFORMA',
        'leverage'  => 'KALDIRAÇ',
        'mtaccount'  => 'MT5 HESABI',
        'pwd'  => 'ŞİFRE',
        'currency'  => 'PARA BİRİMİ ',



    ],
    'deposit'  => [
        'title'  => 'Fonlama',
        'banktransfer'  => 'EFT/Havale',
        'bycreditcard'  => 'Banka Kartıyla',
        'cryptocoins'  => 'Şirket Kripto Cüzdanları',
        'bank_transfer' => [
            'title'  => 'Banka Hesapları',
            'bank_name'  => 'BANKA ADI',
            'accunt_name'  => 'HESAP ADI',
            'iban'  => 'IBAN',
            'swift'  => 'SWIFT Kodu',
            'bank_address'  => 'BANKA ADRESİ',
            'currency'  => 'PARA BİRİMİ',
            ],
        'credit_card' => [
            'well'  => 'Aferin!',
            'wrong'  => 'Bir şeyler ters gitti!',
            'complete_error' => 'Tüm alanların doldurulması zorunludur',
            'ctitle'  => 'Tam ad  (kartta)',
            'fullname'  => 'AD SOYADI',
            'cardno'  => 'Kart numarası',
            'cardnumber'  => 'KART NUMARANIZ',
            'mm'  => 'AA',
            'yy'  => 'YY',
            'exp'  => 'Son Kullanım Tarihi',
            'cvv'  => 'CVV',
            'confirm'  => 'Onayla',
            ],
        'crypto_coins' => [
            'title'  => 'Şirket Kripto Para Adresleri',
            'coin_name'  => 'Kripto para birimi',
            'coin_wallet'  => 'Cüzdan adresi',
        ],
    ],
    'withdrawal'  => [
        'title'  => 'PARA ÇEKME',
        'purpose'  => 'Not',
        'purposelable'  => 'Not',
        'withamount'  => 'Tutarı giriniz',
        'withamountlable'  => 'Tutar',
        'withcurr'  => 'Tercih edilen para birimi',
        'withtype'  => 'Para çekme türü',
        'well'  => 'Mükemmel!',
        'wrong'  => 'Bir şeyler ters gitti!',
        'reqsucc'  => 'İsteğiniz başarıyla gönderildi',
        'create'  => 'Onayla',

    ],
    'report'  => [
        'title'  => 'RAPOR',
        'deposit'  => 'Yatırım',
        'earning'  => 'Kazanç',
        'withdrawal'  => 'Para çekimi',
        'deposits'  => [
            'title'  => '',
            'name'  => 'ADI',
            'amount'  => 'TUTAR',
            'date_time'  => 'TARİH',
            'currency'  => 'PARA BİRİMİ',
        ],
        'earnings'  => [
            'title'  => '',
            'name'  => 'ADI',
            'amount'  => 'TUTAR',
            'currency'  => 'PARA BİRİMİ',
            'date_time'  => 'TARİH',
        ],
        'withdrawals'  => [
            'title'  => 'Para çekme',
            'name'  => 'ADI',
            'amount'  => 'TUTAR',
            'currency'  => 'PARA BİRİMİ',
            'date_time'  => 'TARİH',
            'type'  => 'ÇEKİM TÜRÜ',
            'approve'  => 'DRUM',
        ],

    ],
    'profile'  => [
        'title'  => 'PROFİL',
        'personal_information'  => [
            'avatar'  => 'Avatarı yenile',
            'title'  => 'Kişisel Bilgiler',
            'email'  => 'E-posta Adresi',
            'emailpl'  => 'ör. example@example.com',
            'name'  => 'Adınız',
            'namepl'  => 'Ad',
            'sname'  => 'Soyadınız',
            'snamepl'  => 'Soyadı',
            'dofb'  => 'Doğum Günü',
            'dofbpl'  => 'gg.aa.yyyy',
            'phone'  => 'Telefon Numaras',
            'phonepl'  => 'ör. +1 (111) 111 11 11',
            'gender'  => 'Cinsiyet',
            'preflang'  => 'Tercih Edilen Dil',
            'prefcurr'  => 'Tercih Edilen Para Birimi',
            'country'  => 'Ülke',
            'region'  => 'Bölge',
            'city'  => 'Şehir',
            'resident'  => 'Yerleşik Adres',
            'well'  => 'Mükemmel!',
            'updated'  => 'Değişiklikleriniz başarıyla güncellendi',
            'button_update'  => 'Güncelle',
        ],
        'wallet'  => [
            'title'  => 'Cüzdan',
            'banks'  => [
                'title'  => 'Banka Hesapları',
                'bankname'  => 'BANKA ADI',
                'address'  => 'BANKA ADRESİ',
                'iban'  => 'IBAN',
                'swift'  => 'SWIFT',
                'currency'  => 'PARA BİRİMİ',
                'well_done'  => 'Mükemmel!',
                'created'  => 'Yeni banka hesabınız başarıyla kayıt olundu',
                'bank_details'  => 'Banka Bilgileriniz',
                'button_add'  => 'EKLE',
            ],
            'cards'  => [
                'title'  => 'Banka Kartları',
                'ctitle'  => 'Tam ad (kartta)',
                'fullname'  => 'AD SOYAD',
                'cardno'  => 'Kart numarası',
                'cardnopl'  => 'ör. 1234-5678-9012-3456',
                'mm'  => 'AA',
                'mmm'  => 'Ay',
                'yy'  => 'YY',
                'yyy'  => 'Yıl',
                'currency'  => 'PARA BİRİMİ',
                'help'  => 'Kartınızın arkasındaki üç basamaklı kod',
                'cvv'  => 'CVV',
                'well'  => 'Mükemmel!',
                'well1'  => 'İsteğiniz başarıyla gönderildi',
                'wrong'  => 'Bir şeyler ters gitti!',
                'card_created'  => 'Yeni kredi kartı başarıyla oluşturuldu',
                'button_add'  => 'EKLE',
                'creditcard'  => 'Banka Kartlarınız',
            ],
            'crypto_wallets'  => [
                'title'  => 'Kripto Cüzdanları',
                'crtitle'  => 'Kripto Para Adresleri',
                'crwallet'  => 'Cüzdan adresi',
                'crpref'  => 'Tercih Edilen Para',
                'crname'  => 'Kripto Adı',
                'well'  => 'Mükemmel!',
                'crypto_wallet'  => 'Kripto Cüzdanlarınız',
                'wallet_created'  => 'Yeni kripto para cüzdanı başarıyla oluşturuldu',
                'wrong'  => 'Bir şeyler ters gitti!',
                'button_add'  => 'EKLE',

                ],
        ],
        'reset_password'  => [
            'title'  => 'Parolayı Sıfırla',
            'pwdold'  => 'Mevcut şifre',
            'pwdold1'  => 'Eski şifre',
            'pwdnew'  => 'Yeni şifre',
            'pwdnew1'  => 'Yeni şifre',
            'pwdnew2'  => 'Tekrar yeni şifre',
            'incorrect'  => 'Yanlış eski şifre',
            'well'  => 'Mükemmel!',
            'updated'  => 'Parolanız başarıyla güncellendi',
            'wrong'  => 'Bir şeyler ters gitti!',
            'button_update'  => 'GÜNCELLE',
        ],
        'files'  => [
            'title'  => 'Dosyalar',
            'well'  => 'Mükemmel!',
            'updated'  => 'Dosyalarınız başarıyla yüklendi',
            'button_update'  => 'YÜKLE',
        ],
    ],


      'usersettings'  => 'Ayarlar',


    ];
