<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '

      <h2 class="SectionSubTitle">TEMEL ANALİZ</h2>
        <p>
            <strong>Temel Analizi Anlama</strong> <br>
            Döviz piyasalarını analiz etmek için iki temel yaklaşım temel analiz ve teknik analizdir. Temel unsurlar, arz ve talep güçlerini belirlemek için finansal ve ekonomik teorilerin yanı sıra politik gelişmelere odaklanmaktadır. Temeller ve teknikler arasındaki açık farklardan biri, temel analizin piyasa hareketlerinin nedenlerini, teknik analiz ise piyasa hareketlerinin etkilerini incelemesidir.
            Temel analiz, bir ülkenin para birimini diğerine göre değerlendirirken makroekonomik göstergelerin, varlık piyasalarının ve politik hususların incelenmesini içerir. Makro ekonomik göstergeler, Gayri Safi Yurtiçi Hasıla ile ölçülen büyüme oranları, faiz oranları, enflasyon, işsizlik, para arzı, Döviz rezervleri ve verimlilik gibi rakamları içermektedir. Varlık piyasaları hisse senetleri, tahviller ve mallardan oluşur. Siyasi düşünceler bir ülkenin hükümetine olan güven düzeyini, istikrar ortamını ve kesinlik düzeyini etkiler.
            Bazen hükümetler, para birimlerinin istenmeyen seviyelerden belirgin şekilde sapmasını önlemek için müdahale ederek piyasa güçlerinin para birimlerini etkilemesinin önünde dururlar. Döviz müdahaleleri Merkez Bankaları tarafından yürütülür ve döviz piyasaları üzerinde geçici bir etkisi olmakla birlikte genellikle dikkate değerdir. Bir Merkez Bankası, para biriminin başka bir para birimine karşı tek taraflı alım / satımını yapabilir ya da daha belirgin bir etki için diğer Merkez Bankalarıyla işbirliği yaptığı uyumlu bir müdahaleye girebilir. Alternatif olarak, bazı ülkeler para birimlerini sadece müdahaleyi ima ederek veya tehdit ederek hareket ettirmeyi başarabilirler.Aşağıdaki ABD ekonomik göstergelerinin bir listesi:</p>
        <p>
            <strong>Cari Hesap</strong> <br>
            Cari hesap, satış ve satın alınan mallar, hizmetler, faiz ödemeleri ve tek taraflı transferlerin en geniş ölçüsü olduğundan uluslararası ticaret verilerinin önemli bir parçasıdır. Dış ticaret bakiyesi cari hesapta yer almaktadır. Genel olarak, cari işlemler açığı para birimini zayıflatabilir.</p>
        <p>
            <strong>Ticaret Dengesi</strong> <br>
            Ticaret dengesi, bir ülkenin ihracatı ile mal ithalatı arasındaki farkı yansıtır. Bir ülkenin ihracatı ithalatı aştığında, pozitif bir ticaret dengesi veya fazla gelir oluşur. İhracattan daha fazla mal ithal edildiğinde negatif bir ticaret dengesi veya bir açık oluşur.
            Ticaret dengeleri Forex\'teki oyuncular tarafından etkilenebilecekleri için yakından takip edilmektedir. Genellikle bir ülke veya bölge ekonomisindeki genel ekonomik faaliyetin bir değerlendirmesi olarak kullanılır. İhracat faaliyetleri sadece söz konusu ülkenin rekabetçi konumunu değil, aynı zamanda yurtdışındaki ekonomik faaliyetin gücünü de yansıtmaktadır. İthalat faaliyetindeki eğilimler yurtiçi ekonomik faaliyetin gücünü yansıtmaktadır.
            Dış ticaret dengesi açığı belirgin olan bir ülke genellikle zayıf bir para birimine sahiptir. Ancak, bu, içine giren önemli finansal yatırımlarla dengelenebilir. </p>
        <p>
            <strong>Dayanıklı Tüketim Malları Endeksi</strong> <br>
            Dayanıklı tüketim malları siparişleri, yerli sert üreticilerin derhal ve gelecekteki teslimatı için yerli üreticilere verilen yeni siparişlerin bir ölçüsüdür. Aylık yüzde değişiklikleri, bu siparişlerin değişim oranını yansıtır.
            Dayanıklı tüketim malları sipariş endeksi imalat sanayi trendlerinin önemli bir göstergesidir. Yükselen dayanıklı mal siparişleri normalde daha güçlü ekonomik faaliyetlerle ilişkilidir ve genellikle bir para birimi için destekleyici olan kısa vadeli yüksek faiz oranlarına yol açabilir.</p>
        <p>
            <strong>Gayri Safi Yurtiçi Hasıla</strong> <br>
            Gayri safi yurtiçi hasıla (GSYİH), mevcut toplam ekonomik faaliyetin en geniş ölçüsüdür. Bir ülkede üretilen tüm mal ve hizmetlerin piyasa değerinin bir göstergesidir. GSYİH üç ayda bir raporlanmakta ve ekonomik aktivitenin gücünün birincil göstergesi olduğu için çok yakından takip edilmektedir.
            GSYİH raporunun üç sürümü vardır: 1) önceden tahliye (ilk olarak); 2) ön tahliye (1. revizyon); ve 3) son sürüm (2. ve son revizyon). Bu revizyonların genellikle piyasalar üzerinde önemli bir etkisi vardır.
            Yüksek GSYİH rakamını, genellikle enflasyonist baskılar olmadıkça, en azından kısa vadede, ilgili para birimi için çoğunlukla olumlu olan yüksek faiz oranlarının beklentileri izler.
            GSYİH rakamlarına ek olarak, toplam GSYİH\'deki fiyat değişimlerini ve her bir bileşen için GSYİH deflatörleri vardır. GSYİH deflatörleri TÜFE ile birlikte bir diğer önemli enflasyon ölçümüdür. TÜFE\'nin aksine, GSYİH deflatörleri sabit bir mal ve hizmet sepeti olmama avantajına sahiptir, bu da tüketim modellerindeki değişikliklerin veya yeni mal ve hizmetlerin tanıtımının deflatörlere yansıtılacağı anlamına gelir. </p>
        <p>
            <strong>Konut Başlıyor</strong> <br>
            Konut başlar, her ay konut birimlerinin (evler ve daireler) ilk inşaatını ölçmektedir. Konut başlangıçları, ekonomideki genel duyarlılığın bir göstergesini verdiği için yakından izlenmektedir. Yüksek inşaat faaliyeti genellikle artan ekonomik aktivite ve güven ile ilişkilidir ve daha yüksek kısa vadeli faiz oranlarını tahmin edebilir. </p>
        <p>
            <strong>Tüketici Fiyat Endeksi</strong> <br>
            Tüketici Fiyat Endeksi (TÜFE) enflasyonun bir ölçüsüdür. Tüketiciler tarafından satın alınan sabit bir mal ve hizmet sepetinin ortalama fiyat seviyesini alır.
            Tüketici harcamaları ekonomik faaliyetlerin yaklaşık üçte ikisini oluşturduğundan TÜFE birincil enflasyon göstergesidir. Yükselen TÜFE\'yi genellikle kısa vadede bir para birimi için destekleyici olabilecek yüksek kısa vadeli faiz oranları izler. Ancak, enflasyon uzun vadeli bir sorun haline gelirse, para birimine olan güven nihayetinde zayıflar ve zayıflar.</p>
        <p>
            <strong>Bordro İstihdamı</strong> <br>
            Bordro İstihdamı raporu (İşçi raporu olarak da bilinir) şu anda ABD\'nin tüm ekonomik göstergelerinin en önemlisi olarak kabul edilmektedir. Genellikle ayın ilk Cuma günü yayınlanır. Rapor ekonomiye kapsamlı bir bakış sağlar ve tarım dışı işyerleri ve hükümet birimleri tarafından çalışan olarak ödenen insan sayısının bir ölçüsüdür. Bordro istihdamındaki aylık değişiklikler, ay içinde yaratılan veya kaybedilen net yeni iş sayısını yansıtır. Bordro İstihdam raporu, ekonomik faaliyetin önemli bir göstergesi olarak yaygın bir şekilde takip edilmektedir.
            Bordro istihdamındaki büyük artışlar, en azından kısa vadede genellikle para birimini destekleyen yüksek faiz oranlarına yol açabilecek güçlü ekonomik faaliyet belirtileri olarak kabul edilir. Bununla birlikte, enflasyonist baskının arttığı tahmin ediliyorsa, bu, para birimine olan uzun vadeli güveni zayıflatabilir. </p>
        <p>
            <strong>Üretici Fiyat Endeksi</strong> <br>
            Üretici Fiyat Endeksi, toptan eşya fiyatlarındaki aylık değişimi ölçer ve emtia, endüstri ve üretim aşamasına göre ayrılır.
            ÜFE, imalat sektöründeki fiyat değişimlerini ölçtüğü için önemli bir enflasyon göstergesi vermektedir - ve üretici düzeyinde enflasyon genellikle doğrudan tüketicilere iletilmektedir. </p>
    </div>



        ',

    ],





];
