<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mt'  => 'Meta Trader 5',
        'mtintro'  => 'AMIGFX MetaTrader 5 ile tanışın',
        'mtintro1'  => 'Daha fazla güç. Daha fazla hassasiyet. Daha fazla özellik.',
        'mtintro2'  => 'Şimdi AMIGFX\'te mevcut, bugün ücretsiz indirin!',
        'mtintro3'  => 'AMIGFX MT5, yatırımcıların AMIGFX MT4\'leri hakkında sevdikleri her şeyi, ancak birkaç ekstra geliştirmeyle sunar. Daha fazla güç. Daha fazla hassasiyet. Daha fazla seçenek.',
        'mtintro4'  => 'Yeni eklenen özellikler ve teknik yükseltmelerle, AMIGFX MT5 platformu size pazarlarda almanız gereken tüm araçları sağlayacaktır.',
        'opendemo'  => 'DEMO Hesab aç',
        'register'  => 'REAL Haesab açin',
        'downmtwin'  => 'MetaTrader5 İndir (WINDOWS)',
        'downmtmac'  => 'MetaTrader5 İndir (MAC OS)',
        'why'  => 'Neden AMIG Forex MetaTrader 5\'i seçmelisiniz?',
        'why1'  => 'Entegre hepsi bir arada çok işlevli FX işlem platformu',
        'why2'  => 'Yerleşik MetaTrader Pazarı - Robotların ve teknik göstergelerin en büyük çevrimiçi mağazası',
        'why3'  => 'Gelişmiş web ticareti - herhangi bir tarayıcıdan, herhangi bir cihazdan ticaret yapın',
        'why4'  => 'Üstün kopya ticareti (sinyaller), serbest veritabanı ve Sanal Hosting',
        'why5'  => 'AMIGFX MT5 uzmanından 24 saat uzman desteği',
        'features'  => 'AMIGFX MetaTrader 5\'in özellikleri',
        'benefits'  => 'AMIGFX MetaTrader 5\'in Faydaları',
        'benefits1'  => 'Verimliliği Artırın',
        'benefits2'  => 'Gelişmiş Otomatik Ticaret',
        'benefits3'  => 'Artan Doğruluk',


    ],




];
