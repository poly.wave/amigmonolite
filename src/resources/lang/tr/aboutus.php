<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '2017 yılında kurulan AMIG Fx hızla en büyük FOREX ve CFD brokerlerinden birine dönüştü. Yatırım aracılık pazarını genişletmek ve yaygınlaştırmak için tüm yatırımcılara yeni ve farklı uygulamalarımızdan faydalanmalarını sağlıyoruz.',
        '2' => 'Müşterilerimiz, en son teknolojileri kullanan gelişmiş çevrimiçi forex ticaret platformlarımız sayesinde ihtiyaç duyulan tüm anlık finansal verilere, piyasa analizlerine ve diğer bilgilere hızlı ve güvenli bir şekilde erişebilir ve işleyebilir',
        '3' => 'Müşterilerimize AMIG Fxte analiz ve değerlendirme sürecine daha fazla katkıda bulunmaları, potansiyel bir sorunu yerinde çözmeleri ve kesintisiz destek için özel bir yatırım hizmeti veriyoruz.',
        '4' => 'Gelişmiş CRM sistemimizle her müşteriye daha profesyonel ve daha hızlı hizmet veriyoruz.',
        '5' => 'Farklı Ürün Sunumu',
        '6' => 'Özel ticaret platformu ve MetaTrader 5 ile kullanımı kolay rekabetçi ticaret koşulları sunuyor. Müşteriler, sektördeki en popüler ve güvenilir ticaret platformlarından biri olan MetaTrader 5 ile işlem yapabilirler.',
        '7' => 'Endeksler, CFDler, altın, gümüş, petrol ve diğer emtiaların yanı sıra 60 tan fazla döviz çifti için ticaret hizmeti veriyoruz.',
        '8' => 'AMIG Fx, müşteri maliyetlerini piyasadaki en rekabetçi fiyatlarla düşürerek yatırımcılar için en iyi ticaret koşullarını yaratıyor.',
        '9' => 'AMIG Fx ile aynı zamanda ECN hesap türünü sunuyoruz.',
        '10' => 'Yüksek teknoloji yatırım altyapıları ve dünyanın önde gelen likidite sağlayıcılarından alınan fiyatlar ile yatırımcılar işlemlerini şeffaf, gündüz ve gece kesintisiz, geniş ürün yelpazesi en aza indirilmiş fiyat dağılımı ile gerçekleştirebiliyorlar. sapma ve reddedilen sipariş istatistikleri. ',
        '11' => 'Rekabetçi Farklar',
        '12' => 'AMIG Fx, 5 basamaklı fiyatlandırmamızla sektördeki en dar spreadleri sunuyor. Müşteriler 1: 200 e kadar kaldıraçtan ve mini lotlardan ve mikro lotlardan yararlanırlar. Anlaşma-yürütme sistemlerimiz tamamen otomatiktir; işlem masası yok, dolayısıyla insan müdahalesi yok. ',


    ],


];
