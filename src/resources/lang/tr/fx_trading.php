<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '

 <h2 class="SectionSubTitle">FX ticaretinin avantajları</h2>
        <p>
        Forex piyasası dünyadaki en büyük ve en likit olmasına rağmen, günlük işlemciler şimdiye kadar çoğunlukla hisse senedi ve vadeli işlem piyasalarında kar aramaya odaklanmıştır.
      Bu, temel olarak banka tarafından sunulan Forex ticaret hizmetlerinin kısıtlayıcı doğasından kaynaklanmaktadır.
            </p>
        <p>

Ticaret hisse senetleri ve vadeli işlemlerin aksine, döviz ticareti yapmanın birçok avantajı vardır. Başlıca avantajları aşağıda listelenmiştir.
        </p>

        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">24 Saatlik Pazar
        </h2>
        <p>
           FX hiç uyumayan küresel bir pazardır. Haftanın 7 günü 24 saat aktiftir. Etkinliklerin çoğu Yeni Zelanda pazarının Avrupa\'da Pazar akşamı Pazartesi günü açıldığı ve ABD pazarının Cuma akşamı kapanacağı saatler arasında gerçekleşiyor.

        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">Likidite</h2>
        <p>
            FX piyasası çok büyük ve hala genişliyor. Günlük ortalama hacim 3.2 trilyon doları aştı. Teknoloji bu pazarı hemen hemen herkes için erişilebilir hale getirdi ve perakende tüccarlar FX\'e akın etti.

        </p>
        <div class="divider divider-3"></div>
        <h2 class="SectionSubTitle">Kaldıraç</h2>
        <p>
          Yabancı para marj oranları özkaynaklardan daha yüksek olma eğilimindedir çünkü daha likittir - neredeyse her zaman döviz cinsinden bir fiyat vardır - ve daha az uçucu olma eğilimindedir.
          </p>
    </div>



        ',

    ],


];
