<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title' => 'AMIG  - Авторизация пользывателья',
    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, попробуйте снова через :seconds seconds.',
    'login'  => 'Авторизация',
    'loginbttn'  => 'ВОЙТИ',
    'logindesc'  => 'Войдите в свой аккаунт',
    'forgotpwd'  => 'Забыли пароль?',
    'regbttn'  => 'Зарегистрироваться',
    'regnowbttn'  => 'Зарегистрируйтесь сейчас!',
    'signup'  => 'Зарегистрироваться',
    'singupdesc'  => 'Если у вас нет учетной записи, просто зарегистрируйтесь',

    'regform'  => 'Форма регистрации',
    'loginp'  => 'ВОЙТИ',
    'loginpdesc'  => 'Если у вас есть аккаунт, просто войдите.',
    'newacc'  => 'Создать аккаунт',
    'createacc'  => 'Создать аккаунт',

    'emailaddress'  => 'Электронная почта',
    'password'  => 'Пароль',
    'remember'  => 'запомнить меня',
    
];
