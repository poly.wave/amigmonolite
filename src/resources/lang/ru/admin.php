<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'AMIG Администратор',
    'failed' => 'Эти учетные данные не соответствуют нашим записям.',
    'throttle' => 'Слишком много попыток входа в систему. Пожалуйста, повторите попытку через: секунды, секунды. ',
    'login' => 'Авторизоваться',
    'loginbttn' => 'Вход',
    'logindesc' => 'Войдите в свой аккаунт',
    'forgotpwd' => 'Забыли пароль?',
    'regbttn' => 'Регистрация',
    'regnowbttn' => 'Зарегистрируйтесь сейчас!',
    'signup' => 'Зарегистрироваться',
    'singupdesc' => 'Если у вас нет учетной записи, просто зарегистрируйтесь.',

    'regform' => 'Форма регистрации',
    'loginp' => 'Страница авторизации',
    'loginpdesc' => 'Если у вас есть аккаунт, просто войдите.',
    'newacc' => 'Создать аккаунт',
    'createacc' => 'Создать аккаунт',

    'emailaddress' => 'Адрес электронной почты',
    'password' => 'Пароль',
    'remember' => 'Запомнить меня',

];
