<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
  <p><strong>Условия использования</strong></p>


        <p>
            Внимательно прочитайте эти положения и условия. Обращаясь к этому Всемирному веб-сайту ("Веб-сайт") и любым страницам настоящего Соглашения, вы указываете, что вы прочитали, подтверждаете и соглашаетесь быть связанными настоящими Условиями использования. Если вы не согласны с настоящими Условиями использования, не обращайтесь к этому веб-сайту. Компания AMIG Capital ("AMIG") оставляет за собой право изменять настоящие Условия использования, которые Вы несете за регулярное рассмотрение, и Ваше дальнейшее использование данного веб-сайта является согласием на все такие изменения.
        </p>
        <div class="divider divider-1"></div>




 ',



    ],


];
