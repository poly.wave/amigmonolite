<?php

return [
    /*
    |--------------------------------------------------------------------------
    | USER Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    /*  */



    /*AuthenticatedController*/


    'main'  => 'ГЛАВНАЯ',
    'trading'  => 'ТОРГОВЛЯ',
    'depositby'  => 'ДЕПОЗИТ',
    'banktransfer'  => 'Банковский перевод',
    'creditcard'  => 'Кредитная карта',
    'cryptocoins'  => 'Криптовальюты',
    'withdrawal'  => 'СНЯТИЕ',
    'report'  => 'Репорт',
    'deposits'  => 'Вклады',
    'earnings'  => 'Доход',
    'withdrawals'  => 'Снятие',
    'profile'  => 'ПРОФАЙЛ',
    'personalinfo'  => 'Личная информация',
    'wallet'  => 'Кошелек',
    'reset'  => 'Сбросить пароль',
    'banks'  => 'Банковские счета',
    'creditcards'  => 'Кредитные карты',
    'cryptowallets'  => 'Crypto Wallets',
    'files'  => 'Файлы',
    'usersettings'  => 'Настройки пользователя',








];
