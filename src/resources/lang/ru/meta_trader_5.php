<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mt'  => 'Meta Trader 5',
        'mtintro'  => 'Представляем AMIGFX MetaTrader 5',
        'mtintro1'  => 'Больше власти. Больше точности. Больше особенностей.',
        'mtintro2'  => 'Вы можете бесплатно скачать его прямо сейчас на сайте AMIGFX!',
        'mtintro3'  => 'AMIGFX MT5 поставляет все, что трейдеры стали любить о своих AMIGFX MT5 но с несколькими дополнительными усовершенствованиями.',
        'mtintro4'  => 'С новыми функциями и техническими обновлениями платформа AMIGFX MT5 предоставит вам все инструменты, которые вам потребуется взять на рынок.',
        'opendemo'  => 'Открытый демо-счет',
        'register'  => 'Открытый реальный-счет',
        'downmtwin'  => 'Загрузить MetaTrader5 (WINDOWS)',
        'downmtmac'  => 'Загрузить MetaTrader5 (MAC OS)',
        'why'  => 'Почему выбрать AMIGFX MetaTrader 5',
        'why1'  => 'Интегрированная многофункциональная торговая платформа FX ',
        'why2'  => 'Встроенный MetaTrader Market - крупнейший интернет-магазин роботов &  технические индикаторы ',
        'why3'  => 'Расширенная веб-торговля - торгует от любого браузера, любого устройства ',
        'why4'  => 'Превосходная торговля копиями (сигналы), внештатная база данных и виртуальный хостинг',
        'why5'  => 'Круглосуточная экспертная поддержка от специалиста AMIGFX MT5 ',
        'features'  => 'Особенности AMIGFX MetaTrader 5',
        'benefits'  => 'Преимущества AMIGFX MetaTrader 5',
        'benefits1'  => 'Увеличьте производительность',
        'benefits2'  => 'Передовая автоматизированная торговля',
        'benefits3'  => 'Увеличенная точность',


    ],




];
