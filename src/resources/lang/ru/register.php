<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Register Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    /* Personal info*/
    'email'  => 'Электронная почта',
    'emailpl'  => 'example@example.com',
    'pwdnew'  => 'Создать пароль',
    'pwdnew1'  => 'Пароль',
    'pwdnew2'  => 'Поддтверждение паролья',
    'name'  => 'Ваше Имя',
    'namepl'  => 'Имя',
    'sname'  => 'Фамилия',
    'snamepl'  => 'Фамилия',
    'dofb'  => 'Дата рождения',
    'dofbpl'  => 'дд.мм.гггг',
    'phone'  => 'Номер мобильного телефона',
    'phonepl'  => 'e.g. +1 (111) 111 11 11',
    'gender'  => 'Пол',
    'preflang'  => 'Предпочитаемый язык',
    'prefcurr'  => 'Предпочитаемая валюта',
    'country'  => 'Страна',
    'region'  => 'Регион',
    'city'  => 'Город',
    'resident'  => 'Место проживание ',
    'agree'  => 'Я прочитал и согласен с',
    'agreename'  => 'Декларация о приверженности и ответственности',






];
