<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Layouts Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'title'  => 'AMIG - Инвестиционно-финансовый консалтинг ',
    /*
    Menu
    */
    'main'  => 'Главная',
    'about'  => 'О нас',
    'projects'  => 'Проекты',
    'contacts'  => 'Контакты',
    /*
    Main
    */
    'keep'  => 'На связи',
    'name'  => 'Имя',
    'gobttn'  => 'Далее',
    /*
    Contacts
    */
    'office'  => 'Главной офис',
    'phone'  => 'Номер телефона',
    'email'  => 'Электронная поста',
    'address'  => 'Адресс',
    /*
    Contact Form
    */
    'cfhead' => 'Контактная форма',
    'cfdesc' => 'Мы готовы ответить на ваш вопрос.',
    'fname' => 'Имя',
    'lname' => 'Фамилия',
    'femail' => 'Адрес электронной почты',
    'mobile' => 'Номер телефона',
    'message' => 'Текст',
    'sendbtn' => 'Отправить',

    /*
     Slider
    */
    'finance'  => 'Финансы',
    'fxcrypto'  => 'eТорговля',
    'innovation'  => 'Инновация',
    'invest'  => 'Инвестиция',
    /*
     Footer
    */
    'co'  => 'AMIG Capital LLC',





];
