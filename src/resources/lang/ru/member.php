<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Member Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header'  => [
        'title'  => 'AMIG Members Area',
        'account'  => 'СЧЁТ',
        'settings'  => 'НАСТРОЙКИ',
        'logout'  => 'ВЫЙТИ',
        'lang'  => [
            'en'  => 'English',
            'ru'  => 'Русский',
            'tr'  => 'Türkçe',
        ],
    ],

    'footer'  => [
        'company_fx'  => 'AMIG Forex',
        'company'  => 'AMIG Capital LLC',
        'powered_by'  => 'Все права защищены.',
    ],

    'sidebar'  => [
        'balance'  => 'Средства:',
    ],

    'main'  => [
        'title'  => 'ГЛАВНАЯ',
        'balance'  => 'СРЕДСТВА',
        'stat'  => 'Общая статистика:',
        'withdrawals'  => 'СНЯТИЕ',
        'earnings'  => 'ЗАРАБОТОК',
        'deposits'  => 'ДЕПОЗИТЫ',
    ],
    'trading'  => [
        'title'  => 'Торговые счёта',
        'trdacc'  => 'Список торговых счётов',
        'account'  => 'MT счёт',
        'platform'  => 'Платформа',
        'leverage'  => 'Кредитное плечо',
        'mtaccount'  => 'MT счёт',
        'pwd'  => 'Пароль',
        'currency'  => 'Валюта',
    ],
    'deposit'  => [
        'title'  => 'ДЕПОЗИТ',
        'banktransfer'  => 'Банковские счёта компании',
        'bycreditcard'  => 'Банковской картой',
        'cryptocoins'  => 'Кpиптокошелки компании',
        'bank_transfer' => [
            'title'  => 'Банковские реквизиты компании',
            'bank_name'  => 'Называние Банка',
            'accunt_name'  => 'Имя счёта',
            'iban'  => 'номер счёта-IBAN',
            'swift'  => 'SWIFT Код',
            'bank_address'  => 'Адрес Банка',
            'currency'  => 'Валюта',
            ],
        'credit_card' => [
            'well'  => 'Отлично!',
            'wrong'  => 'Что-то пошло не так!',
            'complete_error' => 'All fields are require to complete',
            'ctitle'  => 'Полное имя (на карточке)',
            'fullname'  => 'ИМЯ ФАМИЛИЯ',
            'cardno'  => 'Номер карты',
            'cardnumber'  => 'ВАШ НОМЕР КАРТЫ',
            'mm'  => 'MM',
            'yy'  => 'ГГ',
            'exp'  => 'Срок действия',
            'cvv'  => 'CVV',
            'confirm'  => 'Подтвердите',
            ],
        'crypto_coins' => [
            'title'  => 'Адреса крипто-кошелков компании',
            'coin_name'  => 'Криптовалюта',
            'coin_wallet'  => 'Адрес кршелка',
        ],
    ],
    'withdrawal'  => [
        'title'  => 'СНЯТИЕ',
        'purpose'  => 'Комментария',
        'purposelable'  => 'коммент',
        'withamount'  => 'Сумма которую хотите снять',
        'withamountlable'  => 'Сумма',
        'withcurr'  => 'Предпочитаемая валюта',
        'withtype'  => 'Тип снятие',
        'well'  => 'Отлично!',
        'wrong'  => 'Что-то пошло не так!',
        'reqsucc'  => 'Ваш запрос успешно отправлен',
        'create'  => 'СОЗДАТЬ',

    ],
    'report'  => [
        'title'  => 'ИСТОРИЯ',
        'deposit'  => 'Депозит',
        'earning'  => 'Прибыли',
        'withdrawal'  => 'Снятие',
        'deposits'  => [
            'title'  => 'История депозита',
            'name'  => 'Имя',
            'amount'  => 'Сумма',
            'date_time'  => 'Время и дата',
            'currency'  => 'Валюта',
            'approve'  => 'Статус',
        ],
        'earnings'  => [
            'title'  => 'История прибыли',
            'name'  => 'Имя',
            'amount'  => 'Сумма',
            'currency'  => 'Валюта',
            'date_time'  => 'Время и дата',
            'approve'  => 'Статус',
        ],
        'withdrawals'  => [
            'title'  => 'История снятие',
            'name'  => 'Имя',
            'amount'  => 'сумма',
            'currency'  => 'Вальюта',
            'date_time'  => 'Время и дата',
            'type'  => 'Тип Снятие',
            'approve'  => 'Статус',
        ],

    ],
    'profile'  => [
        'title'  => 'Профиль',
        'personal_information'  => [
            'avatar'  => 'Изменить изображение',
            'title'  => 'Персональные данные',
            'email'  => 'Электронная почта',
            'emailpl'  => 'example@example.com',
            'name'  => 'Ваше имя',
            'namepl'  => 'Имя',
            'sname'  => 'Ваша Фамилия',
            'snamepl'  => 'Фамилия',
            'dofb'  => 'Дата рождение',
            'dofbpl'  => 'дд.мм.гггг',
            'phone'  => 'Номер телефотна',
            'phonepl'  => '+1 (111) 111 11 11',
            'gender'  => 'Пол',
            'preflang'  => 'Предпочитаемй язык',
            'prefcurr'  => 'Предпочитаемая криптовалюта',
            'country'  => 'Страна',
            'region'  => 'Регион',
            'city'  => 'Город',
            'resident'  => 'Место проживание',
            'well'  => 'Отлично!',
            'updated'  => 'Ваш запрос успешно отправлен.',
            'button_update'  => 'Обновите',
            'two_auth'=>'Две аутентификации',
        ],
        'wallet'  => [
            'title'  => 'Кошелек',
            'banks'  => [
                'title'  => 'Банковские счета',
                'bankname'  => 'Имя Банка',
                'address'  => 'Адрес Банка',
                'iban'  => 'IBAN',
                'swift'  => 'swift',
                'currency'  => 'Валюта',
                'well_done'  => 'Отлично!',
                'created'  => 'Новые данные успешно добавлены',
                'bank_details'  => 'Ваши Банковские данные',
                'button_add'  => 'Добавить',
            ],
            'cards'  => [
                'title'  => 'Банковские карты',
                'ctitle'  => 'Полное имя (на карточке)',
                'fullname'  => 'ИМЯ ФАМИЛИЯ',
                'cardno'  => 'Номер карты',
                'cardnopl'  => '1234-5678-9012-3456',
                'mm'  => 'MM',
                'mmm'  => 'Месяц',
                'yy'  => 'YY',
                'yyy'  => 'Год',
                'currency'  => 'Валюта',
                'help'  => 'Трехзначный код на обратной стороне вашей карты',
                'cvv'  => 'CVV',
                'well'  => 'Отлично!',
                'well1'  => 'Ваш запрос успешно отправлен.',
                'wrong'  => 'Что-то пошло не так!',
                'card_created'  => 'Новая банковская карта добавлена',
                'button_add'  => 'Добавить',
                'creditcard'  => 'Ваша Банковская карта',
            ],
            'crypto_wallets'  => [
                'title'  => 'Крипто Кошелки',
                'crtitle'  => 'Адреса криптовалют',
                'crwallet'  => 'адреса кошелков',
                'crpref'  => 'Предпочитаемая криптовалюта',
                'crname'  => 'Криптовалюта',
                'well'  => 'Отлично!',
                'crypto_wallet'  => 'Ваши Крипто-Кошелки',
                'wallet_created'  => 'Новая Криптокошелек добавлен.',
                'wrong'  => 'Что-то пошло не так!',
                'button_add'  => 'Добавить',

                ],
        ],
        'reset_password'  => [
            'title'  => 'Сбросить Пароль',
            'pwdold'  => 'Напишите текущий пароль',
            'pwdold1'  => 'Старый пароль',
            'pwdnew'  => 'Напишите новый пароль',
            'pwdnew1'  => 'Новый пароль',
            'pwdnew2'  => 'Подтвердите новый пароль',
            'incorrect'  => 'Неверный старый пароль',
            'well'  => 'Отлично!',
            'updated'  => 'Ваш пароль был успешно обновлен',
            'wrong'  => 'Что-то пошло не так!',
            'button_update'  => 'Обновить',
        ],
        'files'  => [
            'title'  => 'Файлы',
            'well'  => 'Отлично!',
            'updated'  => 'Ваши файлы были успешно загружены',
            'button_update'  => 'Загрузить',
        ],
    ],


      'usersettings'  => 'Пользовательские настройки',


    ];
