<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'content'  => [
        '12' => ' <h2>About Us</h2>
        <p>
            Established in 2018, <strong>AMIG Capital LTD</strong> has quickly grown
            into one of the largest <strong>forex</strong> and <strong>CFD</strong> brokers. We provide
            all investors to benefit from our new and different applications in order to enlarge and
            popularize the investment brokerage market.
        </p>
        <p>Our customers can access and process all instant financial data, market analyzes and other
            information needed quickly and safely thanks to our advanced online
            <strong>forex trading</strong> platforms that use the latest technologies.</p>
        <p>We give our customers a special investment service at <strong>AMIG Capital LTD</strong> to contribute more to the analysis and evaluation process, to resolve a potential problem on the spot, and for a seamless support.</p>
        <p>
            We provide more professional and faster service to each customer with our advanced CRM system.
        </p>
        <div class="divider divider-3"></div>

        <h2>Diverse Product Offering</h2>
        <p>
            Offers competitive trading conditions with an easy to use proprietary trading platform as
            well as <strong>MetaTrader 5</strong>. Clients can trade with MetaTrader 5, one of the most
            popular and reliable trading platform available in the industry.
        </p>
        <p>
            We provide trading services for more than 60 currency pairs as well as indices, CFDs, gold,
            silver, oil and other commodities.
        </p>
        <p><strong>AMIG Capital LTD</strong> reduces the client costs with the most competitive
            prices in the market, creating the best trading conditions for investors.</p>
        <p>
            At the same time as AMIG Capital LTD we offer <strong>ECN</strong> account type.
        </p>
        <p>With the high level of technological investment infrastructures and the prices received from
            the leading liquidity providers of the world, investors are able to carry out their transactions
            transparently, day and night without interruption, with wide product range minimized price
            spread deviation and rejected order statistics.</p>
        <div class="divider divider-1"></div>
        <p>
            <img src="images/about.jpg" class="full-img">
        </p>
        <div class="divider divider-2"></div>
        <h2>Competitive Spreads</h2>
        <p>
            AMIG Capital LTD offers some of the tightest spreads in the industry with our
            5-decimal-digit pricing. Clients enjoy leverage
            as high as 1:200, as well as minilots and microlots. Our deal-execution systems are fully automated; there is no dealing desk, and therefore no human intervention.
        </p>',



        '1' => '<h2> О нас </h2>
        <Р>
            <Strong> AMIG Capital LTD </strong>, основанная в 2018 году, быстро выросла
            в одного из крупнейших брокеров <strong> форекс </strong> и <strong> CFD </strong>. Мы предоставляем
            все инвесторы получают выгоду от наших новых и различных приложений, чтобы расширить и
            популяризация инвестиционного брокерского рынка.
        </p>
        <p> Наши клиенты могут получить доступ и обрабатывать все мгновенные финансовые данные, анализ рынка и другие
            информация необходима быстро и безопасно благодаря нашему продвинутому онлайн
            <strong> Forex Trading </strong> платформы, которые используют новейшие технологии. </p>
        <p> Мы предоставляем нашим клиентам специальную инвестиционную услугу в <strong> AMIG Capital LTD </strong>, чтобы внести больший вклад в процесс анализа и оценки, решить потенциальную проблему на месте и обеспечить бесперебойную поддержку. </ p >
        <p>
            Мы предоставляем более профессиональный и быстрый сервис каждому клиенту с нашей передовой системой CRM.
        </p>
        <div class = "divider divider-3"> </div>

        <h2> Предложение разнообразных продуктов </h2>
        <p>
            Предлагает конкурентные условия торговли с простой в использовании проприетарной торговой платформой, как
            а также <strong> MetaTrader 5 </strong>. Клиенты могут торговать с MetaTrader 5, одним из самых
            популярная и надежная торговая платформа, доступная в отрасли.
        </p>
        <p>
            Мы предоставляем торговые услуги для более чем 60 валютных пар, а также индексы, CFD, золото,
            серебро, нефть и другие товары.
        </p>
        <p> <strong> AMIG Capital LTD </strong> сокращает затраты клиентов благодаря наиболее конкурентоспособным
            Цены на рынке, создавая лучшие торговые условия для инвесторов. </p>
        <p>
            В то же время, как AMIG Capital LTD, мы предлагаем <strong> ECN </strong> тип учетной записи.
        </p>
        <p> С высоким уровнем технологической инвестиционной инфраструктуры и цен, полученных от
            ведущие поставщики ликвидности в мире, инвесторы могут осуществлять свои сделки
            прозрачно, днем ​​и ночью без перерыва, с широким ассортиментом продукции, минимизированная цена
            статистика отклонений и отклоненных заказов. </p>
        <div class = "divider divider-1"> </div>
        <p>
            <img src = "images / about.jpg" class = "full-img">
        </p>
        <div class = "divider divider-2"> </div>
        <h2> Конкурентные спреды </h2>
        <p>
            AMIG Capital LTD предлагает некоторые из самых узких спредов в отрасли с нашими
            Пятизначная цена. Клиенты пользуются кредитным плечом
            до 1: 200, а также минилоты и микролоты. Наши системы исполнения сделок полностью автоматизированы; нет дилингового бюро и, следовательно, нет человеческого вмешательства.
        </p>»',


    ],


];
