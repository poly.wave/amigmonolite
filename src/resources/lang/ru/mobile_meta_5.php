<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'mmt'  => ' Mobile Meta Trader 5',
        'mmtintro'  => 'Мобильная торговля на MT5',
        'mmtintro1'  => 'Торговайте из любой точки мира, в любое время, с удобством смартфона или планшета.',
        'mmtintro2'  => 'Супер-быстрая торговля одним щелчком - все с ладони вашей руки.',
        'mmtintro3'  => 'Независимо от того, какое устройство вы выбираете - Android или iOS, с AMIGFX MetaTrader 5 вы
        можете торговать в пути, в любое время, в любом месте! Просто загрузите MT5 приложение из магазина Apple App
        Store или Google Play, и вы можете идти!',
        'ios'  => 'Нажмите кнопку App Store ниже, чтобы загрузить приложение MT5.',
        'ios1'  => 'Выберите сервер AMIG Capital в приложении MT5.',
        'android'  => 'Нажмите кнопку Google Play ниже, чтобы загрузить приложение MT5.',
        'android1'  => 'Выберите сервер AMIG Capital в приложении MT5.',


    ],




];
