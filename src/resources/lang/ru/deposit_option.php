<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [

        '1' => '

        <h2 class = "SectionSubTitle"> Выберите способ депозита </h2>
        <p>
            Для вашего удобства и удобства мы предлагаем несколько способов пополнения счета.
        </p>
        <p>
            Выберите <strong> Банковский перевод </strong>, <strong> Кредитная карта </strong> или другие варианты:
        </p>
        <div class = "special-table">
            <div class = "row">
                <div class = "tr">
                    <div class = "th"> </div>
                    <div class = "th">
                        <div class = "imgLabel outerShadow">
                            <div>
                                <div class = "box" style = "color: # 000;"> Депозит <br> Тарифы </div>
                            </div>
                        </div>
                    </div>
                    <div class = "th">
                        <div class = "imgLabel outerShadow">
                            <div>
                                <div class = "box" style = "color: # 616161;"> Расчетное время <br> </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class = "tr">
                    <div class = "td"> Банковский банковский перевод </div>
                    <div class = "td"> Нет </div>
                    <div class = "td"> 1-5 рабочих дней </div>
                </div>
                <div class = "tr">
                    <div class = "td"> ВИЗА - ОСНОВНЫЕ КАРТЫ </div>
                    <div class = "td"> Нет </div>
                    <div class = "td"> Мгновенно до 2 часов </div>
                </div>
                <div class = "tr">
                    <div class = "td"> BITCOIN </div>
                    <div class = "td"> 3% </div>
                    <div class = "td"> Мгновенно до 2 часов
                        <a href="https://cex.io/r/1/up120172948/1"> Нажмите, чтобы купить </a>

                </div>
            </div>

        </div>'

    ],

];
