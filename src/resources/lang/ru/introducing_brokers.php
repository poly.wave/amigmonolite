<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1' => '
<div class="container text">
        <h2 class="SectionSubTitle">Представляющий Брокер</h2>

        <p>
            У вас уже есть бизнес-команда и вы ищете больше потенциальных продуктов? Вы брокер, предлагающий клиентам услуги онлайн-торговли на вашей торговой платформе MetaTrader?
        </p>

        <p>
            <a href="/contact-us" class="item">Контакты.</a> <strong>Мы свяжемся с вами как можно скорее.</strong>.
        </p>
    </div>


        ',

    ],

];
