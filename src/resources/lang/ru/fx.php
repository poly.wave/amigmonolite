<?php

return [




    'layouts'  => [

        'title' => 'AMIG Forex & Crypto Markets - ',

        'header'  => [
            'open'  => 'Открыть счёт',
            'login'  => 'Вход клиента',
            'logotext'  => 'AMIG Fx',
        ],

        'footer'  => [
            'menu'  => [
                'about'  => 'О нас',
                'account'  => 'Типы счётов',
                'register'  => 'Открыть счёт',
                'special'  => 'Акции',
                'deposit'  => 'Депозиты',
                'withdraw'  => 'Снятие',
                'market'  => 'Инструменты',
                'economic'  => 'Каленьдарь',
                'daily'  => 'Бюллитень',
                'education'  => 'Обучение',
                'whatis'  => 'Форекс?',
                'fxtrading'  => 'Форекс Торговля',
                'fundamental'  => 'Фундаментальный',
                'technical'  => 'Технический',
                'major'  => 'Мажоры',
                'partnership'  => 'Партнёрство',
                'partnering'  => 'Партнёр',
                'ib'  => 'IB',
                'white'  => 'White Label',
                'platforms'  => 'Платформы',
                'mt5'  => 'Meta Trader 5',
                'mmt5'  => 'Мобильный MT5',
                'contactus'  => 'Контакты',
            ],
            'co'  => 'AMIG Forex',
            'riskwarning'  => 'ПРЕДУПРЕЖДЕНИЕ О РИСКАХ! Все финансовые продукты, торгуемые с маржой, несут высокую степень риска для вашего капитала.
                       Они могут не подходить для всех инвесторов, и вы можете потерять больше, чем ваш первоначальный депозит. пожалуйста
                       убедитесь, что вы полностью понимаете риски. Мы не принимаем жителей Соединенных Штатов, Канады и Турции. ',
        ],

        'menu_main'  => [
            'about'  => 'О нас',
            'account'  => 'Типы счётов',
            'register'  => 'Открыть счёт',
            'special'  => 'Акции',
            'deposit'  => 'Депозиты',
            'withdraw'  => 'Снятие',
            'market'  => 'Инструменты',
            'economic'  => 'Каленьдарь',
            'daily'  => 'Бюллитень',
            'education'  => 'Обучение',
            'whatis'  => 'Форекс?',
            'fxtrading'  => 'Форекс Торговля',
            'fundamental'  => 'Фундаментальный',
            'technical'  => 'Технический',
            'major'  => 'Мажоры',
            'partnership'  => 'Партнёрство',
            'partnering'  => 'Партнёр',
            'ib'  => 'IB',
            'white'  => 'White Label',
            'platforms'  => 'Платформы',
            'mt5'  => 'Meta Trader 5',
            'mmt5'  => 'Мобильный MT5',
            'contactus'  => 'Контакты',
            ],

        'slider'  => [
            'want'=> 'Хотите',
            'more'=> 'УЗНАТЬ',
            'info'=> 'БОЛЬШЕ',
            'previous'  => 'Предедущий',
            'next'  => 'Следующий',
            'getstartedbttn'  => 'ОТПРАВИТЬ',
            'agree'  => 'Я хотел бы получать обновление.',
            'fname'  => 'Имя',
            'lname'  => 'Фамилия',
            'email'  => 'Адрес электронной почты',
            'mobile'  => 'Мобильный телефон',
            'message'  => 'Текст',
            'send'  => 'ОТПРАВИТЬ',
            'contact'  => 'Форма Контакта',
            'contact1'  => 'Мы готовы ответить на ваш вопрос.',
        ],
    ],

    'controller'  => [
        'title'  => [
            'home'  => 'Главная',
            'whitelabel'  => 'White Label',
            'withdrawoptions'  => 'Варианты снятия',
            'termofuse'  => 'Условия использования',
            'whatisforex'  => 'Что такое Форекс',
            'specialpro'  => 'Специальные акции',
            'technicalan'  => 'Технический анализ',
            'mobilemt5'  => 'Mobile Meta Trader 5',
            'metatrader5'  => 'Meta Trader 5',
            'aboutus'  => 'О нас',
            'accounttypes'  => 'Типы счетов',
            'advantages'  => 'Преимущества',
            'Campaigns'  => 'Кампании',
            'dailyfx'  => 'Ежедневной бюллитень',
            'connect'  => 'Свяжитесь с нами',
            'companyprofile'  => 'Профиль компании',
            'depositoption'  => 'Вариант депозита',
            'calendar'  => 'Экономический календарь',
            'edu'  => 'Обучение',
            'funal'  => 'Фундаментальный анализ',
            'adfxtrading'  => 'Преимущества торговли на рынке Форекс',
            'ibroker'  => 'Представляющие брокеры',
            'majorplayer'  => 'Мажоры',
        ],
    ],
];
