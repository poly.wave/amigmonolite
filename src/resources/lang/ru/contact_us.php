<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        'fname' => 'Имя',
         'lname' => 'Фамилия',
         'email' => 'Адрес электронной почты',
         'mobile' => 'Номер телефона',
         'message' => 'Текст',
         'send' => 'Оправить',
         'contact' => 'Контактная форма',
         'contact1' => 'Мы готовы ответить на ваш вопрос.',


    ],



];
