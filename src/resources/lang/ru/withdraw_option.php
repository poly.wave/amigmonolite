<?php

return [
    /*
    |--------------------------------------------------------------------------
    | FX Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'content'  => [
        '1'  => '
        <h2 class="SectionSubTitle">Снятие средств выполняется быстро и легко</h2>

        <p>
            В AMIG FX процесс снятия средств является быстрым и простым, и все снятия средств обрабатываются в течение 24 часов.
            Вы можете вывести средства в любое время прямо из области входа в систему клиента.
            Средства могут быть выведены прямо на ваш номинированный банковский счет или обратно на вашу кредитную карту, если вы использовали кредитную карту для внесения средств. Просто так просто.
        </p>
        <p>
            Выберите из <strong>Банковский перевод</strong>, <strong>Кредитная карта</strong> Или другие параметры:
        </p>
        <div class="special-table">
            <div class="row">
                <div class="tr">
                    <div class="th"></div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Снятие<br>комиссия</div>
                            </div>
                        </div>
                    </div>
                    <div class="th">
                        <div class="imgLabel outerShadow">
                            <div>
                                <div class="box" style="color: #000;">Расчетное <br>Время</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tr">
                    <div class="td">Банковский перевод</div>
                    <div class="td">25 - 80 USD</div>
                    <div class="td">24-48 часов</div>
                </div>
                <div class="tr">
                    <div class="td">VISA - MASTER Карты</div>
                    <div class="td">3.5%</div>
                    <div class="td">24-48 часов</div>
                </div>
                <div class="tr">
                    <div class="td">Криптовалюты</div>
                    <div class="td">3%</div>
                    <div class="td">2-5 часов</div>
                </div>
            </div>

        </div>






        ',
    ],

];
