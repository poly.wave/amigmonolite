window.$ = window.jQuery = require('jquery');

require('@fancyapps/fancybox');
require('./libs/slideshow');
require('./libs/menu');
require('parallaxjs');

const site = {
    init: () => {
        site.listeners();
        site.plugins();
        site.watcher();

        setTimeout(() => {
            $('#preloader').fadeOut(500, () => {
                document.body.classList.add('loaded');
            });
        }, 500);

        console.log('app was init');
    },

    plugins: () => {
        if($('.parallax-container').length) $('.parallax-container').parallax();

        $(".fancybox").fancybox();
    },

    listeners: () => {
        let body = document.body;
        let selectLanguage = document.querySelector('.select-language');
        let openMenu = document.querySelector('.open-menu');
        let closeMenu = document.querySelector('.close-menu');
        let topMenu = document.querySelector('.dummy');

        // Reset all on click body
        body.addEventListener('click', (e) => {
            let target = e.target;

            if(
                !target.closest('.select-language') &&
                !target.classList.contains('.select-language')
            ) {
                if(selectLanguage) selectLanguage.classList.remove('selected');
            }

            if(
                !target.closest('.dummy') &&
                !target.classList.contains('.item')
            ) {
                $('.dummy').find('.sublist').css('display', 'none');
            }
        });

        // Toggle select language
        if(selectLanguage) {
            selectLanguage.addEventListener('click', (e) => {
                e.preventDefault();

                if(selectLanguage.classList.contains('selected')) {
                    selectLanguage.classList.remove('selected');
                } else {
                    selectLanguage.classList.add('selected');
                }

                return false;
            });
        }

        // Scroll rendering
        let columnsWrap = $('.choose .three-columns');
        let column = columnsWrap.find('li');

        let scroll = () => {
            if(column.length) {
                let offset = columnsWrap.offset();
                let scrollTop = $(window).scrollTop() + $(window).height();
                let columnHeight = column.height();

                let first = false;
                let second = false;
                if(scrollTop >= offset.top) {
                    first = true;

                    if(scrollTop >= (offset.top + columnHeight)) {
                        second = true;
                    }
                }

                let timeout = 300;

                if(first == true) {
                    let i = 0;

                    setTimeout(() => {
                        column.filter('[data-id="1"]').addClass('active');

                        setTimeout(() => {
                            column.filter('[data-id="2"]').addClass('active');

                            setTimeout(() => {
                                column.filter('[data-id="3"]').addClass('active');
                            }, timeout);
                        }, timeout);
                    }, timeout);
                }

                if(second == true) {
                    let i = 0;

                    setTimeout(() => {
                        column.filter('[data-id="4"]').addClass('active');

                        setTimeout(() => {
                            column.filter('[data-id="5"]').addClass('active');

                            setTimeout(() => {
                                column.filter('[data-id="6"]').addClass('active');
                            }, timeout);
                        }, timeout);
                    }, timeout * 4);
                }
            }
        };

        $(window).scroll(scroll);
        scroll();



        // Open account forms
        let openAccount = $('.open-account');
        let steps = openAccount.find('.steps');

        openAccount.find('form').on('submit', (e) => {
            e.preventDefault();

            let target = $(e.target);
            let self = target.closest('form');


            self.hide();
            steps.find('li').removeClass('selected');

            if(self.hasClass('step-1')) {
                openAccount.find('.step-2').show();
                steps.find('li[data-step="2"]').addClass('selected');
            }

            if(self.hasClass('step-2')) {
                openAccount.find('.step-3').show();
                steps.find('li[data-step="3"]').addClass('selected');
            }

            if(self.hasClass('step-3')) {
                openAccount.find('.step-4').show();
                steps.find('li[data-step="4"]').addClass('selected');
            }


            return false;
        });

        openAccount.find('.back').on('click', (e) => {
            e.preventDefault();

            let selected = steps.find('.selected');
            let step = selected.attr('data-step') * 1;
            step --;

            openAccount.find('form').hide();
            steps.find('li').removeClass('selected');

            openAccount.find('.step-' + step).show();
            steps.find('li[data-step="' + step + '"]').addClass('selected');

            return false;
        });



        /* Hovering */
        let slide = $('.slides .slide');
        let hovered = false;
        $('.buttons .btn-large').hover((e) => {
            let self = $(e.target);
            let id = self.attr('data-id');

            if(!id) {
                self = self.closest('.btn-large');
                id = self.attr('data-id');
            }

            console.log(id);

            slide.removeClass('slide--current');
            slide.filter('[data-id="' + id + '"]').addClass('slide--current');

            hovered = true;
        }, () => {
            //slide.removeClass('slide--current');
            //slide.filter('[data-id="0"]').addClass('slide--current');

            hovered = false;
        });

        setInterval(() => {
            if(hovered == false) {
                let slideCurrent = slide.filter('.slide--current');

                if(slideCurrent.length) {
                    let slideNext = slideCurrent.next('.slide');

                    if(slideNext.length == 0) {
                        slideNext = slide.filter('[data-id="5"]');
                    }

                    slideCurrent.removeClass('slide--current');
                    slideNext.addClass('slide--current');
                }
            }
        }, 7000);
    },

    watcher: () => {
        let hovered = false;
        let header = $('header');

        header.find('form input').on('focus', (e) => {
            e.preventDefault();

            hovered = true;
        });

        header.find('form input').on('blur', (e) => {
            e.preventDefault();

            hovered = false;
        });

        let interval = setInterval(() => {
            if(hovered == false) {
                $('.slidenav__item.slidenav__item--next').click();
            }
        }, 7500);
    }
};

$(document).ready(() => {
    site.init();
});
