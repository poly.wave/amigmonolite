window._ = require('lodash');

window.Popper = require('popper.js').default;
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
import VueRouter from 'vue-router';
window.Vue.use(VueRouter);

require('bootstrap');
require('bootstrap-select');
require('./libs/bootstrap-typeahead');
require('./libs/jquery.mask');
require('datatables');
require('./libs/dataTables.bootstrap4');
require('./libs/jquery-filestyle');
require('pace-progress');
require('chart.js');
require('blueimp-file-upload');
require('@fancyapps/fancybox');
require('@coreui/coreui-pro');

const app = {
    route: null,
    action: null,
    csrf_token: null,
    api_token: null,

    init: () => {
        app.csrf_token = $('head meta[name="csrf-token"]').attr('content');
        app.api_token = $('head meta[name="api-token"]').attr('content');

        // Get route
        let exp = window.location.href.split('/');
        if(exp[3]) app.route = exp[3];
        if(exp[4]) app.action = exp[4];
        else app.action = 'init';

        app.routing();
        app.plugins();
        app.fix();
    },

    routing: () => {
        const route = app.routes[app.route];

        if(route) {
            const action = route[app.action];

            if(action) {
                app.routes[app.route][app.action]();
            }
        }
    },

    routes: {
        register: {
            init: () => {
                let props = {};
                const register = $('form#register');
            }
        },

        member: {
            main: () => {
                Chart.defaults.global.defaultFontColor = 'white';

                let options = {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false,
                        text: 'Per Year Statistic'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: false,
                            scaleLabel: {
                                display: false
                            }
                        }],
                        yAxes: [{
                            display: false,
                            scaleLabel: {
                                display: false
                            }
                        }]
                    }
                };

                let loaded = {};


                $.ajax({
                    url: '/api/user/balance/statistic',
                    dataType: 'json',
                    data: {
                        api_token: app.api_token
                    },
                    success: (response) => {
                        let labels = [];
                        let data = [];

                        $.each(response, function (key, row) {
                            labels.push(key);
                            data.push(row);
                        });

                        loaded.balance = {
                            labels: labels,
                            data: data
                        };

                        const chart = new Chart($('#balanceStatistic'), {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Balance',
                                    backgroundColor: 'transparent',
                                    borderColor: getStyle('--white'),
                                    borderWidth: 2,
                                    data: data
                                }]
                            },
                            options: options
                        });
                    }
                });


                $.ajax({
                    url: '/api/user/deposits/statistic',
                    dataType: 'json',
                    data: {
                        api_token: app.api_token
                    },
                    success: (response) => {
                        let labels = [];
                        let data = [];

                        $.each(response, function (key, row) {
                            labels.push(key);
                            data.push(row);
                        });

                        loaded.deposits = {
                            labels: labels,
                            data: data
                        };

                        const chart = new Chart($('#depositStatistic'), {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Deposit',
                                    backgroundColor: 'transparent',
                                    borderColor: getStyle('--white'),
                                    borderWidth: 2,
                                    data: data
                                }]
                            },
                            options: options
                        });
                    }
                });

                $.ajax({
                    url: '/api/user/earnings/statistic',
                    dataType: 'json',
                    data: {
                        api_token: app.api_token
                    },
                    success: (response) => {
                        let labels = [];
                        let data = [];

                        $.each(response, function (key, row) {
                            labels.push(key);
                            data.push(row);
                        });

                        loaded.earnings = {
                            labels: labels,
                            data: data
                        };

                        const chart = new Chart($('#earningStatistic'), {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Earning',
                                    backgroundColor: 'transparent',
                                    borderColor: getStyle('--white'),
                                    borderWidth: 2,
                                    data: data
                                }]
                            },
                            options: options
                        });
                    }
                });

                $.ajax({
                    url: '/api/user/withdrawals/statistic',
                    dataType: 'json',
                    data: {
                        api_token: app.api_token
                    },
                    success: (response) => {
                        let labels = [];
                        let data = [];

                        $.each(response, function (key, row) {
                            labels.push(key);
                            data.push(row);
                        });

                        loaded.withdrawals = {
                            labels: labels,
                            data: data
                        };

                        const chart = new Chart($('#withdrawalStatistic'), {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: [{
                                    label: 'Withdrawal',
                                    backgroundColor: 'transparent',
                                    borderColor: getStyle('--white'),
                                    borderWidth: 2,
                                    data: data
                                }]
                            },
                            options: options
                        });
                    }
                });



                // Build common chart
                const commonInterval = setInterval(() => {
                    if(loaded.balance && loaded.deposits && loaded.earnings && loaded.withdrawals) {
                        clearInterval(commonInterval);

                        // Create custom data
                        const labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                        let datasets = [];
                        let data = {
                            balance: [],
                            deposits: [],
                            earnings: [],
                            withdrawals: []
                        };

                        $.each(labels, function (key, val) {
                            data.balance.push(0);
                            $.each(loaded.balance.labels, function (_key, _val) {
                                if(val == _val) {
                                    data.balance[key] = loaded.balance.data[_key];
                                }
                            });

                            data.deposits.push(0);
                            $.each(loaded.deposits.labels, function (_key, _val) {
                                if(val == _val) {
                                    data.deposits[key] = loaded.deposits.data[_key];
                                }
                            });

                            data.earnings.push(0);
                            $.each(loaded.earnings.labels, function (_key, _val) {
                                if(val == _val) {
                                    data.earnings[key] = loaded.earnings.data[_key];
                                }
                            });

                            data.withdrawals.push(0);
                            $.each(loaded.withdrawals.labels, function (_key, _val) {
                                if(val == _val) {
                                    data.withdrawals[key] = loaded.withdrawals.data[_key];
                                }
                            });
                        });

                        console.log(data);

                        // Add balance
                        datasets.push({
                            label: 'Balance',
                            backgroundColor: 'transparent',
                            borderColor: getStyle('--warning'),
                            borderWidth: 2,
                            data: data.balance
                        });

                        // Add deposits
                        datasets.push({
                            label: 'Deposit',
                            backgroundColor: 'transparent',
                            borderColor: getStyle('--primary'),
                            borderWidth: 2,
                            data: data.deposits
                        });

                        // Add earnings
                        datasets.push({
                            label: 'Earning',
                            backgroundColor: 'transparent',
                            borderColor: getStyle('--danger'),
                            borderWidth: 2,
                            data: data.earnings
                        });

                        // Add withdrawals
                        datasets.push({
                            label: 'Withdrawal',
                            backgroundColor: 'transparent',
                            borderColor: getStyle('--success'),
                            borderWidth: 2,
                            data: data.withdrawals
                        });

                        let options = {
                            responsive: true,
                            legend: {
                                display: true
                            },
                            title: {
                                display: true,
                                text: 'Per Year Statistic'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true
                                    }
                                }]
                            }
                        };

                        const chart = new Chart($('#commonStatistic'), {
                            type: 'line',
                            data: {
                                labels: labels,
                                datasets: datasets
                            },
                            options: options
                        });
                    }
                }, 500);
            }
        },

        user: {
            settings: () => {
                app.routes.register.init();
            }
        }
    },

    plugins: () => {
        $(".fancybox").fancybox();

        if ($('#hidden-content').length) {
            $.fancybox.open({
                src  : '#hidden-content',
                type : 'inline'
            });
        }

        const phoneMask = '+9 (999) 999-99-99';

        $('#phone.mask').mask(phoneMask).attr('placeholder', phoneMask);

        $('select').each(function () {
            let self = $(this);

            self.change(function () {
                let _self = $(this);
                let _data = _self.data();
                let _name = _self.attr('name');

                if (_data.url) {
                    let _params = {
                        api_token: app.api_token
                    };

                    if (_data.params) {
                        _params = JSON.parse(_data.params)
                        _self.data('params', '');
                        _self.html('');
                    }

                    if (_self.find('option').length === 0) {
                        _self.html('<option value="">Loading...</option>');

                        $.ajax({
                            url: _data.url,
                            data: _params,
                            dataType: 'json',
                            success: (response) => {
                                _self.html('<option value="">Choose...</option>');
                                response.forEach((value) => {
                                    let selected = '';
                                    if (_data.selected) {
                                        if (_data.selected === value[_data.value]) {
                                            selected = ' selected';
                                        }
                                    }

                                    let name = '';
                                    if (_data.name.indexOf(',') > -1) {
                                        _data.name.split(',').forEach((_value) => {
                                            if (_value=="active") {
                                               if (value[_value.replace(/ /g, '')]==1) {
                                                   name += "Yes";
                                               } else {
                                                   name += "No";
                                               }
                                            } else {
                                                name += value[_value.replace(/ /g, '')] + ' / ';
                                            }
                                        });
                                    } else {
                                        name = value[_data.name]
                                    }

                                    _self.append('<option value="' + value[_data.value] + '"' + selected + '>' + name + '</option>');
                                });

                                $('.selectpicker').selectpicker('refresh');

                                if (_data.selected) {
                                    _self.change();
                                }
                            }
                        });
                    }
                }

                $('select').each(function () {
                    let __self = $(this);
                    let __data = __self.data();
                    let __name = __self.attr('name');
                    let __params = {};

                    if (__data.relation_to == _name) {
                        __params[_name] = _self.val();

                        __self.data('params', JSON.stringify(__params));
                        __self.change();
                    }
                });
            });

            self.change();
        });

        $('input').each(function () {
            let self = $(this);
            let data = self.data();

            if (data.url) {
                self.typeahead({
                    ajax: {
                        url: data.url,
                        triggerLength: 1,
                        displayField: data.field,
                        preDispatch: function (query) {
                            let _data = {};

                            _data[data.field] = query;

                            return _data;
                        },
                    }
                });
            }
        });

        //$('input[type="file"]').fileupload();

        $('.dataTable').each(function() {
           let self = $(this);
           let card = self.closest('.card');
           let hide_nav = self.data('hide-nav');
           let hide_thead = self.data('hide-thead');
           let columns = [];

           self.find('thead th').each(function () {
               let _self = $(this);

               columns.push({
                   data: _self.data('data'),
                   name: _self.data('name'),
                   self: _self
               });

               if (hide_thead) {
                   _self.hide();
               }
           });

           let settings = {
               processing: true,
               serverSide: true,
               ajax: {
                   url: self.data('url') + '?api_token=' + app.api_token,
                   dataSrc: function (json) {
                       if (json.data.length == 0) {
                           card.find('table').remove();
                           card.find('.card-body').append('No Data Available');
                           card.fadeIn();
                       } else {
                           let _interval = setInterval(() => {
                               let tr = self.find('tbody tr');
                               if (tr.length == json.data.length) {
                                   clearInterval(_interval);

                                   tr.each(function () {
                                       let _self = $(this);
                                       let td = _self.find('td');
                                       let current = 0;

                                       td.each(function () {
                                           let __self = $(this);
                                           let text = __self.html();
                                           let column = columns[current];

                                           if (column) {
                                               if (column.name == 'icon') {
                                                   column.self.html('');
                                                   __self.html('<i class="fa fa-' + text + '"></i>');
                                               }
                                           }

                                           current ++;
                                       });
                                   });

                                   /* Show table */
                                   self.css('width', '100%');
                                   card.fadeIn();
                               }
                           }, 100);
                       }

                       return json.data;
                   }
               },
               columns: columns,
               order: [
                   [0, 'desc']
               ]
           };

           if (hide_nav) {
               settings['searching'] = false;
               settings['paging'] = false;
               settings['info'] = false;
           }

           $('#' + self.attr('id')).DataTable(settings);
           self.attr('style', 'border-collapse: collapse !important');
        });
    },

    random: () => {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    },

    fix: () => {
        $('input').each(function () {
            let self = $(this);

            self.attr('autocomplete', app.random)
        });
    }
};

$(document).ready(() => {
    app.init();

    $(document).ajaxComplete(function() {
        Pace.restart();
    });
    var s = document.createElement("script");
    s.src = "/js/jquery.table2excel.min.js";
    s.onload = function(e){  };
    document.head.appendChild(s);

    var s = document.createElement("script");
    s.src = "/js/jspdf.debug.js";
    s.onload = function(e){  };
    document.head.appendChild(s);
    $(".btn-excel").click(function() {
      var el = $(this).attr('data-table');
      $("#"+el).table2excel({
         name:el,
         filename:el+".xls",
       });
    });
    $(".btn-pdf").click(function() {
      var el = $(this).attr('data-table');
      var pdf = new jsPDF('p', 'pt', 'letter');

       pdf.cellInitialize();
       pdf.setFontSize(10);
       $.each( $('#'+el+' tr'), function (i, row){
           $.each( $(row).find("td, th"), function(j, cell){
               var txt = $(cell).text().trim() || " ";
               var width = 100; //make with column smaller
               pdf.cell(10, 50, width, 30, txt, i);
           });
       });

       pdf.save(el+'.pdf');
  });
  $("#convert-amount").change(function() {
    var amount = $("#convert-amount").val();
    var from = $("#convert-from").val();
    var to = $("#convert-to").val();
    var formURL = "/convert-currency/"+amount+'/'+from+'/'+to;
    $.ajax({
      type: "GET",
      url:  formURL,
      data: { 'amount':amount,'from':from ,'to':to },
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      success: function (data) {
        $("#amount-charge").html(data);
      }
    });
  });
  $("#convert-from").change(function() {
    var amount = $("#convert-amount").val();
    var from = $("#convert-from").val();
    var to = $("#convert-to").val();
    var formURL = "/convert-currency/"+amount+'/'+from+'/'+to;
    $.ajax({
     type: "GET",
      url:  formURL,
      data: { 'amount':amount,'from':from ,'to':to },
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      success: function (data) {
        $("#amount-charge").html(data);
      }
    });
  });
  $("#convert-to").change(function() {
    var amount = $("#convert-amount").val();
    var from = $("#convert-from").val();
    var to = $("#convert-to").val();
    var formURL = "/convert-currency/"+amount+'/'+from+'/'+to;
    $.ajax({
     type: "GET",
      url:  formURL,
      data: { 'amount':amount,'from':from ,'to':to },
      contentType: "application/json; charset=utf-8",
      dataType: 'json',
      success: function (data) {
        $("#amount-charge").html(data);
      }
    });
  });
});
