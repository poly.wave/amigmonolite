<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            $table->text('first_name');
            $table->text('last_name');
            $table->text('day_of_birth');
            $table->text('phone');
            $table->text('resident_address');
            $table->text('bank_detail');
            $table->text('bank_name');
            $table->text('bank_address');
            $table->text('iban');
            $table->text('swift');
            $table->text('avatar');
            $table->text('api_token');

            $table->float('deposits');
            $table->float('earnings');
            $table->float('withdrawals');
            $table->float('balance');

            $table->integer('gender_id');
            $table->integer('country_id');
            $table->integer('city_id');
            $table->integer('language_id');
            $table->integer('currency_id');
            $table->integer('user_group_id');
            $table->integer('region_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
