<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DepositsMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $type;
    public $user_name;
    public $amount;
    public $currency_name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($type,$user_name,$amount,$currency_name)
    {
        $this->type = $type;
        $this->user_name = $user_name;
        $this->amount = $amount;
        $this->currency_name = $currency_name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('vendor.mail.markdown.deposits')
                  ->subject('Deposit Mail')
                  ->with([
                    'type'=> $this->type,
                    'user_name'=> $this->user_name,
                    'amount'=> $this->amount,
                    'currency_name'=> $this->currency_name
                  ]);
    }
}
