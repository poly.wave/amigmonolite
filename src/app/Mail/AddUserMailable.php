<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddUserMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $password;
    public $email;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$password,$email,$link)
    {
      $this->name = $name;
      $this->password = $password;
      $this->email = $email;
      $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('vendor.mail.markdown.add_user_from_admin')
                  ->subject('Successfully created new account')
                  ->with([
                    'name'=> $this->name,
                    'email'=> $this->email,
                    'password'=> $this->password,
                    'activate'=>route('activate.user', $this->link),
                  ]);
    }
}
