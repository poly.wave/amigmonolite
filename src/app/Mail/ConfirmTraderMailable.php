<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmTraderMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $subject;
    public $text;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject,$text)
    {
      $this->text = $text;
      $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->markdown('vendor.mail.markdown.confirm_trader')
                  ->from('support@amigfx.com')
                  ->subject($this->subject)
                  ->with([
                    'text'=> $this->text,
                  ]);
    }
}
