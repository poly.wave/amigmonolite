<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WithdrawStatusMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $status;
    public $amount;
    public $type;
    public $currency;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($status,$amount,$type,$currency)
    {
      $this->status = $status;
      $this->amount = $amount;
      $this->type = $type;
      $this->currency = $currency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      if ($this->status==1) {
        return $this->markdown('vendor.mail.markdown.withdraw_approved')
                    ->subject('Withdrawal request')
                    ->with([
                      'amount'=>$this->amount,
                      'type'=>$this->type,
                      'currency'=>$this->currency,
                  ]);
      } elseif ($this->status==2) {
        return $this->markdown('vendor.mail.markdown.withdraw_declined')
                    ->subject('Withdrawal request')
                    ->with([
                      'amount'=>$this->amount,
                      'type'=>$this->type,
                      'currency'=>$this->currency,
                  ]);

      } elseif ($this->status==3) {
        return $this->markdown('vendor.mail.markdown.withdraw_pending')
                    ->subject('Withdrawal request')
                    ->with([
                      'amount'=>$this->amount,
                      'type'=>$this->type,
                      'currency'=>$this->currency,
                  ]);
      }
    }
}
