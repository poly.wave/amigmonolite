<?php

namespace App\Models;

class CompanySocial extends Model
{
    protected $table = 'company_social';
    protected $fillable = [
        'name',
        'link'
    ];
}
