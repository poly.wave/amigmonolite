<?php

namespace App\Models;

class DocumentTypes extends Model
{
    protected $table = 'document_types';
    protected $fillable = [
        'name'
    ];
}
