<?php

namespace App\Models;

class Tickets extends Model
{
    protected $table = 'tickets';
    protected $fillable = [
        'name',
        'status',
        'user_id'
    ];
}
