<?php

namespace App\Models;

class Cities extends Model
{
    protected $table = 'cities';
    protected $fillable = [
        'name',
        'region_id',
        'country_id',
        'latitude',
        'longitude'
    ];
}
