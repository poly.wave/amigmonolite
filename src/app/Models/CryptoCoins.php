<?php

namespace App\Models;

class CryptoCoins extends Model
{
    protected $table = 'crypto_coins';
    protected $fillable = [
        'name'
    ];
}
