<?php

namespace App\Models;

class WithdrawalOptions extends Model
{
    protected $table = 'withdrawal_options';
    protected $fillable = [
        'icon',
        'name',
        'fees',
        'estimated_time'
    ];
}
