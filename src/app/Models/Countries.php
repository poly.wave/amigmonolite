<?php

namespace App\Models;

class Countries extends Model
{
    protected $table = 'countries';
    protected $fillable = [
        'short',
        'name'
    ];
}
