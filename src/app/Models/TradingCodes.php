<?php

 namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TradingCodes extends Model
{
    //
        protected $table = 'trading_codes';
        protected $fillable = [
            'user_id',
            'code'
        ];
}
