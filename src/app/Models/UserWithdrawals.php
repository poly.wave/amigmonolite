<?php

namespace App\Models;

class UserWithdrawals extends Model
{
    protected $table = 'user_withdrawals';
    protected $fillable = [
        'name',
        'amount',
        'currency_id',
        'user_id',
        'approve_type_id',
        'withdrawal_option_id',
        'deal',
        'order',
        'action',
        'entry',
        'time',
        'time_msc',
        'symbol',
        'comment'
    ];
}
