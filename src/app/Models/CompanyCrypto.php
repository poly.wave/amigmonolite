<?php

namespace App\Models;

class CompanyCrypto extends Model
{
    protected $table = 'company_crypto';
    protected $fillable = [
        'icon',
        'name',
        'account'
    ];
}
