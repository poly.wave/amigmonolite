<?php

namespace App\Models;

class PaymentCc extends Model
{
    protected $table = 'payment_cc';
    protected $fillable = [
        'wsuser',
        'wspass',
        'apikey',
        'url',
        'minimal_deposit'
    ];
}
