<?php

namespace App\Models;

class MtActions extends Model
{
    protected $table = 'mt_actions';
    protected $fillable = [
        'name',
        'status'
    ];
}
