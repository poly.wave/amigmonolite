<?php

namespace App\Models;

class UserFiles extends Model
{
    protected $table = 'user_files';
    protected $fillable = [
        'user_id',
        'document_type_id',
        'file',
        'approve_type_id'
    ];
}
