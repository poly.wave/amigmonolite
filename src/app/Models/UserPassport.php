<?php

namespace App\Models;

class UserPassport extends Model
{
    protected $table = 'user_passport';
    protected $fillable = [
        'user_id',
        'passport_id',
        'date_of_issue',
        'date_of_expiry',
        'issuring_authority',
        'passport_scan',
        'approve_id'
    ];
}
