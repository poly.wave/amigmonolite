<?php

namespace App\Models;

class UserGroups extends Model
{
    protected $table = 'user_groups';
    protected $fillable = [
        'name',
        'level'
    ];
}
