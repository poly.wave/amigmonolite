<?php

namespace App\Models;

class UserTradingLeverage extends Model
{
    protected $table = 'user_trading_leverage';
    protected $fillable = [
        'user_id', 'name'
    ];
}
