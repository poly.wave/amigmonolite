<?php

namespace App\Models;

class UserEarnings extends Model
{
    protected $table = 'user_earnings';
    protected $fillable = [
        'name',
        'amount',
        'currency_id',
        'user_id',
        'deal',
        'order',
        'action',
        'entry',
        'time',
        'time_msc',
        'symbol',
        'comment'
    ];
}
