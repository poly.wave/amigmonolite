<?php


namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \App\Models\UserDepositsBanks;
use \App\Models\UserDepositsCryptoCompany;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'day_of_birth',
        'gender_id',
        'phone',
        'country_id',
        'region_id',
        'city_id',
        'resident_address',
        'language_id',
        'bank_detail',
        'bank_name',
        'bank_address',
        'iban',
        'swift',
        'currency_id',
        'user_group_id',
        'deposits',
        'earnings',
        'withdrawals',
        'avatar',
        'api_token',
        'balance',
        'withdrawal_percent',
        'withdrawal_day_start',
        'withdrawal_day_end',
        'max_withdrawal_granted',
        'access_credit_card_payment',
        'cc_customer_id',
        'password_without_hash',
        'two_auth',
        'code',
        'created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasBankDeposit()
    {
        if ($bank=UserDepositsBanks::where(['user_id'=>$this->id])->orderBy('id','DESC')->first()) {
            if (strtotime('+30 minutes', $bank->time)>=strtotime(date("Y-m-d H:i"))) {
                return true;
            }
        }
        return false;
    }
    public function hasCryptoDeposit()
    {
        if ($bank=UserDepositsCryptoCompany::where(['user_id'=>$this->id])->orderBy('id','DESC')->first()) {
            if (strtotime('+30 minutes', $bank->time)>=strtotime(date("Y-m-d H:i"))) {
                return true;
            }
        }
        return false;
    }
}
