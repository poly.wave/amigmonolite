<?php

namespace App\Models;

class ApproveTypes extends Model
{
    protected $table = 'approve_types';
    protected $fillable = [
        'name'
    ];
}
