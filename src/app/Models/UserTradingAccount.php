<?php

namespace App\Models;

class UserTradingAccount extends Model
{
    protected $table = 'user_trading_account';
    protected $fillable = [
        'user_id', 'platform','user_trading_leverage_id','currency_id','login','password'
    ];
}
