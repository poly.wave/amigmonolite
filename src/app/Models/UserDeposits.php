<?php

namespace App\Models;

class UserDeposits extends Model
{
    protected $table = 'user_deposits';
    protected $fillable = [
        'name',
        'amount',
        'currency_id',
        'user_id',
        'data',
        'request_type_id',
        'approve_id',
        'deal',
        'order',
        'action',
        'entry',
        'time',
        'time_msc',
        'symbol',
        'comment',
        'bank_name',
        'crypto_id'
    ];
}
