<?php

namespace App\Models;

class Setting extends Model
{
    protected $table = 'settings';
    protected $fillable = [
        'time'
    ];
}
