<?php

namespace App\Models;

class Regions extends Model
{
    protected $table = 'regions';
    protected $fillable = [
        'code',
        'country_id',
        'name'
    ];
}
