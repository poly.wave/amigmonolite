<?php

namespace App\Models;

class Activation extends Model
{
    protected $table = 'activations';
    protected $fillable = [
        'user_id', 'activated', 'activation_key',
    ];
}
