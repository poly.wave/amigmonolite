<?php

namespace App\Models;

class TicketMessages extends Model
{
    protected $table = 'ticket_messages';
    protected $fillable = [
        'message',
        'user_id'
    ];
}
