<?php

namespace App\Models;

class Logs extends Model
{
    protected $table = 'logs';
    protected $fillable = [
        'type',
        'user_id',
        'ip',
        'device',
        'os',
        'location'
    ];
}
