<?php

 namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDepositsBanks extends Model
{
    //
        protected $table = 'user_deposits_banks';
        protected $fillable = [
            'user_id',
            'bank_id',
            'deposit_id',
            'time'
        ];
}
