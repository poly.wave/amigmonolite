<?php

namespace App\Models;

class ApiKeys extends Model
{
    protected $table = 'api_keys';
    protected $fillable = [
        'auth_key'
    ];
}
