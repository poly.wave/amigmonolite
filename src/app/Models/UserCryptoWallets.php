<?php

namespace App\Models;

class UserCryptoWallets extends Model
{
    protected $table = 'user_crypto_wallets';
    protected $fillable = [
        'user_id',
        'name',
        'account',
        'crypto_coin_id'
    ];
}
