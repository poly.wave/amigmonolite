<?php

namespace App\Models;

class CompanyBanks extends Model
{
    protected $table = 'company_banks';
    protected $fillable = [
        'icon',
        'name',
        'account_name',
        'iban',
        'swift',
        'address',
        'currency_id',
    ];
}
