<?php

namespace App\Models;

class Languages extends Model
{
    protected $table = 'languages';
    protected $fillable = [
        'name','code'
    ];
}
