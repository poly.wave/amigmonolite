<?php

namespace App\Models;

class Genders extends Model
{
    protected $table = 'genders';
    protected $fillable = [
        'name'
    ];
}
