<?php

namespace App\Models;

class UserBanks extends Model
{
    protected $table = 'user_banks';
    protected $fillable = [
        'user_id',
        'name',
        'address',
        'iban',
        'swift',
        'currency_id',
        'amount'
    ];
}
