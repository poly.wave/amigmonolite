<?php

namespace App\Models;

class Balance extends Model
{
    protected $table = 'balance';
    protected $fillable = [
        'type',
        'amount',
        'currency_id',
        'user_id'
    ];
}
