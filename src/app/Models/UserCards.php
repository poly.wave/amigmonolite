<?php

namespace App\Models;

class UserCards extends Model
{
    protected $table = 'user_cards';
    protected $fillable = [
        'user_id',
        'number',
        'month',
        'year',
        'cvv',
        'name',
        'currency_id'
    ];
}
