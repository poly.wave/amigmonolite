<?php

namespace App\Models;

class ConvertCurrency extends Model
{
    public $exchrate=null;


    public function fetch_exchange_rates($from,$to,$amount) {
      $now = time();
      $this->exchrate['EUR'] = 1.00;
      $interval=0;

      $url="http://www.ecb.int/stats/eurofxref/eurofxref-daily.xml";
      $stuff = @file( $url);
      $trace = "Fresh XML GET from URL";

      foreach ($stuff as $line)
      {
        if( preg_match("/time='([[:graph:]]+)'/",$line,$gotval) ) //found the time, save it..
        {
          $this->exchange_rate_time =$gotval[1]; //extract the value
        };
        preg_match("/currency='([[:alpha:]]+)'/",$line,$got_currency); //regex out the currency
        if (preg_match("/rate='([[:graph:]]+)'/",$line,$got_rate)) //regex out the rate
        {
          $this->exchrate[$got_currency[1]] = $got_rate[1]; //assign it to the global array
        }
      }
      return round($amount*$this->exchrate[$to] / $this->exchrate[$from],2);
    }
}
