<?php

namespace App\Models\MetaTrader;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
}
