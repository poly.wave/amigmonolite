<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActiveServices extends Model
{
    //
     protected $table = 'active_services';
    protected $fillable = [
        'service_name',
        'active'
    ];
}
