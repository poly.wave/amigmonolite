<?php

namespace App\Models;

class Currencies extends Model
{
    protected $table = 'currencies';
    protected $fillable = [
        'name',
        'symbol'
    ];
}
