<?php

 namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDepositsCryptoCompany extends Model
{
    //
        protected $table = 'user_deposits_crypto_company';
        protected $fillable = [
            'user_id',
            'crypto_company_id',
            'deposit_id',
            'time'
        ];
}
