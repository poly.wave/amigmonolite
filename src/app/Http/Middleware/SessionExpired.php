<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use Auth;
use Session;
use \App\Models\Setting;
use \App\Models\Logs;

class SessionExpired {
    protected $session;

    public function __construct(Store $session){
        $this->session = $session;
    }
    public function handle($request, Closure $next){
        $timeout = 1800;
        if (Setting::first()) {
           $timeout = Setting::first()->time*60;
        }
        if (Auth::user()) {
          if(!Session::has('lastActivityTime')){
            Session::put('lastActivityTime', time());
          } elseif(time() - Session::get('lastActivityTime') > $timeout){
              Session::forget('lastActivityTime');
              // $log = new Logs();
              // if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
              //     $ip= $_SERVER['HTTP_CLIENT_IP'];
              // } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
              //     $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
              // } else {
              //     $ip= $_SERVER['REMOTE_ADDR'];
              // }
              // $location="";
              // if (@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip)) {
              //     $dataArray = json_decode(@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
              //     $location = $dataArray->geoplugin_countryName;
              //   }
              //   $os=$device=$_SERVER["HTTP_USER_AGENT"];
              //   $os = explode(")",$os)[0];
              //   $os1 = explode("(",$os)[1];
              //   $device = explode(" ",$os1)[2];
              //   $os = explode(" ",$os1)[1];
              //  $log->create([
              //      'type'=>'Log out',
              //      'user_id'=>$user->id,
              //      'ip'=>$ip,
              //      'device'=>$device,
              //      'os'=>$os,
              //      'location'=>$location
              //  ]);
              auth()->logout();
          }
           Session::put('lastActivityTime', time());
        }
        return $next($request);
    }
}
