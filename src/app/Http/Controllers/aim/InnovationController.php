<?php


namespace App\Http\Controllers\Aim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InnovationController extends Controller
{
    public function index()
    {
        $this->response->title = 'Innovation';

        return $this->render('aim/innovation');
    }
}
