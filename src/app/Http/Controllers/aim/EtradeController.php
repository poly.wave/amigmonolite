<?php


namespace App\Http\Controllers\Aim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EtradeController extends Controller
{
    public function index()
    {
        $this->response->title = 'eTrade';

        return $this->render('aim/etrade');
    }
}
