<?php


namespace App\Http\Controllers\Aim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FinanceController extends Controller
{
    public function index()
    {
        $this->response->title = 'Finance';

        return $this->render('aim/finance');
    }
}

