<?php


namespace App\Http\Controllers\Aim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class PrivacyController extends Controller
{
    public function index()
    {
        $this->response->title = 'Privacy';

        return $this->render('aim/privacy');
    }
}
{

}
