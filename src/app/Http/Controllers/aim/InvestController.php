<?php


namespace App\Http\Controllers\Aim;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvestController extends Controller
{
    public function index()
    {
        $this->response->title = 'Invest';

        return $this->render('aim/invest');
    }
}
