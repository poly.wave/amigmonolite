<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WithdrawOptionController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.withdrawoptions');

        return $this->render('fx/withdrawOption');
    }
}
