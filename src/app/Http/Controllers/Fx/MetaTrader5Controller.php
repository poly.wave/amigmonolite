<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MetaTrader5Controller extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.metatrader5');

        return $this->render('fx/metaTrader5');
    }
}
