<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FundamentalAnalysisController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.funal');

        return $this->render('fx/fundamentalAnalysis');
    }
}
