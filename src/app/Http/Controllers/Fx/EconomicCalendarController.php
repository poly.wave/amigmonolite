<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EconomicCalendarController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.calendar');
        $this->response->contentClass = 'economic-calendar';

        return $this->render('fx/economicCalendar');
    }
}
