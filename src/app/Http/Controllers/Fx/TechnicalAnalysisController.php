<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TechnicalAnalysisController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.technicalan');

        return $this->render('fx/technicalAnalysis');
    }
}
