<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MajorPlayersController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.majorplayer');

        return $this->render('fx/majorPlayers');
    }
}
