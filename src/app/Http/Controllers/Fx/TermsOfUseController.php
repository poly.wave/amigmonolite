<?php


namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TermsOfUseController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.termofuse');
        $this->response->contentClass = 'terms-of-use';

        return $this->render('fx/termsofUse');
    }
}
