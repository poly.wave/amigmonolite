<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.edu');

        return $this->render('fx/education');
    }
}
