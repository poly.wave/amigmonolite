<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrepaidCardController extends Controller
{
    public function index()
    {
        $this->response->title = 'Prepaid Card';
        $this->response->contentClass = 'prepaid-card';

        return $this->render('fx/prepaidCard');
    }
}
