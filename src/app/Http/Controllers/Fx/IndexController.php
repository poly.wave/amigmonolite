<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->response->isHome = true;
        $this->response->title = __('fx.controller.title.home');

        return $this->render('fx/index');
    }
}
