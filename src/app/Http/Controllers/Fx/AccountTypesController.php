<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountTypesController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.accounttypes');

        return $this->render('fx/accountTypes');
    }
}
