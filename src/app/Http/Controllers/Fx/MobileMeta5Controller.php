<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MobileMeta5Controller extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.mobilemt5');

        return $this->render('fx/mobileMeta5');
    }
}
