<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompanyProfileController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.companyprofile');

        return $this->render('fx/companyProfile');
    }
}
