<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IntroducingBrokersController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.ibroker');

        return $this->render('fx/introducingBrokers');
    }
}
