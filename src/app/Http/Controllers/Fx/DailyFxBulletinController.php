<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DailyFxBulletinController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.dailyfx');
        $this->response->contentClass = 'daily-fx-bulletin';

        return $this->render('fx/dailyFxBulletin');
    }
}
