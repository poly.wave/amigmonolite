<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.aboutus');

        return $this->render('fx/aboutUs');
    }

}
