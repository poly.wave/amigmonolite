<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdvantagesController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.advantages') ;

        return $this->render('fx/advantages');
    }
}
