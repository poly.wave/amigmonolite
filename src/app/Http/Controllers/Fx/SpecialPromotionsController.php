<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SpecialPromotionsController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.specialpro');
        $this->response->contentClass = 'special-promotions';

        return $this->render('fx/specialPromotions');
    }
}
