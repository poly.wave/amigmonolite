<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepositOptionController extends Controller
{
    public function index()
    {
        $this->response->title =  __('fx.controller.title.depositoption');

        return $this->render('fx/depositOption');
    }
}
