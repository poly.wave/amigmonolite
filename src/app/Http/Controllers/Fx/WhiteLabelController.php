<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WhiteLabelController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.whitelabel');

        return $this->render('fx/whiteLabel');
    }
}
