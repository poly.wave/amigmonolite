<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $this->response->title =  __('fx.controller.title.connect');

        return $this->render('fx/contactUs');
    }
}
