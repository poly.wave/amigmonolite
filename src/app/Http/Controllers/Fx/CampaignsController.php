<?php


namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CampaignsController extends Controller
{
    public function index()
    {
        $this->response->title =  __('fx.controller.title.Campaigns');
        $this->response->contentClass = 'Campaigns';

        return $this->render('fx/campaigns');
    }
}
