<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WhatsForexController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.whatisforex');

        return $this->render('fx/whatsForex');
    }
}
