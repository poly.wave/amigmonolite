<?php

namespace App\Http\Controllers\Fx;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FxTradingController extends Controller
{
    public function index()
    {
        $this->response->title = __('fx.controller.title.adfxtrading');

        return $this->render('fx/fxTrading');
    }
}
