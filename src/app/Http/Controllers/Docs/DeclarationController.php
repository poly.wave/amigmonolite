<?php

namespace App\Http\Controllers\Docs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeclarationController extends Controller
{
    public function index()
    {
        $this->response->title = __('doc.docs');

        return $this->render('docs/declaration');
    }

}
