<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Languages;
use App\Models\UserGroups;
use App\Models\UserPassport;
use App\Models\Currencies;
use Illuminate\Support\Facades\Auth;

class AuthenticatedController extends Controller
{
    protected $currentUser;
    protected $level = 0;

    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $currentUser = Auth::user();

            $currentUser->name = explode('@', $currentUser->email)[0];
            $currentUser->cc_username = $currentUser->name;
            $currentUser->cc_password = md5($currentUser->cc_username);

            $cities = Cities::select('id', 'name')
                ->where('id', $currentUser->city_id)
                ->first();
            $currentUser->city = '';
            if(isset($cities->id)) {
                $currentUser->city = $cities->name;
            }

            $languages = Languages::select('id', 'name')
                ->where('id', $currentUser->language_id)
                ->first();
            $currentUser->language = '';
            if(isset($languages->id)) {
                $currentUser->language = $languages->name;
            }

            $currentUser->passport = UserPassport::where('user_id', $currentUser->id)->first();

            $this->currentUser = $currentUser;
            $this->response->currentUser = $currentUser;

            $userGroup = UserGroups::where('id', $currentUser->user_group_id)->first();
            $this->response->userGroup = $userGroup;
            $this->level = $userGroup->level;

            $userCurrency = Currencies::where('id', $currentUser->currency_id)->first();
            $this->response->userCurrency = $userCurrency;

            return $next($request);
        });

        # Generate menu
        $this->response->menu = [
            'main' => (object) [
                'name' => __('authenticated_controller.main'),
                'icon' => 'toolbox'
            ],

            'trading' => (object) [
                'name' => __('authenticated_controller.trading'),
                'icon' => 'chart-line'
            ],

            'deposit' => (object) [
                'name' => __('authenticated_controller.depositby'),
                'icon' => 'shopping-basket',
                'dropdown' => [
                    'bank_transfer' => (object) [
                        'name' => __('authenticated_controller.banktransfer'),
                        'icon' => 'university',
                    ],
                    'credit_card' => (object) [
                        'name' => __('authenticated_controller.creditcard'),
                        'icon' => 'credit-card',
                    ],
                    'crypto_coins' => (object) [
                        'name' => __('authenticated_controller.cryptocoins'),
                        'icon' => 'coins',
                    ]
                ]
            ],

            'withdrawal_request' => (object) [
                'name' => __('authenticated_controller.withdrawal'),
                'icon' => 'money-bill',
            ],

            'report' => (object) [
                'name' => __('authenticated_controller.report'),
                'icon' => 'receipt',
                'dropdown' => [
                    'deposits' => (object) [
                        'name' => __('authenticated_controller.deposits'),
                        'icon' => 'hand-holding-usd',
                    ],
                    'earnings' => (object) [
                        'name' => __('authenticated_controller.earnings'),
                        'icon' => 'money-bill',
                    ],
                    'withdrawals' => (object) [
                        'name' => __('authenticated_controller.withdrawals'),
                        'icon' => 'money-bill-wave',
                    ]
                ]
            ],

            'profile' => (object) [
                'name' => __('authenticated_controller.profile'),
                'icon' => 'user',
                'dropdown' => [
                    'personal_information' => (object) [
                        'name' => __('authenticated_controller.personalinfo'),
                        'icon' => 'user-cog',
                    ],
                    'user_passport' => (object) [
                        'name' => __('authenticated_controller.user_passport'),
                        'icon' => 'user-cog',
                    ],
                    'reset_password' => (object) [
                        'name' => __('authenticated_controller.reset'),
                        'icon' => 'dice-d6',
                    ],
                    'wallet' => (object) [
                        'name' => __('authenticated_controller.wallet'),
                        'icon' => 'wallet',
                        'dropdown' => [
                            'banks' => (object) [
                                'name' => __('authenticated_controller.banks'),
                                'icon' => 'university',
                            ],
                            'cards' => (object) [
                                'name' => __('authenticated_controller.creditcards'),
                                'icon' => 'credit-card',
                            ],
                            'crypto_wallets' => (object) [
                                'name' => __('authenticated_controller.cryptowallets'),
                                'icon' => 'coins',
                            ]
                        ]
                    ],
                    'files' => (object) [
                        'name' => __('authenticated_controller.files'),
                        'icon' => 'folder-open',
                    ]
                ]
            ],
        ];
    }
}
