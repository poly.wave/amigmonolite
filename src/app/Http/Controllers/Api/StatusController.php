<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\ApproveTypes;
use App\Models\CompanyBanks;
use App\Models\CompanyCrypto;

class StatusController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = ApproveTypes::orderBy('name', 'asc')->get();
        return response()->json($data);
    }
    public function companyBanks()
    {
        $data = CompanyBanks::select('company_banks.*', 'currencies.name as currency_name')
                ->join('currencies', 'currencies.id', '=', 'company_banks.currency_id')
                ->get();
        return response()->json($data);
    }
    public function companyCrypto()
    {
        $data = CompanyCrypto::orderBy('name', 'asc')->get();
        return response()->json($data);
    }

}
