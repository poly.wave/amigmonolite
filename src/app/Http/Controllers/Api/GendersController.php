<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Genders;

class GendersController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = Genders::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
