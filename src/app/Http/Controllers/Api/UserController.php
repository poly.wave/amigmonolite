<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\UserTradingAccount;
use App\Models\UserFiles;
use App\Models\DocumentTypes;

class UserController extends Controller
{

    function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function updateHashes()
    {
        $users = User::all();

        foreach ($users as $user) {
            $token = Str::random(60);

            User::where('id', $user->id)->update([
                'api_token' => hash('sha256', $token)
            ]);
        }
    }

    public function list()
    {
        $data = User::orderBy('id', 'asc')->get();

        return response()->json($data);
    }

    public function listMt()
    {
        $ids = UserTradingAccount::pluck('user_id');
        $data = User::whereIn('id',$ids)->orderBy('first_name', 'asc')->get();

        return response()->json($data);
    }

    public function documents($id)
    {
        $ids = UserFiles::where('user_id',$id)->pluck('document_type_id');
        $data = DocumentTypes::whereIn('id',$ids)->orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
