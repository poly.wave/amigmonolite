<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Languages;

class LanguagesController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = Languages::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
