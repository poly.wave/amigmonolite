<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


class QrCodeController extends Controller
{

    function __construct()
    {

    }

    public function generate()
    {
        return QRCode::text('QR Code Test!')
            ->setMargin(0)
            ->svg();
    }
}
