<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Regions;

class RegionsController extends Controller
{

    function __construct()
    {

    }

    public function list(Request $request)
    {
        $data = Regions::orderBy('name', 'asc')
            ->where('country_id', $request->input('country_id'))
            ->where('name', '!=', '')
            ->get();

        return response()->json($data);
    }
}
