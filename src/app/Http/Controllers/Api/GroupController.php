<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\UserGroups;

class GroupController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = UserGroups::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
