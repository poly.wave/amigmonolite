<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\CompanyBanks;
use App\Models\UserDepositsBanks;

class BanksController extends AuthenticatedController
{


    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $time = date('Y-m-d H:i');
        $date = strtotime($time) - 30*60;
        $data = UserDepositsBanks::select('user_deposits_banks.*', 'currencies.name as currency_name','company_banks.name as name','company_banks.account_name as account_name','company_banks.iban as iban'
        ,'company_banks.swift as swift','company_banks.address as address','company_banks.icon as icon')
                ->where('user_id', $this->currentUser->id)
                ->where('time', '>=',(int)$date)
                ->leftJoin('company_banks', 'company_banks.id', '=', 'user_deposits_banks.bank_id')
                ->join('currencies', 'currencies.id', '=', 'company_banks.currency_id')
                ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
