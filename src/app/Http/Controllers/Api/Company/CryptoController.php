<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\CompanyCrypto;

class CryptoController extends AuthenticatedController
{


    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $data = CompanyCrypto::get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
