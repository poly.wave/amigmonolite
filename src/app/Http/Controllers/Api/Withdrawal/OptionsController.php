<?php

namespace App\Http\Controllers\Api\Withdrawal;

use App\Http\Controllers\Controller;

use App\Models\WithdrawalOptions;

class OptionsController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = WithdrawalOptions::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
