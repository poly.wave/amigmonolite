<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Currencies;

class CurrenciesController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = Currencies::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
