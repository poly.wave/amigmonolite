<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\MtActions;

class ActionController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = MtActions::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
