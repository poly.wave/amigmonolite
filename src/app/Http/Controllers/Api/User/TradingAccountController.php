<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserTradingAccount;

class TradingAccountController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $data = UserTradingAccount::select('user_trading_account.*', 'user_trading_leverage.name as user_trading_leverage_name', 'currencies.name as currency_name')
            ->join('user_trading_leverage', 'user_trading_leverage.id', '=', 'user_trading_account.user_trading_leverage_id')
            ->join('currencies', 'currencies.id', '=', 'user_trading_account.currency_id')
            ->where('user_trading_account.user_id', $this->currentUser->id)
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
