<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserCryptoWallets;

class CryptoWalletsController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $data = UserCryptoWallets::select('user_crypto_wallets.*', 'crypto_coins.name as crypto_coin_name')
            ->join('crypto_coins', 'crypto_coins.id', '=', 'user_crypto_wallets.crypto_coin_id')
            ->where('user_crypto_wallets.user_id', $this->currentUser->id)
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
