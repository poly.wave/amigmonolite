<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserEarnings;

class EarningsController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function statistic()
    {
        $data = UserEarnings::select('amount', 'created_at','time')
            ->orderBy('created_at', 'asc')
            ->where('user_id', $this->currentUser->id)
            ->where('time', '>=', strtotime(date('Y') . '-01-01'));

        $data = $data->get();

        $response = [];
        foreach ($data as $row) {
          $month = date('M',$row->time);

            if(empty($response[$month])) {
                $response[$month] = 0;
            }

            $response[$month] += $row->amount;
        }

        return response()->json($response);
    }

    public function table()
    {
        $data = UserEarnings::select('user_earnings.*', 'currencies.name as currency')
            ->join('currencies', 'currencies.id', '=', 'user_earnings.currency_id')
            ->where('user_id', $this->currentUser->id);

        return datatables()->of($data)
            ->addIndexColumn()
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->make(true);
    }
}
