<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserWithdrawals;

class WithdrawalsController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function statistic()
    {
        $data = UserWithdrawals::select('amount', 'created_at','time')
            ->orderBy('created_at', 'asc')
            ->where('user_id', $this->currentUser->id)
            ->where('approve_type_id', 1)
            ->where('time', '>=', strtotime(date('Y') . '-01-01'))
            ->get();

        $response = [];
        foreach ($data as $row) {
          $month = date('M',$row->time);

            if(empty($response[$month])) {
                $response[$month] = 0;
            }

            $response[$month] += $row->amount;
        }

        return response()->json($response);
    }

    public function table()
    {
        $data = UserWithdrawals::select(
            'user_withdrawals.*',
            'currencies.name as currency',
            'approve_types.name as approve',
            'withdrawal_options.name as withdrawal_option_name'
        )
            ->join('currencies', 'currencies.id', '=', 'user_withdrawals.currency_id')
            ->join('approve_types', 'approve_types.id', '=', 'user_withdrawals.approve_type_id')
            ->join('withdrawal_options', 'withdrawal_options.id', '=', 'user_withdrawals.withdrawal_option_id')
            ->where('user_id', $this->currentUser->id);

        return datatables()->of($data)
            ->addIndexColumn()
            ->addColumn('approve', function ($data)
            {
                if ($data->approve == 'approved') {
                  return '<span class="text-success">'.$data->approve.'</span>';
                } elseif ($data->approve == 'pending') {
                  return '<span class="text-warning">'.$data->approve.'</span>';
                } else {
                  return '<span class="text-danger">'.$data->approve.'</span>';
                }
            })
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->rawColumns(['approve'])
            ->make(true);
    }
}
