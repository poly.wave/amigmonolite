<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserCards;

class CardsController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $data = UserCards::select('user_cards.*', 'currencies.name as currency_name')
            ->join('currencies', 'currencies.id', '=', 'user_cards.currency_id')
            ->where('user_cards.user_id', $this->currentUser->id)
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function list()
    {
        $data = UserCards::orderBy('id', 'asc')
            ->where('user_id', $this->currentUser->id)
            ->get();

        return response()->json($data);
    }
}
