<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserBanks;

class BanksController extends AuthenticatedController
{

    function __construct()
    {
        parent::__construct();
    }

    public function table()
    {
        $data = UserBanks::select('user_banks.*', 'currencies.name as currency_name')
            ->join('currencies', 'currencies.id', '=', 'user_banks.currency_id')
            ->where('user_banks.user_id', $this->currentUser->id)
            ->whereNull('user_banks.amount')
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
