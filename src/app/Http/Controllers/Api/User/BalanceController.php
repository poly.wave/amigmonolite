<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\Balance;

class BalanceController extends AuthenticatedController
{
    function __construct()
    {
        parent::__construct();
    }

    public function statistic()
    {
        $data = Balance::select('amount', 'created_at')
            ->orderBy('created_at', 'asc')
            ->where('user_id', $this->currentUser->id)
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime(date('Y') . '-01-01')));

        $data = $data->get();

        $response = [];
        foreach ($data as $row) {
            $month = $row->created_at->format('M');
            $type = $row->type;

            if(empty($response[$month])) {
                $response[$month] = 0;
            }

            if($type == 'minus') {
                $response[$month] -= $row->amount;
            } else {
                $response[$month] += $row->amount;
            }
        }

        return response()->json($response);
    }

    public function table()
    {
        $data = Balance::select('balance.*', 'currencies.name as currency')
            ->join('currencies', 'currencies.id', '=', 'deposits.currency_id')
            ->where('user_id', $this->currentUser->id);

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);
    }
}
