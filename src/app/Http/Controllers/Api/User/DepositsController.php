<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\AuthenticatedController;
use App\Models\UserDeposits;

class DepositsController extends AuthenticatedController
{
    function __construct()
    {
        parent::__construct();
    }

    public function statistic()
    {
        $data = UserDeposits::select('amount', 'created_at','time')
            ->orderBy('created_at', 'asc')
            ->where('user_id', $this->currentUser->id)
            ->where('approve_id', 1)
            ->where('time', '>=', strtotime(date('Y') . '-01-01'));

        $data = $data->get();

        $response = [];
        foreach ($data as $row) {

            $month = date('M',$row->time);
            if(empty($response[$month])) {
                $response[$month] = 0;
            }

            $response[$month] += $row->amount;
        }

        return response()->json($response);
    }

    public function table()
    {
        $data = UserDeposits::select('user_deposits.*', 'currencies.name as currency','approve_types.name as approve')
            ->join('currencies', 'currencies.id', '=', 'user_deposits.currency_id')
            ->join('approve_types', 'approve_types.id', '=', 'user_deposits.approve_id')
            ->where('user_id', $this->currentUser->id);

        return datatables()->of($data)
            ->addColumn('approve', function ($data)
            {
                if ($data->approve == 'approved') {
                  return '<span class="text-success">'.$data->approve.'</span>';
                } elseif ($data->approve == 'pending') {
                  return '<span class="text-warning">'.$data->approve.'</span>';
                } else {
                  return '<span class="text-danger">'.$data->approve.'</span>';
                }
            })
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->rawColumns(['approve'])
            ->addIndexColumn()
            ->make(true);

    }
}
