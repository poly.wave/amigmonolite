<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\CryptoCoins;

class CryptoCoinsController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = CryptoCoins::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
