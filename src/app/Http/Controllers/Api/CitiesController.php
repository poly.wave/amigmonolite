<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cities;

class CitiesController extends Controller
{

    function __construct()
    {

    }

    public function list(Request $request)
    {
        $data = Cities::orderBy('name', 'asc');

        $regionId = $request->input('region_id');
        $countryId = $request->input('country_id');
        $name = $request->input('name');

        if($regionId) {
            $data = $data->where('region_id', $regionId);
        }

        if($countryId) {
            $data = $data->where('country_id', $countryId);
        }

        if($name) {
            $data = $data->where('name', 'ILIKE', '%' . $name . '%');
        }

        $data = $data->limit(100)->get();

        $prepare = [];

        foreach ($data as $row) {
            $prepare[$row->name] = $row;
        }

        $response = [];

        foreach ($prepare as $row) {
            $response[] = $row;
        }

        return response()->json($response);
    }
}
