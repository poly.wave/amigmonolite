<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Countries;

class CountriesController extends Controller
{

    function __construct()
    {

    }

    public function list()
    {
        $data = Countries::orderBy('name', 'asc')->get();

        return response()->json($data);
    }
}
