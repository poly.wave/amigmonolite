<?php

namespace App\Http\Controllers\Capital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $this->response->title = 'Contact Us';

        return $this->render('capital/contactUs');
    }
}
