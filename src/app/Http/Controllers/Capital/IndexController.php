<?php

namespace App\Http\Controllers\Capital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $this->response->isHome = true;
        $this->response->title = 'Home';

        return $this->render('capital/index');
    }
}
