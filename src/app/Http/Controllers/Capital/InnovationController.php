<?php


namespace App\Http\Controllers\Capital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InnovationController extends Controller
{
    public function index()
    {
        $this->response->title = 'Innovation';

        return $this->render('capital/innovation');
    }
}
