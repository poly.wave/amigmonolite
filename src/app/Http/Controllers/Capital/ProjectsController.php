<?php

namespace App\Http\Controllers\Capital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    public function index()
    {
        $this->response->title = 'Projects';

        return $this->render('capital/projects');
    }
}
