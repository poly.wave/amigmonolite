<?php

namespace App\Http\Controllers\Capital;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index()
    {
        $this->response->title = 'About Us';

        return $this->render('capital/aboutUs');
    }
}
