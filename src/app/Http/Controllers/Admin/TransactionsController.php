<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Models\MetaTrader\MTWebAPI;
use App\Models\UserDeposits;
use App\Models\UserEarnings;
use App\Models\UserWithdrawals;
use App\Models\UserTradingAccount;
use App\Models\UserDepositsCryptoCompany;
use App\Models\User;
use \Carbon\Carbon;
use \App\Models\Cities;
use \App\Models\UserDepositsBanks;

class TransactionsController extends AdminController
{
    const VERIFY_CODE_LENGTH = 5;
    private $crypt_key = '682d95a2009e19fb3570ccb4d98b820f+dfgerth';
    private $user;
    private $verify_code;
    private $root_url;
    private $api;

    public function __construct(Request $request)
    {
        $this->api = new MTWebAPI();
        parent::__construct();

        $this->request = $request;
    }

    public function deposits()
    {
        $this->response->title = __('admin.sidebar.Deposits');
        $this->response->icon = 'money-bill';

        return $this->render('admin.transactions.deposits');
    }

    public function deposits_table()
    {
        $data = UserDeposits::select('user_deposits.*', 'currencies.name as currency','approve_types.name as approve','users.first_name as user')
            ->join('users', 'users.id', '=', 'user_deposits.user_id')
            ->join('currencies', 'currencies.id', '=', 'user_deposits.currency_id')
            ->join('approve_types', 'approve_types.id', '=', 'user_deposits.approve_id')
            ->get();

        return datatables()->of($data)
            ->addColumn('approve', function ($data)
            {
                if ($data->approve == 'approved') {
                  return '<span class="text-success">'.$data->approve.'</span>';
                } elseif ($data->approve == 'pending') {
                  return '<span class="text-warning">'.$data->approve.'</span>';
                } else {
                  return '<span class="text-danger">'.$data->approve.'</span>';
                }
            })
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->addColumn('action', function($row){
                $confirm = __('admin.Are you sure you want to delete this item?');
                $action = '<a href="/admin/transactions/edit-deposit/'.$row->id.'" title="'. __('admin.edit').'"><i class="fa fa-edit"></i></a>
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <a href="/admin/transactions/delete-deposit/'.$row->id.'" title="'. __('admin.delete').'"
                            onclick="return confirm(\''.$confirm.'\')"><i class="fa fa-trash-alt"></i>
                            </a>';
                if ($row->name=="Deposit by Bank Transfer") {
                    $action .= '<a href="/admin/transactions/send-bank-info/'.$row->id.'" title="'. __('admin.Send').'"><i class="fa fa-university"></i></a>';
                }
            // if($row->name=="Deposit by crypto currency") {
            //         $action .= '<a href="/admin/transactions/send-crypto-company-info/'.$row->id.'" title="'. __('admin.Send').'"><i class="fas fa-coins"></i></a>';
            //     }
                return $action;
               })
             ->rawColumns(['approve','action'])
             ->addIndexColumn()
             ->make(true);
    }

    public function withdrawals()
    {
        $this->response->title = __('admin.sidebar.Withdrawals');
        $this->response->icon = 'money-bill';

        return $this->render('admin.transactions.withdrawals');
    }

    public function withdrawals_table()
    {
        $data = UserWithdrawals::select(
                'user_withdrawals.*',
                'currencies.name as currency',
                'approve_types.name as approve',
                'users.first_name as user'
            )
            ->join('users', 'users.id', '=', 'user_withdrawals.user_id')
            ->join('currencies', 'currencies.id', '=', 'user_withdrawals.currency_id')
            ->join('approve_types', 'approve_types.id', '=', 'user_withdrawals.approve_type_id')
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->addColumn('approve', function ($data)
            {
                if ($data->approve == 'approved') {
                  return '<span class="text-success">'.$data->approve.'</span>';
                } elseif ($data->approve == 'pending') {
                  return '<span class="text-warning">'.$data->approve.'</span>';
                } else {
                  return '<span class="text-danger">'.$data->approve.'</span>';
                }
            })
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->addColumn('action', function($row){
                $confirm = __('admin.Are you sure you want to delete this item?');
                $action = '<a href="/admin/transactions/edit-withdrawal/'.$row->id.'" title="'. __('admin.edit').'"><i class="fa fa-edit"></i></a>
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <a href="/admin/transactions/delete-withdrawal/'.$row->id.'" title="'. __('admin.delete').'"
                            onclick="return confirm(\''.$confirm.'\')"><i class="fa fa-trash-alt"></i>
                            </a>';
                return $action;
               })
            ->rawColumns(['approve','action'])
            ->make(true);
    }

    public function earnings()
    {
        $this->response->title = __('admin.sidebar.Earnings');
        $this->response->icon = 'money-bill';

        return $this->render('admin.transactions.earnings');
    }

    public function earnings_table()
    {
        $data = UserEarnings::select('user_earnings.*', 'currencies.name as currency','users.first_name as user')
            ->join('currencies', 'currencies.id', '=', 'user_earnings.currency_id')
            ->join('users', 'users.id', '=', 'user_earnings.user_id')
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->editColumn('time', function ($data)
            {
                //change over here
                return date('Y-m-d H:i:s', $data->time);
            })
            ->addColumn('action', function($row){
                $confirm = __('admin.Are you sure you want to delete this item?');
                $action = '<a href="/admin/transactions/edit-earning/'.$row->id.'" title="'. __('admin.edit').'"><i class="fa fa-edit"></i></a>
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <a href="/admin/transactions/delete-earning/'.$row->id.'" title="'. __('admin.delete').'"
                            onclick="return confirm(\''.$confirm.'\')"><i class="fa fa-trash-alt"></i>
                            </a>';
                return $action;
               })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function createDeposit(Request $request)
    {
        $this->response->title = __('admin.transactions.add deposit');
        $this->response->icon = 'search-dollar';

        if ($request->input('amount')) {
            $validate = [
                'name' => ['required', 'string', 'max:255'],
                'amount' => ['required', 'string', 'max:255'],
                'user_id' => ['required'],
                'currency_id' => ['required', 'integer'],
                'approve_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_main = User::find($data['user_id']);
                if (isset($data['manually']) && $data['manually'] == 'on' && $data['approve_id'] == 1) {
                    if ($this->DepositChange($data['amount'],$data['comment'],$user_main->id)) {
                    } else {
                        $order = new UserDeposits;
                        $order->create([
                          'user_id'=>$user_main->id,
                          'amount'=>$data['amount'],
                          'deal'=>'-',
                          'action'=>1,
                          'entry'=>'-',
                          'time'=>strtotime(date("Y-m-d H:i")),
                          'time_msc'=>strtotime(date("Y-m-d H:i")),
                          'symbol'=>'-',
                          'name'=>$data['name'],
                          'currency_id'=>$data['currency_id'],
                          'comment'=>$data['comment'],
                          'approve_id'=>$data['approve_id']
                          ]
                        );
                        $user_main->update([
                          'deposits'=>$user_main->deposits+$data['amount'],
                          'balance'=>$user_main->balance+$data['amount'],
                        ]);
                        return redirect('admin/transactions/deposits')->with('warning','Created deposit without MetaTrader (User not register in the platform MetaTrader)');
                    }
                } else {
                    $order = new UserDeposits;
                    $order->create([
                      'user_id'=>$user_main->id,
                      'amount'=>$data['amount'],
                      'deal'=>'-',
                      'action'=>1,
                      'entry'=>'-',
                      'time'=>strtotime(date("Y-m-d H:i")),
                      'time_msc'=>strtotime(date("Y-m-d H:i")),
                      'symbol'=>'-',
                      'name'=>$data['name'],
                      'currency_id'=>$data['currency_id'],
                      'comment'=>$data['comment'],
                      'approve_id'=>$data['approve_id']
                      ]
                    );
                    $user_main->update([
                      'deposits'=>$user_main->deposits+$data['amount'],
                      'balance'=>$user_main->balance+$data['amount'],
                    ]);
                }
                return redirect('admin/transactions/deposits');
            }
        }
        $this->response->fields = [
            'user_id' => (object) [
                'name' => 'user_id',
                'label' => __('admin.transactions.user'),
                'type' => 'select',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/users/list',
                    'value' => 'id',
                    'name' => 'id,first_name,last_name',
                ],
            ],

            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'amount' => (object) [
                'name' => 'amount',
                'label' => __('admin.transactions.amount'),
                'value' => "",
                'placeholder' => __('admin.transactions.amount'),
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => false,
            ],
            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.transactions.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => __('admin.transactions.name'),
                'value' => "",
                'placeholder' => __('admin.transactions.name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'comment' => (object) [
                'name' => 'comment',
                'label' => __('admin.transactions.comment'),
                'value' => "",
                'placeholder' => __('admin.transactions.comment'),
                'type' => 'text',
                'required' => false,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'manually' => (object) [
                'name' => 'manually',
                'label' => __('admin.transactions.manually'),
                'type' => 'checkbox',
                'required' => false,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => 0,
            ],
        ];

        return $this->render('admin.transactions.create-deposit');
    }

    public function createWithdrawal(Request $request)
    {
        $this->response->title = __('admin.transactions.add withdrawal');
        $this->response->icon = 'search-dollar';

        if ($request->input('amount')) {
            $validate = [
                'name' => ['required', 'string', 'max:255'],
                'amount' => ['required', 'string', 'max:255'],
                'user_id' => ['required'],
                'currency_id' => ['required', 'integer'],
                'approve_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_main = User::find($data['user_id']);
                $amount_new = $data['amount']*(-1);
                if (isset($data['manually']) && $data['manually'] == 'on' && $data['approve_id'] == 1) {
                    if ($this->DepositChange($amount_new,$data['comment'],$user_main->id)) {
                    } else {
                        $order = new UserWithdrawals;
                        $order->create([
                          'user_id'=>$user_main->id,
                          'amount'=>$amount_new,
                          'deal'=>'-',
                          'action'=>1,
                          'entry'=>'-',
                          'time'=>strtotime(date("Y-m-d H:i")),
                          'time_msc'=>strtotime(date("Y-m-d H:i")),
                          'symbol'=>'-',
                          'name'=>$data['name'],
                          'currency_id'=>$data['currency_id'],
                          'comment'=>$data['comment'],
                          'approve_type_id'=>$data['approve_id'],
                          'withdrawal_option_id'=>1
                          ]
                        );
                        $user_main->update([
                          'withdrawals'=>$user_main->withdrawals+$data['amount'],
                          'balance'=>$user_main->balance+$amount_new,
                        ]);
                        return redirect('admin/transactions/withdrawals')->with('warning','Created withdrawal without MetaTrader (User not register in the platform MetaTrader)');
                    }
                } else {
                    $order = new UserWithdrawals;
                    $order->create([
                      'user_id'=>$user_main->id,
                      'amount'=>$amount_new,
                      'deal'=>'-',
                      'action'=>1,
                      'entry'=>'-',
                      'time'=>strtotime(date("Y-m-d H:i")),
                      'time_msc'=>strtotime(date("Y-m-d H:i")),
                      'symbol'=>'-',
                      'name'=>$data['name'],
                      'currency_id'=>$data['currency_id'],
                      'comment'=>$data['comment'],
                      'approve_type_id'=>$data['approve_id'],
                      'withdrawal_option_id'=>1
                      ]
                    );
                    $user_main->update([
                      'withdrawals'=>$user_main->withdrawals+$data['amount'],
                      'balance'=>$user_main->balance+$amount_new,
                    ]);
                }
                return redirect('admin/transactions/withdrawals');
            }
        }
        $this->response->fields = [
            'user_id' => (object) [
                'name' => 'user_id',
                'label' => __('admin.transactions.user'),
                'type' => 'select',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/users/list',
                    'value' => 'id',
                    'name' => 'id,first_name,last_name',
                ],
            ],

            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'amount' => (object) [
                'name' => 'amount',
                'label' => __('admin.transactions.amount'),
                'value' => "",
                'placeholder' => __('admin.transactions.amount'),
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => false,
            ],
            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.transactions.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => __('admin.transactions.name'),
                'value' => "",
                'placeholder' => __('admin.transactions.name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'comment' => (object) [
                'name' => 'comment',
                'label' => __('admin.transactions.comment'),
                'value' => "",
                'placeholder' => __('admin.transactions.comment'),
                'type' => 'text',
                'required' => false,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'manually' => (object) [
                'name' => 'manually',
                'label' => __('admin.transactions.manually'),
                'type' => 'checkbox',
                'required' => false,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => 0,
            ],
        ];

        return $this->render('admin.transactions.create-withdrawal');
    }

    public function createEarning(Request $request)
    {

        $this->response->title = __('admin.transactions.add earning');
        $this->response->icon = 'search-dollar';

        if ($request->input('amount')) {
            $validate = [
                'name' => ['required', 'string', 'max:255'],
                'amount' => ['required', 'string', 'max:255'],
                'user_id' => ['required'],
                'currency_id' => ['required', 'integer']
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_main = User::find($data['user_id']);
                $order = new UserEarnings;
                $order->create([
                  'user_id'=>$user_main->id,
                  'amount'=>$data['amount'],
                  'deal'=>'-',
                  'action'=>1,
                  'entry'=>'-',
                  'time'=>strtotime(date("Y-m-d H:i")),
                  'time_msc'=>strtotime(date("Y-m-d H:i")),
                  'symbol'=>'-',
                  'name'=>$data['name'],
                  'currency_id'=>$data['currency_id'],
                  'comment'=>$data['comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$data['amount'],
                  'balance'=>$user_main->balance+$data['amount'],
                ]);
                return redirect('admin/transactions/earnings');
            }
        }
        $this->response->fields = [
            'user_id' => (object) [
                'name' => 'user_id',
                'label' => __('admin.transactions.user'),
                'type' => 'select',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/users/list',
                    'value' => 'id',
                    'name' => 'id,first_name,last_name',
                ],
            ],

            'amount' => (object) [
                'name' => 'amount',
                'label' => __('admin.transactions.amount'),
                'value' => "",
                'placeholder' => __('admin.transactions.amount'),
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => false,
            ],
            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.transactions.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => __('admin.transactions.name'),
                'value' => "",
                'placeholder' => __('admin.transactions.name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'comment' => (object) [
                'name' => 'comment',
                'label' => __('admin.transactions.comment'),
                'value' => "",
                'placeholder' => __('admin.transactions.comment'),
                'type' => 'text',
                'required' => false,
                'icon' => 'building',
                'autofocus' => false,
            ],
        ];

        return $this->render('admin.transactions.create-earning');
    }

    public function editDeposit($id, Request $request)
    {
        $this->response->title = __('admin.transactions.edit deposit');
        $this->response->icon = 'search-dollar';
        $this->response->id = $id;

        $order = UserDeposits::find($id);
        if ($request->input('approve_id')) {
            $validate = [
                'approve_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $order = UserDeposits::find($id);
                $user_main = User::find($order->user_id);
                if (isset($data['manually']) && $data['manually'] == 'on' && $data['approve_id'] == 1) {
                    if ($this->DepositChange($order->amount,$order->comment,$user_main->id)) {
                        $order->delete();
                    } else {
                        $order->update([
                          'approve_id'=>$data['approve_id']
                          ]
                        );
                        $user_main->update([
                          'deposits'=>$user_main->deposits+$order->amount,
                          'balance'=>$user_main->balance+$order->amount,
                        ]);
                        return redirect('admin/transactions/deposits')->with('warning','Updated deposit without MetaTrader (User not register in the platform MetaTrader)');
                    }
                } else {
                    $order->update([
                      'approve_id'=>$data['approve_id']
                      ]
                    );
                    $user_main->update([
                      'deposits'=>$user_main->deposits+$order->amount,
                      'balance'=>$user_main->balance+$order->amount,
                    ]);
                }

                return redirect('admin/transactions/deposits');
            }
        }
        $this->response->fields = [
            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => $order->approve_id,
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'manually' => (object) [
                'name' => 'manually',
                'label' => __('admin.transactions.manually'),
                'type' => 'checkbox',
                'required' => false,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => 0,
            ],
        ];

        return $this->render('admin.transactions.edit-deposit');
    }

    public function sendBankInfo($id, Request $request)
    {
        $this->response->title = __('admin.transactions.send bank information');
        $this->response->icon = 'search-dollar';
        $this->response->id = $id;

        $order = UserDepositsBanks::find($id);
        if ($request->input('bank_id')) {
            $validate = [
                'bank_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_deposit_banks = UserDepositsBanks::where(['deposit_id'=>$id])->get();
                foreach ($user_deposit_banks as $user_deposit) {
                    $user_deposit->delete();
                }
                $user_deposit_bank = new UserDepositsBanks;
                $deposit = UserDeposits::find($id);
                $user_deposit_bank->create([
                  'bank_id'=>$data['bank_id'],
                  'deposit_id'=>$id,
                  'user_id'=>$deposit->user_id,
                  'time'=>strtotime(date("Y-m-d H:i")),
                  ]
                );
                return redirect('admin/transactions/deposits');
            }
        }

        $this->response->fields = [
            'bank_id' => (object) [
                'name' => 'bank_id',
                'label' => __('admin.transactions.bank name'),
                'type' => 'select-bank',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => !empty(UserDepositsBanks::where(['deposit_id'=>$id])->first())?UserDepositsBanks::where(['deposit_id'=>$id])->first()->bank_id:"",
                'data' => [
                    'url' => '/api/status/company-banks',
                    'value' => 'id',
                    'name' => 'id,name,account_name,address,currency_name,limit,active',
                ],
            ]
        ];

        return $this->render('admin.transactions.send-bank-info');
    }


        public function sendCryptoCompanyInfo($id, Request $request)
        {
            $this->response->title = __('admin.transactions.send crypto company information');
            $this->response->icon = 'search-dollar';
            $this->response->id = $id;

            if ($request->input('crypto_company_id')) {
                $validate = [
                    'crypto_company_id' => ['required', 'integer'],
                ];
                if ($request->validate($validate)) {
                    $data = $request->all();
                    $user_deposits_crypto_company = UserDepositsCryptoCompany::where(['deposit_id'=>$id])->get();
                    foreach ($user_deposits_crypto_company as $user_deposit) {
                        $user_deposit->delete();
                    }
                    $user_crypto_company= new UserDepositsCryptoCompany;
                    $deposit = UserDeposits::find($id);
                    $user_crypto_company->create([
                      'crypto_company_id'=>$data['crypto_company_id'],
                      'deposit_id'=>$id,
                      'user_id'=>$deposit->user_id,
                      'time'=>strtotime(date("Y-m-d H:i")),
                      ]
                    );
                    return redirect('admin/transactions/deposits');
                }
            }
            $this->response->fields = [
                'crypto_company_id' => (object) [
                    'name' => 'crypto_company_id',
                    'label' => __('admin.transactions.crypto company name'),
                    'type' => 'select',
                    'required' => true,
                    'icon' => 'group',
                    'autofocus' => false,
                    'value' => !empty(UserDepositsCryptoCompany::where(['deposit_id'=>$id])->first())?UserDepositsCryptoCompany::where(['deposit_id'=>$id])->first()->crypto_company_id:"",
                    'data' => [
                        'url' => '/api/status/crypto-company',
                        'value' => 'id',
                        'name' => 'name',
                    ],
                ]
            ];

            return $this->render('admin.transactions.send-crypto-company-info');
        }

    public function editWithdrawal($id, Request $request)
    {
        $this->response->title = __('admin.transactions.edit withdrawal');
        $this->response->icon = 'search-dollar';
        $this->response->id = $id;

        $order = UserWithdrawals::find($id);
        if ($request->input('approve_id')) {
            $validate = [
                'approve_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_main = User::find($order->user_id);
                if (isset($data['manually']) && $data['manually'] == 'on' && $data['approve_id'] == 1) {
                    if ($this->DepositChange($order->amount,$order->comment,$user_main->id)) {
                        $order->delete();
                    } else {
                        $order->update([
                          'approve_type_id'=>$data['approve_id']
                          ]
                        );
                        $user_main->update([
                          'withdrawals'=>$user_main->withdrawals+abs($order->amount),
                          'balance'=>$user_main->balance+$order->amount,
                        ]);
                        return redirect('admin/transactions/withdrawals')->with('warning','Updated withdrawals without MetaTrader (User not register in the platform MetaTrader)');
                    }
                } else {
                    $order->update([
                          'approve_type_id'=>$data['approve_id']
                      ]
                    );
                    $user_main->update([
                      'withdrawals'=>$user_main->withdrawals+abs($order->amount),
                      'balance'=>$user_main->balance+$order->amount,
                    ]);
                }

                return redirect('admin/transactions/withdrawals');
            }
        }
        $this->response->fields = [
            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => $order->approve_type_id,
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'manually' => (object) [
                'name' => 'manually',
                'label' => __('admin.transactions.manually'),
                'type' => 'checkbox',
                'required' => false,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => 0,
            ],
        ];

        return $this->render('admin.transactions.edit-withdrawal');
    }

    public function editEarning($id, Request $request)
    {
        $this->response->title = __('admin.transactions.edit earning');
        $this->response->icon = 'search-dollar';

        $this->response->id = $id;
        $transaction = UserEarnings::find($id);

        if ($request->input('amount')) {
            $validate = [
                'name' => ['required', 'string', 'max:255'],
                'amount' => ['required', 'string', 'max:255'],
                'user_id' => ['required'],
                'currency_id' => ['required', 'integer']
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $user_main = User::find($data['user_id']);
                $new_amount = $data['amount'] - $transaction->amount;
                $transaction->update([
                  'user_id'=>$user_main->id,
                  'amount'=>$data['amount'],
                  'time'=>strtotime(date("Y-m-d H:i")),
                  'time_msc'=>strtotime(date("Y-m-d H:i")),
                  'name'=>$data['name'],
                  'currency_id'=>$data['currency_id'],
                  'comment'=>$data['comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$new_amount,
                  'balance'=>$user_main->balance+$new_amount,
                ]);
                return redirect('admin/transactions/earnings');
            }
        }
        $this->response->fields = [
            'user_id' => (object) [
                'name' => 'user_id',
                'label' => __('admin.transactions.user'),
                'type' => 'select',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => $transaction->user_id,
                'data' => [
                    'url' => '/api/users/list',
                    'value' => 'id',
                    'name' => 'id,first_name,last_name',
                ],
            ],

            'amount' => (object) [
                'name' => 'amount',
                'label' => __('admin.transactions.amount'),
                'value' => $transaction->amount,
                'placeholder' => __('admin.transactions.amount'),
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => false,
            ],
            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.transactions.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => $transaction->currency_id,
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => __('admin.transactions.name'),
                'value' => $transaction->name,
                'placeholder' => __('admin.transactions.name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
            'comment' => (object) [
                'name' => 'comment',
                'label' => __('admin.transactions.comment'),
                'value' => $transaction->comment,
                'placeholder' => __('admin.transactions.comment'),
                'type' => 'text',
                'required' => false,
                'icon' => 'building',
                'autofocus' => false,
            ],
        ];

        return $this->render('admin.transactions.edit-earning');

    }

    public function deleteDeposit($id)
    {
        $transaction = UserDeposits::find($id);
        $transaction->delete();
        return back();
    }

    public function deleteWithdrawal($id)
    {
        $transaction = UserWithdrawals::find($id);
        $transaction->delete();
        return back();
    }

    public function deleteEarning($id)
    {
        $transaction = UserEarnings::find($id);
        $transaction->delete();
        return back();
    }

    public function DepositChange($amount,$comment,$user_id)
    {
      if ($account = UserTradingAccount::where('user_id',$user_id)->first()) {
          $login = $account->login;
          try {
              $this->api->UserDepositChange($login, $amount, $comment, 2);
              $this->GetUserInfo($user_id);
          } catch (\Exception $e) {

          }


          return true;
      }
      return false;
    }

    public function GetUserInfo($user_id)
    {
      $account = UserTradingAccount::where('user_id',$user_id)->first();
      if ($account) {
        $params       = array();
        $answer       = array();
        $answer_boddy = array();
        $params['login']=$account->login;
        $params['from']=strtotime('01.01.2019');
        $params['to']=strtotime(Carbon::now()->addDays(1));
        $params['OFFSET ']='0';
        $params['TOTAL']='100';
        $str =$this->api->CustomSend('DEAL_GET_PAGE', $params, '', $answer, $answer_boddy);
        $json = preg_replace('/[[:cntrl:]]/', '', $str);
        $json = json_decode($json, true);
        $user_main = \App\Models\User::find($user_id);
        if (gettype($json) == 'array') {
          foreach ($json as $key) {
            if ($key['Order'] == 0 && $key['Action'] == 2 && $key['Entry'] == 0) {
              if ($key['Profit']/abs($key['Profit'])==1) {
                $order = new UserDeposits;
                if (!UserDeposits::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_id'=>1,
                    'request_type_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'deposits'=>$user_main->deposits+$key['Profit'],
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
              if ($key['Profit']/abs($key['Profit'])==-1) {
                $order = new UserWithdrawals;
                if (!UserWithdrawals::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_type_id'=>1,
                    'withdrawal_option_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'withdrawals'=>$user_main->withdrawals+abs($key['Profit']),
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
            }
            if ($key['Order'] != 0 && $key['Entry'] == 1) {
              $order = new UserEarnings;
              if (!UserEarnings::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                $order->create([
                  'user_id'=>$user_id,
                  'amount'=>$key['Profit'],
                  'deal'=>$key['Deal'],
                  'order'=>$key['Order'],
                  'action'=>$key['Action'],
                  'entry'=>$key['Entry'],
                  'time'=>$key['Time'],
                  'time_msc'=>$key['TimeMsc'],
                  'symbol'=>$key['Symbol'],
                  'name'=>$key['Symbol'],
                  'currency_id'=>1,
                  'comment'=>$key['Comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$key['Profit'],
                  'balance'=>$user_main->balance+$key['Profit'],
                ]);
              }
            }
          }
        }
      }
  }
}
