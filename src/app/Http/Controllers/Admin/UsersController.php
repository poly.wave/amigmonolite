<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\AddUserMailable;
use App\Models\User;
use App\Models\Cities;
use App\Models\UserBanks;
use App\Models\UserCards;
use App\Models\UserCryptoWallets;
use App\Models\UserDeposits;
use App\Models\UserEarnings;
use App\Models\UserFiles;
use App\Models\UserPassport;
use App\Models\UserTradingAccount;
use App\Models\UserTradingLeverage;
use App\Models\UserWithdrawals;

class UsersController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->response->title = __('admin.sidebar.Users');
        $this->response->icon = 'users';

        return $this->render('admin.users.index');
    }

    public function table()
    {
        $data = User::select('users.*', 'user_trading_account.login as mt_login', 'user_groups.name as group')
            ->leftJoin('user_trading_account', 'user_trading_account.user_id', '=', 'users.id')
            ->join('user_groups', 'user_groups.id', '=', 'users.user_group_id')
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $confirm = __('admin.Are you sure you want to delete this user?');
                $action = '<a href="/admin/users/edit/'.$row->id.'" title="'. __('admin.edit').'"><i class="fa fa-edit"></i></a>
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <a href="/admin/users/delete/'.$row->id.'" title="'. __('admin.delete').'"
                            onclick="return confirm(\''.$confirm.'\')"><i class="fa fa-trash-alt"></i>
                            </a>';
                if (UserFiles::where('user_id',$row->id)->first()) {
                    $action.='<a href="/admin/users/edit-files/'.$row->id.'" title="'. __('admin.users.change files status').'" style="padding-right:5px"><i class="fa fa-file"></i></a>';
                }
                if (UserPassport::where('user_id',$row->id)->first()) {
                    $action.='<a href="/admin/users/edit-passport/'.$row->id.'" title="'. __('admin.users.change passport status').'"><i class="fa fa-passport"></i></a>';
                }
                return $action;
               })
            ->rawColumns(['action'])
            ->make(true);

    }
    public function create(Request $request)
    {
        $this->response->title = __('admin.users.add user');
        $this->response->icon = 'user-cog';

        $update = [];
        if ($request->input('email')) {
            $validate = [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
                'password' => ['required', 'string', 'max:255'],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'day_of_birth' => ['required', 'string'],
                'phone' => ['required', 'string', 'unique:users,phone'],
                'resident_address' => ['required', 'string'],
                'city' => ['required', 'string'],
                'region_id' => ['required', 'integer'],
                'country_id' => ['required', 'integer'],
                'gender_id' => ['required', 'integer'],
                'user_group_id' => ['required', 'integer'],
                'language_id' => ['required', 'integer'],
                'currency_id' => ['required', 'integer'],
            ];
            $avatar="";
            $id = User::orderBy('id','DESC')->first()->id;
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $file_name = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();

                $path = 'profile/' . $id . '/' . $file_name;
                Storage::put($path, File::get($file));

                $avatar = Storage::url($path);
            }
            if ($request->validate($validate)) {
                $data = $request->all();
                $cityId = 0;
                if($data['city']) {
                    $cities = Cities::select('id', 'name');
                    $cities = $cities->where('name', $data['city']);
                    $city = $cities->first();

                    if(isset($city->id)) {
                        $cityId = $city->id;
                    }
                }
                $token = Str::random(60);
                $link = hash('sha256', $token);
                $update = [
                    'avatar' => $avatar,
                    'email' => $data['email'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'day_of_birth' => $data['day_of_birth'],
                    'phone' => $data['phone'],
                    'resident_address' => $data['resident_address'],
                    'city_id' => $cityId,
                    'region_id' => $data['region_id'],
                    'country_id' => $data['country_id'],
                    'gender_id' => $data['gender_id'],
                    'currency_id' => $data['currency_id'],
                    'language_id' => $data['language_id'],
                    'user_group_id' => $data['user_group_id'],
                    'password_without_hash'=>$data['password'],
                    'password' => Hash::make($data['password']),
                    'created_by'=>\Auth::user()->first_name." ".\Auth::user()->last_name,
                    'api_token' => $link,
                ];

                $user = new User();
                $user->fill($update);

                if ($user->save()) {
                    $update['city'] = $data['city'];
                    $this->response->success = true;
                    $name = $user->first_name." ".$user->last_name;
                    $email = $user->email;
                    $password = $user->password_without_hash;
                    Mail::to($email)->send(new AddUserMailable($name, $password,$email,$link));
                    return redirect('admin/users');
                }
            }
        }
        $this->response->fields = [
          ## Avatar
          'avatar' => (object) [
              'name' => 'avatar',
              'label' => __('admin.users.avatar'),
              'value' =>'',
              'placeholder' => '',
              'type' => 'file',
              'required' => false,
              'icon' => 'image',
              'mime_type' => 'image',
              'autofocus' => false,
          ],
            ## Email
            'email' => (object) [
                'name' => 'email',
                'label' => __('admin.users.email'),
                'value' => "",
                'placeholder' => __('admin.users.email'),
                'type' => 'email',
                'required' => true,
                'icon' => 'at',
                'autofocus' => true,
            ],
            'password' => (object) [
                'name' => 'password',
                'label' => __('admin.users.password'),
                'value' => "",
                'placeholder' => __('admin.users.password'),
                'type' => 'password',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],
            ## Name
            'first_name' => (object) [
                'name' => 'first_name',
                'label' => __('admin.users.first_name'),
                'value' => "",
                'placeholder' => __('admin.users.first_name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],
            'last_name' => (object) [
                'name' => 'last_name',
                'label' => __('admin.users.last_name'),
                'value' => "",
                'placeholder' => __('admin.users.last_name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],

            ## Birthday
            'day_of_birth' => (object) [
                'name' => 'day_of_birth',
                'label' => __('admin.users.day_of_birth'),
                'value' => "",
                'placeholder' => __('admin.users.day_of_birth'),
                'type' => 'date',
                'required' => true,
                'icon' => 'calendar',
                'autofocus' => false,
            ],

            ## Phone
            'phone' => (object) [
                'name' => 'phone',
                'label' => __('admin.users.phone'),
                'value' => "",
                'placeholder' => __('admin.users.phone'),
                'type' => 'tel',
                'required' => true,
                'icon' => 'phone',
                'autofocus' => false,
            ],

            ## Gender
            'gender_id' => (object) [
                'name' => 'gender_id',
                'label' => __('admin.users.gender'),
                'type' => 'select',
                'required' => true,
                'icon' => 'venus-mars',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/genders/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Other
            'user_group_id' => (object) [
                'name' => 'user_group_id',
                'label' => __('admin.users.group'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/group/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'language_id' => (object) [
                'name' => 'language_id',
                'label' => __('admin.users.language'),
                'type' => 'select',
                'required' => true,
                'icon' => 'language',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/languages/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.users.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Location
            'country_id' => (object) [
                'name' => 'country_id',
                'label' => __('admin.users.country'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/countries/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'region_id' => (object) [
                'name' => 'region_id',
                'label' => __('admin.users.region'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/regions/list',
                    'value' => 'id',
                    'name' => 'name',
                    'relation_to' => 'country_id',
                ],
            ],
            'city' => (object) [
                'name' => 'city',
                'label' => __('admin.users.city'),
                'placeholder' => __('admin.users.city'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
                'value' => "",
                'data' => [
                    'url' => '/api/cities/list',
                    'value' => 'id',
                    'field' => 'name',
                ],
            ],
            'resident_address' => (object) [
                'name' => 'resident_address',
                'label' => __('admin.users.resident_address'),
                'value' => "",
                'placeholder' => __('admin.users.resident_address'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
        ];

        return $this->render('admin.users.create');
    }

    public function edit($id,Request $request)
    {
        $this->response->title = __('admin.users.update user');
        $this->response->icon = 'user-cog';

        $update = [];
        $user = User::find($id);
        if ($request->input('email')) {
            $validate = [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $id],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'day_of_birth' => ['required', 'string'],
                'phone' => ['required', 'string', 'unique:users,phone,'.$id],
                'resident_address' => ['required', 'string'],
                'city' => ['required', 'string'],
                'region_id' => ['required', 'integer'],
                'country_id' => ['required', 'integer'],
                'gender_id' => ['required', 'integer'],
                'user_group_id' => ['required', 'integer'],
                'language_id' => ['required', 'integer'],
                'currency_id' => ['required', 'integer'],
            ];
            $avatar=$user->avatar;
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $file_name = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();

                $path = 'profile/' . $id . '/' . $file_name;
                Storage::put($path, File::get($file));

                $avatar = Storage::url($path);
            }
            if ($request->validate($validate)) {
                $data = $request->all();
                $cityId = 0;
                if($data['city']) {
                    $cities = Cities::select('id', 'name');
                    $cities = $cities->where('name', $data['city']);
                    $city = $cities->first();

                    if(isset($city->id)) {
                        $cityId = $city->id;
                    }
                }
                $update = [
                    'avatar' => $avatar,
                    'email' => $data['email'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'day_of_birth' => $data['day_of_birth'],
                    'phone' => $data['phone'],
                    'resident_address' => $data['resident_address'],
                    'city_id' => $cityId,
                    'region_id' => $data['region_id'],
                    'country_id' => $data['country_id'],
                    'gender_id' => $data['gender_id'],
                    'currency_id' => $data['currency_id'],
                    'language_id' => $data['language_id'],
                    'user_group_id' => $data['user_group_id'],
                ];

                $user->fill($update);

                if ($user->save()) {
                    $update['city'] = $data['city'];
                    $this->response->success = true;
                    return redirect('admin/users');
                }
            }
        }
        $this->response->id=$id;
        $this->response->fields = [
          ## Avatar
          'avatar' => (object) [
              'name' => 'avatar',
              'label' => __('admin.users.avatar'),
              'value' =>'',
              'placeholder' => '',
              'type' => 'file',
              'required' => false,
              'icon' => 'image',
              'mime_type' => 'image',
              'autofocus' => false,
          ],
            ## Email
            'email' => (object) [
                'name' => 'email',
                'label' => __('admin.users.email'),
                'value' => $user->email,
                'placeholder' => __('admin.users.email'),
                'type' => 'email',
                'required' => true,
                'icon' => 'at',
                'autofocus' => true,
            ],
            ## Name
            'first_name' => (object) [
                'name' => 'first_name',
                'label' => __('admin.users.first_name'),
                'value' => $user->first_name,
                'placeholder' => __('admin.users.first_name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],
            'last_name' => (object) [
                'name' => 'last_name',
                'label' => __('admin.users.last_name'),
                'value' => $user->last_name,
                'placeholder' => __('admin.users.last_name'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],

            ## Birthday
            'day_of_birth' => (object) [
                'name' => 'day_of_birth',
                'label' => __('admin.users.day_of_birth'),
                'value' => $user->day_of_birth,
                'placeholder' => __('admin.users.day_of_birth'),
                'type' => 'date',
                'required' => true,
                'icon' => 'calendar',
                'autofocus' => false,
            ],

            ## Phone
            'phone' => (object) [
                'name' => 'phone',
                'label' => __('admin.users.phone'),
                'value' => $user->phone,
                'placeholder' => __('admin.users.phone'),
                'type' => 'tel',
                'required' => true,
                'icon' => 'phone',
                'autofocus' => false,
            ],

            ## Gender
            'gender_id' => (object) [
                'name' => 'gender_id',
                'label' => __('admin.users.gender'),
                'type' => 'select',
                'required' => true,
                'icon' => 'venus-mars',
                'autofocus' => false,
                'value' => $user->gender_id,
                'data' => [
                    'url' => '/api/genders/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Other
            'user_group_id' => (object) [
                'name' => 'user_group_id',
                'label' => __('admin.users.group'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => $user->user_group_id,
                'data' => [
                    'url' => '/api/group/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'language_id' => (object) [
                'name' => 'language_id',
                'label' => __('admin.users.language'),
                'type' => 'select',
                'required' => true,
                'icon' => 'language',
                'autofocus' => false,
                'value' => $user->language_id,
                'data' => [
                    'url' => '/api/languages/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('admin.users.currency'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => $user->currency_id,
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Location
            'country_id' => (object) [
                'name' => 'country_id',
                'label' => __('admin.users.country'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => $user->country_id,
                'data' => [
                    'url' => '/api/countries/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'region_id' => (object) [
                'name' => 'region_id',
                'label' => __('admin.users.region'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => $user->region_id,
                'data' => [
                    'url' => '/api/regions/list',
                    'value' => 'id',
                    'name' => 'name',
                    'relation_to' => 'country_id',
                ],
            ],
            'city' => (object) [
                'name' => 'city',
                'label' => __('admin.users.city'),
                'placeholder' => __('admin.users.city'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
                'value' => $user->city,
                'data' => [
                    'url' => '/api/cities/list',
                    'value' => 'id',
                    'field' => 'name',
                ],
            ],
            'resident_address' => (object) [
                'name' => 'resident_address',
                'label' => __('admin.users.resident_address'),
                'value' => $user->resident_address,
                'placeholder' => __('admin.users.resident_address'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
        ];

        return $this->render('admin.users.edit');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user_banks = UserBanks::where('user_id',$id)->get();
        $user_cards = UserCards::where('user_id',$id)->get();
        $user_crypto_wallets = UserCryptoWallets::where('user_id',$id)->get();
        $user_deposits = UserDeposits::where('user_id',$id)->get();
        $user_earnings = UserEarnings::where('user_id',$id)->get();
        $user_files = UserFiles::where('user_id',$id)->get();
        $user_passports = UserPassport::where('user_id',$id)->get();
        $user_trading_accounts = UserTradingAccount::where('user_id',$id)->get();
        $user_trading_leverages = UserTradingLeverage::where('user_id',$id)->get();
        $user_withdrawals = UserWithdrawals::where('user_id',$id)->get();

        foreach ($user_banks as $u) {
            $u->delete();
        }
        foreach ($user_cards as $u) {
            $u->delete();
        }
        foreach ($user_crypto_wallets as $u) {
            $u->delete();
        }
        foreach ($user_deposits as $u) {
            $u->delete();
        }
        foreach ($user_earnings as $u) {
            $u->delete();
        }
        foreach ($user_files as $u) {
            $u->delete();
        }
        foreach ($user_passports as $u) {
            $u->delete();
        }
        foreach ($user_trading_accounts as $u) {
            $u->delete();
        }
        foreach ($user_withdrawals as $u) {
            $u->delete();
        }
        $user->delete();
        return back();
    }

    public function editPassport($id, Request $request)
    {
        $this->response->title = __('admin.users.change passport status');
        $this->response->icon = 'user-cog';
        $this->response->id = $id;

        $order = UserPassport::where('user_id',$id)->first();
        if ($request->input('approve_id')) {
            $validate = [
                'approve_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $order->update([
                  'approve_id'=>$data['approve_id']
                  ]
                );

                return redirect('admin/users');
            }
        }
        $this->response->fields = [
            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => $order->approve_id,
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
        ];

        return $this->render('admin.users.edit-passport');
    }

    public function editFiles($id, Request $request)
    {
        $this->response->title = __('admin.users.change files status');
        $this->response->icon = 'user-cog';
        $this->response->id = $id;

        if ($request->input('approve_id')) {
            $validate = [
                'approve_id' => ['required', 'integer'],
                'document_type_id' => ['required', 'integer'],
            ];
            if ($request->validate($validate)) {
                $data = $request->all();
                $order = UserFiles::where([['user_id',$id],['document_type_id',$data['document_type_id']]])->first();
                $order->update([
                  'approve_type_id'=>$data['approve_id'],
                  'document_type_id'=>$data['document_type_id']
                  ]
                );

                return redirect('admin/users');
            }
        }

        $this->response->fields = [
            'document_type_id' => (object) [
                'name' => 'document_type_id',
                'label' => __('admin.transactions.document_type_id'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => '',
                'data' => [
                    'url' => '/api/users/documents/'.$id,
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'approve_id' => (object) [
                'name' => 'approve_id',
                'label' => __('admin.transactions.approve'),
                'type' => 'select',
                'required' => true,
                'icon' => 'group',
                'autofocus' => false,
                'value' => '',
                'data' => [
                    'url' => '/api/status/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
        ];
        $data = UserFiles::select('user_files.*','approve_types.name as approve')
            ->join('approve_types', 'approve_types.id', '=', 'user_files.approve_type_id')
            ->where('user_id', $id)->get();
        $this->response->files = $data;

        return $this->render('admin.users.edit-files');
    }
}
