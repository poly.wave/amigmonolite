<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class DashboardController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->response->title = __('admin.title');
        $this->response->icon = 'hand-holding-usd';

        return $this->render('admin/dashboard');
    }
}
