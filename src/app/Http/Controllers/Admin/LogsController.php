<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Logs;

class LogsController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->response->title = __('admin.sidebar.Logs');
        $this->response->icon = 'user';

        return $this->render('admin.logs.index');
    }

    public function table()
    {
        $data = Logs::select('logs.*', 'users.first_name as username')
            ->join('users', 'users.id', '=', 'logs.user_id')
            ->get();

        return datatables()->of($data)
            ->addIndexColumn()
            ->make(true);

    }
}
