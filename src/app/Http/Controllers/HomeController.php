<?php

namespace App\Http\Controllers;

use App\Models\MetaTrader\WebPageForm;
use Illuminate\Http\Request;
use App\Mail\ConfirmTraderMailable;
use App\Models\Activation;
use App\Models\UserTradingAccount;
use App\Models\UserTradingLeverage;
use App\Models\UserDeposits;
use App\Models\UserEarnings;
use App\Models\UserWithdrawals;
use App\Models\ConvertCurrency;
use App\Models\MetaTrader\MTWebAPI;
use App\Models\MetaTrader\MTRetCode;
use Illuminate\Support\Facades\Mail;
use \Carbon\Carbon;


class HomeController extends Controller
{
  const VERIFY_CODE_LENGTH = 5;
  private $crypt_key = '682d95a2009e19fb3570ccb4d98b820f+dfgerth';
  private $user;
  private $verify_code;
  private $root_url;
  private $api;

  function __construct()
    {
    //---- start session
    session_start();
    //---- initialize
    $this->user = array('email' => '', 'name' => '', 'password' => '', 'country' => '', 'state' => '', 'city' => '', 'zipcode' => '', 'phone' => '', 'phone_password' => '', 'address' => '', 'password' => '', 'confirm_password' => '', 'invest_password' => '');
    //---
    $this->root_url = $_SERVER['HTTP_HOST'];
    //---
    $this->api = new MTWebAPI();
    // $this->api->SetLoggerWriteDebug(IS_WRITE_DEBUG_LOG);
    }
    public function image()
    {
        return view('member.image');
    }
  public function register(Request $request)
    {

    //--- validation request
    if (!isset($_REQUEST['user']) || !is_array($_REQUEST['user']) || !isset($_REQUEST['verify_code']) || empty($_REQUEST['verify_code']) || !isset($_SESSION['verify_code']) || empty($_SESSION['verify_code']))
      {
        return back()->with('error','Invalid registration data. Please correct it and try again.');
      // exit;
      }
    //--- receive input
    $this->user = $_REQUEST['user'];
    $this->verify_code = $_REQUEST['verify_code'];
    //----
    foreach ($this->user as $key => $value) $value = trim($value);
    //--- validation input
    if ($_SESSION['verify_code'] != $this->verify_code || $this->user['password'] != $this->user['confirm_password'] || empty($this->user['email']) || empty($this->user['name']) || empty($this->user['password']) || empty($this->user['country']) || empty($this->user['state']) || empty($this->user['city']) || empty($this->user['zipcode']) || empty($this->user['phone']) || empty($this->user['address']) || !$this->CheckPassword($this->user['password']))
      {
      $error = '';
      if ($_SESSION['verify_code'] != $this->verify_code) $error .= 'Invalid verify code<br>';
      if ($this->user['password'] != $this->user['confirm_password']) $error .= 'Password not confirm<br>';
      if (empty($this->user['email'])) $error .= 'Empty email<br>';
      if (empty($this->user['name'])) $error .= 'Empty name<br>';
      if (empty($this->user['password'])) $error .= 'Empty password<br>';
      if (empty($this->user['country'])) $error .= 'Empty country<br>';
      if (empty($this->user['state'])) $error .= 'Empty state<br>';
      if (empty($this->user['city'])) $error .= 'Empty city<br>';
      if (empty($this->user['group'])) $error .= 'Empty Group<br>';
      if (empty($this->user['phone'])) $error .= 'Empty phone<br>';
      if (!$this->CheckPassword($this->user['password'])) $error .= 'Password is invalid (must be number, lower case, upper case and symbol)<br>';
      //---
      $this->GenerateVerifyCode();
      return back()->with('error',$error);
      }
    //--- prepare line
    $line = $this->user['email'] . "\n" . $this->user['password'] . "\n" . $this->user['group'] . "\n" . $this->user['leverage'] . "\n" . $this->user['zipcode'] . "\n" . $this->user['country'] . "\n" . $this->user['state'] . "\n" . $this->user['city'] . "\n" . $this->user['address'] . "\n" . $this->user['phone'] . "\n" . $this->user['name'] . "\n" . $this->user['phone_password'] . "\n" . $this->user['invest_password'] . "\n" . time();
    $line .= "\n" . base_convert(crc32($line), 10, 36);
    // //--- create new tools
    // // $tools = new CTools();
    // //--- compress line
    $line = gzcompress($line);
    //--- prepare url and key
    $url = config('app.url'). "/trading-activate?key=";
    $key = str_replace(array('+', '/'), array('&', ','), rtrim(base64_encode($this->Crypt($line, $this->crypt_key)), '='));
    $url = $url . str_replace('&', '_', $key);
    $mail_subject = 'Confirmation email from Trader';
    Mail::to($this->user['email'])->send(new ConfirmTraderMailable($mail_subject,$url));

    //--- add activate
    $this->AddActivation($key);
    //--- redirect
    return back()->with('success','To complete registration go to your email box and click on the confirmation link.');
    }
  /**
   * User click on activate url
   * @return void
   */
  function OnActivate(Request $request)
    {
    $key = $request->key;
    $key = str_replace('_', '&', $key);
    // if (!Activation::where('activation_key',$key)->first()) {
    //   return redirect('/')->with('error','Key not valid');
    // }
    $data = base64_decode(str_replace(array('&', ','), array('+', '/'), $key));
    $data = explode("\n", gzuncompress($this->Crypt($data, $this->crypt_key)));
    $crc32 = base_convert(crc32(implode("\n", array_slice($data, 0, -1))), 10, 36);
    $this->SetActivation($key);
    if ($activation = Activation::where('activation_key',$key)->first()) {
        $user_id = $activation->user_id;
    } else {
        return redirect('/')->with('danger','Key has invalid or expired!');
    }
    if (!UserTradingAccount::where('user_id',$user_id)->first()) {
        $t="1:".$data[3];
        if ($trading_leverage = UserTradingLeverage::where(['name'=>$t])->first()) {
            $t = $data[3];
        } elseif ($trading_leverage = UserTradingLeverage::find($data[3])->first()) {
            $t = explode("1:",$trading_leverage->name)[1];
        }
      $new_user = $this->api->UserCreate();
      $new_user->Email = $data[0];
      $new_user->MainPassword = $data[1];
      $new_user->Group = $data[2];
      $new_user->Leverage = $t;
      $new_user->ZipCode = $data[4];
      $new_user->Country = $data[5];
      $new_user->State = $data[6];
      $new_user->City = $data[7];
      $new_user->Address = $data[8];
      $new_user->Phone = $data[9];
      $new_user->Name = $data[10];
      $new_user->PhonePassword = $data[11];
      $new_user->InvestPassword = $data[12];
      if ($this->CreateAccount($new_user, $user_server)) {
        $trading_account = new UserTradingAccount;
        $leverage_name = "1:".$data[3];
        $trading_leverage_id = $data[3];
        if ($trading_leverage = UserTradingLeverage::where('name',$leverage_name)->first()) {
            $trading_leverage_id = $trading_leverage->id;
        }
        $trading_account->create([
          'user_id'=>$user_id,
          'user_trading_leverage_id'=>$trading_leverage_id,
          'currency_id'=>1,
          'login'=>$user_server->Login,
          'password'=>$new_user->MainPassword,
          'platform'=>"Meta Trader 5",

        ]);
        return redirect('/member/trading');
      } else {
        return redirect('/')->with('danger','Failed!');
      }
    }
    return redirect('/member/trading');


    }
  /**
   * Check password
   * @param $password string
   * @return bool
   */
  function CheckPassword($password)
    {
    $digit = 0;
    $upper = 0;
    $lower = 0;
    //--- check password size
    if (strlen($password) < 5) return (false);
    //--- check password
    for ($i = 0; $i < strlen($password); $i++)
      {
      if (ctype_digit($password[$i])) $digit = 1;
      if (ctype_lower($password[$i])) $lower = 1;
      if (ctype_upper($password[$i])) $upper = 1;
      }
    //--- final check
    return (($digit + $upper + $lower) >= 2);
    }
  /**
   * Create user on MT server
   * @param $user MTUser
   * @param $user_server MTUser
   * @return bool
   */
  function CreateAccount($user, &$user_server)
    {
    if (!$this->api->IsConnected())
      {
      if (($error_code = $this->api->Connect("95.154.219.141", 443, 300, 2170, "murad12345")) != MTRetCode::MT_RET_OK)
        {
          return false;
        }
        return true;
      }
    //--- add user
    if (($error_code = $this->api->UserAdd($user, $user_server)) != MTRetCode::MT_RET_OK)
      {
        return false;
      }
      return true;

    }

  function AddActivation($key)
    {
      $user_id=\Auth::user()->id;

      $activations = Activation::where(['user_id'=>$user_id])->get();
      foreach ($activations as $a) {
          $a->delete();
      }
      $activation = new Activation;
      $activation->create([
        'create_time'=>now(),
        'user_id' => $user_id,
        'activation_key'=>$key
        ]);
    }
  /**
   * Check activation
   * @param $key string
   * @return void
   */
  function CheckActivation($key)
    {
    if ($result = Activation::where('activation_key',$key)->first())
      {
       return true;
      } else {
        return false;
      }
    }
  /**
   * Update activation record into database
   * @param string $key
   * @return void
   */
  function SetActivation($key)
    {
      if ($activation = Activation::where('activation_key',$key)->first()) {
          $activation->update([
                'activated'=>1
            ]);
      }

    }
  /**
   * Generate new verify code
   * @return void
   */
  private function GenerateVerifyCode()
    {
    $random_string = '';
    for ($i = 0; $i < self::VERIFY_CODE_LENGTH; $i++)
      {
      $random_string .= rand(0, 9);
      }
    $_SESSION['verify_code'] = $random_string;
    }

    function Crypt($data, $ckey)
    {
    $key = $box = array();
    $keylen = strlen($ckey);
    //----
    for ($i = 0; $i <= 255; ++$i)
      {
      $key[$i] = ord($ckey{$i % $keylen});
      $box[$i] = $i;
      }
    //---
    for ($x = $i = 0; $i <= 255; $i++)
      {
      $x = ($x + $box[$i] + $key[$i]) % 256;
      list($box[$i], $box[$x]) = array($box[$x], $box[$i]);
      }
    //----
    $k = $cipherby = $cipher = '';
    $datalen = strlen($data);
    //----
    for ($a = $j = $i = 0; $i < $datalen; $i++)
      {
      $a = ($a + 1) % 256;
      $j = ($j + $box[$a]) % 256;
      //----
      list ($box[$a], $box[$j]) = array($box[$j], $box[$a]);
      $k = $box[($box[$a] + $box[$j]) % 256];
      $cipherby = ord($data{$i}) ^ $k;
      $cipher .= chr($cipherby);
      }
    //----
    return $cipher;
    }
    public function ConvertCurrency($amount,$from,$to)
    {
      $rate = new ConvertCurrency;
      return $rate->fetch_exchange_rates($from,$to,$amount);
    }
}
