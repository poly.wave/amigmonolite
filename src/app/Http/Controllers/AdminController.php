<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    protected $currentUser;
    protected $level = 0;

    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $currentUser = Auth::user();
            // superadmin equal 2
            // user equal 1
            if ($currentUser->type!=2) {
                Auth::logout();
                return redirect('/login');
            }
            return $next($request);
        });

        # Generate menu
        $this->response->menu = [
            'main' => (object) [
                'name' => __('authenticated_controller.main'),
                'icon' => 'toolbox'
            ]
        ];
    }
}
