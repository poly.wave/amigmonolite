<?php

namespace App\Http\Controllers\Rest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\ApiKeys;
use App\Models\Cities;
use App\Models\Countries;
use App\Models\Regions;
use App\Models\Currencies;
use App\Models\Languages;
use App\Models\Genders;
use App\Models\DocumentTypes;
use App\Models\PasswordReset;
use App\Models\UserDeposits;
use App\Models\UserEarnings;
use App\Models\ApproveTypes;
use App\Models\UserWithdrawals;
use App\Models\UserPassport;
use App\Models\UserFiles;
use App\Models\CompanyCrypto;
use App\Models\CompanySocial;
use App\Models\Activation;
use App\Models\ActiveServices;
use App\Models\UserTradingAccount;
use App\Models\UserBanks;
use App\Models\WithdrawalOptions;
use App\Models\UserTradingLeverage;
use App\Models\UserCards;
use App\Models\CryptoCoins;
use App\Models\UserDepositsCryptoCompany;
use App\Models\UserCryptoWallets;
use App\Models\PaymentCc;
use App\Models\TradingCodes;
use App\Models\ConvertCurrency;
use App\Models\UserDepositsBanks;
use App\Mail\UserRegisteredSuccessfully;
use App\Mail\UserRegisteredMailable;
use App;
use Illuminate\Support\Facades\DB;
use App\Mail\BankAddedMailable;
use App\Mail\DepositsMailable;
use App\Mail\ConfirmTraderMailable;
use App\Mail\CreditCardAddedMailable;
use App\Mail\CryptoWalletsAddedMailable;
use Illuminate\Support\Facades\Storage;
use \Carbon\Carbon;
use App\Models\MetaTrader\MTWebAPI;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class MainController extends Controller
{
  // user login
  private $crypt_key = '682d95a2009e19fb3570ccb4d98b820f+dfgerth';
  const VERIFY_CODE_LENGTH = 5;
  private $verify_code;
  private $api;
  private $root_url;

  function __construct()
    {
    //---- start session
    session_start();
    //---- initialize
    //---
    $this->root_url = $_SERVER['HTTP_HOST'];
    //---
    $this->api = new MTWebAPI();
    // $this->api->SetLoggerWriteDebug(IS_WRITE_DEBUG_LOG);
    }
  public function login(Request $request) {
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, true);
    if(isset($decoded['locale'])) {
        $locale=$decoded['locale'];
        if(!in_array($locale, ['tr','ru'])) {
            $locale='en';
        }
        App::setLocale($locale);
    }
    if(ActiveServices::where('service_name','user-login')->where('active','1')->first()) {
        if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
            $validations = [
                'email' => "required|email",
                'password' => "required",
            ];
            $validator = Validator::make($decoded, $validations);
            if($validator->fails()){
                return $this->sendError("Validation Error", $validator->errors());
            }
            $email_status  = User::where("email", $decoded['email'])->first();
            if(!is_null($email_status)){
                if (empty($email_status->email_verified_at)){
                    return response()->json(["status" => false,'code'=>404,"message" =>__('rest.confirm_email')]);
                }
                $password_status = Hash::check($decoded['password'],$email_status->password);
                if($password_status) {

                    $user = $this->userDetail($decoded['email']);
                    $today=date("d-m-Y");
                    $from_date=strtotime(date("d-m-Y", strtotime("$today -1 month")));
                    $this->GetTradingUserInfo($user['id'],$from_date);
                    return response()->json(["status" => true, 'code'=>200,"message" =>__('rest.logged_success'),
                                            "data" => $user]);
                } else {
                    return response()->json(["status" => false,'code'=>404,"message" =>__('rest.incorrect_password')]);
                }
            } else {
                return response()->json(["status" => false,'code'=>404, "message" =>__('rest.email_ntexist')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
        }
    } else {
        return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
    }
  }

    // user registration
    public function registration(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-registration')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'password' => ['required', 'string', 'min:8'],
                    'first_name' => ['required', 'string', 'max:255'],
                    'last_name' => ['required', 'string', 'max:255'],
                    'day_of_birth' => ['required', 'date', 'before:18 years ago'],
                    'phone' => ['required', 'string', 'unique:users'],
                    'resident_address' => ['required', 'string'],
                    'city_id' => ['required', 'integer'],
                    'country_id' => ['required', 'integer'],
                    'gender_id' => ['required', 'integer'],
                    'language_id' => ['required', 'integer'],
                    'currency_id' => ['required', 'integer'],
                    'region_id'=>['required', 'integer'],
                    'agreement' => ['accepted']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $token = Str::random(60);
                User::create([
                    'email' => $decoded['email'],
                    'password_without_hash'=>$decoded['password'],
                    'password' => Hash::make($decoded['password']),
                    'first_name' => $decoded['first_name'],
                    'last_name' => $decoded['last_name'],
                    'day_of_birth' => $decoded['day_of_birth'],
                    'phone' => $decoded['phone'],
                    'resident_address' => $decoded['resident_address'],
                    'city_id' =>$decoded['city_id'],
                    'region_id' => $decoded['region_id'],
                    'country_id' => $decoded['country_id'],
                    'gender_id' => $decoded['gender_id'],
                    'currency_id' => $decoded['currency_id'],
                    'language_id' => $decoded['language_id'],
                    'user_group_id' => 2,
                    'api_token' => hash('sha256', $token),
                    'balance' => '0',
                    'deposits' => '0',
                    'earnings' => '0',
                    'withdrawals' => '0',
                ]);
                Mail::to($decoded['email'])->send(new UserRegisteredSuccessfully(hash('sha256', $token)));
                $user_support = User::select('email')
                                    ->where('id', 2)
                                    ->first();
                if ($user_support) {
                    Mail::to($user_support->email)->send(new UserRegisteredMailable($decoded['email']));
                }
                return response()->json(['status' => true,'code'=>200,'message' =>__('rest.success_registr')]);
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getCountries(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-countries')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=Countries::select('id','name','short')->orderBy('name','asc')->get();
                return response()->json(['status' => true,'code'=>200,'message' => $message]);
            }else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getRegions(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-regions')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'country_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=Regions::select('id','name','code')->where('country_id',$decoded['country_id'])
                                                            ->where('name','!=','')->orderBy('name','asc')
                                                            ->get();
                if(!$message->isEmpty()){
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_regions')]);
                }
            } else{
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getCities(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-cities')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'country_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=Cities::select('id','name','latitude','longitude')->where('country_id',$decoded['country_id'])
                                                                           ->where('name','!=','')->orderBy('name','asc')->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                }
                else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_cities')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getCurrencyList(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-currency-list')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=Currencies::select('id','name','symbol')->get();
                return response()->json(['status' => true,'code'=>200,'message' => $message]);
            } else{
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else{
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getLanguagesList(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-languages-list')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=Languages::select('id','name','code')->get();
                return response()->json(['status' => true,'code'=>200,'message' => $message]);
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else{
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getGenderList(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-gender-list')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=Genders::select('id','name')->get();
                return response()->json(['status' => true,'code'=>200,'message' => $message]);
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getDocumentTypes(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-document-types')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=DocumentTypes::select('id','name')->get();
                return response()->json(['status' => true,'code'=>200,'message' => $message]);
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserInfo(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-info')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=User::find($decoded['user_id']);
                if($message){
                    $message->avatar="https://www.amigfx.co/$message->avatar";
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function ResetPassword(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-password/reset')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'email' => ['required','email', 'max:255']
                ];
                $validator = Validator::make($decoded, $validations);

                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user = User::where('email',$decoded['email'])->first();
                if (!$user) {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.ntf_email')]);
                } else {
                    $token = Str::random(60);
                    PasswordReset::create([
                        'email' => $decoded['email'],
                        'token' => $token
                    ]);
                    $user->notify(new ResetPasswordNotification($token));
                    return response()->json(['status' => true,'code'=>200,'message'=>__('rest.password_reset')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function ChangePassword(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])) {
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-reset-password')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'email' => ['required','email', 'max:255','exists:users,email'],
                    'password'=>['required','min:8','string'],
                    'confirm_password' =>['required','same:password','string']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $token_table=PasswordReset::select('token')->where('email',$decoded['email'])
                                                           ->where('token',$decoded['token'])
                                                           ->first();
                if (!$token_table) {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.invalid_token')]);
                } else {
                    $users=User::where('email',$decoded['email'])->first();
                    $users->password=Hash::make($decoded['password']);
                    $users->save();
                    return response()->json(['status' => true,'code'=>200,'message'=>__('rest.rst_password')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }


    public function getUpdateUserInfo(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-personal-information')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id'=>['required','integer'],
                    'avatar' => ['string'],
                    'email' => ['required','email', 'string', 'max:255','unique:users,email,' . $decoded['user_id']],
                    'your_name' => ['required', 'string'],
                    'your_surname' => ['required', 'string'],
                    'day_of_birth' => ['required', 'date','before:18 years ago'],
                    'phone_number' => ['required', 'string'],
                    'gender_id' => ['required', 'integer'],
                    'language_id' => ['required', 'integer'],
                    'currency_id' => ['required', 'integer'],
                    'country_id' => ['required', 'integer'],
                    'region_id'=>['required', 'integer'],
                    'city_id' => ['required','integer'],
                    'resident_address' => ['required', 'string'],
                    'two_authentication' => ['integer','digits_between:0,1']
                ];
                $validator = Validator::make($decoded, $validations);

                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $info=User::find($decoded['user_id']);
                if($info){
                    if(isset($decoded['avatar']) && $decoded['avatar']!=""){
                        $image_parts = explode(";base64,", $decoded['avatar']);
                        $image_type_aux = explode("image/", $image_parts[0]);
                        $image_type = $image_type_aux[1];
                        $image_base64 = base64_decode($image_parts[1]);
                        $folderPath="profile/".$decoded['user_id']."/";
                        $filename=time().'.png';
                        Storage::disk('public')->put($folderPath.$filename, $image_base64);
                        $avatar = Storage::url($folderPath.$filename);
                        $info->avatar=$avatar;
                    }
                    $info->email=$decoded['email'];
                    $info->first_name=$decoded['your_name'];
                    $info->last_name=$decoded['your_surname'];
                    $info->day_of_birth=$decoded['day_of_birth'];
                    $info->phone=$decoded['phone_number'];
                    $info->gender_id=$decoded['gender_id'];
                    $info->language_id=$decoded['language_id'];
                    $info->currency_id=$decoded['currency_id'];
                    $info->country_id=$decoded['country_id'];
                    $info->region_id=$decoded['region_id'];
                    $info->city_id=$decoded['city_id'];
                    $info->resident_address=$decoded['resident_address'];
                    $info->save();
                    return response()->json(['status' => true,'code'=>200,'message' =>__('rest.userinfo_update')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function ProfileResetPassword(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-modify-password')->where('active','1')->first()){
            if(!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'current_password' => ['required','string','min:8'],
                    'new_password'=>['required','min:8','string'],
                    'confirm_password' =>['required','same:new_password','string'],
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::select('password')->where('id',$decoded['user_id'])
                                              ->first();
                if(!$user){
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
                if (Hash::check($decoded['current_password'], $user->password)){
                    $user=User::find($decoded['user_id']);
                    $user->password=Hash::make($decoded['new_password']);
                    $user->save();
                    return response()->json(['status' => true,'code'=>200,'message'=>__('rest.password_update')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.incorrect_oldpassword')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserDeposits(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-deposits')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer'],
                    'from_date' => ['string']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                if(!isset($decoded['from_date'])) {
                    $message=UserDeposits::where('user_id',$decoded['user_id'])->orderBy('id','desc')->get();
                } else {
                $message=UserDeposits::where('user_id',$decoded['user_id'])
                                    ->where('time','>=',strtotime($decoded['from_date']))
                                    ->get();
                }
                if(!$message->isEmpty()){
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message){
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function test()
    {
        // $content = trim(file_get_contents("php://input"));
        // $decoded = json_decode($content, true);
        $users = User::first();
        return response()->json($users);
    }
    public function getUserDepositBank(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
            $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-deposit-bank')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer'],
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user = User::find($decoded['user_id']);
                if ($user->hasBankDeposit()) {
                    $time = date('Y-m-d H:i');
                    $date = strtotime($time) - 30*60;
                    $message = UserDepositsBanks::select('user_deposits_banks.*', 'currencies.name as currency_name','company_banks.name as name','company_banks.account_name as account_name','company_banks.iban as iban'
                    ,'company_banks.swift as swift','company_banks.address as address','company_banks.icon as icon')
                            ->where('user_id', $decoded['user_id'])
                            ->where('time', '>=',(int)$date)
                            ->leftJoin('company_banks', 'company_banks.id', '=', 'user_deposits_banks.bank_id')
                            ->join('currencies', 'currencies.id', '=', 'company_banks.currency_id')
                            ->get();
                    if(!$message->isEmpty()){
                        return response()->json(['status' => true,'code'=>200,'message' => $message]);
                    } else {
                        $message=User::find($decoded['user_id']);
                        if(!$message){
                            return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                        } else {
                            return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                        }
                    }
                }  else {
                    $message=User::find($decoded['user_id']);
                    if(!$message){
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }

            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserWithdrawals(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-withdrawals')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer'],
                    'from_date' => ['string']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                if(!isset($decoded['from_date'])) {
                    $message=UserWithdrawals::where('user_id',$decoded['user_id'])
                                            ->orderBy('id','desc')
                                            ->get();
                } else {
                $message=UserWithdrawals::where('user_id',$decoded['user_id'])
                                        ->where('time','>=',strtotime($decoded['from_date']))
                                        ->get();
                }
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserEarnings(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if(!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-earnings')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'user_id' => ['required', 'integer'],
                    'from_date' => ['string']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                if(!isset($decoded['from_date'])) {
                    $message=UserEarnings::where('user_id',$decoded['user_id'])
                                        ->orderBy('id','desc')
                                        ->get();
                } else {
                    $message=UserEarnings::where('user_id',$decoded['user_id'])
                                        ->where('time','>=',strtotime($decoded['from_date']))
                                        ->get();
                }
                if(!$message->isEmpty()){
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserApprovedDeposits(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-approved-deposits')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $approved_type=ApproveTypes::select('id')
                                             ->where('name','approved')
                                             ->first();
                $message=UserDeposits::where('user_id',$decoded['user_id'])
                                    ->where('approve_id', $approved_type->id)
                                    ->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserApprovedWithdrawals(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                    $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-approved-withdrawals')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $approved_type=ApproveTypes::select('id')
                                             ->where('name','approved')
                                             ->first();
                $message=UserWithdrawals::where('user_id',$decoded['user_id'])
                                        ->where('approve_type_id', $approved_type->id)
                                        ->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserPassportInfo(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-passport-info')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserPassport::where('user_id',$decoded['user_id'])->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else{
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.ntinfo_passport')]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function getUserTradingAccountInfo(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-trading-account-info')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserTradingAccount::where('user_id',$decoded['user_id'])->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>NULL]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreatePassportInfo(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if(!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-create-passport-info')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer'],
                    'passport_id' => ['required', 'integer'],
                    'date_of_issue' => ['required', 'date'],
                    'date_of_expiry' => ['required', 'date'],
                    'issuring_authority' => ['required', 'string'],
                    'passport_scan' => ['required', 'string'],
                    // 'approve_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $info=User::find($decoded['user_id']);
                if($info){
                    UserPassport::create([
                        'user_id' => $decoded['user_id'],
                        'passport_id' =>$decoded['passport_id'],
                        'date_of_issue' =>$decoded['date_of_issue'],
                        'date_of_expiry' =>$decoded['date_of_expiry'],
                        'issuring_authority' =>$decoded['issuring_authority'],
                        'passport_scan' =>$decoded['passport_scan'],
                        'approve_id' =>3,
                    ]);
                    return response()->json(['status' => true,'code'=>200,'message' =>__('rest.create_passport')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreateUserFile(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','create-user-file')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {

                $validations = [
                    'user_id' => ['required', 'integer'],
                    'document_type_id' => ['required', 'integer'],
                    'file' => ['required', 'string'],
                    // 'approve_type_id' => ['required','integer']
                ];

                $validator = Validator::make($decoded, $validations);

                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    $file_base64 = base64_decode($decoded['file']);
                    $mime_type = finfo_buffer(finfo_open(),$file_base64, FILEINFO_MIME_TYPE);
                    $extension =$this->mime2ext($mime_type);
                    $filename=time().'.'.$extension;
                    $folderPath='profile/files/' . $user->id . '/';
                    Storage::disk('public')->put($folderPath.$filename, $file_base64);
                    $decoded['file'] = Storage::url($folderPath.$filename);
                    UserFiles::create([
                        'user_id' => $decoded['user_id'],
                        'document_type_id' =>$decoded['document_type_id'],
                        'file' =>$decoded['file'],
                        'approve_type_id' =>3,
                    ]);
                    return response()->json(['status' => true,'code'=>200,'message' =>__('rest.create_file')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function mime2ext($mime)
    {
        $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp",
        "image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp",
        "image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp",
        "application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg",
        "image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],
        "wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],
        "ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg",
        "video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],
        "kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],
        "rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application",
        "application\/x-jar"],"zip":["application\/x-zip","application\/zip",
        "application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],
        "7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],
        "svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],
        "mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],
        "webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],
        "pdf":["application\/pdf","application\/octet-stream"],
        "pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],
        "ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office",
        "application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],
        "xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],
        "xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel",
        "application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],
        "xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo",
        "video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],
        "log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],
        "wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],
        "tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop",
        "image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],
        "mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar",
        "application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40",
        "application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],
        "cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary",
        "application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],
        "ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],
        "wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],
        "dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php",
        "application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],
        "swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],
        "mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],
        "rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],
        "jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],
        "eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],
        "p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],
        "p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
        $all_mimes = json_decode($all_mimes,true);
        foreach ($all_mimes as $key => $value) {
            if(array_search($mime,$value) !== false) return $key;
        }
        return false;
    }

    public function GetUserFiles(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-files')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserFiles::where('user_id',$decoded['user_id'])->get();
                if($message){
                    foreach($message as $item)
                        $item->file="https://www.amigfx.co/$item->file";
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.nt_file')]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }


    public function CreateUserWithdrawals(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','create-user-withdrawals')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'name' => ['required', 'string'],
                    'amount' => ['required', 'numeric','min:100'],
                    'currency_id' => ['required', 'integer'],
                    'user_id' => ['required', 'integer'],
                    'withdrawal_option_id' => ['required','integer'],
                    'deal' => ['string'],
                    'order' => ['string'],
                    'entry' => ['string'],
                    'action' => ['string'],
                    'symbol' => ['required','string'],
                    'comment' => ['text']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $info=User::find($decoded['user_id']);
                $amount_new = $decoded['amount']*(-1);
                if($info) {
                    UserWithdrawals::create([
                        'name' => $decoded['name'],
                        'amount' =>$amount_new,
                        'currency_id' =>$decoded['currency_id'],
                        'user_id' =>$decoded['user_id'],
                        'approve_type_id' => 3,
                        'withdrawal_option_id' =>$decoded['withdrawal_option_id'],
                        'deal' =>$decoded['deal'] ?? NULL ,
                        'order' =>$decoded['order'] ?? NULL,
                        'entry' =>$decoded['entry'] ?? NULL,
                        'action' => $decoded['action'] ?? NULL,
                        'time' =>strtotime(date("Y-m-d H:i")),
                        'time_msc' =>strtotime(date("Y-m-d H:i")),
                        'symbol' =>$decoded['symbol'],
                        'comment' =>'Withdrawal',
                    ]);
                    return response()->json(['status' => true,'code'=>200,'message' =>__('rest.withdrawal_committed')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function AddDepositBankTransfer(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-add-deposit-by-bank-transfer')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'amount' => ['required', 'numeric','min:100'],
                    'name' => ['required'],
                    'user_id'=>['required','integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    // $user_bank_id=0;
                    // if ($user_bank = UserBanks::where(['user_id'=>$user->id])->first()) {
                    //     $user_bank_id=$user_bank->id;
                    // }
                    $currency_name = Currencies::find(1)->name;
                    $user_deposits = UserDeposits::create([
                        'user_id'=>$user->id,
                        'amount'=>$decoded['amount'],
                        'name'=>"Deposit by Bank Transfer",
                        'comment'=>"Deposit by Bank Transfer",
                        'currency_id'=>1,
                        'approve_id'=>3,
                        'time'=>strtotime(date("Y-m-d H:i")),
                        'bank_name'=>$decoded['name']
                    ]);
                    $user_support = User::select('email')
                                        ->where('id', 2)
                                        ->first();
                    if ($user_support) {
                        Mail::to($user_support->email)->send(new DepositsMailable('Bank Transfer',$user->first_name." ".$user->last_name,$decoded['amount'],$currency_name));
                    }
                    return response()->json(['status' => true,'code'=>200,'message' =>__('rest.bank_transfer')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreateWalletBankTransfer(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-create-wallet-by-bank-transfer')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'name' => ['required', 'string'],
                    'address' => ['required', 'string'],
                    'iban' => ['required', 'string'],
                    'swift' => ['required', 'string'],
                    'currency_id' => ['required', 'integer'],
                    'user_id' => ['required', 'integer'],
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    $data = $request->all();
                    try {
                        $data['user_id'] = $user->id;
                        DB::transaction(function () use ($data) {
                            UserBanks::create($data);
                        });
                        $data = [];
                        Mail::to($user->email)->send(new BankAddedMailable());
                        return response()->json(['status' => true,'code'=>200,'message' =>__('rest.wallet_created')]);
                    } catch (\Exception $err) {
                        return response()->json(['status' => false,'code'=>404,'message' =>$err]);
                    }
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else{
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreateWalletCreditCard(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-create-wallet-by-credit-card')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'name' => ['required', 'string'],
                    'number' => ['required', 'string'],
                    'month' => ['required', 'string'],
                    'year' => ['required', 'string'],
                    'cvv' => ['required', 'integer'],
                    'currency_id' => ['required', 'integer'],
                    'user_id' => ['required', 'integer'],
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    $data = $request->all();
                    try {
                        $data['user_id'] = $user->id;
                        DB::transaction(function () use ($data) {
                            UserCards::create($data);
                        });
                        Mail::to($user->email)->send(new CreditCardAddedMailable());
                        $data = [];
                        return response()->json(['status' => true,'code'=>200,'message' =>__('rest.wallet_created')]);
                    } catch (\Exception $err) {
                        return response()->json(['status' => false,'code'=>404,'message' =>$err]);
                    }
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreateWalletCryptoCurrency(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-create-wallet-by-crypto-currency')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'name' => ['required', 'string'],
                    'account' => ['required', 'string'],
                    'crypto_coin_id' => ['required', 'integer'],
                    'user_id' => ['required', 'integer'],
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    $data = $request->all();
                    try {
                        $data['user_id'] = $user->id;
                        DB::transaction(function () use ($data) {
                            UserCryptoWallets::create($data);
                        });
                        Mail::to($user->email)->send(new CryptoWalletsAddedMailable());
                        $data = [];
                        return response()->json(['status' => true,'code'=>200,'message' =>__('rest.wallet_created')]);
                    } catch (\Exception $err) {
                        return response()->json(['status' => false,'code'=>404,'message' =>$err]);
                    }
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function AddDepositCreditCard(Request $request,$endpoint = null)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-add-deposit-by-credit-card')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $payment_cc = PaymentCc::orderBy('id', 'asc')->limit(1)->first();
                $validations = [
                    // 'user_deposit_id' => ['required', 'integer'],
                    'user_id' => ['required', 'integer'],
                    'amount' => ['required', 'numeric','min:'.$payment_cc->minimal_deposit],
                ];

                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }

                $user=User::find($decoded['user_id']);
                if($user) {
                    // if ($endpoint && $user_deposit_id) {
                        $user_deposit = new UserDeposits;
                        $user_deposit->create([
                              'user_id'=>$user->id,
                              'amount'=>$decoded['amount'],
                              'deal'=>'-',
                              'action'=>1,
                              'entry'=>'-',
                              'time'=>strtotime(date("Y-m-d H:i")),
                              'time_msc'=>strtotime(date("Y-m-d H:i")),
                              'symbol'=>'-',
                              'name'=>'Deposit by Credit Card',
                              'currency_id'=>1,
                              'comment'=>'Deposit by Credit Card',
                              'approve_id'=>3
                          ]);
                        $user_deposit_data = json_decode($user_deposit->data);
                        $response = $this->client('PaymentCheckCall', [
                            'referencecode' => $user_deposit->ReferenceCode
                        ], true,false,$user);
                        if ($response->data->IsOkey) {
                            DB::transaction(function () use ($user, $user_deposit) {
                                UserDeposits::where('id', $user_deposit->id)->delete();
                            // User::where('id', $user->id)->update([
                            //     'deposits' => ($user->money + $user_deposit->amount)
                            // ]);
                            });
                            $this->response->success = 'Payment transaction successful';
                            $this->DepositChange($user_deposit->amount,'Deposit by Credit card');
                            $currency_name = Currencies::find($user_deposit->currency_id)->name;
                            $user_support = User::select('email')
                                                ->where('id', 2)
                                                ->first();
                            if ($user_support) {
                                Mail::to($user_support->email)->send(new DepositsMailable('Cred Card',$user->first_name." ".$user->last_name,$user_deposit->amount,$currency_name));
                            }
                        } else {
                            UserDeposits::where('id', $user_deposit->id)->update([
                                'approve_id' => 2
                            ]);
                            \Session::put('user_deposit_id', null);
                            return response()->json(['status' => false,'code'=>404,'message' =>'Payment transaction error']);
                        }
                    // } else {
                    //     // Create cc customer
                    //     $cc_customer_id=$cc_username=$cc_password=NULL;
                    //     $info=UserPassport::where('user_id',$decoded['user_id'])->first();
                    //     $passport_id=$info->passport_id;
                    //     if (!$cc_customer_id) {
                    //         if (isset($passport_id)){
                    //             $year = date('Y', strtotime($user->day_of_birth));
                    //             $month = date('m', strtotime($user->day_of_birth));
                    //             $day = date('d', strtotime($user->day_of_birth));
                    //
                    //             $response = $this->client('NewCustomer', [
                    //                 'name' => $user->first_name,
                    //                 'surname' => $user->last_name,
                    //                 'mail' => $user->email,
                    //                 'username' => $cc_username,
                    //                 'password' => $cc_password,
                    //                 'date_day' => $day,
                    //                 'date_month' => $month,
                    //                 'date_year' => $year,
                    //                 'country' => $user->country,
                    //                 'city' => $user->city,
                    //                 'zipcode' => '',
                    //                 'adress' => $user->resident_address,
                    //                 'phone' => str_replace('+', '', $user->phone),
                    //                 'identity' => $passport_id
                    //             ]);
                    //             if ($response->success){
                    //                 User::where('id', $user->id)->update([
                    //                     'cc_customer_id' => $response->data->CustomerId
                    //                 ]);
                    //                 $cc_customer_id = $response->data->CustomerId;
                    //             } else {
                    //                 return response()->json(['status' => false,'code'=>404,'message' =>$response->message]);
                    //             }
                    //         } else {
                    //             return response()->json(['status' => false,'code'=>404,'message' =>'No passport data']);
                    //         }
                    //     }
                    //     if ($cc_customer_id) {
                    //         $rate = new ConvertCurrency;
                    //         try {
                    //             $response = $this->client('PaymentCall', [
                    //                 'price' => $rate->fetch_exchange_rates("USD","TRY",$decoded['amount']),
                    //                 'failurl' => url('/member/deposit/credit_card/failure'),
                    //                 'successurl' => url('/member/deposit/credit_card/success')
                    //             ], true, true);
                    //             if ($response->success){
                    //                 $user_deposit_id = UserDeposits::create([
                    //                     'name' => 'Deposit by credit card',
                    //                     'user_id' => $user->id,
                    //                     'currency_id' => $user->currency_id,
                    //                     'amount' => $decoded['amount'],
                    //                     'data' => json_encode($response->data),
                    //                     'approve_id' => 3,
                    //                     'request_type_id' => 3
                    //                 ])->id;
                    //                 \Session::put('user_deposit_id', $user_deposit_id);
                    //                 return response()->json(['status' =>true,'code'=>200,'message' =>__('rest.deposed_created')]);
                    //             } else {
                    //                 return response()->json(['status' => false,'code'=>404,'message' =>'Payment error']);
                    //             }
                    //         } catch (\Exception $err) {
                    //             return response()->json(['status' => false,'code'=>404,'message' =>$err]);
                    //         }
                    //     } else {
                    //         return response()->json(['status' => false,'code'=>404,'message' =>'No cc_customer_id']);
                    //     }
                    //  }
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function AddDepositCryptoCurrency(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale']))
        {
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','user-add-deposit-by-crypto-currency')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'amount_crypto'=>['required', 'numeric','min:100'],
                    'user_id' => ['required', 'integer'],
                    'crypto_id' => ['required', 'integer'],
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                    $ch = curl_init();
                    $name = $user->first_name.' '.$user->last_name;
                    $description  = "Crypto currency payment";
                    $amount =  $decoded['amount_crypto'];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.commerce.coinbase.com/charges');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n       \"name\": \"$name\",\n       \"description\": \"$description\",\n       \"local_price\": {\n         \"amount\": \"$amount\",\n         \"currency\": \"USD\"\n       },\n       \"pricing_type\": \"fixed_price\",\n       \"metadata\": {\n         \"customer_id\": \"id_1005\",\n         \"customer_name\": \"$name\"\n       },\n       \"redirect_url\": \"https://www.amigfx.com/member/deposit/crypto-coins-completed\",\n       \"cancel_url\": \"https://www.amigfx.com/member/deposit/crypto-coins-canceled\"\n     }");

                    $headers = array();
                    $headers[] = 'Content-Type: application/json';
                    $headers[] = 'X-Cc-Api-Key: 2041b869-5f20-4d64-874f-8e07d914e28d';
                    $headers[] = 'X-Cc-Version: 2018-03-22';
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $result = curl_exec($ch);
                    if (curl_errno($ch)){
                        echo 'Error:' . curl_error($ch);
                    }
                    curl_close($ch);
                    $json = preg_replace('/[[:cntrl:]]/', '', $result);
                    $json = json_decode($json, true);
                    $user_bank_id=0;
                    if ($user_bank = UserBanks::where(['user_id'=>$user->id])->first()) {
                        $user_bank_id=$user_bank->id;
                    }
                    $user_deposit_id = UserDeposits::create([
                        'name' => 'Deposit by crypto currency',
                        'comment' => 'Deposit by crypto currency',
                        'user_id' => $user->id,
                        'currency_id' =>1,
                        'amount' => $amount,
                        'data' => '',
                        'approve_id' => 3,
                        'request_type_id' => 3,
                        'crypto_id'=>$decoded['crypto_id'],
                        'time'=>strtotime(date("Y-m-d H:i")),
                    ]);
                    $user_crypto_company= new UserDepositsCryptoCompany;
                    $user_crypto_company->create([
                      'crypto_company_id'=>$decoded['crypto_id'],
                      'deposit_id'=>$user_deposit_id->id,
                      'user_id'=>$user->id,
                      'time'=>strtotime(date("Y-m-d H:i")),
                      ]
                    );
                    //$user_deposit_id= $user_deposit_id->id;
                    \Session::put('user_deposit_id', $user_deposit_id);
                    $currency_name=Currencies::find(1);
                    $currency_name=$currency_name->name;
                    $user_support = User::select('email')
                                        ->where('id', 2)
                                        ->first();
                    if ($user_support) {
                        Mail::to($user_support->email)->send(new DepositsMailable('Bitcoin',$user->first_name." ".$user->last_name,$amount,$currency_name));
                    }
                    return response()->json(['status' => true,'code'=>200,'message' =>$json['data']['hosted_url']]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function getUserBankTransfers(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-bank-transfers')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserBanks::where('user_id',$decoded['user_id'])->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getUserCreditCards(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-credit-cards')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()) {
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserCards::where('user_id',$decoded['user_id'])->get();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function getUserCryptoCurrencies(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if(!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-user-crypto-currencies')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=UserCryptoWallets::where('user_id',$decoded['user_id'])->get();
                if(!$message->isEmpty()){
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function userDetail($email) {
        $user = [];
        if($email != "") {
            $user = User::where("email", $email)->first();
            return $user;
        }
    }

    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    private function client($action, $params, $use_cutomer = false, $reference_code = false,$user)
    {
        $payment_cc = PaymentCc::orderBy('id', 'asc')->limit(1)->first();

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:WSPINIntf-IWSPIN" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<ns1:' . $action . '>';

        $xml .= '<wsuser xsi:type="xsd:string">' . $payment_cc->wsuser . '</wsuser>';
        $xml .= '<wspass xsi:type="xsd:string">' . $payment_cc->wspass . '</wspass>';
        $xml .= '<apikey xsi:type="xsd:string">' . $payment_cc->apikey . '</apikey>';

        if ($use_cutomer === true) {
            $xml .= '<username xsi:type="xsd:string">' . $user->cc_username . '</username>';
            $xml .= '<password xsi:type="xsd:string">' . $user->cc_password . '</password>';
        }

        if ($reference_code === true) {
            $reference_code = md5(time() . $user->id);
            $xml .= '<referencecode xsi:type="xsd:string">' . $reference_code . '</referencecode>';
        }

        foreach ($params as $key => $value) {
            $xml .= '<' . $key . ' xsi:type="xsd:string">' . $value . '</' . $key . '>';
        }

        $xml .= '</ns1:' . $action . '>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';

        $ch = curl_init($payment_cc->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        $output = curl_exec($ch);
        $json = strip_tags($output);
        $response = json_decode($json);

        $result = (object) [
            'success' => false,
            'code' => isset($response->ResultCode) ? $response->ResultCode : null,
            'message' => isset($response->ResultMessage) ? $response->ResultMessage : null,
            'data' => (object) $response->ResultObject
        ];

        if (isset($response->ResultStatus)) {
            if ($response->ResultStatus) {
                $result->success = true;
            }
        }

        if ($reference_code) {
            $result->data->reference_code = $reference_code;
        }

        return $result;
    }

    public function getLaveragesNames(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-laverages-names')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=UserTradingLeverage::all();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.nt_leverage')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getWithdrawalTypes(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-withdrawal-types')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=WithdrawalOptions::all();
                if(!$message->isEmpty()) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message' =>__('rest.notoptions_withdrawals')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getCryptoCoins(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-crypto-coins')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $message=CryptoCoins::all();
                if(!$message->isEmpty()){
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    return response()->json(['status' => false,'code'=>404, 'message' =>__('rest.not_cryptocoins')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    public function getTradingVerifyCode(Request $request)
    {
        if (isset($_GET['auth_key'])) {
            $decoded['auth_key'] = $_GET['auth_key'];
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }

        if (isset($_GET['user_id'])) {
            $decoded['user_id'] = $_GET['user_id'];
        }
        if(isset($_GET['locale'])){
            $locale=$_GET['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-trading-verify-code')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $message=User::find($decoded['user_id']);
                if($message){
                    $random_string = '';
                    for ($i = 0; $i < 5; $i++){
                        $random_string .= rand(0, 9);
                    }
                    $old_codes = TradingCodes::where('user_id',$decoded['user_id'])->get();
                    foreach ($old_codes as $old_code) {
                        $old_code->delete();
                    }
                    $code = TradingCodes::create([
                        'user_id' =>$decoded['user_id'],
                        'code' =>$random_string
                    ]);
                    $_SESSION['verify_code']=$random_string;
                    return view('member.image');
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function CreateUserTradingAccount(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','create-user-trading-account')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $decoded['confirm_code']=0;
                if ($tbcode=TradingCodes::where('user_id',$decoded['user_id'])->first()) {
                    $decoded['confirm_code']=$tbcode->code;
                }
                $validations = [
                    'user_id' => ['required', 'integer'],
                    'leverage' => ['required', 'integer'],
                    'password'=> ['required','string','same:confirm_password'],
                    'confirm_password' => ['required','string'],
                    'verification_code' => ['required','same:confirm_code']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                       $country_id = $user->country_id;
                       $err = array();
                       $user['email']=$user->email;
                       $user['name']=$user->first_name." ".$user->last_name;
                       $user['state']='Dashboard';
                       $user['zipcode']='34000';
                       $user['city']='Web site';
                       $user['group']='real\AMG\MVariable-USD-Mini';
                       $user['invest_password']='Qwerty123';
                       if ($country = \App\Models\Countries::find($country_id)->first()) {
                           $user['country']=$country->name;
                       } else {
                           $user['country']='Turkey';
                       }
                       $user['address']='Web Dashboard';
                       $user['phone']='+900000001';
                       $user['phone_password']='Qwerty123';

                       $line = $user['email'] . "\n" . $decoded['password'] . "\n" . $user['group'] . "\n" . $decoded['leverage'] . "\n"
                                . $user['zipcode'] . "\n" . $user['country'] . "\n" . $user['state'] . "\n" . $user['city'] . "\n"
                                . $user['address'] . "\n" . $user['phone'] . "\n" . $user['name'] . "\n" . $user['phone_password']
                                . "\n" . $user['invest_password'] . "\n" . time();
                       $line .= "\n" . base_convert(crc32($line), 10, 36);
                       // //--- create new tools
                       // // $tools = new CTools();
                       // //--- compress line
                       $line = gzcompress($line);
                       //--- prepare url and key
                       $url = config('app.url'). "/trading-activate?key=";
                       $key = str_replace(array('+', '/'), array('&', ','), rtrim(base64_encode($this->Crypt($line, $this->crypt_key)), '='));
                       $url = $url . str_replace('&', '_', $key);
                       $mail_subject = 'Confirmation email from Trader';
                       Mail::to($user['email'])->send(new ConfirmTraderMailable($mail_subject,$url));
                       $user['id'] = $decoded['user_id'];
                       $this->AddActivation($key,$user);
                       //--- redirect
                       //--- add activate
                       return response()->json(['status' =>true,'code'=>200,'message'=>__('rest.success_trading_acc')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
    private function Crypt($data, $ckey)
    {
        $key = $box = array();
        $keylen = strlen($ckey);
        //----
        for ($i = 0; $i <= 255; ++$i)
          {
              $key[$i] = ord($ckey[$i % $keylen]);
              $box[$i] = $i;
          }
        //---
        for ($x = $i = 0; $i <= 255; $i++)
          {
              $x = ($x + $box[$i] + $key[$i]) % 256;
              list($box[$i], $box[$x]) = array($box[$x], $box[$i]);
          }
        //----
        $k = $cipherby = $cipher = '';
        $datalen = strlen($data);
        //----
        for ($a = $j = $i = 0; $i < $datalen; $i++)
          {
              $a = ($a + 1) % 256;
              $j = ($j + $box[$a]) % 256;
              //----
              list ($box[$a], $box[$j]) = array($box[$j], $box[$a]);
              $k = $box[($box[$a] + $box[$j]) % 256];
              $cipherby = ord($data[$i]) ^ $k;
              $cipher .= chr($cipherby);
          }
        //----
        return $cipher;
    }
    private function GenerateVerifyCode()
      {
          $random_string = '';
          for ($i = 0; $i < self::VERIFY_CODE_LENGTH; $i++)
            {
                $random_string .= rand(0, 9);
            }
          $_SESSION['verify_code'] = $random_string;
      }
     private function AddActivation($key,$user)
      {
        $user_id=$user['id'];
        $activations = Activation::where(['user_id'=>$user_id])->get();

        foreach ($activations as $a) {
            $a->delete();
        }
        $activation = new Activation;
        $activation->create([
              'create_time'=>now(),
              'user_id' => $user_id,
              'activation_key'=>$key
          ]);
      }
     private function CheckPassword($password)
    {
        $digit = 0;
        $upper = 0;
        $lower = 0;
        //--- check password size
        if (strlen($password) < 5) return (false);
        //--- check password
        for ($i = 0; $i < strlen($password); $i++)
          {
          if (ctype_digit($password[$i])) $digit = 1;
          if (ctype_lower($password[$i])) $lower = 1;
          if (ctype_upper($password[$i])) $upper = 1;
          }
        //--- final check
        return (($digit + $upper + $lower) >= 2);
    }
    public function DepositChange($amount,$comment,$user_id)
    {
      if ($account = UserTradingAccount::where('user_id',$user_id)->first()) {
          $login = $account->login;
          $this->api->UserDepositChange($login, $amount, $comment, 2);
          $this->GetTradingUserInfo($user_id,strtotime('01.01.2019'));
          return true;
      }
      return false;
    }

    public function GetTradingUserInfo($user_id,$from_date)
    {
      $account = UserTradingAccount::where('user_id',$user_id)->first();
      if ($account) {
        $params       = array();
        $answer       = array();
        $answer_boddy = array();
        $params['login']=$account->login;

        $params['from']=$from_date;//strtotime('01.01.2019');
        $params['to']=strtotime(Carbon::now()->addDays(1));
        $params['OFFSET ']='0';
        $params['TOTAL']='100';
        $str =$this->api->CustomSend('DEAL_GET_PAGE', $params, '', $answer, $answer_boddy);
        $json = preg_replace('/[[:cntrl:]]/', '', $str);
        $json = json_decode($json, true);
        $user_main = \App\Models\User::find($user_id);
        if (gettype($json) == 'array') {
          foreach ($json as $key) {
            if ($key['Order'] == 0 && $key['Action'] == 2 && $key['Entry'] == 0) {
              if ($key['Profit']/abs($key['Profit'])==1) {
                $order = new UserDeposits;
                if (!UserDeposits::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_id'=>1,
                    'request_type_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'deposits'=>$user_main->deposits+$key['Profit'],
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
              if ($key['Profit']/abs($key['Profit'])==-1) {
                $order = new UserWithdrawals;
                if (!UserWithdrawals::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_type_id'=>1,
                    'withdrawal_option_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'withdrawals'=>$user_main->withdrawals+abs($key['Profit']),
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
            }
            if ($key['Order'] != 0 && $key['Entry'] == 1) {
              $order = new UserEarnings;
              if (!UserEarnings::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                $order->create([
                  'user_id'=>$user_id,
                  'amount'=>$key['Profit'],
                  'deal'=>$key['Deal'],
                  'order'=>$key['Order'],
                  'action'=>$key['Action'],
                  'entry'=>$key['Entry'],
                  'time'=>$key['Time'],
                  'time_msc'=>$key['TimeMsc'],
                  'symbol'=>$key['Symbol'],
                  'name'=>$key['Symbol'],
                  'currency_id'=>1,
                  'comment'=>$key['Comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$key['Profit'],
                  'balance'=>$user_main->balance+$key['Profit'],
                ]);
              }
            }
          }
        }
      }
  }

    public function updateUserEarnings(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','update-user-earnings')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' => ['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user=User::find($decoded['user_id']);
                if($user){
                        $today=date("d-m-Y");
                        $from_date=strtotime(date("d-m-Y", strtotime("$today -1 month")));
                        $this->GetTradingUserInfo($user['id'],$from_date);
                        return response()->json(['status' =>true,'code'=>200,'message'=>__('rest.update_user_earnings')]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.not_user')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getCryptoCompanies(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-crypto-compaines')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){

                $info=CompanyCrypto::all();
                if(!$info->isEmpty()){
                        return response()->json(['status' =>true,'code'=>200,'message'=>$info]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.not_crypto_company')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getSocialCompanies(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-social-company')->where('active','1')->first()) {
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){

                $info=CompanySocial::all();
                if(!$info->isEmpty()){
                        return response()->json(['status' =>true,'code'=>200,'message'=>$info]);
                } else {
                    return response()->json(['status' => false,'code'=>404,'message'=>__('rest.not_social_company')]);
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }

    public function getActiveCryptoCompany(Request $request)
    {
        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content, true);
        if(isset($decoded['locale'])){
            $locale=$decoded['locale'];
            if (!in_array($locale, ['tr','ru'])){
                $locale='en';
            }
            App::setLocale($locale);
        }
        if(ActiveServices::where('service_name','get-active-crypto-company')->where('active','1')->first()){
            if (!empty($decoded['auth_key']) && ApiKeys::where(['auth_key'=>$decoded['auth_key']])->first()){
                $validations = [
                    'user_id' =>['required', 'integer']
                ];
                $validator = Validator::make($decoded, $validations);
                if($validator->fails()){
                    return $this->sendError("Validation Error", $validator->errors());
                }
                $user = User::find($decoded['user_id']);
                if ($user->hasCryptoDeposit()) {
                    $time = date('Y-m-d H:i');
                    $date = strtotime($time) - 30*60;
                    $message = UserDepositsCryptoCompany::select('user_deposits_crypto_company.*','company_crypto.*')
                            ->where('user_id', $decoded['user_id'])
                            ->where('time', '>=',(int)$date)
                            ->leftJoin('company_crypto', 'company_crypto.id', '=', 'user_deposits_crypto_company.crypto_company_id')
                            ->get();
                    if(!$message->isEmpty()){
                        return response()->json(['status' => true,'code'=>200,'message' => $message]);
                    } else {
                        $message=User::find($decoded['user_id']);
                        if(!$message){
                            return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                        } else {
                            return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                        }
                    }
                }  else {
                    $message=User::find($decoded['user_id']);
                    if(!$message){
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
                if($message) {
                    return response()->json(['status' => true,'code'=>200,'message' => $message]);
                } else {
                    $message=User::find($decoded['user_id']);
                    if(!$message) {
                        return response()->json(['status' => false,'code'=>404,'message' =>__('rest.not_user')]);
                    } else {
                        return response()->json(['status' => false,'code'=>404,'message' =>[]]);
                    }
                }
            } else {
                return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_auth')]);
            }
        } else {
            return response()->json(['status' => false,'code'=>404,'message'=>__('rest.error_service')]);
        }
    }
}
