<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TwoAuthMailable;
use App\Models\Logs;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/member/main';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');
    }

    public function logout()
    {
        $user = Auth::user();
        $log = new Logs();
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip= $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip= $_SERVER['REMOTE_ADDR'];
        }
        $location="";
        if (@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip)) {
            $dataArray = json_decode(@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
            $location = $dataArray->geoplugin_countryName;
          }
        $str=$_SERVER["HTTP_USER_AGENT"];
        $pos1 = strpos($str, '(')+1;
        $pos2 = strpos($str, ')')-$pos1;
        $part = substr($str, $pos1, $pos2);
        $parts = explode(" ", $part);
        $os = $parts[1];
        $ismobile = 0;
        $container = $_SERVER['HTTP_USER_AGENT'];
        // A list of mobile devices
        $useragents = array (
        'Blazer' ,
        'Palm' ,
        'Handspring' ,
        'Nokia' ,
        'Kyocera',
        'Samsung' ,
        'Motorola' ,
        'Smartphone',
        'Windows CE' ,
        'Blackberry' ,
        'WAP' ,
        'SonyEricsson',
        'PlayStation Portable',
        'LG',
        'MMP',
        'OPWV',
        'Symbian',
        'EPOC',
        );

        foreach ( $useragents as $useragents ) {
         if(strstr($container,$useragents)) {
           $ismobile = 1;
         }
        }
        if ($ismobile != 1) {
            $device = "Desktop";
        } else {
            $device = "Mobile";
        }
        if ($os=="Android" || $os=="IOS" || $os=="OS") {
            $device = "Mobile";
        }
        $log->create([
            'type'=>'Log out',
            'user_id'=>$user->id,
            'ip'=>$ip,
            'device'=>$device,
            'os'=>$os,
            'location'=>$location
        ]);
        Auth::logout();
        if(Session::has('lastActivityTime')){
            Session::forget('lastActivityTime');
        }
        return redirect('/login');
    }

    public function showLoginForm()
    {
        # Generate fields
        $this->response->fields = [
            ## Email
            'email' => (object) [
                'name' => 'email',
                'label' => '',
                'value' => '',
                'placeholder' => __('auth.emailaddress'),
                'type' => 'email',
                'required' => true,
                'icon' => 'at',
                'autofocus' => true
            ],

            ## Password
            'password' => (object) [
                'name' => 'password',
                'label' => '',
                'value' => '',
                'placeholder' => __('auth.password'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => false
            ],

            ## Remember
            'remember' => (object) [
                'name' => 'remember',
                'label' => __('auth.remember'),
                'value' => '',
                'type' => 'checkbox'
            ]
        ];

        return $this->render('auth.login');
    }

    protected function authenticated(Request $request, $user){
        if ($user->type==2) {
            return redirect('/admin/main');
        }
      if(!empty($user->email_verified_at)){
        if ($user->two_auth==1) {
          if (isset($request->code)) {
              if ($request->code!=$user->code) {
                auth()->logout();
                return back()->withInput($request->input())->with('code_error','Please check your email and write code');
              } else {
                $user->update([
                  'code'=>''
                ]);
                $log = new Logs();
                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip= $_SERVER['HTTP_CLIENT_IP'];
                } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
                } else {
                    $ip= $_SERVER['REMOTE_ADDR'];
                }
                $location="";
                if (@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip)) {
                    $dataArray = json_decode(@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
                    $location = $dataArray->geoplugin_countryName;
                  }
                  $str=$_SERVER["HTTP_USER_AGENT"];
                  $pos1 = strpos($str, '(')+1;
                  $pos2 = strpos($str, ')')-$pos1;
                  $part = substr($str, $pos1, $pos2);
                  $parts = explode(" ", $part);
                  $os = $parts[1];
                  $ismobile = 0;
                  $container = $_SERVER['HTTP_USER_AGENT'];
                  // A list of mobile devices
                  $useragents = array (
                  'Blazer' ,
                  'Palm' ,
                  'Handspring' ,
                  'Nokia' ,
                  'Kyocera',
                  'Samsung' ,
                  'Motorola' ,
                  'Smartphone',
                  'Windows CE' ,
                  'Blackberry' ,
                  'WAP' ,
                  'SonyEricsson',
                  'PlayStation Portable',
                  'LG',
                  'MMP',
                  'OPWV',
                  'Symbian',
                  'EPOC',
                  );

                  foreach ( $useragents as $useragents ) {
                   if(strstr($container,$useragents)) {
                     $ismobile = 1;
                   }
                  }
                  if ($ismobile != 1) {
                      $device = "Desktop";
                  } else {
                      $device = "Mobile";
                  }
                  if ($os=="Android" || $os=="IOS" || $os=="OS") {
                      $device = "Mobile";
                  }
                 $log->create([
                     'type'=>'Log in',
                     'user_id'=>$user->id,
                     'ip'=>$ip,
                     'device'=>$device,
                     'os'=>$os,
                     'location'=>$location
                 ]);
                return redirect('/member/main')->with('pass',$request->password);
              }
          } else {
            $random_string = '';
            for ($i = 0; $i < 6; $i++)
            {
              $random_string .= rand(0, 9);
            }
            $user->update([
              'code'=>$random_string
            ]);
            Mail::to($user->email)->send(new TwoAuthMailable($random_string));
            auth()->logout();
            return back()->withInput($request->input())->with('code_error','Please check your email and write code');
          }
        }
        $log = new Logs();
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip= $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip= $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip= $_SERVER['REMOTE_ADDR'];
        }
        $location="";
        if (@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip)) {
            $dataArray = json_decode(@file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));
            $location = $dataArray->geoplugin_countryName;
          }
          $str=$_SERVER["HTTP_USER_AGENT"];
          $pos1 = strpos($str, '(')+1;
          $pos2 = strpos($str, ')')-$pos1;
          $part = substr($str, $pos1, $pos2);
          $parts = explode(" ", $part);
          $os = $parts[1];
          $ismobile = 0;
          $container = $_SERVER['HTTP_USER_AGENT'];
          // A list of mobile devices
          $useragents = array (
          'Blazer' ,
          'Palm' ,
          'Handspring' ,
          'Nokia' ,
          'Kyocera',
          'Samsung' ,
          'Motorola' ,
          'Smartphone',
          'Windows CE' ,
          'Blackberry' ,
          'WAP' ,
          'SonyEricsson',
          'PlayStation Portable',
          'LG',
          'MMP',
          'OPWV',
          'Symbian',
          'EPOC',
          );

          foreach ( $useragents as $useragents ) {
           if(strstr($container,$useragents)) {
             $ismobile = 1;
           }
          }
          if ($ismobile != 1) {
              $device = "Desktop";
          } else {
              $device = "Mobile";
          }
          if ($os=="Android" || $os=="IOS" || $os=="OS") {
              $device = "Mobile";
          }
         $log->create([
             'type'=>'Log in',
             'user_id'=>$user->id,
             'ip'=>$ip,
             'device'=>$device,
             'os'=>$os,
             'location'=>$location
         ]);
        return redirect('/member/main')->with('pass',$request->password);
      } else {
        auth()->logout();
        return redirect('/')->with('danger','Please check your email and activate account.');
      }
    }
}
