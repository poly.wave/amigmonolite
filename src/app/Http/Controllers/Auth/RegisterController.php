<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Cities;
use Illuminate\Support\Facades\Mail;
use App\Mail\UserRegisteredSuccessfully;
use App\Mail\UserRegisteredMailable;
use App\Mail\UserInfoMailable;
use Carbon\Carbon;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/';

      /**
       * Create a new controller instance.
       *
       * @return void
       */
      public function __construct()
      {
          parent::__construct();
          $this->middleware('guest');
      }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'day_of_birth' => ['required', 'date', 'before:18 years ago'],
            'phone' => ['required', 'string', 'unique:users'],
            'resident_address' => ['required', 'string'],
            'city' => ['required', 'string'],
            'region_id' => ['integer'],
            'country_id' => ['required', 'integer'],
            'gender_id' => ['required', 'integer'],
            'language_id' => ['required', 'integer'],
            'currency_id' => ['required', 'integer'],
            'agreement' => ['accepted']
        ]);
    }

    public function showRegistrationForm()
    {
        # Generate fields
        $this->response->fields = [
            ## Email
            'email' => (object) [
                'name' => 'email',
                'label' => __('register.email'),
                'value' => '',
                'placeholder' => __('register.emailpl'),
                'type' => 'email',
                'required' => true,
                'icon' => 'at',
                'autofocus' => true
            ],

            ## Password
            'password' => (object) [
                'name' => 'password',
                'label' => __('register.pwdnew'),
                'value' => '',
                'placeholder' => __('register.pwdnew1'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => false
            ],
            'password_confirmation' => (object) [
                'name' => 'password_confirmation',
                'label' => '',
                'value' => '',
                'placeholder' => __('register.pwdnew2'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => false
            ],

            ## Name
            'first_name' => (object) [
                'name' => 'first_name',
                'label' => __('register.name'),
                'value' => '',
                'placeholder' => __('register.namepl'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false
            ],
            'last_name' => (object) [
                'name' => 'last_name',
                'label' => __('register.sname'),
                'value' => '',
                'placeholder' => __('register.snamepl'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false
            ],

            ## Birthday
            'day_of_birth' => (object) [
                'name' => 'day_of_birth',
                'label' => __('register.dofb'),
                'value' => '',
                'placeholder' => __('register.dofbpl'),
                'type' => 'date',
                'required' => true,
                'icon' => 'calendar',
                'autofocus' => false
            ],

            ## Phone
            'phone' => (object) [
                'name' => 'phone',
                'label' => __('register.phone'),
                'value' => '',
                'placeholder' => __('register.phonepl'),
                'type' => 'tel',
                'required' => true,
                'icon' => 'phone',
                'autofocus' => false
            ],

            ## Gender
            'gender_id' => (object) [
                'name' => 'gender_id',
                'label' => __('register.gender'),
                'type' => 'select',
                'required' => true,
                'icon' => 'venus-mars',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/genders/list',
                    'value' => 'id',
                    'name' => 'name'
                ]
            ],

            ## Other
            'language_id' => (object) [
                'name' => 'language_id',
                'label' => __('register.preflang'),
                'type' => 'select',
                'required' => true,
                'icon' => 'language',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/languages/list',
                    'value' => 'id',
                    'name' => 'name'
                ]
            ],

            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('register.prefcurr'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name'
                ]
            ],

            ## Location
            'country_id' => (object) [
                'name' => 'country_id',
                'label' => __('register.country'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/countries/list',
                    'value' => 'id',
                    'name' => 'name'
                ]
            ],
            'region_id' => (object) [
                'name' => 'region_id',
                'label' => __('register.region'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/regions/list',
                    'value' => 'id',
                    'name' => 'name',
                    'relation_to' => 'country_id'
                ]
            ],
            'city' => (object) [
                'name' => 'city',
                'label' => __('register.city'),
                'value' => '',
                'placeholder' =>  __('register.city'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
                'data' => [
                    'url' => '/api/cities/list',
                    'value' => 'id',
                    'field' => 'name'
                ]
            ],
            'resident_address' => (object) [
                'name' => 'resident_address',
                'label' =>  __('register.resident'),
                'value' => '',
                'placeholder' =>  __('register.resident'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false
            ],

            ## Agree
            'agreement' => (object) [
                'name' => 'agreement',
                'label' => __('register.agree'),
                'link' => (object) [
                    'name' => __('register.agreename'),
                    'url' => '/docs/declaration'
                ],
                'value' => '',
                'type' => 'checkbox'
            ]
        ];

        return $this->render('auth.register');
    }

    protected function register(Request $request)
    {
      $this->validate( request(), [

        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
        'day_of_birth' => ['required', 'date', 'before:18 years ago'],
        'phone' => ['required', 'string', 'unique:users'],
        'resident_address' => ['required', 'string'],
        'city' => ['required', 'string'],
        'region_id' => ['integer'],
        'country_id' => ['required', 'integer'],
        'gender_id' => ['required', 'integer'],
        'language_id' => ['required', 'integer'],
        'currency_id' => ['required', 'integer'],
        'agreement' => ['accepted']

      ]);
        $data =$request->all();

        $cityId = 0;
        if($data['city']) {
            $cities = Cities::select('id', 'name');
            $cities = $cities->where('name', $data['city']);
            $city = $cities->first();

            if(isset($city->id)) {
                $cityId = $city->id;
            }
        }

        $token = Str::random(60);

        User::create([
            'email' => $data['email'],
            'password_without_hash'=>$data['password'],
            'password' => Hash::make($data['password']),
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'day_of_birth' => $data['day_of_birth'],
            'phone' => $data['phone'],
            'resident_address' => $data['resident_address'],
            'city_id' => $cityId,
            'region_id' => $data['region_id'],
            'country_id' => $data['country_id'],
            'gender_id' => $data['gender_id'],
            'currency_id' => $data['currency_id'],
            'language_id' => $data['language_id'],
            'user_group_id' => 2,
            'api_token' => hash('sha256', $token),
            'balance' => '0',
            'deposits' => '0',
            'earnings' => '0',
            'withdrawals' => '0',
        ]);

        Mail::to($data['email'])->send(new UserRegisteredSuccessfully(hash('sha256', $token)));
        $user_support = User::select('email')
            ->where('id', 2)
            ->first();
        if ($user_support) {
          Mail::to($user_support->email)->send(new UserRegisteredMailable($data['email']));
        }
        return redirect('/')->with('success', 'Successfully created a new account. Please check your email and activate your account.');
    }

    public function activateUser(string $activationCode)
     {
         // try {
             $user = app(User::class)->where('api_token', $activationCode)->first();
             if (!$user) {
                 return "The code does not exist for any user in our system.";
             }
             Mail::to($user->email)->send(new UserInfoMailable($user->email,$user->password_without_hash));
             $user->email_verified_at = Carbon::now();
             $user->password_without_hash = null;
             $user->save();
             auth()->login($user);
         // } catch (\Exception $exception) {
         //     logger()->error($exception);
         //     return "Whoops! something went wrong.";
         // }

         return redirect()->to('/member/main');
     }
}
