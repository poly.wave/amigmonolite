<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $response;
    public $months = [
        '01' => 'Jan',
        '02' => 'Feb',
        '03' => 'Mar',
        '04' => 'Apr',
        '05' => 'May',
        '06' => 'Jun',
        '07' => 'Jul',
        '08' => 'Aug',
        '09' => 'Sep',
        '10' => 'Oct',
        '11' => 'Nov',
        '12' => 'Dec',
    ];

    public function __construct()
    {
        $this->response = (object) [];

        $this->middleware(function (Request $request, $next) {
            $lang = $request->get('lang');
            $session_lang = \Session::get('lang');

            if ($lang) {
                \Session::put('lang', $lang);

                \App::setLocale($lang);
            } else if ($session_lang) {
                \App::setLocale($session_lang);
            }

            return $next($request);
        });
    }

    public function render(string $view)
    {
        return view($view, (array) $this->response);
    }
}
