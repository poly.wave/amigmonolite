<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\AuthenticatedController;
use App\Models\ApproveTypes;
use App\Models\Currencies;
use App\Models\UserWithdrawals;
use App\Models\WithdrawalOptions;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\NewUserWithdrawalRequest;
use App\Mail\WithdrawStatusMailable;
use Illuminate\Support\Facades\Mail;

class WithdrawalRequestController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->response->title = __('member.withdrawal.title');
        $this->response->icon = 'money-bill';

        $data = $request->all();
        if (count($data)) {
            $validate = [
                'name' => ['required', 'string'],
                'amount' => ['required', 'numeric'],
                'currency_id' => ['required', 'integer'],
                'withdrawal_option_id' => ['required', 'integer'],
            ];

            if ($request->validate($validate)) {
                $user_withdrawals = UserWithdrawals::where('created_at', '>=', date('Y-m-') . '01 00:00:00.000000')
                    ->where('created_at', '<=', date('Y-m-t') . ' 23:59:59.000000')
                    ->get();

                $max_withdrawal_granted = $this->currentUser->max_withdrawal_granted > 0 ? $this->currentUser->max_withdrawal_granted : 1;

                if (count($user_withdrawals) >= $max_withdrawal_granted) {
                    $this->response->error = 'You can create max ' . $max_withdrawal_granted . ' withdraw request in month';
                } else {
                    $day = date('d');
                    $withdrawal_day_start = $this->currentUser->withdrawal_day_start > 0 ? $this->currentUser->withdrawal_day_start : 1;
                    $withdrawal_day_end = $this->currentUser->withdrawal_day_end > 0 ? $this->currentUser->withdrawal_day_end : 5;

                    if (
                        $day >= $withdrawal_day_start &&
                        $day <= $withdrawal_day_end
                    ) {
                        $balance = $this->currentUser->balance;

                        if ($balance > 0) {
                            $withdrawal_percent = $this->currentUser->withdrawal_percent > 0 ? $this->currentUser->withdrawal_percent : 10;
                            $balance_percent = $balance / 100;
                            $accept_balance = $balance_percent * $withdrawal_percent;

                            if ($data['amount'] > $accept_balance) {
                                $this->response->error = 'You can withdraw a maximum of ' . $withdrawal_percent . '% of your account at a time';
                            } else {
                                try {
                                    DB::transaction(function () use ($data) {
                                        $approve_type = ApproveTypes::where([
                                            'name' => 'pending'
                                        ])->first();
                                        $approve_type_id = $approve_type->id;
                                        UserWithdrawals::create([
                                            'name' => $data['name'],
                                            'amount' => $data['amount'],
                                            'currency_id' => $data['currency_id'],
                                            'withdrawal_option_id' => $data['withdrawal_option_id'],
                                            'user_id' => $this->currentUser->id,
                                            'approve_type_id' => $approve_type_id,
                                        ]);

                                        $currency = Currencies::where('id', $data['currency_id'])->first();
                                        $withdrawal_option = WithdrawalOptions::where('id', $data['withdrawal_option_id'])->first();

                                        $data['currency_name'] = $currency->name;
                                        $data['withdrawal_option_name'] = $withdrawal_option->name . ' ' . $withdrawal_option->fees;

                                        Mail::to($this->currentUser->email)->send(new WithdrawStatusMailable($approve_type_id,$data['amount'],$data['withdrawal_option_name'],$data['currency_name']));
                                        $users = User::select('email')
                                            ->where('id', 1)
                                            ->orWhere('id', 2)
                                            ->get();

                                        foreach ($users as $user) {
                                            Mail::to($user->email)->send(new NewUserWithdrawalRequest($data, $this->currentUser));
                                        }
                                    });

                                    $this->response->success = true;
                                    $data = [];

                                    return redirect('/member/report/withdrawals');
                                } catch (\Exception $err) {
                                    $this->response->error = $err;
                                }
                            }
                        } else {
                            $this->response->error = 'Insufficient funds on account';
                        }
                    } else {
                        $this->response->error = 'Withdrawal of money from account only from ' . $withdrawal_day_start . ' to ' . $withdrawal_day_end . ' of each month';
                    }
                }
            }
        }

        # Generate fields
        $this->response->fields = [
            ## Password

            'withdrawal_option_id' => (object) [
                'name' => 'withdrawal_option_id',
                'label' => __('member.withdrawal.withtype'),
                'type' => 'select',
                'required' => true,
                'icon' => 'cash-register',
                'autofocus' => false,
                'value' => isset($data['withdrawal_option_id']) ? $data['withdrawal_option_id'] : 0,
                'data' => [
                    'url' => '/api/withdrawal/options/list',
                    'value' => 'id',
                    'name' => 'name, fees'
                ]
            ],
            'amount' => (object) [
                'name' => 'amount',
                'label' => __('member.withdrawal.withamount'),
                'value' => isset($data['amount']) ? $data['amount'] : '',
                'placeholder' => __('member.withdrawal.withamountlable'),
                'type' => 'text',
                'required' => true,
                'icon' => 'tram',
                'autofocus' => false
            ],

            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('member.withdrawal.withcurr'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => isset($data['currency_id']) ? $data['currency_id'] : $this->currentUser->currency_id,
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name'
                ]
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => __('member.withdrawal.purpose'),
                'value' => isset($data['name']) ? $data['name'] : '',
                'placeholder' => __('member.withdrawal.purposelable'),
                'type' => 'text',
                'required' => true,
                'icon' => 'signature',
                'autofocus' => true
            ],

        ];

        return $this->render('member.withdrawal_request');
    }
}
