<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\AuthenticatedController;
use \Carbon\Carbon;
use App\Models\UserTradingAccount;
use App\Models\UserTradingLeverage;
use App\Models\UserDeposits;
use App\Models\UserEarnings;
use App\Models\UserWithdrawals;
use App\Models\MetaTrader\MTWebAPI;

class MainController extends AuthenticatedController
{
    const VERIFY_CODE_LENGTH = 5;
    private $crypt_key = '682d95a2009e19fb3570ccb4d98b820f+dfgerth';
    private $user;
    private $verify_code;
    private $root_url;
    private $api;
    public function __construct()
    {
      $this->api = new MTWebAPI();
        parent::__construct();
    }

    public function index()
    {
        if (\Auth::user()->type==2) {
            return redirect('/admin/main');
        }
        $this->response->title = __('member.main.title');
        $this->response->icon = 'toolbox';
        try {
            $this->GetUserInfo();
        } catch (\Exception $e) {}
        return $this->render('member/main');
    }

    public function image()
    {
      return view('member.image');
    }

    public function GetUserInfo()
    {
      $user_id=\Auth::user()->id;
      $account = UserTradingAccount::where('user_id',$user_id)->first();
      if ($account) {
        $params       = array();
        $answer       = array();
        $answer_boddy = array();
        $params['login']=$account->login;
        $params['from']=strtotime('01.01.2019');
        $params['to']=strtotime(Carbon::now()->addDays(1));
        $params['OFFSET ']='0';
        $params['TOTAL']='100';
        $str =$this->api->CustomSend('DEAL_GET_PAGE', $params, '', $answer, $answer_boddy);
        $json = preg_replace('/[[:cntrl:]]/', '', $str);
        $json = json_decode($json, true);
        $user_main = \App\Models\User::find($user_id);
        if (gettype($json) == 'array') {
          foreach ($json as $key) {
            if ($key['Order'] == 0 && $key['Action'] == 2 && $key['Entry'] == 0) {
              if ($key['Profit']/abs($key['Profit'])==1) {
                $order = new UserDeposits;
                if (!UserDeposits::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_id'=>1,
                    'request_type_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'deposits'=>$user_main->deposits+$key['Profit'],
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
              if ($key['Profit']/abs($key['Profit'])==-1) {
                $order = new UserWithdrawals;
                if (!UserWithdrawals::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_type_id'=>1,
                    'withdrawal_option_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'withdrawals'=>$user_main->withdrawals+abs($key['Profit']),
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
            }
            if ($key['Order'] != 0 && $key['Entry'] == 1) {
              $order = new UserEarnings;
              if (!UserEarnings::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                $order->create([
                  'user_id'=>$user_id,
                  'amount'=>$key['Profit'],
                  'deal'=>$key['Deal'],
                  'order'=>$key['Order'],
                  'action'=>$key['Action'],
                  'entry'=>$key['Entry'],
                  'time'=>$key['Time'],
                  'time_msc'=>$key['TimeMsc'],
                  'symbol'=>$key['Symbol'],
                  'name'=>$key['Symbol'],
                  'currency_id'=>1,
                  'comment'=>$key['Comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$key['Profit'],
                  'balance'=>$user_main->balance+$key['Profit'],
                ]);
              }
            }
          }
        }
      }
  }
}
