<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\AuthenticatedController;
use App\Models\ApproveTypes;
use App\Models\Cities;
use App\Models\DocumentTypes;
use App\Models\UserFiles;
use App\Models\User;
use App\Models\UserPassport;
use App\Mail\ProfileInfoChangesMailable;
use App\Mail\PasswordChangesMailable;
use App\Mail\FilesAddedMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Auth;

class ProfileController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->response->title = __('member.profile.title');
        $this->response->icon = 'user';

        return $this->render('member.profile');
    }

    public function personal_information(Request $request)
    {
        $this->response->title = __('member.profile.personal_information.title');
        $this->response->icon = 'user-cog';

        $update = [];
        if ($request->input('email')) {
            $validate = [
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $this->currentUser->id],
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'day_of_birth' => ['required', 'string'],
                'phone' => ['required', 'string', 'unique:users,phone,' . $this->currentUser->id],
                'resident_address' => ['required', 'string'],
                'city' => ['required', 'string'],
                'region_id' => ['required', 'integer'],
                'country_id' => ['required', 'integer'],
                'gender_id' => ['required', 'integer'],
                'language_id' => ['required', 'integer'],
                'currency_id' => ['required', 'integer'],
            ];
            $avatar="";
            if ($request->hasFile('avatar')) {
                $file = $request->file('avatar');
                $file_name = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();

                $path = 'profile/' . $this->currentUser->id . '/' . $file_name;
                Storage::put($path, File::get($file));

                $avatar = Storage::url($path);
            }
            if ($request->validate($validate)) {
                $data = $request->all();
                $cityId = 0;
                if($data['city']) {
                    $cities = Cities::select('id', 'name');
                    $cities = $cities->where('name', $data['city']);
                    $city = $cities->first();

                    if(isset($city->id)) {
                        $cityId = $city->id;
                    }
                }
                $update = [
                    'avatar' => $avatar,
                    'email' => $data['email'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'day_of_birth' => $data['day_of_birth'],
                    'phone' => $data['phone'],
                    'resident_address' => $data['resident_address'],
                    'city_id' => $cityId,
                    'region_id' => $data['region_id'],
                    'country_id' => $data['country_id'],
                    'gender_id' => $data['gender_id'],
                    'currency_id' => $data['currency_id'],
                    'language_id' => $data['language_id'],
                    'two_auth' => isset($data['two_auth']) && $data['two_auth']=='on'?1:0,
                ];

                $user = User::find($this->currentUser->id);
                $user->fill($update);

                if ($user->save()) {
                    $update['city'] = $data['city'];

                    $this->response->success = true;
                    Mail::to($user->email)->send(new ProfileInfoChangesMailable());
                    return redirect('member/profile/personal_information');
                }
            }
        }
        $this->response->fields = [
          ## Avatar
          'avatar' => (object) [
              'name' => 'avatar',
              'label' => __('member.profile.personal_information.avatar'),
              'value' =>'',
              'placeholder' => '',
              'type' => 'file',
              'required' => false,
              'icon' => 'image',
              'mime_type' => 'image',
              'autofocus' => false,
          ],
            ## Email
            'email' => (object) [
                'name' => 'email',
                'label' => __('member.profile.personal_information.email'),
                'value' => isset($update['email']) ? $update['email'] : $this->currentUser->email,
                'placeholder' => __('member.profile.personal_information.emailpl'),
                'type' => 'email',
                'required' => true,
                'icon' => 'at',
                'autofocus' => true,
            ],

            ## Name
            'first_name' => (object) [
                'name' => 'first_name',
                'label' => __('member.profile.personal_information.name'),
                'value' => isset($update['first_name']) ? $update['first_name'] : $this->currentUser->first_name,
                'placeholder' => __('member.profile.personal_information.namepl'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],
            'last_name' => (object) [
                'name' => 'last_name',
                'label' => __('member.profile.personal_information.sname'),
                'value' => isset($update['last_name']) ? $update['last_name'] : $this->currentUser->last_name,
                'placeholder' => __('member.profile.personal_information.snamepl'),
                'type' => 'text',
                'required' => true,
                'icon' => 'user-tag',
                'autofocus' => false,
            ],

            ## Birthday
            'day_of_birth' => (object) [
                'name' => 'day_of_birth',
                'label' => __('member.profile.personal_information.dofb'),
                'value' => isset($update['day_of_birth']) ? $update['day_of_birth'] : $this->currentUser->day_of_birth,
                'placeholder' => __('member.profile.personal_information.dofbpl'),
                'type' => 'date',
                'required' => true,
                'icon' => 'calendar',
                'autofocus' => false,
            ],

            ## Phone
            'phone' => (object) [
                'name' => 'phone',
                'label' => __('member.profile.personal_information.phone'),
                'value' => isset($update['phone']) ? $update['phone'] : $this->currentUser->phone,
                'placeholder' => __('member.profile.personal_information.phonepl'),
                'type' => 'tel',
                'required' => true,
                'icon' => 'phone',
                'autofocus' => false,
            ],

            ## Gender
            'gender_id' => (object) [
                'name' => 'gender_id',
                'label' => __('member.profile.personal_information.gender'),
                'type' => 'select',
                'required' => true,
                'icon' => 'venus-mars',
                'autofocus' => false,
                'value' => isset($update['gender_id']) ? $update['gender_id'] : $this->currentUser->gender_id,
                'data' => [
                    'url' => '/api/genders/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Other
            'language_id' => (object) [
                'name' => 'language_id',
                'label' => __('member.profile.personal_information.preflang'),
                'type' => 'select',
                'required' => true,
                'icon' => 'language',
                'autofocus' => false,
                'value' => isset($update['language_id']) ? $update['language_id'] : $this->currentUser->language_id,
                'data' => [
                    'url' => '/api/languages/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            'currency_id' => (object) [
                'name' => 'currency_id',
                'label' => __('member.profile.personal_information.prefcurr'),
                'type' => 'select',
                'required' => true,
                'icon' => 'coins',
                'autofocus' => false,
                'value' => isset($update['currency_id']) ? $update['currency_id'] : $this->currentUser->currency_id,
                'data' => [
                    'url' => '/api/currencies/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],

            ## Location
            'country_id' => (object) [
                'name' => 'country_id',
                'label' => __('member.profile.personal_information.country'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => isset($update['country_id']) ? $update['country_id'] : $this->currentUser->country_id,
                'data' => [
                    'url' => '/api/countries/list',
                    'value' => 'id',
                    'name' => 'name',
                ],
            ],
            'region_id' => (object) [
                'name' => 'region_id',
                'label' => __('member.profile.personal_information.region'),
                'type' => 'select',
                'required' => true,
                'icon' => 'globe',
                'autofocus' => false,
                'value' => isset($update['region_id']) ? $update['region_id'] : $this->currentUser->region_id,
                'data' => [
                    'url' => '/api/regions/list',
                    'value' => 'id',
                    'name' => 'name',
                    'relation_to' => 'country_id',
                ],
            ],
            'city' => (object) [
                'name' => 'city',
                'label' => __('member.profile.personal_information.city'),

                'placeholder' => __('member.profile.personal_information.city'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
                'value' => isset($update['city']) ? $update['city'] : $this->currentUser->city,
                'data' => [
                    'url' => '/api/cities/list',
                    'value' => 'id',
                    'field' => 'name',
                ],
            ],
            'resident_address' => (object) [
                'name' => 'resident_address',
                'label' => __('member.profile.personal_information.resident'),
                'value' => isset($update['resident_address']) ? $update['resident_address'] : $this->currentUser->resident_address,
                'placeholder' => __('member.profile.personal_information.resident'),
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => false,
            ],
            ## two_auth
            'two_auth' => (object) [
                'name' => 'two_auth',
                'label' => __('member.profile.personal_information.two_auth'),
                'type' => 'checkbox',
                'required' => false,
                'icon' => 'user-tag',
                'autofocus' => false,
                'value' => isset($update['two_auth']) ? $update['two_auth'] : $this->currentUser->two_auth,
            ],
        ];

        return $this->render('member.profile.personal_information');
    }

    public function reset_password(Request $request)
    {
        $this->response->title = __('member.profile.reset_password.title');
        $this->response->icon = 'dice-d6';

        if ($request->input('password')) {
            $validate = [
                'old_password' => ['required', 'string', 'min:8'],
                'password' => ['required', 'string', 'min:8', 'confirmed']
            ];

            if ($request->validate($validate)) {
                $data = $request->all();

                $update = [
                    'password' => Hash::make($data['password'])
                ];

                if (Hash::check($data['old_password'], $this->currentUser->password)) {
                    $user = User::find($this->currentUser->id);
                    $user->fill($update);

                    if ($user->save()) {
                        $this->response->success = true;
                        Mail::to($user->email)->send(new PasswordChangesMailable());
                    }
                } else {
                    $this->response->error = __('member.profile.reset_password.incorrect');
                }
            }
        }

        # Generate fields
        $this->response->fields = [
            ## Password
            'old_password' => (object) [
                'name' => 'old_password',
                'label' => __('member.profile.reset_password.pwdold'),
                'value' => '',
                'placeholder' => __('member.profile.reset_password.pwdold1'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => true,
            ],
            'password' => (object) [
                'name' => 'password',
                'label' => __('member.profile.reset_password.pwdnew'),
                'value' => '',
                'placeholder' => __('member.profile.reset_password.pwdnew1'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => false,
            ],
            'password_confirmation' => (object) [
                'name' => 'password_confirmation',
                'label' => '',
                'value' => '',
                'placeholder' => __('member.profile.reset_password.pwdnew2'),
                'type' => 'password',
                'required' => true,
                'icon' => 'lock',
                'autofocus' => false,
            ],
        ];

        return $this->render('member.profile.reset_password');
    }

    public function files(Request $request)
    {
        $this->response->title = __('member.profile.files.title');
        $this->response->icon = 'folder-open';

        # Generate fields

        # Get default approve type
        $approve_type = ApproveTypes::where([
            'name' => 'pending'
        ])->first();
        $default_approve_type_id = $approve_type->id;
        $approve_type_id = $default_approve_type_id;

        ## Get document types
        $document_types = DocumentTypes::where('alias',$request->type)->get();
        foreach ($document_types as $row) {
            ## Upload files
            $image = null;
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $file_name = md5(time() . $file->getClientOriginalName()) . '.' . $file->getClientOriginalExtension();

                $path = 'profile/files/' . $this->currentUser->id . '/' . $file_name;
                Storage::put($path, File::get($file));

                ## Set db data
                UserFiles::where([
                    'user_id' => $this->currentUser->id,
                    'document_type_id' => $row->id
                ])->delete();

                $approve_type_id = $default_approve_type_id;

                UserFiles::create([
                    'user_id' => $this->currentUser->id,
                    'document_type_id' => $row->id,
                    'file' => $path,
                    'approve_type_id' => $approve_type_id
                ]);

                $image = Storage::url($path);
                Mail::to($this->currentUser->email)->send(new FilesAddedMailable());
            } else {
                $user_file = UserFiles::where([
                    'user_id' => $this->currentUser->id,
                    'document_type_id' => $row->id
                ])->first();

                if (isset($user_file->id)) {
                    $approve_type_id = $user_file->approve_type_id;
                    if (isset($user_file->file)) {
                        $image = Storage::url($user_file->file);
                    }
                }
            }

            ## Get current approve
            $approve_type = ApproveTypes::where([
                'id' => $approve_type_id
            ])->first();

            $border_color = 'info';
            switch ($approve_type->name) {
                case 'approved':
                    $border_color = 'success';
                    break;

                case 'rejected':
                    $border_color = 'danger';
                    break;

                case 'pending':
                    $border_color = 'primary';
                    break;
            }
        }
        $data = UserFiles::select('user_files.*','approve_types.name as approve')
            ->join('approve_types', 'approve_types.id', '=', 'user_files.approve_type_id')
            ->where('user_id', $this->currentUser->id)->get();
        $this->response->files = $data;

        return $this->render('member.profile.files');
    }

    public function user_passport_table()
    {
        $data = UserPassport::select('user_passport.*','approve_types.name as approve')
            ->join('approve_types', 'approve_types.id', '=', 'user_passport.approve_id')
            ->where('user_id', $this->currentUser->id);

        return datatables()->of($data)
            ->addIndexColumn()
            ->addColumn('approve', function ($data)
            {
                if ($data->approve == 'approved') {
                  return '<span class="text-success">'.$data->approve.'</span>';
                } elseif ($data->approve == 'pending') {
                  return '<span class="text-warning">'.$data->approve.'</span>';
                } else {
                  return '<span class="text-danger">'.$data->approve.'</span>';
                }
            })
            ->rawColumns(['approve'])
            ->make(true);
    }
    public function user_passport()
    {
        $this->response->title = __('member.profile.passport.title');
        $this->response->icon = 'user-cog';

        return $this->render('member.profile.passport');
    }
}
