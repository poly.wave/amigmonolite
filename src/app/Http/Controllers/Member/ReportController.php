<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\AuthenticatedController;

class ReportController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->response->title = __('member.report.title');
        $this->response->icon = 'receipt';

        return $this->render('member.report');
    }

    public function deposits()
    {
        $this->response->title = __('member.report.deposit');
        $this->response->icon = 'hand-holding-usd';

        return $this->render('member.report.deposits');
    }

    public function earnings()
    {
        $this->response->title = __('member.report.earning');
        $this->response->icon = 'money-bill';

        return $this->render('member.report.earnings');
    }

    public function withdrawals()
    {
        $this->response->title = __('member.report.withdrawal');
        $this->response->icon = 'money-bill-wave';

        return $this->render('member.report.withdrawals');
    }
}
