<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\AuthenticatedController;
use App\Models\User;
use App\Models\Currencies;
use App\Models\PaymentCc;
use App\Models\UserDeposits;
use App\Models\ConvertCurrency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use App\Models\UserTradingAccount;
use App\Models\UserTradingLeverage;
use App\Models\UserEarnings;
use App\Models\UserWithdrawals;
use App\Models\UserBanks;
use App\Models\MetaTrader\MTWebAPI;
use Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\DepositsMailable;

class DepositController extends AuthenticatedController
{
    const VERIFY_CODE_LENGTH = 5;
    private $crypt_key = '682d95a2009e19fb3570ccb4d98b820f+dfgerth';
    private $user;
    private $verify_code;
    private $root_url;
    private $api;
    public function __construct(Request $request)
    {
        $this->api = new MTWebAPI();
        parent::__construct();

        $this->request = $request;
    }

    private function client($action, $params, $use_cutomer = false, $reference_code = false)
    {
        $payment_cc = PaymentCc::orderBy('id', 'asc')->limit(1)->first();

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:WSPINIntf-IWSPIN" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';
        $xml .= '<SOAP-ENV:Body>';
        $xml .= '<ns1:' . $action . '>';

        $xml .= '<wsuser xsi:type="xsd:string">' . $payment_cc->wsuser . '</wsuser>';
        $xml .= '<wspass xsi:type="xsd:string">' . $payment_cc->wspass . '</wspass>';
        $xml .= '<apikey xsi:type="xsd:string">' . $payment_cc->apikey . '</apikey>';

        if ($use_cutomer === true) {
            $xml .= '<username xsi:type="xsd:string">' . $this->currentUser->cc_username . '</username>';
            $xml .= '<password xsi:type="xsd:string">' . $this->currentUser->cc_password . '</password>';
        }

        if ($reference_code === true) {
            $reference_code = md5(time() . $this->currentUser->id);

            $xml .= '<referencecode xsi:type="xsd:string">' . $reference_code . '</referencecode>';
        }

        foreach ($params as $key => $value) {
            $xml .= '<' . $key . ' xsi:type="xsd:string">' . $value . '</' . $key . '>';
        }

        $xml .= '</ns1:' . $action . '>';
        $xml .= '</SOAP-ENV:Body>';
        $xml .= '</SOAP-ENV:Envelope>';

        $ch = curl_init($payment_cc->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        $output = curl_exec($ch);
        $json = strip_tags($output);
        $response = json_decode($json);

        $result = (object) [
            'success' => false,
            'code' => isset($response->ResultCode) ? $response->ResultCode : null,
            'message' => isset($response->ResultMessage) ? $response->ResultMessage : null,
            'data' => (object) $response->ResultObject
        ];

        if (isset($response->ResultStatus)) {
            if ($response->ResultStatus) {
                $result->success = true;
            }
        }

        if ($reference_code) {
            $result->data->reference_code = $reference_code;
        }

        return $result;
    }

    public function index()
    {
        $this->response->title = __('member.deposit');
        $this->response->icon = 'search-dollar';

        return $this->render('member.deposit');
    }

    public function bank_transfer()
    {
        $this->response->title = __('member.deposit.banktransfer');
        $this->response->icon = 'exchange-alt';
        $this->response->fields = [
            'amount' => (object) [
                'name' => 'amount',
                'label' => 'Amount',
                'value' => '',
                'placeholder' => '',
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => true
            ],
            'name' => (object) [
                'name' => 'name',
                'label' => 'Bank Name',
                'value' => '',
                'placeholder' => '',
                'type' => 'text',
                'required' => true,
                'icon' => 'building',
                'autofocus' => true
            ],
        ];
        return $this->render('member.deposit.bank_transfer');
    }

    public function credit_card($endpoint = null)
    {
        $payment_cc = PaymentCc::orderBy('id', 'asc')->limit(1)->first();

        $this->response->title = __('member.deposit.bycreditcard');
        $this->response->icon = 'credit-card';

        $user_deposit_id = \Session::get('user_deposit_id');

        if ($endpoint && $user_deposit_id) {
            $user_deposit = UserDeposits::where('id', $user_deposit_id)->first();
            $user_deposit_data = json_decode($user_deposit->data);

            $response = $this->client('PaymentCheckCall', [
                'referencecode' => $user_deposit_data->ReferenceCode
            ], true);

            if ($response->data->IsOkey) {
                $user = $this->currentUser;

                DB::transaction(function () use ($user, $user_deposit) {
                    UserDeposits::where('id', $user_deposit->id)->delete();

                    // User::where('id', $user->id)->update([
                    //     'deposits' => ($user->money + $user_deposit->amount)
                    // ]);

                    \Session::put('user_deposit_id', null);
                });
                $this->response->success = 'Payment transaction successful';
                $this->DepositChange($user_deposit->amount,'Deposit by Credit card');

                $currency_name = Currencies::find($user_deposit->currency_id)->name;
                $user_support = User::select('email')
                    ->where('id', 2)
                    ->first();
                if ($user_support) {
                  Mail::to($user_support->email)->send(new DepositsMailable('Cred Card',$user->first_name." ".$user->last_name,$user_deposit->amount,$currency_name));
                }
            } else {
                UserDeposits::where('id', $user_deposit_id)->update([
                    'approve_id' => 2
                ]);

                \Session::put('user_deposit_id', null);

                $this->response->error = 'Payment transaction error';
            }
        } else {
            // Create cc customer
            if (!$this->currentUser->cc_customer_id) {
                if (isset($this->currentUser->passport->passport_id)) {
                    $year = date('Y', strtotime($this->currentUser->day_of_birth));
                    $month = date('m', strtotime($this->currentUser->day_of_birth));
                    $day = date('d', strtotime($this->currentUser->day_of_birth));

                    $response = $this->client('NewCustomer', [
                        'name' => $this->currentUser->first_name,
                        'surname' => $this->currentUser->last_name,
                        'mail' => $this->currentUser->email,
                        'username' => $this->currentUser->cc_username,
                        'password' => $this->currentUser->cc_password,
                        'date_day' => $day,
                        'date_month' => $month,
                        'date_year' => $year,
                        'country' => $this->currentUser->country,
                        'city' => $this->currentUser->city,
                        'zipcode' => '',
                        'adress' => $this->currentUser->resident_address,
                        'phone' => str_replace('+', '', $this->currentUser->phone),
                        'identity' => $this->currentUser->passport->passport_id
                    ]);

                    if ($response->success) {
                        User::where('id', $this->currentUser->id)->update([
                            'cc_customer_id' => $response->data->CustomerId
                        ]);

                        $this->currentUser->cc_customer_id = $response->data->CustomerId;
                    } else {
                        $this->response->error = $response->message;
                    }
                } else {
                    $this->response->error = 'No passport data';
                }
            }

            if ($this->currentUser->cc_customer_id) {
                $data = $this->request->all();
                if (count($data)) {
                    $validate = [
                        'amount' => ['required', 'numeric','min:' . $payment_cc->minimal_deposit],
                    ];

                    if ($this->request->validate($validate)) {
                      $rate = new ConvertCurrency;
                        try {
                            $response = $this->client('PaymentCall', [
                                'price' => $rate->fetch_exchange_rates("USD","TRY",$data['amount']),
                                'failurl' => url('/member/deposit/credit_card/failure'),
                                'successurl' => url('/member/deposit/credit_card/success')
                            ], true, true);

                            if ($response->success) {
                                $user_deposit_id = UserDeposits::create([
                                    'name' => 'Deposit by credit card',
                                    'user_id' => $this->currentUser->id,
                                    'currency_id' => $this->currentUser->currency_id,
                                    'amount' => $data['amount'],
                                    'data' => json_encode($response->data),
                                    'approve_id' => 3,
                                    'request_type_id' => 3
                                ])->id;

                                \Session::put('user_deposit_id', $user_deposit_id);

                                return redirect($response->data->PaymentUrl);
                            } else {
                                $this->response->error = 'Payment error';
                            }
                        } catch (\Exception $err) {
                            $this->response->error = $err;
                        }
                    }
                }
            } else {
                $this->response->error = 'No cc_customer_id';
            }
        }

        # Generate fields
        $this->response->fields = [
            'amount' => (object) [
                'name' => 'amount',
                'label' => 'Amount',
                'value' => isset($data['amount']) ? $data['amount'] : '',
                'placeholder' => 'U.S. Dollars',
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => true
            ]
        ];

        return $this->render('member.deposit.credit_card');
    }

    public function crypto_coins()
    {
        $this->response->title = __('member.deposit.cryptocoins');
        $this->response->icon = 'coins';

        $this->response->fields = [
            'amount_crypto' => (object) [
                'name' => 'amount_crypto',
                'label' => 'Amount',
                'value' => '',
                'placeholder' => 'U.S. Dollars',
                'type' => 'number',
                'required' => true,
                'icon' => 'dollar-sign',
                'autofocus' => true
            ]
        ];

        return $this->render('member.deposit.crypto_coins');
    }
    public function bankTransfer(Request $request)
    {
        $validate = [
            'amount' => ['required', 'numeric','min:100'],
            'name' => ['required'],
        ];

        if ($this->request->validate($validate)) {
            $user=User::find(\Auth::user()->id);
            $currency_name = Currencies::find(1)->name;
            // $user_bank = UserBanks::create([
            //     'user_id'=>$user->id,
            //     'amount'=>$request->post('amount'),
            //     'name'=>$request->post('name'),
            //     'currency_id'=>1,
            //     'address'=>"non set",
            //     'iban'=>"non set",
            //     'swift'=>"non set"
            //
            // ]);
            $user_bank_id=0;
            if ($user_bank = UserBanks::where(['user_id'=>$user->id])->first()) {
                $user_bank_id=$user_bank->id;
            }
            $user_deposits = UserDeposits::create([
                'user_id'=>$user->id,
                'amount'=>$request->post('amount'),
                'name'=>"Deposit by Bank Transfer",
                'comment'=>"Deposit by Bank Transfer",
                'currency_id'=>1,
                'approve_id'=>3,
                'time'=>strtotime(date("Y-m-d H:i")),
            ]);
            $user_support = User::select('email')
                ->where('id', 2)
                ->first();
            if ($user_support) {
              Mail::to($user_support->email)->send(new DepositsMailable('Bank Transfer',$user->first_name." ".$user->last_name,$request->post('amount'),$currency_name));
            }
            return back()->with('success','Bank Transfer successfuly added!');
        }
    }
    public function DepositChange($amount,$comment)
    {
      if ($account = UserTradingAccount::where('user_id',\Auth::user()->id)->first()) {
          $login = $account->login;
          try {
              $this->api->UserDepositChange($login, $amount, $comment, 2);
              $this->GetUserInfo();
          } catch (\Exception $e) {

          }


      }
    }
    public function GetUserInfo()
    {
      $user_id=\Auth::user()->id;
      $account = UserTradingAccount::where('user_id',$user_id)->first();
      if ($account) {
        $params       = array();
        $answer       = array();
        $answer_boddy = array();
        $params['login']=$account->login;
        $params['from']=strtotime(Carbon::now()->subDays(1));
        $params['to']=strtotime(Carbon::now()->addDays(1));
        $params['OFFSET ']='0';
        $params['TOTAL']='100';
        $str =$this->api->CustomSend('DEAL_GET_PAGE', $params, '', $answer, $answer_boddy);
        $json = preg_replace('/[[:cntrl:]]/', '', $str);
        $json = json_decode($json, true);
        $user_main = \App\Models\User::find($user_id);
        if (gettype($json) == 'array') {
          foreach ($json as $key) {
            if ($key['Order'] == 0 && $key['Action'] == 2 && $key['Entry'] == 0) {
              if ($key['Profit']/abs($key['Profit'])==1) {
                $order = new UserDeposits;
                if (!UserDeposits::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_id'=>1,
                    'request_type_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'deposits'=>$user_main->deposits+$key['Profit'],
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
              if ($key['Profit']/abs($key['Profit'])==-1) {
                $order = new UserWithdrawals;
                if (!UserWithdrawals::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                  $order->create([
                    'user_id'=>$user_id,
                    'amount'=>$key['Profit'],
                    'deal'=>$key['Deal'],
                    'order'=>$key['Order'],
                    'action'=>$key['Action'],
                    'entry'=>$key['Entry'],
                    'time'=>$key['Time'],
                    'time_msc'=>$key['TimeMsc'],
                    'symbol'=>$key['Symbol'],
                    'name'=>$key['Comment'],
                    'currency_id'=>1,
                    'approve_type_id'=>1,
                    'withdrawal_option_id'=>1,
                    'comment'=>$key['Comment']
                    ]
                  );
                  $user_main->update([
                    'withdrawals'=>$user_main->withdrawals+abs($key['Profit']),
                    'balance'=>$user_main->balance+$key['Profit'],
                  ]);
                }
              }
            }
            if ($key['Order'] != 0 && $key['Entry'] == 1) {
              $order = new UserEarnings;
              if (!UserEarnings::where([['time',$key['Time']],['deal',$key['Deal']]])->first()) {
                $order->create([
                  'user_id'=>$user_id,
                  'amount'=>$key['Profit'],
                  'deal'=>$key['Deal'],
                  'order'=>$key['Order'],
                  'action'=>$key['Action'],
                  'entry'=>$key['Entry'],
                  'time'=>$key['Time'],
                  'time_msc'=>$key['TimeMsc'],
                  'symbol'=>$key['Symbol'],
                  'name'=>$key['Symbol'],
                  'currency_id'=>1,
                  'comment'=>$key['Comment']
                  ]
                );
                $user_main->update([
                  'earnings'=>$user_main->earnings+$key['Profit'],
                  'balance'=>$user_main->balance+$key['Profit'],
                ]);
              }
            }
          }
        }
      }
  }
  public function chargeCrypto(Request $request)
  {
    $validate = [
        'amount_crypto' => ['required', 'numeric','min:100'],
    ];

    if ($this->request->validate($validate)) {
      $ch = curl_init();
      $user = \Auth::user();
      $name = $user->first_name.' '.$user->last_name;
      $description  = "Crypto currency payment";
      $amount = $request->post('amount_crypto');
      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, 'https://api.commerce.coinbase.com/charges');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n       \"name\": \"$name\",\n       \"description\": \"$description\",\n       \"local_price\": {\n         \"amount\": \"$amount\",\n         \"currency\": \"USD\"\n       },\n       \"pricing_type\": \"fixed_price\",\n       \"metadata\": {\n         \"customer_id\": \"id_1005\",\n         \"customer_name\": \"$name\"\n       },\n       \"redirect_url\": \"https://www.amigfx.com/member/deposit/crypto-coins-completed\",\n       \"cancel_url\": \"https://www.amigfx.com/member/deposit/crypto-coins-canceled\"\n     }");

      $headers = array();
      $headers[] = 'Content-Type: application/json';
      $headers[] = 'X-Cc-Api-Key: 2041b869-5f20-4d64-874f-8e07d914e28d';
      $headers[] = 'X-Cc-Version: 2018-03-22';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);

      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      }
      curl_close($ch);
      $json = preg_replace('/[[:cntrl:]]/', '', $result);
      $json = json_decode($json, true);
      $user_deposit_id = UserDeposits::create([
          'name' => 'Deposit by crypto currency',
          'comment' => 'Deposit by crypto currency',
          'user_id' => $this->currentUser->id,
          'currency_id' => $this->currentUser->currency_id,
          'amount' => $amount,
          'data' => '',
          'approve_id' => 3,
          'request_type_id' => 3
      ])->id;

      \Session::put('user_deposit_id', $user_deposit_id);
      $currency_name = Currencies::find($user_deposit_id->currency_id)->name;
      $user_support = User::select('email')
          ->where('id', 2)
          ->first();
      if ($user_support) {
        Mail::to($user_support->email)->send(new DepositsMailable('Bitcoin',$user->first_name." ".$user->last_name,$amount,$currency_name));
      }
      return Redirect::to($json['data']['hosted_url']);
    }
  }
  public function cryptoCoinsCanceled()
  {
    $this->response->title = __('member.deposit.cryptocoins');
    $this->response->icon = 'coins';

    $user_deposit_id = \Session::get('user_deposit_id');

    if ($user_deposit_id) {
        $user_deposit = UserDeposits::where('id', $user_deposit_id)->first();
        $user_deposit->update([
          'approve_id'=>2,
          'time'=> strtotime(date("Y-m-d H:i:s")),
        ]);
        \Session::put('user_deposit_id', null);
    }
    return $this->render('member.deposit.crypto_coins_canceled');
  }
  public function cryptoCoinsCompleted()
  {
    $this->response->title = __('member.deposit.cryptocoins');
    $this->response->icon = 'coins';
    $user_deposit_id = \Session::get('user_deposit_id');

    if ($user_deposit_id) {
        $user_deposit = UserDeposits::where('id', $user_deposit_id)->first();
        $this->DepositChange($user_deposit->amount,$user_deposit->comment);
        \Session::put('user_deposit_id', null);
        $user_deposit->delete();
    }
    return $this->render('member.deposit.crypto_coins_completed');
  }
}
