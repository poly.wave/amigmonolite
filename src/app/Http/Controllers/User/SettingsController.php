<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Cities;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends AuthenticatedController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $this->response->title = __('user.usersettings');
        $this->response->icon = 'user-cog';

        return $this->render('user.settings');
    }
}
