<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class cUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'day_of_birth',
        'gender_id',
        'phone',
        'country_id',
        'region_id',
        'city_id',
        'resident_address',
        'language_id',
        'bank_detail',
        'bank_name',
        'bank_address',
        'iban',
        'swift',
        'currency_id',
        'user_group_id',
        'deposits',
        'earnings',
        'withdrawals',
        'avatar',
        'api_token',
        'balance',
        'withdrawal_percent',
        'withdrawal_day_start',
        'withdrawal_day_end',
        'max_withdrawal_granted',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
