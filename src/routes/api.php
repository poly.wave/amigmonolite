<?php

use Illuminate\Http\Request;
use App\PasswordReset;


Route::prefix('rest')->group(function () {
    Route::any('/test', 'Rest\MainController@test');

    Route::post('/login', 'Rest\MainController@login');
    Route::post('/registration', 'Rest\MainController@registration');
    Route::post('/countries', 'Rest\MainController@getCountries');
    Route::post('/regions', 'Rest\MainController@getRegions');
    Route::post('/cities', 'Rest\MainController@getCities');
    Route::post('/currency-list', 'Rest\MainController@getCurrencyList');
    Route::post('/languages-list', 'Rest\MainController@getLanguagesList');
    Route::post('/gender-list', 'Rest\MainController@getGenderList');
    Route::post('/document-types', 'Rest\MainController@getDocumentTypes');
    Route::post('/user-info', 'Rest\MainController@getUserInfo');

    Route::post('/user-deposits', 'Rest\MainController@getUserDeposits');
    Route::post('/user-deposit-bank', 'Rest\MainController@getUserDepositBank');

    Route::post('/user-withdrawals', 'Rest\MainController@getUserWithdrawals');

    Route::post('/user-earnings', 'Rest\MainController@getUserEarnings');


    Route::post('/user-approved-deposits', 'Rest\MainController@getUserApprovedDeposits');

    Route::post('/user-approved-withdrawals', 'Rest\MainController@getUserApprovedWithdrawals');


    Route::post('/user-passport-info', 'Rest\MainController@getUserPassportInfo');

    Route::post('/user-trading-account-info', 'Rest\MainController@getUserTradingAccountInfo');

    Route::post('/personal-information', 'Rest\MainController@getUpdateUserInfo');
    Route::post('/password/reset', 'Rest\MainController@ResetPassword');
    Route::post('/reset-password','Rest\MainController@ChangePassword');

   Route::post('/modify-password','Rest\MainController@ProfileResetPassword');

   Route::post('/create-passport-info','Rest\MainController@CreatePassportInfo');


   Route::post('/create-user-file','Rest\MainController@CreateUserFile');

   Route::post('/user-files','Rest\MainController@GetUserFiles');

   Route::post('/create-user-withdrawals','Rest\MainController@CreateUserWithdrawals');

   Route::post('/add-deposit-by-bank-transfer','Rest\MainController@AddDepositBankTransfer');

   Route::post('/create-wallet-by-bank-transfer','Rest\MainController@CreateWalletBankTransfer');

   Route::post('/create-wallet-by-credit-card','Rest\MainController@CreateWalletCreditCard');

   Route::post('/create-wallet-by-crypto-currency','Rest\MainController@CreateWalletCryptoCurrency');



   Route::post('/add-deposit-by-credit-card','Rest\MainController@AddDepositCreditCard');

   Route::post('/add-deposit-by-crypto-currency','Rest\MainController@AddDepositCryptoCurrency');

   Route::post('/get-user-bank-transfers','Rest\MainController@getUserBankTransfers');

   Route::post('/get-user-credit-cards','Rest\MainController@getUserCreditCards');

  Route::post('/get-user-crypto-currencies','Rest\MainController@getUserCryptoCurrencies');

   Route::post('/create-user-trading-account','Rest\MainController@CreateUserTradingAccount');

    Route::post('/get-laverages-names','Rest\MainController@getLaveragesNames');
    Route::post('/get-withdrawal-types','Rest\MainController@getWithdrawalTypes');

    Route::post('/get-crypto-coins','Rest\MainController@getCryptoCoins');

    Route::any('/get-trading-verify-code','Rest\MainController@getTradingVerifyCode');

    Route::post('/update-user-earnings','Rest\MainController@updateUserEarnings');

    Route::post('/get-crypto-companies','Rest\MainController@getCryptoCompanies');

    Route::post('/get-social-company','Rest\MainController@getSocialCompanies');

    Route::post('/get-active-crypto-company','Rest\MainController@getActiveCryptoCompany');
});

Route::middleware('auth:api')->group(function() {
    Route::prefix('user')->group(function() {
        ## Deposits
        Route::prefix('balance')->group(function() {
            Route::any('statistic', 'Api\User\BalanceController@statistic');
            Route::any('table', 'Api\User\BalanceController@table');
        });

        ## Deposits
        Route::prefix('deposits')->group(function() {
            Route::any('statistic', 'Api\User\DepositsController@statistic');
            Route::any('table', 'Api\User\DepositsController@table');
        });

        ## Earnings
        Route::prefix('earnings')->group(function() {
            Route::any('statistic', 'Api\User\EarningsController@statistic');
            Route::any('table', 'Api\User\EarningsController@table');
        });

        ## Withdrawals
        Route::prefix('withdrawals')->group(function() {
            Route::any('statistic', 'Api\User\WithdrawalsController@statistic');
            Route::any('table', 'Api\User\WithdrawalsController@table');
        });

        Route::prefix('trading_account')->group(function() {
            Route::any('table', 'Api\User\TradingAccountController@table');
        });

        Route::prefix('cards')->group(function() {
            Route::any('table', 'Api\User\CardsController@table');
            Route::any('list', 'Api\User\CardsController@list');
        });

        Route::prefix('banks')->group(function() {
            Route::any('table', 'Api\User\BanksController@table');
        });

        Route::prefix('crypto_wallets')->group(function() {
            Route::any('table', 'Api\User\CryptoWalletsController@table');
        });
    });


    Route::prefix('company')->group(function() {
        Route::prefix('crypto')->group(function() {
            Route::any('table', 'Api\Company\CryptoController@table');
        });

        Route::prefix('banks')->group(function() {
            Route::any('table', 'Api\Company\BanksController@table');
        });
    });
});


/*Route::prefix('userTools')->group(function() {
    Route::any('updateHashes', 'Api\UserController@updateHashes');
});*/

Route::prefix('withdrawal')->group(function() {
    Route::prefix('options')->group(function() {
        Route::any('list', 'Api\Withdrawal\OptionsController@list');
    });
});



Route::prefix('genders')->group(function() {
    Route::any('list', 'Api\GendersController@list');
});

Route::prefix('countries')->group(function() {
    Route::any('list', 'Api\CountriesController@list');
});

Route::prefix('regions')->group(function() {
    Route::any('list', 'Api\RegionsController@list');
});

Route::prefix('cities')->group(function() {
    Route::any('list', 'Api\CitiesController@list');
});

Route::prefix('languages')->group(function() {
    Route::any('list', 'Api\LanguagesController@list');
});

Route::prefix('group')->group(function() {
    Route::any('list', 'Api\GroupController@list');
});

Route::prefix('status')->group(function() {
    Route::any('list', 'Api\StatusController@list');
    Route::any('company-banks', 'Api\StatusController@companyBanks');
    Route::any('crypto-company', 'Api\StatusController@companyCrypto'); //add
});

Route::prefix('actions')->group(function() {
    Route::any('list', 'Api\ActionController@list');
});

Route::prefix('users')->group(function() {
    Route::any('list', 'Api\UserController@list');
    Route::any('list-mt', 'Api\UserController@listMt');
    Route::any('documents/{id}', 'Api\UserController@documents');
});

Route::prefix('currencies')->group(function() {
    Route::any('list', 'Api\CurrenciesController@list');
});

Route::prefix('crypto_coins')->group(function() {
    Route::any('list', 'Api\CryptoCoinsController@list');
});
