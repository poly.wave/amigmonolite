<?php

use Illuminate\Support\Facades\Notification;
use App\Notifications\NewMessage;

//Route::get('/test-mail', function (){
//    Notification::route('mail', 'poly.wave@yandex.ru')->notify(new NewMessage());
//    return 'Sent';
//});
Route::any('/trading/register', 'HomeController@register');
Route::any('/trading-activate', 'HomeController@OnActivate');
Route::any('/image', 'HomeController@image');
Route::get('/convert-currency/{amount}/{from}/{to}', 'HomeController@ConvertCurrency');

Route::get('/config-clear', function() {
    $exitCode = Artisan::call('config:clear');
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');
Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');

## Docs
Route::prefix('docs')->group(function () {
    Route::any('declaration', 'Docs\DeclarationController@index');
});


Route::prefix('admin')->group(function () {
    Route::prefix('main')->group(function () {
        Route::any('/', 'Admin\DashboardController@index');
    });
    Route::any('users', 'Admin\UsersController@index');
    Route::any('users/create', 'Admin\UsersController@create');
    Route::any('users/edit/{id}', 'Admin\UsersController@edit');
    Route::any('users/edit-files/{id}', 'Admin\UsersController@editFiles');
    Route::any('users/edit-passport/{id}', 'Admin\UsersController@editPassport');

    Route::any('users/delete/{id}', 'Admin\UsersController@delete');
    Route::any('users/table', 'Admin\UsersController@table');

    Route::any('transactions/deposits', 'Admin\TransactionsController@deposits');
    Route::any('transactions/deposits-table', 'Admin\TransactionsController@deposits_table');
    Route::any('transactions/create-deposit', 'Admin\TransactionsController@createDeposit');
    Route::any('transactions/edit-deposit/{id}', 'Admin\TransactionsController@editDeposit');
    Route::any('transactions/send-bank-info/{id}', 'Admin\TransactionsController@sendBankInfo');
    Route::any('transactions/delete-deposit/{id}', 'Admin\TransactionsController@deleteDeposit');

    Route::any('transactions/send-crypto-company-info/{id}', 'Admin\TransactionsController@sendCryptoCompanyInfo'); //add

    Route::any('transactions/withdrawals', 'Admin\TransactionsController@withdrawals');
    Route::any('transactions/withdrawals-table', 'Admin\TransactionsController@withdrawals_table');
    Route::any('transactions/create-withdrawal', 'Admin\TransactionsController@createWithdrawal');
    Route::any('transactions/edit-withdrawal/{id}', 'Admin\TransactionsController@editWithdrawal');
    Route::any('transactions/delete-withdrawal/{id}', 'Admin\TransactionsController@deleteWithdrawal');

    Route::any('transactions/earnings', 'Admin\TransactionsController@earnings');
    Route::any('transactions/earnings-table', 'Admin\TransactionsController@earnings_table');
    Route::any('transactions/create-earning', 'Admin\TransactionsController@createEarning');
    Route::any('transactions/edit-earning/{id}', 'Admin\TransactionsController@editEarning');
    Route::any('transactions/delete-earning/{id}', 'Admin\TransactionsController@deleteEarning');

    Route::any('logs', 'Admin\LogsController@index');
    Route::any('logs/table', 'Admin\LogsController@table');

});

## It is for admin
Route::prefix('member')->group(function () {
    Route::any('/', function () {
    });

    Route::prefix('main')->group(function () {
        Route::any('/', 'Member\MainController@index');
    });

    Route::prefix('trading')->group(function () {
        Route::any('/', 'Member\TradingController@index');
    });

    Route::prefix('deposit')->group(function () {
        Route::any('/', 'Member\DepositController@index');
        Route::any('bank_transfer', 'Member\DepositController@bank_transfer');
        Route::any('bank-transfer', 'Member\DepositController@bankTransfer');
        Route::any('credit_card', 'Member\DepositController@credit_card');
        Route::any('credit_card/{endpoint}', 'Member\DepositController@credit_card');
        Route::any('crypto_coins', 'Member\DepositController@crypto_coins');
        Route::any('charge-crypto', 'Member\DepositController@chargeCrypto');
        Route::any('crypto-coins-canceled', 'Member\DepositController@cryptoCoinsCanceled');
        Route::any('crypto-coins-completed', 'Member\DepositController@cryptoCoinsCompleted');
    });

    Route::prefix('withdrawal_request')->group(function () {
        Route::any('/', 'Member\WithdrawalRequestController@index');
    });

    Route::prefix('report')->group(function () {
        Route::any('/', 'Member\ReportController@index');
        Route::any('deposits', 'Member\ReportController@deposits');
        Route::any('earnings', 'Member\ReportController@earnings');
        Route::any('withdrawals', 'Member\ReportController@withdrawals');
    });

    Route::prefix('profile')->group(function () {
        Route::any('/', 'Member\ProfileController@index');
        Route::any('personal_information', 'Member\ProfileController@personal_information');
        Route::any('user_passport', 'Member\ProfileController@user_passport');
        Route::any('user_passport_table', 'Member\ProfileController@user_passport_table');
        Route::any('reset_password', 'Member\ProfileController@reset_password');
        Route::any('files', 'Member\ProfileController@files');

        Route::prefix('wallet')->group(function () {
            Route::any('/', 'Member\Profile\WalletController@index');
            Route::any('banks', 'Member\Profile\WalletController@banks');
            Route::any('cards', 'Member\Profile\WalletController@cards');
            Route::any('crypto_wallets', 'Member\Profile\WalletController@crypto_wallets');
        });
    });
});

Route::prefix('user')->group(function () {
    Route::any('/', function () {
        return redirect('member/dashboard');
    });

    Route::prefix('settings')->group(function () {
        Route::any('/', 'User\SettingsController@index');
    });
});

## Its for amigfx.com
$amigfx = function () {
    ## Start page
    Route::prefix('/')->group(function () {
        Route::any('/', 'Fx\IndexController@index');
    });

    Route::prefix('/about-us')->group(function () {
        Route::any('/', 'Fx\AboutUsController@index'); ## link to controller this is name oftion its can be others
    });

    Route::prefix('/account-types')->group(function () {
        Route::any('/', 'Fx\AccountTypesController@index');
    });

    Route::prefix('/company-profile')->group(function () {
        Route::any('/', 'Fx\CompanyProfileController@index');
    });

    Route::prefix('/contact-us')->group(function () {
        Route::any('/', 'Fx\ContactUsController@index');
    });

    Route::prefix('/daily-fx-bulletin')->group(function () {
        Route::any('/', 'Fx\DailyFxBulletinController@index');
    });

    Route::prefix('/economic-calendar')->group(function () {
        Route::any('/', 'Fx\EconomicCalendarController@index');
    });

    Route::prefix('/meta-trader-5')->group(function () {
        Route::any('/', 'Fx\MetaTrader5Controller@index');
    });

    Route::prefix('/mobile-meta-5')->group(function () {
        Route::any('/', 'Fx\MobileMeta5Controller@index');
    });

    Route::prefix('/prepaid-card')->group(function () {
        Route::any('/', 'Fx\PrepaidCardController@index');
    });

    Route::prefix('/special-promotions')->group(function () {
        Route::any('/', 'Fx\SpecialPromotionsController@index');
    });

    Route::prefix('/education')->group(function () {
        Route::any('/', 'Fx\EducationController@index');
    });

    Route::prefix('/whats-forex')->group(function () {
        Route::any('/', 'Fx\WhatsForexController@index');
    });

    Route::prefix('/fx-trading')->group(function () {
        Route::any('/', 'Fx\FxTradingController@index');
    });

    Route::prefix('/campaigns')->group(function () {
        Route::any('/', 'Fx\CampaignsController@index');
    });

    Route::prefix('/terms-of-use')->group(function () {
        Route::any('/', 'Fx\TermsOfUseController@index');
    });

    Route::prefix('/fundamental-analysis')->group(function () {
        Route::any('/', 'Fx\FundamentalAnalysisController@index');
    });

    Route::prefix('/technical-analysis')->group(function () {
        Route::any('/', 'Fx\TechnicalAnalysisController@index');
    });

    Route::prefix('/major-players')->group(function () {
        Route::any('/', 'Fx\MajorPlayersController@index');
    });

    Route::prefix('/advantages')->group(function () {
        Route::any('/', 'Fx\AdvantagesController@index');
    });

    Route::prefix('/introducing-brokers')->group(function () {
        Route::any('/', 'Fx\IntroducingBrokersController@index');
    });

    Route::prefix('/white-label')->group(function () {
        Route::any('/', 'Fx\WhiteLabelController@index');
    });

    Route::prefix('/deposit-option')->group(function () {
        Route::any('/', 'Fx\DepositOptionController@index');
    });

    Route::prefix('/withdraw-option')->group(function () {
        Route::any('/', 'Fx\WithdrawOptionController@index');
    });

    Route::prefix('/term-of-use')->group(function () {
        Route::any('/', 'Fx\TermsOfUseController@index');
    });
};



## Its for amigcapital
$amigcapital = function () {
    Route::prefix('/')->group(function () {
        Route::any('/', 'Capital\IndexController@index');
    });

    Route::prefix('/about-us')->group(function () {
        Route::any('/', 'Capital\AboutUsController@index');
    });
    Route::prefix('/projects')->group(function () {
        Route::any('/', 'Capital\Projects@index');
    });

    Route::prefix('/contact-us')->group(function () {
        Route::any('/', 'Capital\ContactUsController@index');
    });

    Route::prefix('/finance')->group(function () {
        Route::any('/', 'Capital\FinanceController@index');
    });
    Route::prefix('/invest')->group(function () {
        Route::any('/', 'Capital\InvestController@index');
    });
    Route::prefix('/innovation')->group(function () {
        Route::any('/', 'Capital\InnovationController@index');
    });
    Route::prefix('/etrade')->group(function () {
        Route::any('/', 'Capital\EtradeController@index');
    });
    Route::prefix('/privacy')->group(function () {
        Route::any('/', 'Capital\PrivacyController@index');
    });
};

## Its for aim
$aim = function () {
    Route::prefix('/')->group(function () {
        Route::any('/', 'Aim\IndexController@index');
    });

    Route::prefix('/about-us')->group(function () {
        Route::any('/', 'Aim\AboutUsController@index');
    });
    Route::prefix('/projects')->group(function () {
        Route::any('/', 'Aim\Projects@index');
    });

    Route::prefix('/contact-us')->group(function () {
        Route::any('/', 'Aim\ContactUsController@index');
    });

    Route::prefix('/finance')->group(function () {
        Route::any('/', 'Aim\FinanceController@index');
    });
    Route::prefix('/invest')->group(function () {
        Route::any('/', 'Aim\InvestController@index');
    });
    Route::prefix('/innovation')->group(function () {
        Route::any('/', 'Aim\InnovationController@index');
    });
    Route::prefix('/etrade')->group(function () {
        Route::any('/', 'Aim\EtradeController@index');
    });
    Route::prefix('/privacy')->group(function () {
        Route::any('/', 'Aim\PrivacyController@index');
    });
};


## List of domains
Route::domain('migmaz.com')->group($aim);
Route::domain('www.migmaz.com')->group($aim);

Route::domain('ai.aimteso.com')->group($aim);
Route::domain('www.ai.aimteso.com')->group($aim);

Route::domain('amigfx.com')->group($amigfx);
Route::domain('www.amigfx.com')->group($amigfx);

Route::domain('amigfx1.com')->group($amigfx);
Route::domain('www.amigfx1.com')->group($amigfx);

Route::domain('amigfx.co')->group($amigfx);
Route::domain('www.amigfx.co')->group($amigfx);

Route::domain('amigcapital.com')->group($amigcapital);
Route::domain('www.amigcapital.com')->group($amigcapital);

Route::domain('amigcapital.me')->group($amigcapital);
Route::domain('www.amigcapital.me')->group($amigcapital);

Route::domain('localhost')->group($amigfx);
Route::domain('www.localhost')->group($amigfx);

Route::domain('capital.localhost')->group($amigcapital);
Route::domain('www.capital.localhost')->group($amigcapital);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
